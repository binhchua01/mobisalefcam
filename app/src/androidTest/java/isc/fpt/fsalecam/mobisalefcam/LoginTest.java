package isc.fpt.fsalecam.mobisalefcam;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;
import com.robotium.solo.Solo;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import isc.fpt.fsalecam.mobisalefcam.view.activities.ActiveImeiActivity;
import isc.fpt.fsalecam.mobisalefcam.view.activities.LoginActivity;
import isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity;
import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * Created by Hau Le on 2018-11-05.
 */
@RunWith(AndroidJUnit4.class)
public class LoginTest{
    private Solo solo;
    private static final String TAG = "LoginActivity";
    private static final String LOGIN_ACTIVITY = LoginActivity.class.getSimpleName();
    private static final String MAIN_ACTIVITY = MainActivity.class.getSimpleName();
    private static final String ACTIVE_IMEI_ACTIVITY = ActiveImeiActivity.class.getSimpleName();

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(LoginActivity.class);

    @Before
    public void setUp(){
        //setUp() is run before a test case is started.
        //This is where the solo object is created.
        Solo.Config config = new Solo.Config();
        config.commandLogging = true;
        solo = new Solo(getInstrumentation(), config, mActivityRule.getActivity());
    }

    @After
    public void tearDown(){
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }


    @Test
    public void checkLoginSuccess(){
        solo.waitForActivity(TAG, 2000);
        solo.assertCurrentActivity("Error!!!!!!!!!!!!!!!", LOGIN_ACTIVITY);
        solo.clickOnText("Skip");
        solo.enterText((EditText) solo.getView(R.id.edt_username), "PP.Chang Uymeng");
        solo.enterText((EditText) solo.getView(R.id.edt_password), "123456");
        solo.clickOnButton("Login");
        solo.waitForActivity(TAG, 2000);
        solo.assertCurrentActivity("Error!!!!!!!!!!!!!!!", MAIN_ACTIVITY);
    }

    @Test
    public void checkLoginInvalidPassword(){
        solo.waitForActivity(TAG, 2000);
        solo.assertCurrentActivity("Error!!!!!!!!!!!!!!!", LOGIN_ACTIVITY);
        solo.clickOnText("Skip");
        solo.enterText(solo.getEditText(0), "PP.Chang Uymeng");
        solo.enterText(solo.getEditText(1), "22222");
        solo.clickOnButton("Login");
        solo.getText("Username, Password invalid.");
        solo.clickOnText("Agree");
        solo.waitForActivity(TAG, 2000);
        solo.assertCurrentActivity("Error!!!!!!!!!!!!!!!", LOGIN_ACTIVITY, true);
    }

    @Test
    public void checkLoginInvalidUsername(){
        solo.waitForActivity(TAG, 2000);
        solo.assertCurrentActivity("Error!!!!!!!!!!!!!!!", LOGIN_ACTIVITY);
        solo.clickOnText("Skip");
        solo.enterText(solo.getEditText(0), "acb");
        solo.enterText(solo.getEditText(1), "123456");
        solo.clickOnButton("Login");
        solo.getText("Not found DeviceImei or SaleName in MBS.");
        solo.clickOnText("Agree");
        solo.waitForActivity(TAG, 2000);
        solo.assertCurrentActivity("Error!!!!!!!!!!!!!!!", LOGIN_ACTIVITY);
    }

    @Test
    public void checkActiveImeiAlertDialog(){
        solo.waitForActivity("LoginActivity", 2000);
        solo.assertCurrentActivity("Error!!!!!!!!!!", LOGIN_ACTIVITY);
        solo.getText("You do not have IMEI. Please active to login.");
        solo.clickOnText("Active");
        solo.waitForActivity(TAG, 2000);
        solo.assertCurrentActivity("Error!!!!!!!!!!!!!!!", ACTIVE_IMEI_ACTIVITY);
    }
}
