package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateRegistrationImage;

/**
 * Created by Hau Le on 2018-09-11.
 */
public interface ImageRegistrationView {
    void updateRegistrationSuccess(ArrayList<UpdateRegistrationImage> list);
    void uploadImageRegistrationSuccess(String listId);
    void uploadImageFailed(String mes);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressLoading();
    void hiddenProgressLoading();
}
