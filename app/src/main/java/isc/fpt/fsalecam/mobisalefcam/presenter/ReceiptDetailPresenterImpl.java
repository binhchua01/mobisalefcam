package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdatePayment;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdatePayment;
import isc.fpt.fsalecam.mobisalefcam.database.PaymentType;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ReceiptDetailView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-04.
 */
public class ReceiptDetailPresenterImpl implements ReceiptDetailPresenter {
    private ReceiptDetailView view;

    public ReceiptDetailPresenterImpl(ReceiptDetailView view) {
        this.view = view;
    }

    @Override
    public void doGetTypePaymentList(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(PaymentType.class).findAll().size() == 0){
            view.showProgressLoading();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetPaymentMethodList(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetPaymentMethodList(context, apiService, realm);
            }
        }else{
            view.getTypePaymentList((ArrayList<PaymentType>) realm.copyFromRealm(realm.where(PaymentType.class).findAll()));
        }
    }

    @Override
    public void doUpdatePayment(final Context context, final APIService apiService, final ReqUpdatePayment obj) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiUpdatePayment(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiUpdatePayment(context, apiService, obj);
        }
    }

    private void callApiUpdatePayment(final Context context, final APIService apiService, ReqUpdatePayment obj){
        apiService.doUpdatePayment(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjUpdatePayment())
                .enqueue(new Callback<ResUpdatePayment>() {
                    @Override
                    public void onResponse(Call<ResUpdatePayment> call, Response<ResUpdatePayment> response) {
                        validateReturnCodeUpdatePayment(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResUpdatePayment> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetPaymentMethodList(final Context context, APIService apiService, final Realm realm){
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        apiService.doGetPaymentMethodList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.toString()),
                obj)
                .enqueue(new Callback<ResParentStructure>() {
                    @Override
                    public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                        validateReturnCode(context, response, realm);
                    }

                    @Override
                    public void onFailure(Call<ResParentStructure> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCodeUpdatePayment(Context context, Response<ResUpdatePayment> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.updatePaymentSuccess();
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> response, Realm realm){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                savePaymentType((ArrayList<ItemOfListRes>) response.body().getData(), realm);
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void savePaymentType(ArrayList<ItemOfListRes> list, Realm realm) {
        realm.beginTransaction();
        realm.where(PaymentType.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                PaymentType district = realm.createObject(PaymentType.class, list.get(i).getId());
                district.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            view.getTypePaymentList((ArrayList<PaymentType>) realm.copyFromRealm(realm.where(PaymentType.class).findAll()));
        }else{
            view.getTypePaymentList((ArrayList<PaymentType>) realm.copyFromRealm(realm.where(PaymentType.class).findAll()));
        }
    }
}
