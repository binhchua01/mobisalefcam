package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.view.activities.AddressSetupActivity;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.database.Nationality;
import isc.fpt.fsalecam.mobisalefcam.presenter.CusInfoFragPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.CusInfoFragPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.CustomerType;
import isc.fpt.fsalecam.mobisalefcam.utils.CallbackDateTime;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.CusInfoFragView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.TAG_SERVICE_TYPE;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.checkStep;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.presenter;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.registrationById;

/**
 * Created by Hau Le on 2018-07-23.
 */

public class CusInfoFragment extends BaseFragment
        implements View.OnClickListener, CusInfoFragView{
    @BindView(R.id.layout_customer_info_frag) RelativeLayout layoutWrapper;
    @BindView(R.id.layout_choose_customer_type) RelativeLayout chooseCustomerType;
    @BindView(R.id.layout_info_personal) LinearLayout layoutPersonal;
    @BindView(R.id.layout_info_company) LinearLayout layoutCompany;
    @BindView(R.id.tv_customer_type) TextView tvCustomerType;
    @BindView(R.id.btn_next_step_2) Button btnNextStep;
    @BindView(R.id.layout_choose_nationality) RelativeLayout loChooseNationality;
    @BindView(R.id.tv_nationality) TextView tvNationality;
    @BindView(R.id.layout_choose_personal_birthday) RelativeLayout loChooseBirthday;
    @BindView(R.id.layout_choose_founding) RelativeLayout loChooseFounding;
    @BindView(R.id.tv_birthday) TextView tvBirthday;
    @BindView(R.id.tv_founding_date) TextView tvFoundingDate;
    @BindView(R.id.edt_mobile_phone_1) EditText edtMobile1;
    @BindView(R.id.edt_mobile_phone_2) EditText edtMobile2;
    @BindView(R.id.edt_email) EditText edtEmail;
    @BindView(R.id.layout_enter_email) RelativeLayout loEmail;
    @BindView(R.id.edt_person_nal_full_name) EditText edtPersonalFullName;
    @BindView(R.id.edt_company_name) EditText edtCompanyName;
    @BindView(R.id.edt_representive) EditText edtRepresentative;
    @BindView(R.id.tv_address) TextView tvAddress;
    @BindView(R.id.edt_note_address) EditText edtNoteAddress;
    @BindView(R.id.edt_passport) EditText edtPassport;
    @BindView(R.id.edt_tax_code) EditText edtTaxCode;
    @BindView(R.id.edt_contact_1) EditText edtContact1;
    @BindView(R.id.edt_contact_2) EditText edtContact2;
    @BindView(R.id.layout_company_name) RelativeLayout loCompanyName;
    @BindView(R.id.layout_representive) RelativeLayout loRepresentative;
    @BindView(R.id.layout_tax_code) RelativeLayout loTaxCode;
    @BindView(R.id.layout_full_name) RelativeLayout loFullName;
    @BindView(R.id.layout_passport) RelativeLayout loPassport;
    @BindView(R.id.tv_update_address) TextView tvUpdateAddress;

    private Dialog dialog;
    private CusInfoFragPresenter cusInfoFragPresenter;
    private APIService apiService;
    //cus type 12 is personal
    private int cusTypeId = 12;
    private Realm realm;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        hiddenKeySoft(layoutWrapper, getActivity());
        realm = Realm.getDefaultInstance();
        setEvent();
        setUpParamsCallApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        initDataView();
        setOldInfoUpdate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_customer_information;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_choose_customer_type:
                cusInfoFragPresenter.doGetCusTypeList(getActivity(), apiService, realm);
                break;
            case R.id.btn_next_step_2:
                if(checkValueCusInfo()){
                    putDataToObject();
                    changedFragment(new ServiceTypeFragment(), TAG_SERVICE_TYPE);
                    //refresh ui
                    presenter.changedUIActivity(2);//call method from parent
                    checkStep = 2;
                }
                break;
            case R.id.layout_choose_nationality:
                //call api get nationality list
                cusInfoFragPresenter.doGetNationalityList(getActivity(), apiService, realm);
                break;
            case R.id.layout_choose_personal_birthday:
                DialogUtils.showDialogChooseDate(getContext(), new CallbackDateTime() {
                    @Override
                    public void dateTime(String date) {
                        tvBirthday.setText(date);
                    }
                }).show();
                break;
            case R.id.layout_choose_founding:
                DialogUtils.showDialogChooseDate(getContext(), new CallbackDateTime() {
                    @Override
                    public void dateTime(String date) {
                        tvFoundingDate.setText(date);
                    }
                }).show();
                break;
            case R.id.tv_update_address:
                startActivity(new Intent(getContext(), AddressSetupActivity.class));
                break;
        }
    }

    @Override
    public void getCusTypeListSuccess(ArrayList<CustomerType> list) {
        DialogUtils.dialogChooseCustomerType(getContext(), list, new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                String[] type = data.split(":");
                Utils.updateCusInfo.setCusTypeDetail(type[0]);
                displayUiWhenChangeCustomerType(Integer.parseInt(type[0]), type[1]);
            }
        }).show();
    }

    @Override
    public void getNationalityListSuccess(ArrayList<Nationality> list) {
        DialogUtils.dialogChooseNationality(getContext(), list,
            new DialogCallbackDataToActivity() {
                @Override
                public void dataCallback(String data) {
                    String[] value = data.split(":");
                    Utils.updateCusInfo.setNationality(value[0]);
                    Utils.updateCusInfo.setNationalityName(value[1]);
                    tvNationality.setText(value[1]);
                }
            }).show();
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(getContext(), mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(getContext(), mes).show();
    }

    @Override
    public void showProgressDialog() {
        dialog = DialogUtils.showLoadingDialog(getContext(), getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @SuppressLint("SetTextI18n")
    private void initDataView() {
        tvAddress.setText(Utils.updateCusInfo.getBillToNumber() + " "
                + Utils.updateCusInfo.getBillToStreet() + ", "
                + Utils.updateCusInfo.getBillToWard() + ", "
                + Utils.updateCusInfo.getBillToDistrict() + ", "
                + Utils.updateCusInfo.getBillToCity());
    }

    @SuppressLint("SetTextI18n")
    public void setOldInfoUpdate() {
        //create first, no data
        if(!Utils.updateCusInfo.getCusTypeDetail().equals("")){
            //this step use for create new cus, and nav between step 1,2,3
            //set data had enter before
            if(Integer.parseInt(Utils.updateCusInfo.getCusTypeDetail()) == Utils.CUS_TYPE_HOME){
                tvCustomerType.setText(getResources().getString(R.string.personal));
                edtPersonalFullName.setText(Utils.updateCusInfo.getFullName());
                tvBirthday.setText(Utils.updateCusInfo.getBirthday());
                edtPassport.setText(Utils.updateCusInfo.getPassport());
            }else{
                tvCustomerType.setText(getResources().getString(R.string.company));
                edtCompanyName.setText(Utils.updateCusInfo.getFullName());
                edtRepresentative.setText(Utils.updateCusInfo.getRepresentive());
                edtTaxCode.setText(Utils.updateCusInfo.getTaxCode());
                tvFoundingDate.setText(Utils.updateCusInfo.getBirthday());
            }
            edtMobile1.setText(Utils.updateCusInfo.getPhone1());
            edtContact1.setText(Utils.updateCusInfo.getContact1());
            edtMobile2.setText(Utils.updateCusInfo.getPhone2());
            edtContact2.setText(Utils.updateCusInfo.getContact2());
            if(!Utils.updateCusInfo.getNationality().equals("")){
                tvNationality.setText(Utils.updateCusInfo.getNationalityName());
            }
            edtEmail.setText(Utils.updateCusInfo.getEmail());
            edtNoteAddress.setText(Utils.updateCusInfo.getNoteAddress());
        }

        if(registrationById == null){
            return;
        }
        //hidden button update address
        tvUpdateAddress.setVisibility(View.GONE);
        //bellow init data when update cus info, if create new, skip this
        tvCustomerType.setText(registrationById.getCusTypeDetailName());
        //set cus type id
        cusTypeId = registrationById.getCusTypeDetail();
        //control display UI by customer type
        if(cusTypeId == Utils.CUS_TYPE_HOME){
            //show layout by cus id
            layoutPersonal.setVisibility(View.VISIBLE);
            layoutCompany.setVisibility(View.GONE);
            //set data to view
            edtPersonalFullName.setText(registrationById.getFullName());
            tvBirthday.setText((registrationById.getBirthday()).split(" ")[0]);
            edtPassport.setText(registrationById.getPassport());
        }else{
            layoutPersonal.setVisibility(View.GONE);
            layoutCompany.setVisibility(View.VISIBLE);
            edtCompanyName.setText(registrationById.getFullName());
            edtRepresentative.setText(registrationById.getRepresentive());
            tvFoundingDate.setText(registrationById.getBirthday().split(" ")[0]);
            edtTaxCode.setText(registrationById.getTaxCode());
        }

        tvNationality.setText(registrationById.getNationalityName());
        edtMobile1.setText(registrationById.getPhone1());
        edtContact1.setText(registrationById.getContact1());
        tvAddress.setText(registrationById.getAddress());
        //params is not required, check isn't empty before set to view
        if(!registrationById.getContact2().equals("")){
            edtContact2.setText(registrationById.getContact2());
        }
        if(!registrationById.getPhone2().equals("")){
            edtMobile2.setText(registrationById.getPhone2());
        }
        if(!registrationById.getNoteAddress().equals("")){
            edtNoteAddress.setText(registrationById.getNoteAddress());
        }
        if(!registrationById.getEmail().equals("")){
            edtEmail.setText(registrationById.getEmail());
        }
    }

    private void setUpParamsCallApi() {
        apiService = ApiUtils.getApiService();
        cusInfoFragPresenter = new CusInfoFragPresenterImpl(this);
    }

    private void setEvent() {
        chooseCustomerType.setOnClickListener(this);
        btnNextStep.setOnClickListener(this);
        loChooseNationality.setOnClickListener(this);
        loChooseBirthday.setOnClickListener(this);
        loChooseFounding.setOnClickListener(this);
        tvUpdateAddress.setOnClickListener(this);
        //add text change
        tvCustomerType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvCustomerType.setTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    chooseCustomerType.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtCompanyName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtCompanyName.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setFullName(String.valueOf(editable));
                    loCompanyName.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtRepresentative.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtRepresentative.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setRepresentive(String.valueOf(editable));
                    loRepresentative.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtTaxCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtTaxCode.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setTaxCode(String.valueOf(editable));
                    loTaxCode.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvFoundingDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvFoundingDate.setTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setBirthday(String.valueOf(editable));
                    loChooseFounding.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvNationality.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvNationality.setTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    loChooseNationality.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtPersonalFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtPersonalFullName.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setFullName(String.valueOf(editable));
                    loFullName.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvBirthday.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvBirthday.setTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setBirthday(String.valueOf(editable));
                    loChooseBirthday.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtPassport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtPassport.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setPassport(String.valueOf(editable));
                    loPassport.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtMobile1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtMobile1.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setPhone1(String.valueOf(editable));
                    edtMobile1.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtContact1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtContact1.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setContact1(String.valueOf(editable));
                    edtContact1.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtMobile2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtMobile2.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setPhone2(String.valueOf(editable));
                    edtMobile2.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtContact2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtContact2.setHintTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setContact2(String.valueOf(editable));
                    edtContact2.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String email = String.valueOf(editable);
                if(email.isEmpty()){
                    return;
                }
                if(Utils.isValidMail(email)){
                    edtEmail.setHintTextColor(getResources().getColor(R.color.s_black));
                    Utils.updateCusInfo.setEmail(email);
                    loEmail.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }else{
                    edtEmail.setHintTextColor(getResources().getColor(R.color.wrong_pink));
                    loEmail.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
                }
            }
        });
        edtNoteAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).isEmpty()){
                    Utils.updateCusInfo.setNoteAddress(edtNoteAddress.getText().toString());
                }
            }
        });
    }

    private void displayUiWhenChangeCustomerType(int num, String name){
        switch (num){
            case 10:
                layoutCompany.setVisibility(View.VISIBLE);
                layoutPersonal.setVisibility(View.GONE);
                tvCustomerType.setText(name);
                cusTypeId = num;
                break;
            case 12:
                layoutCompany.setVisibility(View.GONE);
                layoutPersonal.setVisibility(View.VISIBLE);
                tvCustomerType.setText(name);
                cusTypeId = num;
                break;
        }
        refreshUI();
    }

    private void changedFragment(Fragment fragment, String tag){
        //create fragment manager
        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.content_frag_new_register, fragment, tag);
        fragmentTransaction.commit();
    }

    public void putDataToObject(){
        Utils.updateCusInfo.setAddress(tvAddress.getText().toString());
        Utils.updateCusInfo.setSaleId
                (SharedPref.get(getContext(), SharedPref.Key.SALE_ID, ""));
        Utils.updateCusInfo.setBookportIdentityId
                (SharedPref.get(getContext(), SharedPref.Key.BookportIdentityID, ""));
    }

    public boolean checkValueCusInfo(){
        //validation all to high light field required
        if(tvCustomerType.getText().toString().equals(getResources().getString(R.string.choose_type_of_cus))){
            chooseCustomerType.setBackground(Objects.requireNonNull(getContext())
                    .getResources().getDrawable(R.drawable.background_spinner_error));
            tvCustomerType.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            chooseCustomerType.setBackground(Objects.requireNonNull(getContext())
                    .getResources().getDrawable(R.drawable.background_spinner));
            tvCustomerType.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(cusTypeId == Utils.CUS_TYPE_COMPANY){
            if(edtCompanyName.getText().toString().isEmpty()){
                loCompanyName.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                edtCompanyName.setHintTextColor(getResources().getColor(R.color.wrong_pink));
            }else{
                loCompanyName.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                edtCompanyName.setHintTextColor(getResources().getColor(R.color.s_black));
            }

            if(edtRepresentative.getText().toString().isEmpty()){
                loRepresentative.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                edtRepresentative.setHintTextColor(getResources().getColor(R.color.wrong_pink));
            }else{
                loRepresentative.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                edtRepresentative.setHintTextColor(getResources().getColor(R.color.s_black));
            }

            if(edtTaxCode.getText().toString().isEmpty()){
                loTaxCode.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                edtTaxCode.setHintTextColor(getResources().getColor(R.color.wrong_pink));
            }else{
                loTaxCode.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                edtTaxCode.setHintTextColor(getResources().getColor(R.color.s_black));
            }

            if(tvFoundingDate.getText().toString()
                    .equals(Objects.requireNonNull(getContext())
                            .getResources().getString(R.string.ui_text_choose_establish_date))){
                loChooseFounding.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                tvFoundingDate.setTextColor(getResources().getColor(R.color.wrong_pink));
            }else{
                loChooseFounding.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                tvFoundingDate.setTextColor(getResources().getColor(R.color.s_black));
            }

            if(edtMobile2.getText().toString().isEmpty()){
                edtMobile2.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                edtMobile2.setHintTextColor(getResources().getColor(R.color.wrong_pink));
            }else{
                edtMobile2.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                edtMobile2.setHintTextColor(getResources().getColor(R.color.s_black));
            }

            if(edtContact2.getText().toString().isEmpty()){
                edtContact2.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                edtContact2.setHintTextColor(getResources().getColor(R.color.wrong_pink));
            }else{
                edtContact2.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                edtContact2.setHintTextColor(getResources().getColor(R.color.s_black));
            }
        }else{
            if(edtPersonalFullName.getText().toString().isEmpty()){
                loFullName.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                edtPersonalFullName.setHintTextColor(getResources().getColor(R.color.wrong_pink));
            }else{
                loFullName.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                edtPersonalFullName.setHintTextColor(getResources().getColor(R.color.s_black));
            }

            if(tvBirthday.getText().toString()
                    .equals(Objects.requireNonNull(getContext())
                            .getResources().getString(R.string.ui_text_choose_date_of_birth))){
                loChooseBirthday.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                tvBirthday.setTextColor(getResources().getColor(R.color.wrong_pink));
            }else{
                loChooseBirthday.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                tvBirthday.setTextColor(getResources().getColor(R.color.s_black));
            }

            if(edtPassport.getText().toString().isEmpty()){
                loPassport.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
                edtPassport.setHintTextColor(getResources().getColor(R.color.wrong_pink));
            }else {
                loPassport.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
                edtPassport.setHintTextColor(getResources().getColor(R.color.s_black));
            }

        }

        if(tvNationality.getText().toString().equals(getResources().getString(R.string.choose_nationality))){
            loChooseNationality.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
            tvNationality.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseNationality.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
            tvNationality.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(edtMobile1.getText().toString().isEmpty()){
            edtMobile1.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
            edtMobile1.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            edtMobile1.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
            edtMobile1.setHintTextColor(getResources().getColor(R.color.s_black));
        }

        if(edtContact1.getText().toString().isEmpty()){
            edtContact1.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner_error));
            edtContact1.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            edtContact1.setBackground(getContext().getResources().getDrawable(R.drawable.background_spinner));
            edtContact1.setHintTextColor(getResources().getColor(R.color.s_black));
        }

        //validate each line
        if(cusTypeId == Utils.CUS_TYPE_COMPANY){
            if(tvCustomerType.getText().toString().equals(getResources().getString(R.string.choose_type_of_cus))){
                showDialogRequired(getResources().getString(R.string.mes_cus_type_required)).show();
                return false;
            }else if(edtCompanyName.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_company_name_required)).show();
                return false;
            }else if(edtRepresentative.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_representative_required)).show();
                return false;
            }else if(edtTaxCode.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_tax_code_required)).show();
                return false;
            }else if(tvFoundingDate.getText().toString().equals(Objects.requireNonNull(getContext())
                    .getResources().getString(R.string.ui_text_choose_establish_date))){
                showDialogRequired(getResources().getString(R.string.mes_establish_date_required)).show();
                return false;
            }else if(tvNationality.getText().toString()
                    .equals(getResources().getString(R.string.choose_nationality))){
                showDialogRequired(getResources().getString(R.string.mes_nationality_required)).show();
                return false;
            }else if(edtMobile1.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_phone_1_required)).show();
                return false;
            }else if(edtContact1.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_contact_1_required)).show();
                return false;
            }else if(edtMobile2.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_phone_2_required)).show();
                return false;
            }else if(edtContact2.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_contact_2_required)).show();
                return false;
            }
        }else{
            if(tvCustomerType.getText().toString().equals(getResources().getString(R.string.choose_type_of_cus))){
                showDialogRequired(getResources().getString(R.string.mes_cus_type_required)).show();
                return false;
            }else if(edtPersonalFullName.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_cus_name_required)).show();
                return false;
            }else if(tvBirthday.getText().toString().equals(Objects.requireNonNull(getContext())
                    .getResources().getString(R.string.ui_text_choose_date_of_birth))){
                showDialogRequired(getResources().getString(R.string.mes_birthday_required)).show();
                return false;
            }else if(edtPassport.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_passport_required)).show();
                return false;
            }else if(tvNationality.getText().toString()
                    .equals(getResources().getString(R.string.choose_nationality))){
                showDialogRequired(getResources().getString(R.string.mes_nationality_required)).show();
                return false;
            }else if(edtMobile1.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_phone_1_required)).show();
                return false;
            }else if(edtContact1.getText().toString().isEmpty()){
                showDialogRequired(getResources().getString(R.string.mes_contact_1_required)).show();
                return false;
            }
        }
        return true;
    }

    private void refreshUI(){
        //refresh highlight
        chooseCustomerType.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        loFullName.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        loChooseBirthday.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        loPassport.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        loChooseNationality.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        edtMobile1.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        edtContact1.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        edtMobile2.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        edtContact2.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        loCompanyName.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        loRepresentative.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        loTaxCode.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        loChooseFounding.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        //text view
        edtCompanyName.setHintTextColor(getResources().getColor(R.color.s_black));
        edtRepresentative.setHintTextColor(getResources().getColor(R.color.s_black));
        edtTaxCode.setHintTextColor(getResources().getColor(R.color.s_black));
        edtContact2.setHintTextColor(getResources().getColor(R.color.s_black));
        edtMobile2.setHintTextColor(getResources().getColor(R.color.s_black));
        tvFoundingDate.setTextColor(getResources().getColor(R.color.s_black));
        tvBirthday.setTextColor(getResources().getColor(R.color.s_black));
        edtPersonalFullName.setHintTextColor(getResources().getColor(R.color.s_black));
        edtContact2.setHintTextColor(getResources().getColor(R.color.s_black));
        edtMobile2.setHintTextColor(getResources().getColor(R.color.s_black));
        edtPassport.setHintTextColor(getResources().getColor(R.color.s_black));
        edtContact1.setHintTextColor(getResources().getColor(R.color.s_black));
        edtMobile1.setHintTextColor(getResources().getColor(R.color.s_black));
        tvNationality.setTextColor(getResources().getColor(R.color.s_black));
    }

    private Dialog showDialogRequired(String str){
        return DialogUtils.showMessageDialogVerButton(getContext(), str);
    }
}
