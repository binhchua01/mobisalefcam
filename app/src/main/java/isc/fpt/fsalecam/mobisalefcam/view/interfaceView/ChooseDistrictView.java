package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.database.District;

/**
 * Created by Hau Le on 2018-08-01.
 */
public interface ChooseDistrictView {
    void getDistrictListSuccess();
    void callApiFailed(String mes);
    void authorizedInvalid(String mes);
    void search(ArrayList<District> list);
    void showLoadingProgress();
    void hiddenLoadingProgress();
}
