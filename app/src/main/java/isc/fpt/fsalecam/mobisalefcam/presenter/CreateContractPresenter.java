package isc.fpt.fsalecam.mobisalefcam.presenter;


import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCreateContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;

/**
 * Created by Hau Le on 2018-08-28.
 */
public interface CreateContractPresenter {
    void doCreateContract(Context context, APIService apiService, ReqCreateContract obj);
    void doUploadImageSignature(Context context, APIService apiService,
                                String filePath, String regId, SaleOfInfo saleOfInfo);
}
