package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.database.Building;

/**
 * Created by Hau Le on 2018-09-18.
 */
public interface ChooseBuildingView {
    void getBuildingListSuccess();
    void search(ArrayList<Building> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
