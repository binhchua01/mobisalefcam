package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;

/**
 * Created by Hau Le on 2018-08-02.
 */
public interface MainView {
    void getInfoSuccess(SaleOfInfo saleInfo);
    void callApiFailed(String mes);
    void changePasswordSuccess(String mes);
    void expiredToken(String mes);
    void showLoadingDialog();
    void hiddenLoadingDialog();
}
