package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.view.activities.CusInfoDetailsActivity;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateCusInfo;
import isc.fpt.fsalecam.mobisalefcam.presenter.TotalServiceFeePresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.TotalServiceFeePresenter;
import isc.fpt.fsalecam.mobisalefcam.database.DepositFee;
import isc.fpt.fsalecam.mobisalefcam.database.Vat;
import isc.fpt.fsalecam.mobisalefcam.utils.ContinueRegisterService;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.ResizeDialogPercent;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.TotalServiceFeeView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.registrationById;
import static isc.fpt.fsalecam.mobisalefcam.view.fragment.CusWaitConfirmFragment.cusInfo;

/**
 * Created by Hau Le on 2018-07-23.
 */
public class TotalServiceFeeFragment extends BaseFragment
        implements TotalServiceFeeView, View.OnClickListener{
    private TotalServiceFeePresenter presenter;
    private APIService apiService;
    private Dialog dialog;
    private Dialog dialogUpdateRegistrationSuccess;
    private Realm realm;
    private ReqUpdateRegistrationTotal obj;
    private boolean flag = false;

    @BindView(R.id.layout_deposit_fee) RelativeLayout loDepositFee;
    @BindView(R.id.layout_choose_vat) RelativeLayout loVat;
    @BindView(R.id.btn_create_cus_info) Button btnCreateCusInfo;
    @BindView(R.id.tv_total_money_device) TextView tvTotalMoneyDevice;
    @BindView(R.id.tv_total_internet) TextView tvTotalInternet;
    @BindView(R.id.tv_deposit_fee) TextView tvDepositFee;
    @BindView(R.id.tv_vat) TextView tvVat;
    @BindView(R.id.tv_total_money) TextView tvTotalMoney;
    @BindView(R.id.tv_connection_fee) TextView tvConnectionFee;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDataView();
        setEvent();
        initParamsCallApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        setOldDataToUpdate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_total_service_fee;
    }

    @Override
    public void getDepositListSuccess(ArrayList<DepositFee> list) {
        flag = true;
        DialogUtils.showDialogChooseDepositFee(getContext(),
                getResources().getString(R.string.choose_deposit_fee),
                list, new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                tvDepositFee.setText(data.split(":")[1]);
                //put data to objReport
                Utils.updateCusInfo.setDepositFee(data.split(":")[1]);
            }
        }).show();
    }

    @Override
    public void getVatListSuccess(ArrayList<Vat> list) {
        flag = true;
        DialogUtils.showDialogChooseVat(getContext(),
                getResources().getString(R.string.ui_text_vat),
                list, new DialogCallbackDataToActivity() {
            @SuppressLint("SetTextI18n")
            @Override
            public void dataCallback(String data) {
                tvVat.setText(data.split(":")[1] + "%");
                //put data to objReport
                Utils.updateCusInfo.setVat(data.split(":")[1]);
            }
        }).show();
    }

    @Override
    public void calculatorTotalAmount(ArrayList<CalNewTotal> list) {
        //put to obj register
        Utils.updateCusInfo.setInternetTotal(String.valueOf(list.get(0).getInternetTotal()));
        Utils.updateCusInfo.setDeviceTotal(String.valueOf(list.get(0).getDeviceTotal()));
        Utils.updateCusInfo.setTotal(String.valueOf(list.get(0).getTotal()));
        //show total device to view
        tvTotalInternet.setText(String.valueOf(list.get(0).getInternetTotal()));
        tvTotalMoneyDevice.setText(String.valueOf(list.get(0).getDeviceTotal()));
        tvConnectionFee.setText(Utils.updateCusInfo.getConnectionFee());
        tvTotalMoney.setText(String.valueOf(list.get(0).getTotal()));
    }

    @Override
    public void updateCusInfoSuccess(final UpdateCusInfo obj) {
        if(Utils.DO_UPDATE == 1){
            dialogUpdateRegistrationSuccess = DialogUtils.showDialogUpdateRegistrationSuccess(getContext(),
                    getResources().getString(R.string.mes_update_registration_success),
                    new ContinueRegisterService() {
                        @Override
                        public void continueRegister() {
                            int SPLASH_DISPLAY_LENGTH = 2000;
                            Handler handler = new Handler();
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    Objects.requireNonNull(getActivity()).finish();
                                }
                            };
                            handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);
                        }
                    });
        }else{
            dialogUpdateRegistrationSuccess = DialogUtils.showDialogUpdateRegistrationSuccess(getContext(),
                    getResources().getString(R.string.mes_create_registration_success),
                    new ContinueRegisterService() {
                        @Override
                        public void continueRegister() {
                            int SPLASH_DISPLAY_LENGTH = 2000;
                            Handler handler = new Handler();
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    //Start activity here
                                    Intent intent = new Intent(getContext(), CusInfoDetailsActivity.class);
                                    cusInfo.setRegID(obj.getRegID());
                                    cusInfo.setRegCode(obj.getRegCode());
                                    startActivity(intent);
                                    Objects.requireNonNull(getActivity()).finish();
                                }
                            };
                            handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);
                        }
                    });
        }
        dialogUpdateRegistrationSuccess.show();
        ResizeDialogPercent.resize(getContext(), dialogUpdateRegistrationSuccess);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(getContext(), mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(getContext(), mes).show();
    }

    @Override
    public void showProgressLoading() {
        dialog = DialogUtils.showLoadingDialog(getContext(),
                Objects.requireNonNull(getContext()).getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressLoading() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_deposit_fee:
                presenter.doGetDepositList(getContext(), apiService, Utils.updateCusInfo.getLocationId(), realm);
                break;
            case R.id.layout_choose_vat:
                presenter.doGetVatList(getContext(), apiService, realm);
                break;
            case R.id.btn_create_cus_info:
                if(checkValueIsOk()){
                    presenter.doUpdateCusInfo(getContext(), apiService, Utils.updateCusInfo);
                }
                break;
        }
    }

    private void initDataView() {
        if(Utils.DO_UPDATE == 1){
            btnCreateCusInfo.setText(getResources().getString(R.string.ui_text_update_cus_info));
        }
    }

    private void setEvent() {
        loDepositFee.setOnClickListener(this);
        loVat.setOnClickListener(this);
        btnCreateCusInfo.setOnClickListener(this);
        //refresh ui when text change
        tvDepositFee.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvDepositFee.setTextColor(getResources().getColor(R.color.s_black));
                if(!tvDepositFee.getText().toString().equals(getResources().getString(R.string.choose_deposit_fee))){
                    Utils.updateCusInfo.setDepositFee(String.valueOf(editable));
                    obj.setDepositFee(Double.parseDouble(Utils.updateCusInfo.getDepositFee()));
                    loDepositFee.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    if(Utils.DO_UPDATE == 1 && !flag){
                        return;
                    }
                    //call api calculator total amount
                    presenter.doCalculatorTotalAmount(getContext(), apiService, obj);
                }
            }
        });
        tvVat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvVat.setTextColor(getResources().getColor(R.color.s_black));
                if(!tvVat.getText().toString().equals(getResources().getString(R.string.ui_text_choose_vat))){
                    Utils.updateCusInfo.setVat(String.valueOf(editable).replace("%", "").trim());
                    obj.setVat(Integer.parseInt(Utils.updateCusInfo.getVat()));
                    loVat.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    if(Utils.DO_UPDATE == 1 && !flag){
                        return;
                    }
                    //call api calculator total amount
                    presenter.doCalculatorTotalAmount(getContext(), apiService, obj);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(dialogUpdateRegistrationSuccess!=null && dialogUpdateRegistrationSuccess.isShowing()){
            dialogUpdateRegistrationSuccess.dismiss();
        }
    }

    private void initParamsCallApi() {
        apiService = ApiUtils.getApiService();
        presenter = new TotalServiceFeePresenterImpl(this);
        obj = new ReqUpdateRegistrationTotal();
        obj.setLocationId(Integer.parseInt(Utils.updateCusInfo.getLocationId()));
        obj.setLocalType(Integer.parseInt(Utils.updateCusInfo.getLocalType()));
        obj.setPromotionId(Integer.parseInt(Utils.updateCusInfo.getPromotionId()));
        obj.setMonthOfPrepaid(Integer.parseInt(Utils.updateCusInfo.getMonthOfPrepaid()));
        obj.setConnectionFee(Double.parseDouble(Utils.updateCusInfo.getConnectionFee()));
        obj.setList((ArrayList<ReqDevice>) Utils.updateCusInfo.getListDevice());
        if(Utils.DO_UPDATE == 1 ){
            return;
        }
        //call api calculator total amount
        presenter.doCalculatorTotalAmount(getContext(), apiService, obj);
    }

    @SuppressLint("SetTextI18n")
    private void setOldDataToUpdate(){
        if(!Utils.updateCusInfo.getDepositFee().equals("")){
            tvDepositFee.setText(Utils.updateCusInfo.getDepositFee());
        }
        if(!Utils.updateCusInfo.getVat().equals("")){
            tvVat.setText(Utils.updateCusInfo.getVat() + "%");
        }
        //TODO: use for fill had cus info
        if(registrationById == null){
            return;
        }
        tvTotalInternet.setText(String.valueOf(registrationById.getInternetTotal()));
        tvTotalMoneyDevice.setText(String.valueOf(registrationById.getDeviceTotal()));
        tvDepositFee.setText(String.valueOf(registrationById.getDepositFee()));
        tvVat.setText(registrationById.getvAT() + "%");
        tvTotalMoney.setText(String.valueOf(registrationById.getTotal()));
        tvConnectionFee.setText(String.valueOf(registrationById.getConnectionFee()));
        //
        Utils.updateCusInfo.setInternetTotal(String.valueOf(registrationById.getInternetTotal()));
        Utils.updateCusInfo.setDeviceTotal(String.valueOf(registrationById.getDeviceTotal()));
        Utils.updateCusInfo.setDepositFee(String.valueOf(registrationById.getDepositFee()));
        Utils.updateCusInfo.setVat(String.valueOf(registrationById.getvAT()));
        Utils.updateCusInfo.setTotal(String.valueOf(registrationById.getTotal()));
        Utils.updateCusInfo.setConnectionFee(String.valueOf(registrationById.getConnectionFee()));
    }

    public boolean checkValueIsOk(){
        if(tvDepositFee.getText().toString().equals(getResources().getString(R.string.choose_deposit_fee))){
            loDepositFee.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvDepositFee.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loDepositFee.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvDepositFee.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvVat.getText().toString().equals(getResources().getString(R.string.ui_text_choose_vat))){
            loVat.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvVat.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loVat.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvVat.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvDepositFee.getText().toString()
                .equals(getResources().getString(R.string.choose_deposit_fee))){
            showDialogRequired(getResources().getString(R.string.mes_deposit_fee_required)).show();
            return false;
        }else if(tvVat.getText().toString()
                .equals(getResources().getString(R.string.ui_text_choose_vat))){
            showDialogRequired(getResources().getString(R.string.mes_vat_required)).show();
            return false;
        }
        return true;
    }

    private Dialog showDialogRequired(String str){
        return DialogUtils.showMessageDialogVerButton(getContext(), str);
    }
}
