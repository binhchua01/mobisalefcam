package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-01.
 */
public class ReqGetStreetList {
    private String locationId, districtId, wardId, type;

    public ReqGetStreetList(String locationId, String districtId, String wardId, String type) {
        this.locationId = locationId;
        this.districtId = districtId;
        this.wardId = wardId;
        this.type = type;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getWardId() {
        return wardId;
    }

    public void setWardId(String wardId) {
        this.wardId = wardId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LinkedHashMap<String, String> generateObjGetStreetList(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("LocationID", locationId);
        obj.put("DistrictID", districtId);
        obj.put("WardID", wardId);
        obj.put("Type", type);
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("LocationID", getLocationId());
            obj.put("DistrictID", getDistrictId());
            obj.put("WardID", getWardId());
            obj.put("Type", getType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
