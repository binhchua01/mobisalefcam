package isc.fpt.fsalecam.mobisalefcam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;

/**
 * Created by Hau Le on 2018-08-27.
 */
public class DeviceListCusDetailAdapter extends RecyclerView.Adapter<DeviceListCusDetailAdapter.SimpleViewHolder> {
    private Context context;
    private ArrayList<ReqDevice> list;

    public DeviceListCusDetailAdapter(Context context, ArrayList<ReqDevice> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_device_on_cus_info_details, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        holder.tvDeviceName.setText(list.get(position).getName());
        holder.tvDevicePrice.setText(String.valueOf(list.get(position).getTotalPrice()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_device_name) TextView tvDeviceName;
        @BindView(R.id.tv_device_price) TextView tvDevicePrice;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void notifyData(ArrayList<ReqDevice> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
