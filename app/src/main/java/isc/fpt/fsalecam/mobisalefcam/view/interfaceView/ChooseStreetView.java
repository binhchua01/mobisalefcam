package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.database.Street;

/**
 * Created by Hau Le on 2018-08-02.
 */
public interface ChooseStreetView {
    void getStreetListSuccess();
    void getStreetListFailed(String mes);
    void expiredToken(String mes);
    void showProgressLoading();
    void hiddenProgressLoading();
    void searchStreet(ArrayList<Street> list);
}
