package isc.fpt.fsalecam.mobisalefcam.api;

/**
 * Created by Hau Le on 2018-07-05.
 */
public class ApiUtils {
    private static final String STAGE = "https://sapi.fpt.vn/mobi-mobisaleglobalstag/";
    private static final String DEV = "";
    private static final String PRODUCTION = "https://sapi.fpt.vn/mobi-mobisaleglobal/";

    public static APIService getApiService() {
        return RetrofitClient.getClient(PRODUCTION).create(APIService.class);
    }
}
