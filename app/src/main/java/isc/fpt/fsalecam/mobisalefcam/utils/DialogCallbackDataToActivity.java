package isc.fpt.fsalecam.mobisalefcam.utils;

/**
 * Created by Hau Le on 2018-08-02.
 */
public interface DialogCallbackDataToActivity {
    void dataCallback(String data);
}
