package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.PotentialCusResult;

/**
 * Created by Hau Le on 2018-09-14.
 */
public interface NewPotentialCusView {
    void createPotentialCusSuccess(PotentialCusResult obj);
    void generateStringAddressSuccess(String address);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
