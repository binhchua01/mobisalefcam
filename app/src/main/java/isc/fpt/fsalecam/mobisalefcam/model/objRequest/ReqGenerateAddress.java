package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-10.
 */
public class ReqGenerateAddress {
    private int locationId, districtId, wardId, streetId, homeType, buildingId;
    private String houseNumber;

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getWardId() {
        return wardId;
    }

    public void setWardId(int wardId) {
        this.wardId = wardId;
    }

    public int getStreetId() {
        return streetId;
    }

    public void setStreetId(int streetId) {
        this.streetId = streetId;
    }

    public int getHomeType() {
        return homeType;
    }

    public void setHomeType(int homeType) {
        this.homeType = homeType;
    }

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public LinkedHashMap<String, String> generateObjAddressReq(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("LocationID", String.valueOf(locationId));
        obj.put("DistrictID", String.valueOf(districtId));
        obj.put("WardID", String.valueOf(wardId));
        obj.put("StreetId", String.valueOf(streetId));
        obj.put("HouseNumber", String.valueOf(houseNumber));
        obj.put("HomeType", String.valueOf(homeType));
        obj.put("BuildingId", String.valueOf(buildingId));
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("LocationID", String.valueOf(getLocationId()));
            obj.put("DistrictID", String.valueOf(getDistrictId()));
            obj.put("WardID", String.valueOf(getWardId()));
            obj.put("StreetId", String.valueOf(getStreetId()));
            obj.put("HouseNumber", String.valueOf(getHouseNumber()));
            obj.put("HomeType", String.valueOf(getHomeType()));
            obj.put("BuildingId", String.valueOf(getBuildingId()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
