package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hau Le on 2018-08-17.
 */
public class UpdateCusInfo implements Serializable{
    @SerializedName("ResultID")
    @Expose
    private Integer resultID;
    @SerializedName("RegID")
    @Expose
    private Integer regID;
    @SerializedName("RegCode")
    @Expose
    private String regCode;
    @SerializedName("Result")
    @Expose
    private String result;

    public Integer getResultID() {
        return resultID;
    }

    public void setResultID(Integer resultID) {
        this.resultID = resultID;
    }

    public Integer getRegID() {
        return regID;
    }

    public void setRegID(Integer regID) {
        this.regID = regID;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
