package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-08-02.
 */
public class LocationOfSale {
    @SerializedName("LocationId")
    @Expose
    private Integer locationId;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("IsCurrentLocation")
    @Expose
    private Integer isCurrentLocation;

    public LocationOfSale(Integer locationId, String description, Integer isCurrentLocation) {
        this.locationId = locationId;
        this.description = description;
        this.isCurrentLocation = isCurrentLocation;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsCurrentLocation() {
        return isCurrentLocation;
    }

    public void setIsCurrentLocation(Integer isCurrentLocation) {
        this.isCurrentLocation = isCurrentLocation;
    }
}
