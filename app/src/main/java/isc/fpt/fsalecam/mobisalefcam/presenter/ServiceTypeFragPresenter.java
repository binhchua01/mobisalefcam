package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-08-16.
 */
public interface ServiceTypeFragPresenter {
    void doGetServiceTypeList(Context context, APIService apiService);
    void doGetPaymentMethodList(Context context, APIService apiService, Realm realm);
    void doGetConnectionFeeList(Context context, APIService apiService, String locationId, Realm realm);
}
