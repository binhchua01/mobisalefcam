package isc.fpt.fsalecam.mobisalefcam.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import isc.fpt.fsalecam.mobisalefcam.BuildConfig;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetRegistrationDetail;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateCusInfo;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;
import okhttp3.ResponseBody;

/**
 * Created by Hau Le on 2018-07-16.
 */

public class Utils {
    public static String CONTENT_TYPE = "application/json";
    //Authorization system
    public static String TOKEN_TYPE = "Bearer ";
    public static String TOKEN_AUTH_KONG = "Basic dGhhbmhkYzNAZnB0LmNvbS52bjoxMjM0NTY=";
    public static String FTEL_MOBISALECAM_HEADER = "FTEL_MOBISALECAM_HEADER";
    //url generate token kong
    public static final String URL_GENERATE_TOKEN_KONG = "https://sapi.fpt.vn/token/GenerateToken";
    //constrant params
    public static String CITY_ID = "cityID";
    public static String DISTRICT_ID = "districtID";
    public static String WARD_ID = "wardID";
    public static String BUILDING_ID ="buildingId";
    public static String SERVICE_PACKAGE_ID = "packageServiceID";
    public static String DISTRICT_NAME = "name";
    public static int TYPE = 0;
    public static final String STREET_NAME = "name";
    public static final String WARD_NAME = "name";
    public static final String BUILDING_NAME = "name";
    public static final String HOME_TYPE_NAME = "name";
    public static final String DISTRICT = "district";
    public static final String WARD = "ward";
    public static final String STREET = "street";
    public static final String BUILDING = "building";
    public static final String HOME_TYPE = "homeType";
    //bookport
    public static String BOOKPORT_IDENTITY_ID = "";
    public static String OBJ_BOOKPORT_MANUAL = "objBookportManual";
    //return code
    public static int SUCCESS = 1;
    public static int EXP_TOKEN = -1;
    public static int CREATE_POTENTIAL_CUS = -150;
    public static int INDOOR_INVALID = -107;
    public static int OUTDOOR_INVALID = -108;
    //params select service type
    public static int DEVICES = 1;
    public static String PACKAGE_SERVICE = "packageService";
    public static String PROMOTION = "promotion";
    public static String SERVICE_PACKAGE_NAME = "name";
    public static String PROMOTION_NAME = "promotionName";
    public static int CUS_TYPE_COMPANY = 10;
    public static int CUS_TYPE_HOME = 12;
    public static String ACTION_BOOKPORT = "actionBookport";
    public static String AUTO = "auto";
    public static String MANUAL = "manual";
    public static final int REQUEST_CODE_PACKAGE_SERVICE = 2;
    public static final int REQUEST_CODE_PROMOTION = 3;
    public static final int REQUEST_CODE_DEVICE = 4;
    public static final int REQUEST_CODE_DISTRICT = 5;
    public static final int REQUEST_CODE_WARD = 6;
    public static final int REQUEST_CODE_STREET = 7;
    public static final int REQUEST_CODE_BUILDING = 8;
    public static final int REQUEST_CODE_HOME_TYPE = 9;
    //param use for call api get device list
    public static String TYPE_SALE_NEW = "1";
    public static String DEVICE_LIST_SELECTED = "deviceListSelected";
    public static String UPDATE_BOOKPORT = "updateBookport";
    public static String NEW_BOOKPORT = "newBookport";
    public static String BOOKPORT_TYPE = "bookportType";
    public static String CONTRACT_INFO_BY_REG = "contractUnPaid";
    public static String CONTRACT_INFO = "contractInfo";
    public static String CAN_BACK = "canBack";
    public static int DO_UPDATE = 0;
    //use for check to back fragment 2(cus info list)
    public static String FLAG_FRAGMENT = "flag";
    public static int FLAG_FRAG_VALUE = 2;
    //set objReport default
    public static ReqUpdateCusInfo updateCusInfo = new ReqUpdateCusInfo();
    //this objReport reference at ContractModelActivity, CusInfoDetailActivity,
    public static ReqGetRegistrationDetail reqGetRegistrationDetail = new ReqGetRegistrationDetail();

    public static SaleOfInfo saleOfInfo = new SaleOfInfo();

    //function create checksum
    public static String checkSum(String strBody){
        String strContent = strBody.replace("\n", "")
                .replace("\r", "")
                .replace(" ", "")
                .replace("\\", "");

        String base64 = "";
        try {
            base64 = Base64.encodeToString(strContent.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringBuffer str = new StringBuffer(base64);
        str.append("ftel-mobisale-cam");
        String s = str.toString().replace("\n", "");
        return md5(s);
    }

    //hash md5
    private static String md5(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes("UTF-8"));
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    @SuppressLint("HardwareIds")
    public static String getIMEIDevice(Context context){
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
            return "";
        }
        assert telephonyManager != null;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            for(int i=0; i<telephonyManager.getPhoneCount(); i++){
//                Log.d("TAG", "getIMEIDevice " + i + ": " + telephonyManager.getDeviceId(i));
//            }
//        }
        return telephonyManager.getDeviceId();
    }

    //hash password
    public static String hashPassword(String pass){
        String hashPass = "";
        try {
            //hash password
            hashPass = EncryptDecryptPassword.encyrpt(pass).trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashPass;
    }

    //this function use for convert cast bitmap to real path file
    public static String getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return getPathFromUri(inContext, Uri.parse(path));
    }

    //this function use for cast uri to real path file
    public static String getPathFromUri(final Context context, final Uri uri) {
        @SuppressLint("ObsoleteSdkInt")
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static void saveParamHeaders(Context ctx, String headerAuth){
        if(headerAuth==null){
            return;
        }
        SharedPref.put(ctx, SharedPref.Key.FTEL_MOBISALECAM_HEADER, headerAuth);
        try {
            JSONObject jsonObject = new JSONObject(headerAuth);
            SharedPref.put(ctx, SharedPref.Key.USERNAME, jsonObject.getString("Username"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String setTimerToRefreshToken(Calendar calendar){
        //add 1 hour
        calendar.add(Calendar.HOUR, 1);
        return String.valueOf(calendar.getTimeInMillis());
    }

    public static boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String formatLatLngByPattern(Double lat, Double lng) {
        return lat + "," + lng;
    }

    public static String convertDateEffect(String dateOriginal){
        //date original format (mm/dd/yyyy)
        String[] dateSplit = dateOriginal.split("/");
        //date format effect "mm/dd"
        return dateSplit[0] + "/" + dateSplit[1];
    }

    /** Returns the consumer friendly device name */
    public static String getDeviceName(){
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

    //return version name current of app
    public static String getVersionName(Context context){
        return context.getResources().getString(R.string.app_name) + " V" + BuildConfig.VERSION_NAME;
    }

    public static boolean writeFilePdfToDisk(ResponseBody body, int objId) {
        try {
            //TODO: create folder
            String folderMain = "MobisaleFCAM";
            String subFolder = "Contract";
            File f = new File(Environment.getExternalStorageDirectory(), folderMain);
            if (!f.exists()) {
                f.mkdirs();
            }
            //create sub folder
            File f1 = new File(Environment.getExternalStorageDirectory() + "/" + folderMain, subFolder);
            if (!f1.exists()) {
                f1.mkdirs();
            }
            //create path and file name
            String path = Environment.getExternalStorageDirectory() + "/" + folderMain + "/"
                    + subFolder + File.separator + objId + ".pdf";
            File futureStudioIconFile = new File(path);
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                }
                outputStream.flush();
                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public static String writeFileApkToDisk(ResponseBody body){
        //make folder
        String folderMain = "MobisaleFCAM";
        String subFolder = "Apk";
        File f = new File(Environment.getExternalStorageDirectory(), folderMain);
        if (!f.exists()) {
            f.mkdirs();
        }
        //create sub folder
        File f1 = new File(Environment.getExternalStorageDirectory() + "/" + folderMain, subFolder);
        if (!f1.exists()) {
            f1.mkdirs();
        }
        String path = Environment.getExternalStorageDirectory() + "/" + folderMain + "/" + subFolder;
        File futureStudioIconFile = new File(f1, "app.apk");
        //create path and file name
        InputStream inputStream ;
        OutputStream outputStream;
        //write file
        try {
            outputStream = new FileOutputStream(futureStudioIconFile);
            inputStream = body.byteStream();

            byte[] buffer = new byte[1024];
            int len1;
            while ((len1 = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, len1);
            }
            outputStream.close();
            inputStream.close();
            return path;
        } catch (Exception e) {
            return null;
        }
    }

    public static String locationToStringAddress(Context context, double lat, double lng){
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            return addresses.get(0).getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Address getLocationFromAddress(Context context, String str) {
        //generate lat, lng from str address
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        try {
            address = coder.getFromLocationName(str, 5);
            if (address != null) {
                return address.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String getCurrentDateTime(){
        Calendar c = Calendar.getInstance();
        int date = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);
        return month + "/" + date + "/" + year;
    }
}
