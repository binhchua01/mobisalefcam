package isc.fpt.fsalecam.mobisalefcam.database;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by Hau Le on 2018-08-22.
 */
public class DeviceList extends RealmObject implements Serializable{
    @SerializedName("IsSelected")
    @Expose(deserialize = false, serialize = false)
    private int isSelected;
    @SerializedName("Value")
    @Expose
    private String value;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ChangeNumber")
    @Expose
    private String changeNumber;
    @SerializedName("Price")
    @Expose
    private String price;

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChangeNumber() {
        return changeNumber;
    }

    public void setChangeNumber(String changeNumber) {
        this.changeNumber = changeNumber;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
