package isc.fpt.fsalecam.mobisalefcam.app;

import android.app.Application;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Hau Le on 2018-08-17.
 */
public class mobisalefcam extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("mobisaleDB.realm")
                .schemaVersion(7)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
