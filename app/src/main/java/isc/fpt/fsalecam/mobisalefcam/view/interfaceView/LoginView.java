package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

/**
 * Created by Hau Le on 2018-07-06.
 */
public interface LoginView {
    void loginSuccess();
    void callApiFailed(String message);
    void returnIMEI(String imei);
    void userError();
    void passError();
    void showDialogProgress();
    void hiddenDialogProgress();
}
