package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetRegistrationDetail;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetRegistrationById;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.NewCustomerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-07-23.
 */
public class NewCustomerPresenterImpl implements NewCustomerPresenter {
    private NewCustomerView view;

    public NewCustomerPresenterImpl(NewCustomerView newCustomerView) {
        this.view = newCustomerView;
    }

    @Override
    public void changedUIActivity(int num) {
        view.refreshUI(num);
    }

    @Override
    public void doGetRegistrationById(final Context context, final APIService apiService, final ReqGetRegistrationDetail obj) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetRegistrationById(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetRegistrationById(context, apiService, obj);
        }
    }

    private void callApiGetRegistrationById(final Context context, APIService apiService, ReqGetRegistrationDetail obj){
        apiService.doGetRegistrationById(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetRegistrationDetails())
                .enqueue(new Callback<ResGetRegistrationById>() {
                    @Override
                    public void onResponse(Call<ResGetRegistrationById> call, Response<ResGetRegistrationById> response) {
                        validateReturnCode(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResGetRegistrationById> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResGetRegistrationById> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getRegistrationByIdSuccess(response.body().getData().get(0));
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else {
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else {
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }
}
