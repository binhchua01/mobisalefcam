package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-08-10.
 */
public class ResAddress {
    @SerializedName("ResultID")
    @Expose
    private Integer resultID;
    @SerializedName("Result")
    @Expose
    private String result;

    public Integer getResultID() {
        return resultID;
    }

    public void setResultID(Integer resultID) {
        this.resultID = resultID;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
