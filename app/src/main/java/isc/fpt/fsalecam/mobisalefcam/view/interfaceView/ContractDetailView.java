package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ContractDetails;

/**
 * Created by Hau Le on 2018-08-30.
 */
public interface ContractDetailView {
    void getContractDetailSuccess(ContractDetails obj);
    void getUrlDownLoadContractPdf(String url);
    void doDownloadFilePdf(String message);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
