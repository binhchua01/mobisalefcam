package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqRegistrationAll;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Registration;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetRegistrationAll;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.UnPaidFragView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-30.
 */
public class UnPaidFragPresenterImpl implements UnPaidFragPresenter {
    private UnPaidFragView view;

    public UnPaidFragPresenterImpl(UnPaidFragView view) {
        this.view = view;
    }

    @Override
    public void doGetCusUnpaidList(final Context context, final APIService apiService, final ReqRegistrationAll obj) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetCusUnPaidList(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetCusUnPaidList(context, apiService, obj);
        }
    }

    private void callApiGetCusUnPaidList(final Context context, APIService apiService, ReqRegistrationAll obj){
        apiService.doGetRegistrationAll(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetRegistrationAll())
                .enqueue(new Callback<ResGetRegistrationAll>() {
                    @Override
                    public void onResponse(Call<ResGetRegistrationAll> call, Response<ResGetRegistrationAll> response) {
                        validateReturnCode(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResGetRegistrationAll> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResGetRegistrationAll> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getCusUnPaidSuccess((ArrayList<Registration>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else {
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else {
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }
}
