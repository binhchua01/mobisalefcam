package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-12.
 */
public class ReqUpdateRegistrationImage {
    private int regId;
    private String regCode;
    private String imageInfo;

    public int getRegId() {
        return regId;
    }

    public void setRegId(int regId) {
        this.regId = regId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getImageInfo() {
        return imageInfo;
    }

    public void setImageInfo(String imageInfo) {
        this.imageInfo = imageInfo;
    }

    public LinkedHashMap<String, String> generateObjUpdateRegistrationImage(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("RegId", String.valueOf(getRegId()));
        obj.put("RegCode", getRegCode());
        obj.put("ImageInfo", getImageInfo());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("RegId", String.valueOf(getRegId()));
            obj.put("RegCode", getRegCode());
            obj.put("ImageInfo", getImageInfo());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
