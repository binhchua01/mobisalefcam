package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-09-24.
 */
public class CurrentVersion {
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("Link")
    @Expose
    private String link;
    @SerializedName("IsNew")
    @Expose
    private Integer isNew;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }
}
