package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;

/**
 * Created by Hau Le on 2018-08-27.
 */
public class RegistrationDetail implements Serializable{
    @SerializedName("RegId")
    @Expose
    private Integer regId;
    @SerializedName("RegCode")
    @Expose
    private String regCode;
    @SerializedName("ObjId")
    @Expose
    private Integer objId;
    @SerializedName("Contract")
    @Expose
    private String contract;
    @SerializedName("RegStatus")
    @Expose
    private String regStatus;
    @SerializedName("RegType")
    @Expose
    private Integer regType;
    @SerializedName("RegTypeName")
    @Expose
    private String regTypeName;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Phone1")
    @Expose
    private String phone1;
    @SerializedName("ImageInfo")
    @Expose
    private String imageInfo;
    @SerializedName("IsUpdateImage")
    @Expose
    private Integer isUpdateImage;
    @SerializedName("LocalType")
    @Expose
    private Integer localType;
    @SerializedName("PromotionID")
    @Expose
    private Integer promotionID;
    @SerializedName("PromotionName")
    @Expose
    private String promotionName;
    @SerializedName("PromotionDescription")
    @Expose
    private String promotionDescription;
    @SerializedName("MonthOfPrepaid")
    @Expose
    private Integer monthOfPrepaid;
    @SerializedName("Total")
    @Expose
    private Double total;
    @SerializedName("InternetTotal")
    @Expose
    private Double internetTotal;
    @SerializedName("DeviceTotal")
    @Expose
    private Double deviceTotal;
    @SerializedName("DepositFee")
    @Expose
    private Double depositFee;
    @SerializedName("ConnectionFee")
    @Expose
    private Double connectionFee;
    @SerializedName("VAT")
    @Expose
    private Integer vAT;
    @SerializedName("GroupPoints")
    @Expose
    private String groupPoints;
    @SerializedName("BookPortStatus")
    @Expose
    private String bookPortStatus;
    @SerializedName("IsBookPort")
    @Expose
    private Integer isBookPort;
    @SerializedName("ODCCableType")
    @Expose
    private String oDCCableType;
    @SerializedName("ContractTemp")
    @Expose
    private String contractTemp;
    @SerializedName("LocationId")
    @Expose
    private Integer locationId;
    @SerializedName("PaymentMethod")
    @Expose
    private Integer paymentMethod;
    @SerializedName("PaymentMethodName")
    @Expose
    private String paymentMethodName;
    @SerializedName("ListDevice")
    @Expose
    private List<ReqDevice> listDevice = null;
    @SerializedName("ListServiceType")
    @Expose
    private List<ItemOfListRes> listServiceType = null;

    public String getPromotionDescription() {
        return promotionDescription;
    }

    public void setPromotionDescription(String promotionDescription) {
        this.promotionDescription = promotionDescription;
    }

    public Integer getRegId() {
        return regId;
    }

    public void setRegId(Integer regId) {
        this.regId = regId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public Integer getObjId() {
        return objId;
    }

    public void setObjId(Integer objId) {
        this.objId = objId;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public Integer getRegType() {
        return regType;
    }

    public void setRegType(Integer regType) {
        this.regType = regType;
    }

    public String getRegTypeName() {
        return regTypeName;
    }

    public void setRegTypeName(String regTypeName) {
        this.regTypeName = regTypeName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getImageInfo() {
        return imageInfo;
    }

    public void setImageInfo(String imageInfo) {
        this.imageInfo = imageInfo;
    }

    public Integer getIsUpdateImage() {
        return isUpdateImage;
    }

    public void setIsUpdateImage(Integer isUpdateImage) {
        this.isUpdateImage = isUpdateImage;
    }

    public Integer getLocalType() {
        return localType;
    }

    public void setLocalType(Integer localType) {
        this.localType = localType;
    }

    public Integer getPromotionID() {
        return promotionID;
    }

    public void setPromotionID(Integer promotionID) {
        this.promotionID = promotionID;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public Integer getMonthOfPrepaid() {
        return monthOfPrepaid;
    }

    public void setMonthOfPrepaid(Integer monthOfPrepaid) {
        this.monthOfPrepaid = monthOfPrepaid;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getInternetTotal() {
        return internetTotal;
    }

    public void setInternetTotal(Double internetTotal) {
        this.internetTotal = internetTotal;
    }

    public Double getDeviceTotal() {
        return deviceTotal;
    }

    public void setDeviceTotal(Double deviceTotal) {
        this.deviceTotal = deviceTotal;
    }

    public Double getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(Double depositFee) {
        this.depositFee = depositFee;
    }

    public Double getConnectionFee() {
        return connectionFee;
    }

    public void setConnectionFee(Double connectionFee) {
        this.connectionFee = connectionFee;
    }

    public Integer getVAT() {
        return vAT;
    }

    public void setVAT(Integer vAT) {
        this.vAT = vAT;
    }

    public String getGroupPoints() {
        return groupPoints;
    }

    public void setGroupPoints(String groupPoints) {
        this.groupPoints = groupPoints;
    }

    public String getBookPortStatus() {
        return bookPortStatus;
    }

    public void setBookPortStatus(String bookPortStatus) {
        this.bookPortStatus = bookPortStatus;
    }

    public Integer getIsBookPort() {
        return isBookPort;
    }

    public void setIsBookPort(Integer isBookPort) {
        this.isBookPort = isBookPort;
    }

    public String getODCCableType() {
        return oDCCableType;
    }

    public void setODCCableType(String oDCCableType) {
        this.oDCCableType = oDCCableType;
    }

    public String getContractTemp() {
        return contractTemp;
    }

    public void setContractTemp(String contractTemp) {
        this.contractTemp = contractTemp;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public List<ReqDevice> getListDevice() {
        return listDevice;
    }

    public void setListDevice(List<ReqDevice> listDevice) {
        this.listDevice = listDevice;
    }

    public List<ItemOfListRes> getListServiceType() {
        return listServiceType;
    }

    public void setListServiceType(List<ItemOfListRes> listServiceType) {
        this.listServiceType = listServiceType;
    }
}
