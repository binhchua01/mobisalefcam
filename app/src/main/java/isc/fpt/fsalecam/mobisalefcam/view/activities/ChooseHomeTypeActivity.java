package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.ChooseHouseTypeAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChooseHomeTypePresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChooseHomeTypePresenter;
import isc.fpt.fsalecam.mobisalefcam.database.HomeType;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseHomeTypeView;

public class ChooseHomeTypeActivity extends BaseActivity
        implements ChooseHomeTypeView, View.OnClickListener{
    @BindView(R.id.layout_choose_home_type) LinearLayout loParent;
    @BindView(R.id.recycler_view_home_type) RecyclerView recyclerView;
    @BindView(R.id.img_nav_back_home_type) RelativeLayout loBack;
    @BindView(R.id.img_home_type_refresh) RelativeLayout loRefresh;
    @BindView(R.id.edt_search_home_type) EditText edtHomeType;

    private APIService apiService;
    private ChooseHomeTypePresenter presenter;
    private ArrayList<HomeType> list;
    private ChooseHouseTypeAdapter adapter;
    private Realm realm;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        initDB();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        callApi();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_choose_home_type;
    }

    @Override
    public void getHomeTypeListSuccess() {
        this.list = (ArrayList<HomeType>) realm.copyFromRealm(realm.where(HomeType.class).findAll());
        adapter.notifyData(list);
    }

    @Override
    public void search(ArrayList<HomeType> list) {
        this.list = list;
        adapter.notifyData(list);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_nav_back_home_type:
                onBackPressed();
                break;
            case R.id.img_home_type_refresh:
                presenter.refreshHomeTypeList(this, apiService, realm);
                break;
        }
    }

    private void setEvent() {
        loBack.setOnClickListener(this);
        loRefresh.setOnClickListener(this);
        //add text change
        edtHomeType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.searchHomeTypeList(realm, String.valueOf(editable));
            }
        });
    }

    private void initView() {
        list = new ArrayList<>();
        adapter = new ChooseHouseTypeAdapter(this, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //TODO: callback data
                Intent intent = new Intent();
                intent.putExtra(Utils.HOME_TYPE, list.get(position).getId() + ":" + list.get(position).getName());
                setResult(Utils.REQUEST_CODE_HOME_TYPE, intent);
                finish();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void initDB() {
        realm = Realm.getDefaultInstance();
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new ChooseHomeTypePresenterImpl(this);
        presenter.getHomeTypeList(this, apiService, realm);
    }
}
