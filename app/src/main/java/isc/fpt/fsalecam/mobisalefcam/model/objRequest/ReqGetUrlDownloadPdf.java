package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-12.
 */
public class ReqGetUrlDownloadPdf {
    private int objId;

    public int getObjId() {
        return objId;
    }

    public void setObjId(int objId) {
        this.objId = objId;
    }

    public LinkedHashMap<String, String> generateObjGetUrlDownload(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("ObjId", String.valueOf(getObjId()));
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("ObjId", String.valueOf(getObjId()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
