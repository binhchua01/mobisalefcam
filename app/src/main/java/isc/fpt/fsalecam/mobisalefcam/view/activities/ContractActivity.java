package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Objects;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCreateContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Contract;
import isc.fpt.fsalecam.mobisalefcam.presenter.CreateContractPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.CreateContractPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.ContinueRegisterService;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.ResizeDialogPercent;
import isc.fpt.fsalecam.mobisalefcam.utils.ReturnBitmap;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.CreateContractView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.CusInfoDetailsActivity.isUploadImage;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.CusInfoDetailsActivity.registrationDetail;

public class ContractActivity extends BaseActivity
        implements View.OnClickListener, CreateContractView {
    @BindView(R.id.ic_icon_nav_back_contract) RelativeLayout imgBack;
    @BindView(R.id.btn_signature) Button btnSignature;
    @BindView(R.id.img_view_signature) ImageView imgViewSignature;
    @BindView(R.id.btn_see_contract_model) Button btnSeeContractModel;
    @BindView(R.id.btn_create_contract) Button btnCreateContract;
    @BindView(R.id.tv_cus_name) TextView tvCusName;
    @BindView(R.id.tv_reg_code) TextView tvRegCode;
    @BindView(R.id.tv_number_phone) TextView tvNumberPhone;
    @BindView(R.id.tv_email) TextView tvEmail;
    @BindView(R.id.layout_email) RelativeLayout loEmail;
    @BindView(R.id.tv_address) TextView tvAddress;
    @BindView(R.id.tv_cus_name_at_signature)TextView tvCusNameAtSignature;

    private Dialog dialog;
    private Dialog dialogSignature;
    private Dialog dialogCreateContractSuccess;
    private CreateContractPresenter presenter;
    private APIService apiService;
    private int regId;
    private static final int MY_PERMISSIONS_REQUEST = 100;
    private Bitmap bm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataToView();
        // No explanation needed; request the permission
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST);
    }

    private void initDataToView() {
        if(registrationDetail!=null){
            tvCusName.setText(registrationDetail.getFullName());
            tvRegCode.setText(registrationDetail.getRegCode());
            tvNumberPhone.setText(registrationDetail.getPhone1());
            tvEmail.setText(registrationDetail.getEmail());
            tvAddress.setText(registrationDetail.getAddress());
            tvCusNameAtSignature.setText(registrationDetail.getFullName());
            regId = registrationDetail.getRegId();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        initParamToCallApi();
    }

    private void initParamToCallApi() {
        apiService = ApiUtils.getApiService();
        presenter = new CreateContractPresenterImpl(this);
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        btnSignature.setOnClickListener(this);
        btnSeeContractModel.setOnClickListener(this);
        btnCreateContract.setOnClickListener(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_contract;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_icon_nav_back_contract:
                onBackPressed();
                break;
            case R.id.btn_signature:
                if(!askForPermission()){
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST);
                }else{
                    dialogSignature.show();
                }
                break;
            case R.id.btn_see_contract_model:
                startActivity(new Intent(this, ContractModelActivity.class));
                break;
            case R.id.btn_create_contract:
                if(isUploadImage == 0){
                    return;
                }
                createContract();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS_REQUEST){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                dialogSignature();
            }
        }
    }

    @Override
    public void createContractSuccess(final Contract contract) {
        dialogCreateContractSuccess = DialogUtils.showDialogUpdateRegistrationSuccess(this,
                getResources().getString(R.string.mes_update_registration_success),
                new ContinueRegisterService() {
            @Override
            public void continueRegister() {
                int SPLASH_DISPLAY_LENGTH = 2000;
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        //Start activity here
                        Intent intent = new Intent(ContractActivity.this, ContractDetailsActivity.class);
                        intent.putExtra(Utils.CONTRACT_INFO, contract);
                        startActivity(intent);
                        finishAffinity();
                    }
                };
                handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);
            }
        });
        dialogCreateContractSuccess.show();
        ResizeDialogPercent.resize(this, dialogCreateContractSuccess);
    }

    @Override
    public void uploadSignatureSuccess(int imageId) {
        //call api create contract after upload image success
        ReqCreateContract obj = new ReqCreateContract();
        obj.setUsername(SharedPref.get(getApplicationContext(), SharedPref.Key.USERNAME, ""));
        obj.setRegCode(tvRegCode.getText().toString());
        obj.setImgSignature(String.valueOf(imageId));
        presenter.doCreateContract(getApplicationContext(), apiService, obj);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(dialogCreateContractSuccess!=null && dialogCreateContractSuccess.isShowing()){
            dialogCreateContractSuccess.dismiss();
        }
    }

    private boolean askForPermission() {
        return ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void createContract(){
        if (bm == null){
            DialogUtils.showMessageDialogVerButton(this,
                    getResources().getString(R.string.mes_confirm_contract)).show();
            return;
        }
        //call api upload image signature
        presenter.doUploadImageSignature(this, apiService,
                Utils.getImageUri(this, bm), String.valueOf(regId), Utils.saleOfInfo);
    }

    private void dialogSignature(){
        if(dialogSignature!=null){
            return;
        }
        dialogSignature = DialogUtils.dialogSignature(this, new ReturnBitmap() {
            @Override
            public void bitmapExport(Bitmap bitmap) {
                //set signature to image view
                imgViewSignature.setImageBitmap(bitmap);
                bm = bitmap;
            }
        });
    }
}
