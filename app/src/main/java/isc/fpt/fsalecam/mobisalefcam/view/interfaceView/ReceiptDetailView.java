package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.database.PaymentType;

/**
 * Created by Hau Le on 2018-09-04.
 */
public interface ReceiptDetailView {
    void getTypePaymentList(ArrayList<PaymentType> list);
    void updatePaymentSuccess();
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressLoading();
    void hiddenProgressLoading();
}
