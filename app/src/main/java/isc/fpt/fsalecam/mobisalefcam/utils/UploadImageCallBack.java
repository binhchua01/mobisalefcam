package isc.fpt.fsalecam.mobisalefcam.utils;

/**
 * Created by Hau Le on 2018-09-06.
 */
public interface UploadImageCallBack {
    void uploadSuccess(String mes, int imageId);
    void uploadFailed(String mes);
}
