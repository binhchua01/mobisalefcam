package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-10.
 */
public class ReqListOfPoints {
    private String username, locationId, wardId,
            lat, lng, typeOfNetwork, bookportIdentityId;

    public ReqListOfPoints(String username, String locationId,
                           String wardId, String lat, String lng,
                           String typeOfNetwork, String bookportIdentityId) {
        this.username = username;
        this.locationId = locationId;
        this.wardId = wardId;
        this.lat = lat;
        this.lng = lng;
        this.typeOfNetwork = typeOfNetwork;
        this.bookportIdentityId = bookportIdentityId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getWardId() {
        return wardId;
    }

    public void setWardId(String wardId) {
        this.wardId = wardId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getTypeOfNetwork() {
        return typeOfNetwork;
    }

    public void setTypeOfNetwork(String typeOfNetwork) {
        this.typeOfNetwork = typeOfNetwork;
    }

    public String getBookportIdentityId() {
        return bookportIdentityId;
    }

    public void setBookportIdentityId(String bookportIdentityId) {
        this.bookportIdentityId = bookportIdentityId;
    }

    public LinkedHashMap<String, String> generateObjGetListOfPoints(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("Username", username);
        obj.put("LocationId", locationId);
        obj.put("WardId", wardId);
        obj.put("latitude", lat);
        obj.put("longitude", lng);
        obj.put("typeOfNetworkInfrastructure", typeOfNetwork);
        obj.put("BookportIdentityID", bookportIdentityId);
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("Username", getUsername());
            obj.put("LocationId", getLocationId());
            obj.put("WardId", getWardId());
            obj.put("latitude", getLat());
            obj.put("longitude", getLng());
            obj.put("typeOfNetworkInfrastructure", getTypeOfNetwork());
            obj.put("BookportIdentityID", getBookportIdentityId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
