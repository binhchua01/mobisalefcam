package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetDistrictList;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqSaleMan;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.ConnectionFee;
import isc.fpt.fsalecam.mobisalefcam.database.CusInfoPaymentMethod;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ServiceTypeFragView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-16.
 */
public class ServiceTypeFragPresenterImpl implements ServiceTypeFragPresenter {
    private ServiceTypeFragView view;

    public ServiceTypeFragPresenterImpl(ServiceTypeFragView view) {
        this.view = view;
    }

    @Override
    public void doGetServiceTypeList(final Context context, final APIService apiService) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetServiceTypeList(context, apiService);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetServiceTypeList(context, apiService);
        }
    }

    @Override
    public void doGetPaymentMethodList(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(CusInfoPaymentMethod.class).findAll().size() == 0){
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetPaymentMethodList(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetPaymentMethodList(context, apiService, realm);
            }
        }else{
            view.getPaymentMethodSuccess((ArrayList<CusInfoPaymentMethod>)
                    realm.copyFromRealm(realm.where(CusInfoPaymentMethod.class).findAll()));
        }
    }

    @Override
    public void doGetConnectionFeeList(final Context context, final APIService apiService,
                                       final String locationId, final Realm realm) {
        if(realm.where(ConnectionFee.class).equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll().size() == 0){
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetConnectionFeeList(context, apiService, locationId, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetConnectionFeeList(context, apiService, locationId, realm);
            }
        }else{
            ArrayList<ConnectionFee> list = (ArrayList<ConnectionFee>) realm.copyFromRealm(realm.where(ConnectionFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll());
            view.getConnectionFeeList(list);
        }
    }

    private void callApiGetConnectionFeeList(final Context context, APIService apiService,
                                             final String locationId, final Realm realm){
        ReqGetDistrictList obj = new ReqGetDistrictList(locationId);
        apiService.doGetConnectionFeeList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetDistrictList()).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCodeConnectionFee(context, response, realm, locationId);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiGetPaymentMethodList(final Context context, APIService apiService, final Realm realm){
        ReqSaleMan obj = new ReqSaleMan(SharedPref.get(context, SharedPref.Key.USERNAME, ""));
        apiService.doGetPaymentMethodPerMonthList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateSaleManObj()).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCodePaymentMethod(context, response, realm);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiGetServiceTypeList(final Context context, APIService apiService){
        ReqSaleMan obj = new ReqSaleMan(SharedPref.get(context, SharedPref.Key.USERNAME, ""));
        apiService.doGetServiceTypeList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateSaleManObj()).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCode(context, response);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> response){
        if(response.body() != null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getServiceTypeListSuccess((ArrayList<ItemOfListRes>) response.body().getData());
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodePaymentMethod(Context context, Response<ResParentStructure> response, Realm realm) {
        if(response.body() != null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                savePaymentMethod(response.body().getData(), realm);
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodeConnectionFee(Context context, Response<ResParentStructure> response,
                                                 Realm realm, String cityID){
        if(response.body() != null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                saveConnectionFee((ArrayList<ItemOfListRes>) response.body().getData(), realm, cityID);
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void saveConnectionFee(ArrayList<ItemOfListRes> list, Realm realm, String cityID) {
        realm.beginTransaction();
        realm.where(ConnectionFee.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                ConnectionFee connectionFee = realm.createObject(ConnectionFee.class, list.get(i).getId());
                connectionFee.setName(list.get(i).getName());
                connectionFee.setCityID(Integer.parseInt(cityID));
                realm.commitTransaction();
            }
            ArrayList<ConnectionFee> connectionFeesList = (ArrayList<ConnectionFee>) realm.copyFromRealm(realm.where(ConnectionFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(cityID)).findAll());
            view.getConnectionFeeList(connectionFeesList);
        }else{
            ArrayList<ConnectionFee> connectionFeesList = (ArrayList<ConnectionFee>) realm.copyFromRealm(realm.where(ConnectionFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(cityID)).findAll());
            view.getConnectionFeeList(connectionFeesList);
        }
    }

    private void savePaymentMethod(List<ItemOfListRes> list, Realm realm) {
        realm.beginTransaction();
        realm.where(CusInfoPaymentMethod.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                CusInfoPaymentMethod cusInfoPaymentMethod = realm.createObject(CusInfoPaymentMethod.class, list.get(i).getId());
                cusInfoPaymentMethod.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            view.getPaymentMethodSuccess((ArrayList<CusInfoPaymentMethod>)
                    realm.copyFromRealm(realm.where(CusInfoPaymentMethod.class).findAll()));
        }else{
            view.getPaymentMethodSuccess((ArrayList<CusInfoPaymentMethod>)
                    realm.copyFromRealm(realm.where(CusInfoPaymentMethod.class).findAll()));
        }
    }
}
