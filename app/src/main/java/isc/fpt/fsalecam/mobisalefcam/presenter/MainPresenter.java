package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqChangePass;

/**
 * Created by Hau Le on 2018-08-02.
 */
public interface MainPresenter {
    void doGetInfoOfSale(Context context, APIService apiService, Realm realm, String username);
    void doChangePassword(Context context, APIService apiService, ReqChangePass obj);
}
