package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmMigration;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-08-01.
 */
public interface ChooseDistrictPresenter {
    void getDistrictList(Context context, APIService apiService, String cityId, Realm realm);
    void refreshDistrictList(Context context, APIService apiService, String cityId, Realm realm);
    void searchDistrictList(Realm realm, String keyQuery, String cityId);
}
