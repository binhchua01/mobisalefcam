package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.database.Ward;

/**
 * Created by Hau Le on 2018-08-02.
 */
public interface ChooseWardView {
    void getWardListSuccess();
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressLoading();
    void hiddenProgressLoading();
    void searchWard(ArrayList<Ward> list);
}
