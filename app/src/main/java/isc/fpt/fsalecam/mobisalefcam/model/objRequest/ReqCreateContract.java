package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-28.
 */
public class ReqCreateContract{
    private String username, regCode, imgSignature;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getImgSignature() {
        return imgSignature;
    }

    public void setImgSignature(String imgSignature) {
        this.imgSignature = imgSignature;
    }

    public LinkedHashMap<String, String> generateObjCreateContract(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("Username", getUsername());
        obj.put("RegCode", getRegCode());
        obj.put("ImageSignature", getImgSignature());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("Username", getUsername());
            obj.put("RegCode", getRegCode());
            obj.put("ImageSignature", getImgSignature());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
