package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Registration;

/**
 * Created by Hau Le on 2018-08-29.
 */
public interface CusWaitConfirmView {
    void getRegistrationAllSuccess(ArrayList<Registration> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
