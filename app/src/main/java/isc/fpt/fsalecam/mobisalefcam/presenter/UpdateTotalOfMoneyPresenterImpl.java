package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetDistrictList;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetRegistrationDetail;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqSaleMan;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetRegistrationById;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.database.ConnectionFee;
import isc.fpt.fsalecam.mobisalefcam.database.DepositFee;
import isc.fpt.fsalecam.mobisalefcam.database.Vat;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.UpdateTotalOfMoneyView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-06.
 */
public class UpdateTotalOfMoneyPresenterImpl implements UpdateTotalOfMoneyPresenter {
    private UpdateTotalOfMoneyView view;

    public UpdateTotalOfMoneyPresenterImpl(UpdateTotalOfMoneyView view) {
        this.view = view;
    }

    @Override
    public void doGetServiceTypeList(final Context context, final APIService apiService) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetServiceTypeList(context, apiService);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetServiceTypeList(context, apiService);
        }
    }

    @Override
    public void doGetConnectionFeeList(final Context context, final APIService apiService,
                                       final String locationId, final Realm realm) {
        if(realm.where(ConnectionFee.class).equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll().size() == 0){
            view.showProgressLoading();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetConnectionFeeList(context, apiService, locationId, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetConnectionFeeList(context, apiService, locationId, realm);
            }
        }else{
            ArrayList<ConnectionFee> list = (ArrayList<ConnectionFee>) realm.copyFromRealm(realm.where(ConnectionFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll());
            view.getConnectionFeeListSuccess(list);
        }
    }

    @Override
    public void doGetRegistrationById(final Context context, final APIService apiService, final ReqGetRegistrationDetail obj) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetRegistrationById(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetRegistrationById(context, apiService, obj);
        }
    }

    @Override
    public void doGetDepositList(final Context context, final APIService apiService, final String locationId, final Realm realm) {
        if(realm.where(DepositFee.class).equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll().size() == 0){
            //show dialog
            view.showProgressLoading();
            long timerRefresh = Long.parseLong(SharedPref.get(context,
                    SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiDetDepositList(context, apiService, locationId, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiDetDepositList(context, apiService, locationId, realm);
            }
        }else{
            ArrayList<DepositFee> list = (ArrayList<DepositFee>) realm.copyFromRealm(realm.where(DepositFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll());
            view.getDepositListSuccess(list);
        }
    }

    @Override
    public void doGetVatList(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(Vat.class).findAll().size() == 0){
            //show dialog
            view.showProgressLoading();
            long timerRefresh = Long.parseLong(SharedPref.get(context,
                    SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetVatList(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiGetVatList(context, apiService, realm);
            }
        }else{
            ArrayList<Vat> data = (ArrayList<Vat>) realm.copyFromRealm(realm.where(Vat.class).findAll());
            view.getVatListSuccess(data);
        }
    }

    @Override
    public void doGetCalNewTotal(final Context context, final APIService apiService, final ReqUpdateRegistrationTotal obj) {
        //show dialog
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetCalNewTotal(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetCalNewTotal(context, apiService, obj);
        }
    }

    @Override
    public void doUpdateRegistrationTotal(final Context context, final APIService apiService, final ReqUpdateRegistrationTotal obj) {
        //show dialog
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiUpdateRegistrationTotal(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiUpdateRegistrationTotal(context, apiService, obj);
        }
    }

    private void callApiUpdateRegistrationTotal(final Context context, APIService apiService, ReqUpdateRegistrationTotal obj){
        apiService.doUpdateRegistrationTotal(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjUpdateRegistrationTotal())
                .enqueue(new Callback<ResUpdateRegistrationTotal>() {
                    @Override
                    public void onResponse(Call<ResUpdateRegistrationTotal> call, Response<ResUpdateRegistrationTotal> response) {
                        validateReturnCodeUpdateRegistrationTotal(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResUpdateRegistrationTotal> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetCalNewTotal(final Context context, APIService apiService, ReqUpdateRegistrationTotal obj){
        apiService.doCalNewTotal(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjUpdateRegistrationTotal())
                .enqueue(new Callback<ResCalNewTotal>() {
                    @Override
                    public void onResponse(Call<ResCalNewTotal> call, Response<ResCalNewTotal> response) {
                        validateReturnCodeCalNewTotal(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResCalNewTotal> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetVatList(final Context context, APIService apiService, final Realm realm){
        final LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        apiService.doGetVatList(
            Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
            Utils.CONTENT_TYPE,
            SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
            Utils.checkSum(obj.toString()), obj)
            .enqueue(new Callback<ResParentStructure>() {
                @Override
                public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                    validateReturnCodeVat(context, response, realm);
                }

                @Override
                public void onFailure(Call<ResParentStructure> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    private void callApiDetDepositList(final Context context, APIService apiService, final String locationId, final Realm realm){
        ReqGetDistrictList obj = new ReqGetDistrictList(locationId);
        apiService.doGetDepositList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetDistrictList())
                .enqueue(new Callback<ResParentStructure>() {
                    @Override
                    public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                        validateReturnCodeDepositFee(context, response, locationId, realm);
                    }

                    @Override
                    public void onFailure(Call<ResParentStructure> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetRegistrationById(final Context context, APIService apiService, ReqGetRegistrationDetail obj){
        apiService.doGetRegistrationById(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetRegistrationDetails())
                .enqueue(new Callback<ResGetRegistrationById>() {
                    @Override
                    public void onResponse(Call<ResGetRegistrationById> call, Response<ResGetRegistrationById> response) {
                        validateReturnCodeGetRegistrationById(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResGetRegistrationById> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetConnectionFeeList(final Context context, APIService apiService, final String locationId, final Realm realm){
        ReqGetDistrictList obj = new ReqGetDistrictList(locationId);
        apiService.doGetConnectionFeeList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetDistrictList()).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCodeConnectionFee(context, response, realm, locationId);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressLoading();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiGetServiceTypeList(final Context context, APIService apiService){
        ReqSaleMan obj = new ReqSaleMan(SharedPref.get(context, SharedPref.Key.USERNAME, ""));
        apiService.doGetServiceTypeList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateSaleManObj()).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCodeServiceType(context, response);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressLoading();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCodeConnectionFee(Context context, Response<ResParentStructure> response,
                                                 Realm realm, String cityID){
        if(response.body() != null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                saveConnectionFee((ArrayList<ItemOfListRes>) response.body().getData(), realm, cityID);
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCodeDepositFee(Context context, Response<ResParentStructure> response,
                                              String locationId, Realm realm) {
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                saveDepositFee(realm, (ArrayList<ItemOfListRes>) response.body().getData(), locationId);
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else {
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCodeServiceType(Context context, Response<ResParentStructure> response){
        if(response.body() != null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getServiceTypeListSuccess((ArrayList<ItemOfListRes>) response.body().getData());
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCodeVat(Context context, Response<ResParentStructure> response, Realm realm){
        if(response.body() != null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                saveVat(realm, (ArrayList<ItemOfListRes>) response.body().getData());
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCodeGetRegistrationById(Context context, Response<ResGetRegistrationById> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getRegistrationById(response.body().getData().get(0));
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCodeCalNewTotal(Context context, Response<ResCalNewTotal> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getCalNewTotal((ArrayList<CalNewTotal>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCodeUpdateRegistrationTotal(Context context, Response<ResUpdateRegistrationTotal> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getUpdateRegistrationTotal((ArrayList<UpdateRegistrationTotal>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void saveConnectionFee(ArrayList<ItemOfListRes> list, Realm realm, String cityID) {
        realm.beginTransaction();
        realm.where(ConnectionFee.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                ConnectionFee connectionFee = realm.createObject(ConnectionFee.class, list.get(i).getId());
                connectionFee.setName(list.get(i).getName());
                connectionFee.setCityID(Integer.parseInt(cityID));
                realm.commitTransaction();
            }
            ArrayList<ConnectionFee> connectionFeesList = (ArrayList<ConnectionFee>) realm.copyFromRealm(realm.where(ConnectionFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(cityID)).findAll());
            view.getConnectionFeeListSuccess(connectionFeesList);
        }else{
            ArrayList<ConnectionFee> connectionFeesList = (ArrayList<ConnectionFee>) realm.copyFromRealm(realm.where(ConnectionFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(cityID)).findAll());
            view.getConnectionFeeListSuccess(connectionFeesList);
        }
    }

    private void saveVat(Realm realm, ArrayList<ItemOfListRes> list) {
        realm.beginTransaction();
        realm.where(Vat.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if (list.size() != 0) {
            for (int i = 0; i < list.size(); i++) {
                realm.beginTransaction();
                Vat vat = realm.createObject(Vat.class, list.get(i).getId());
                vat.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            ArrayList<Vat> data = (ArrayList<Vat>) realm.copyFromRealm(realm.where(Vat.class).findAll());
            view.getVatListSuccess(data);
        } else {
            ArrayList<Vat> data = (ArrayList<Vat>) realm.copyFromRealm(realm.where(Vat.class).findAll());
            view.getVatListSuccess(data);
        }
    }

    private void saveDepositFee(Realm realm, ArrayList<ItemOfListRes> list, String locationId) {
        realm.beginTransaction();
        realm.where(DepositFee.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                DepositFee depositFee = realm.createObject(DepositFee.class, list.get(i).getId());
                depositFee.setName(list.get(i).getName());
                depositFee.setCityID(Integer.parseInt(locationId));
                realm.commitTransaction();
            }
            ArrayList<DepositFee> data = (ArrayList<DepositFee>) realm.copyFromRealm(realm.where(DepositFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll());
            view.getDepositListSuccess(data);
        }else{
            ArrayList<DepositFee> data = (ArrayList<DepositFee>) realm.copyFromRealm(realm.where(DepositFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll());
            view.getDepositListSuccess(data);
        }
    }
}
