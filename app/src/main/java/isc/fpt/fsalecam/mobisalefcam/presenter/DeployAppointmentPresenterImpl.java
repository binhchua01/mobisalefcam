package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetAbility;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetSubTeam;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateAppointment;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Ability;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Appointment;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetAbility;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetSubTeam;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdateAppointment;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.TimeZone;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.DeployAppointmentView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-13.
 */
public class DeployAppointmentPresenterImpl implements DeployAppointmentPresenter {
    private DeployAppointmentView view;

    public DeployAppointmentPresenterImpl(DeployAppointmentView view) {
        this.view = view;
    }

    @Override
    public void doGetSubTeamIdOfContract(final Context context, final APIService apiService, final ReqGetSubTeam obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetSubTeam(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetSubTeam(context, apiService, obj);
        }
    }

    @Override
    public void doGetAbility(final Context context, final APIService apiService, final ReqGetAbility obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetAbility(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetAbility(context, apiService, obj);
        }
    }

    @Override
    public void doUpdateAppointment(final Context context, final APIService apiService, final ReqUpdateAppointment obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiUpdateDeployAppointment(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiUpdateDeployAppointment(context, apiService, obj);
        }
    }

    @Override
    public void doGetTimeZoneList(final Context context, final APIService apiService) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetTimeZone(context, apiService);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetTimeZone(context, apiService);
        }
    }

    private void callApiGetTimeZone(final Context context, APIService apiService){
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        apiService.doGetTimeZone(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.toString()),
                obj)
                .enqueue(new Callback<TimeZone>() {
                    @Override
                    public void onResponse(Call<TimeZone> call, Response<TimeZone> response) {
                        validateReturnCodeTimeZone(context, response);
                    }

                    @Override
                    public void onFailure(Call<TimeZone> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiUpdateDeployAppointment(final Context context, final APIService apiService, final ReqUpdateAppointment obj){
        apiService.doUpdateAppointment(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjUpdateAppointment())
                .enqueue(new Callback<ResUpdateAppointment>() {
                    @Override
                    public void onResponse(Call<ResUpdateAppointment> call, Response<ResUpdateAppointment> response) {
                        validateReturnCodeUpdateAppointment(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResUpdateAppointment> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetAbility(final Context context, APIService apiService, ReqGetAbility obj){
        apiService.doGetAbility(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetAbility())
                .enqueue(new Callback<ResGetAbility>() {
                    @Override
                    public void onResponse(Call<ResGetAbility> call, Response<ResGetAbility> response) {
                        validateReturnCodeGetAbility(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResGetAbility> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetSubTeam(final Context context, APIService apiService, ReqGetSubTeam obj){
        apiService.doGetSubTeamIdOfContract(
            Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
            Utils.CONTENT_TYPE,
            SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
            Utils.checkSum(obj.objToString()),
            obj.generateObjGetSubTeam())
            .enqueue(new Callback<ResGetSubTeam>() {
                @Override
                public void onResponse(Call<ResGetSubTeam> call, Response<ResGetSubTeam> response) {
                    validateReturnCodeGetSubTeam(context, response);
                }

                @Override
                public void onFailure(Call<ResGetSubTeam> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    private void validateReturnCodeGetSubTeam(Context context, Response<ResGetSubTeam> response){
        if(response!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getSubTeamListSuccess(response.body().getData().get(0));
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodeGetAbility(Context context, Response<ResGetAbility> response){
        if(response!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                convertJsonData((ArrayList<Ability>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodeUpdateAppointment(Context context, Response<ResUpdateAppointment> response){
        if(response!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                checkResultUpdateAppointment(response.body().getData().get(0));
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodeTimeZone(Context context, Response<TimeZone> response) {
        if(response!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getTimeZoneSuccess((ArrayList<String>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void checkResultUpdateAppointment(Appointment obj){
        if(obj.getResultID() == Utils.SUCCESS){
            view.updateDeployAppointment();
        }else{
            view.callApiFailed(obj.getResult());
        }
    }

    private void convertJsonData(ArrayList<Ability> list){
        if (list == null){
            return;
        }
        //create list save date
        ArrayList<String> listDate = new ArrayList<>();
        //create two list ability group by TimeZone
        ArrayList<Integer> listTimeZone1 = new ArrayList<>();
        ArrayList<Integer> listTimeZone2 = new ArrayList<>();
        //scan original list, getDateEffect convert and add to listDate
        for(int i=0; i<list.size(); i += 2){
            //convert date to effect format and add to list
            listDate.add(Utils.convertDateEffect(list.get(i).getDateEffect()));
        }
        //scan original list, getAbility and add to list sort by Timezone
        String timeZone1 = "08:00 - 12:00";
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).getTimezone().equals(timeZone1)){
                listTimeZone1.add(list.get(i).getAbility());
            }else{
                listTimeZone2.add(list.get(i).getAbility());
            }
        }
        //return data to view
        view.getAbilityListSuccess(listDate, listTimeZone1, listTimeZone2);
    }
}
