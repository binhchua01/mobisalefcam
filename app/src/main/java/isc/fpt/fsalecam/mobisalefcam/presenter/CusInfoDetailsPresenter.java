package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetRegistrationDetail;

/**
 * Created by Hau Le on 2018-08-27.
 */
public interface CusInfoDetailsPresenter {
    void doGetRegistrationDetails(Context context, APIService apiService, ReqGetRegistrationDetail obj);
    void doGetRegistrationById(Context context, APIService apiService, ReqGetRegistrationDetail obj);
}
