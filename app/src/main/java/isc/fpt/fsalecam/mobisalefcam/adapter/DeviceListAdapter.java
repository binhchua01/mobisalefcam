package isc.fpt.fsalecam.mobisalefcam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.database.DeviceList;
import isc.fpt.fsalecam.mobisalefcam.utils.CallbackDeviceListSelected;

/**
 * Created by Hau Le on 2018-08-22.
 */
public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.SimpleViewHolder>{
    private Context context;
    private ArrayList<DeviceList> list;
    private CallbackDeviceListSelected callback;

    public DeviceListAdapter(Context context, ArrayList<DeviceList> list, CallbackDeviceListSelected callback) {
        this.context = context;
        this.list = list;
        this.callback = callback;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_choose_device, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressServiceWhenClick(mViewHolder);
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        holder.tvDeviceName.setText(list.get(position).getName());

        if(list.get(position).getIsSelected() == 1){
            holder.loParent.setBackground(context.getResources().getDrawable(R.drawable.background_button_clear));
            holder.tvDeviceName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.imgCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_check));
        }else{
            holder.loParent.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_solid));
            holder.tvDeviceName.setTextColor(context.getResources().getColor(R.color.blue_blur));
            holder.imgCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_check_blur));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_device_name) TextView tvDeviceName;
        @BindView(R.id.img_check) ImageView imgCheck;
        @BindView(R.id.layout_choose_device) RelativeLayout loParent;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void progressServiceWhenClick(SimpleViewHolder mViewHolder){
        if(list.get(mViewHolder.getAdapterPosition()).getIsSelected() == 0){
            //set value flag
            list.get(mViewHolder.getAdapterPosition()).setIsSelected(1);
            //refresh UI
            refreshUI(mViewHolder, list.get(mViewHolder.getAdapterPosition()).getIsSelected());
            //callbackDeviceList data
            callback.callbackDeviceList(list);
        }else{
            list.get(mViewHolder.getAdapterPosition()).setIsSelected(0);
            refreshUI(mViewHolder, list.get(mViewHolder.getAdapterPosition()).getIsSelected());
            callback.callbackDeviceList(list);
        }
    }

    private void refreshUI(SimpleViewHolder viewHolder, int value){
        if(value == 1){
            viewHolder.loParent.setBackground(context.getResources().getDrawable(R.drawable.background_button_clear));
            viewHolder.tvDeviceName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            viewHolder.imgCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_check));
        }else {
            viewHolder.loParent.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_solid));
            viewHolder.tvDeviceName.setTextColor(context.getResources().getColor(R.color.blue_blur));
            viewHolder.imgCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_check_blur));
        }
    }

    public void notifyData(ArrayList<DeviceList> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
