package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCreateContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCreateContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.UploadImageCallBack;
import isc.fpt.fsalecam.mobisalefcam.utils.UploadImages;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.CreateContractView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-28.
 */
public class CreateContractPresenterImpl implements CreateContractPresenter {
    private CreateContractView view;

    public CreateContractPresenterImpl(CreateContractView view) {
        this.view = view;
    }

    @Override
    public void doCreateContract(final Context context, final APIService apiService, final ReqCreateContract obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiCreateContract(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiCreateContract(context, apiService, obj);
        }
    }

    @Override
    public void doUploadImageSignature(final Context context, final APIService apiService,
                                       final String filePath, final String regId,
                                       final SaleOfInfo saleOfInfo) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiUploadImageSignature(context, apiService, filePath, regId, saleOfInfo);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiUploadImageSignature(context, apiService, filePath, regId, saleOfInfo);
        }
    }

    private void callApiUploadImageSignature(final Context context, final APIService apiService,
                                             String filePath, String redId, SaleOfInfo saleOfInfo){
        UploadImages.uploadImages(
            context,
            apiService,
            saleOfInfo.getUploadImageUrl(),
            Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
            filePath,
            redId,
            saleOfInfo.getUploadTypeContract(),
            saleOfInfo.getFullName(),
            saleOfInfo.getUploadLinkId(),
            saleOfInfo.getUploadImageToken(),
            new UploadImageCallBack() {
                @Override
                public void uploadSuccess(String mes, int imageId) {
                    view.hiddenProgressDialog();
                    view.uploadSignatureSuccess(imageId);
                }

                @Override
                public void uploadFailed(String mes) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(mes);
                }
            }
        );
    }

    private void callApiCreateContract(final Context context, APIService apiService, ReqCreateContract obj){
        apiService.doCreateContract(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjCreateContract())
                .enqueue(new Callback<ResCreateContract>() {
                    @Override
                    public void onResponse(Call<ResCreateContract> call, Response<ResCreateContract> response) {
                        validateReturnCode(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResCreateContract> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResCreateContract> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.createContractSuccess(response.body().getData().get(0));
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }
}
