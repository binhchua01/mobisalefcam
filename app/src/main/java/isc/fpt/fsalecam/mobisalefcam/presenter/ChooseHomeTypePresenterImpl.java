package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import io.realm.Case;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.HomeType;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseHomeTypeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-27.
 */
public class ChooseHomeTypePresenterImpl implements ChooseHomeTypePresenter {
    private ChooseHomeTypeView view;

    public ChooseHomeTypePresenterImpl(ChooseHomeTypeView view) {
        this.view = view;
    }

    @Override
    public void getHomeTypeList(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(HomeType.class).findAll().size() == 0){
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetHomeTypeList(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetHomeTypeList(context, apiService, realm);
            }
        }else{
            view.getHomeTypeListSuccess();
        }

    }

    @Override
    public void refreshHomeTypeList(final Context context, final APIService apiService, final Realm realm) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetHomeTypeList(context, apiService, realm);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetHomeTypeList(context, apiService, realm);
        }
    }

    @Override
    public void searchHomeTypeList(Realm realm, String keyQuery) {
        ArrayList<HomeType> list = (ArrayList<HomeType>) realm.copyFromRealm(realm.where(HomeType.class)
                .like(Utils.HOME_TYPE_NAME,  "*" + keyQuery + "*", Case.SENSITIVE)
                .findAll());
        view.search(list);
    }

    private void callApiGetHomeTypeList(final Context context, final APIService apiService, final Realm realm){
        //generate objReport null to send SV
        final LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        //call api
        //generate token success
        apiService.doGetHomeTypeList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.toString()),
                obj).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCode(context, response, realm);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> obj, Realm realm){
        if(obj.body()!= null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                //save list HomeType to DB local
                saveListHomeTypeLocal((ArrayList<ItemOfListRes>) obj.body().getData(), realm);
                //save Authorization header
                Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(obj.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void saveListHomeTypeLocal(final ArrayList<ItemOfListRes> list, Realm realm) {
        realm.beginTransaction();
        realm.where(HomeType.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size() != 0){
            //add
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                HomeType homeType = realm.createObject(HomeType.class, list.get(i).getId());
                homeType.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            view.getHomeTypeListSuccess();
        }else{
            view.getHomeTypeListSuccess();
        }
    }
}
