package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;


/**
 * Created by Hau Le on 2018-08-28.
 */
public interface ContractModelView {
    void getHtmlModelContractSuccess(String htmlContent);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showDialogProgress();
    void hiddenDialogProgress();
}
