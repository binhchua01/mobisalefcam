package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import android.widget.RelativeLayout;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-08-16.
 */
public interface PackageServicePresenter {
    //contract id use for customer had create contract, but add more package
    //if haven't contract -> set ""
    void doGetPackageServiceList(Context context, APIService apiService, Realm realm, String contractId);
    void doRefreshPackageService(Context context, APIService apiService, Realm realm, String contractId);
    void search(Realm realm, String keyQuery);
}
