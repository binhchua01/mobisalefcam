package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import io.realm.Case;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetDistrictList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.District;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseDistrictView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-01.
 */
public class ChooseDistrictPresenterImpl implements ChooseDistrictPresenter {
    private ChooseDistrictView chooseDistrictView;

    public ChooseDistrictPresenterImpl(ChooseDistrictView chooseDistrictView) {
        this.chooseDistrictView = chooseDistrictView;
    }

    @Override
    public void getDistrictList(final Context context, final APIService apiService,
                                final String cityId, final Realm realm) {
        //check DB local is null -> call api
        if(realm.where(District.class).findAll().size() == 0 ||
                realm.where(District.class)
                .equalTo(Utils.CITY_ID, Integer.parseInt(cityId))
                .findAll().size() == 0){
            //show dialog
            chooseDistrictView.showLoadingProgress();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetDistrictList(context, apiService, cityId, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        chooseDistrictView.callApiFailed(context.getResources().getString(R.string.check_internet));
                        chooseDistrictView.hiddenLoadingProgress();
                    }
                });
            }else{
                callApiGetDistrictList(context, apiService, cityId, realm);
            }
        }else{
            chooseDistrictView.getDistrictListSuccess();
        }
    }

    @Override
    public void refreshDistrictList(final Context context, final APIService apiService,
                                    final String cityId, final Realm realm) {
        //show dialog
        chooseDistrictView.showLoadingProgress();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetDistrictList(context, apiService, cityId, realm);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    chooseDistrictView.hiddenLoadingProgress();
                    chooseDistrictView.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetDistrictList(context, apiService, cityId, realm);
        }
    }

    @Override
    public void searchDistrictList(Realm realm, String keyQuery, String cityId) {
        ArrayList<District> list = (ArrayList<District>) realm.copyFromRealm(realm.where(District.class)
                .equalTo(Utils.CITY_ID, Integer.parseInt(cityId))
                .like(Utils.DISTRICT_NAME,  "*" + keyQuery + "*", Case.SENSITIVE)
                .findAll());
        chooseDistrictView.search(list);
    }

    private void callApiGetDistrictList(final Context context, APIService apiService,
                                        final String locationId, final Realm realm){
        //put data to object
        final ReqGetDistrictList location = new ReqGetDistrictList(locationId);
        //call api get district list...
        apiService.doGetDistrictList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(location.objToString()),
                location.generateObjGetDistrictList())
                .enqueue(new Callback<ResParentStructure>() {
                    @Override
                    public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                        //success
                        validateReturnCode(context, response, realm, Integer.parseInt(locationId));
                    }

                    @Override
                    public void onFailure(Call<ResParentStructure> call, Throwable t) {
                        chooseDistrictView.hiddenLoadingProgress();
                        chooseDistrictView.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> obj, Realm realm, int cityID){
        if(obj.body()!=null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                //success
                //save district list to DB local
                saveDistrictListToLocal((ArrayList<ItemOfListRes>) obj.body().getData(), realm, cityID);
                //save Authorization header
                Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    chooseDistrictView.authorizedInvalid(context.getResources().getString(R.string.mes_login_again));
                }else{
                    chooseDistrictView.callApiFailed(obj.body().getMessage());
                }
            }
        }else{
            chooseDistrictView.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        chooseDistrictView.hiddenLoadingProgress();
    }

    private void saveDistrictListToLocal(final ArrayList<ItemOfListRes> list, Realm realm, final int cityID) {
        realm.beginTransaction();
        realm.where(District.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                District district = realm.createObject(District.class, list.get(i).getId());
                district.setName(list.get(i).getName());
                district.setCityID(cityID);
                realm.commitTransaction();
            }
            chooseDistrictView.getDistrictListSuccess();
        }else{
            chooseDistrictView.getDistrictListSuccess();
        }
    }
}
