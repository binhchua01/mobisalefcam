package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqContractDetails;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetUrlDownloadPdf;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResContractDetails;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetUrlDownloadPdf;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ContractDetailView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-30.
 */
public class ContractDetailsPresenterImpl implements ContractDetailPresenter {
    private ContractDetailView view;

    public ContractDetailsPresenterImpl(ContractDetailView view) {
        this.view = view;
    }

    @Override
    public void doGetContractDetails(final Context context, final APIService apiService, final ReqContractDetails obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetContractDetails(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetContractDetails(context, apiService, obj);
        }
    }

    @Override
    public void doGetUrlContractPdf(final Context context, final APIService apiService, final int objId) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetUrlContractPdf(context, apiService, objId);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetUrlContractPdf(context, apiService, objId);
        }
    }

    @Override
    public void doDownloadFilePdf(final Context context, final APIService apiService, final String url, final int objId) {
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiDownloadPdf(context, apiService, url, objId);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiDownloadPdf(context, apiService, url, objId);
        }
    }

    private void callApiDownloadPdf(final Context context, APIService apiService, String url, final int objId){
        apiService.downloadContractPdf(
                url,
            Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, "")
            ).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.body() != null) {
                        boolean writtenToDisk = Utils.writeFilePdfToDisk(response.body(), objId);
                        if (writtenToDisk) {
                            view.doDownloadFilePdf(context.getResources().getString(R.string.mes_download_complete));
                        }else{
                            view.doDownloadFilePdf(context.getResources().getString(R.string.mes_cant_write_file_to_disk));
                        }
                        view.hiddenProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    private void callApiGetUrlContractPdf(final Context context, APIService apiService, int objId){
        ReqGetUrlDownloadPdf obj = new ReqGetUrlDownloadPdf();
        obj.setObjId(objId);
        apiService.doGetUrlDownloadContractPdf(
            Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
            Utils.CONTENT_TYPE,
            SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
            Utils.checkSum(obj.objToString()),
            obj.generateObjGetUrlDownload())
            .enqueue(new Callback<ResGetUrlDownloadPdf>() {
                @Override
                public void onResponse(Call<ResGetUrlDownloadPdf> call, Response<ResGetUrlDownloadPdf> response) {
                    validateReturnCodeGetUrlPdf(context, response);
                }

                @Override
                public void onFailure(Call<ResGetUrlDownloadPdf> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    private void callApiGetContractDetails(final Context context, APIService apiService, ReqContractDetails obj){
        apiService.deGetContractDetails(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjContractDetails())
                .enqueue(new Callback<ResContractDetails>() {
            @Override
            public void onResponse(Call<ResContractDetails> call, Response<ResContractDetails> response) {
                validateReturnCode(context, response);
            }

            @Override
            public void onFailure(Call<ResContractDetails> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCode(Context context, Response<ResContractDetails> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getContractDetailSuccess(response.body().getData().get(0));
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodeGetUrlPdf(Context context, Response<ResGetUrlDownloadPdf> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getUrlDownLoadContractPdf(response.body().getData().get(0).getPdfUrl());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
    }


}
