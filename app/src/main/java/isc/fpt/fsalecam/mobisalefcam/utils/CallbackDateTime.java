package isc.fpt.fsalecam.mobisalefcam.utils;

/**
 * Created by Hau Le on 2018-08-16.
 */
public interface CallbackDateTime {
    void dateTime(String date);
}
