package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.database.PackageService;

/**
 * Created by Hau Le on 2018-08-16.
 */
public interface PackageServiceView {
    void getPackageServiceListSuccess(ArrayList<PackageService> list);
    void searchServicePackage(ArrayList<PackageService> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showDialogProgress();
    void hiddenDialogProgress();
}
