package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationImage;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdateRegistrationImage;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateRegistrationImage;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.UploadImageCallBack;
import isc.fpt.fsalecam.mobisalefcam.utils.UploadImages;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ImageRegistrationView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-12.
 */
public class ImageRegistrationPresenterImpl implements ImageRegistrationPresenter {
    private ImageRegistrationView view;

    public ImageRegistrationPresenterImpl(ImageRegistrationView view) {
        this.view = view;
    }

    @Override
    public void doUpdateRegistration(final Context context, final APIService apiService,
                                     final ReqUpdateRegistrationImage obj) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiUpdateRegistration(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiUpdateRegistration(context, apiService, obj);
        }
    }

    @Override
    public void doUploadImageRegistration(final Context context, final APIService apiService,
                                          final ArrayList<String> list, final String regId, final SaleOfInfo saleInfo) {
        //show dialog progress
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiUploadImage(context, apiService, list, regId, saleInfo);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiUploadImage(context, apiService, list, regId, saleInfo);
        }
    }

    private void callApiUploadImage(final Context context, APIService apiService,
                                    final ArrayList<String> list, String regId, SaleOfInfo saleInfo){
        /*
         * this param use for check upload image success
         * checkDone == 5
         * it's mean function uploadSuccess called 5th
         *
         * */
        final int[] checkDone = {0};
        final int[] missItem = {0};
        final String[] listImageId = {""};
        for(int i=0; i<list.size(); i++){
            UploadImages.uploadImages(
                context,
                apiService,
                saleInfo.getUploadImageUrl(),
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                list.get(i),
                String.valueOf(saleInfo.getUploadSource()),
                saleInfo.getUploadTypeContract(),
                saleInfo.getFullName(),
                regId,
                saleInfo.getUploadImageToken(),
                new UploadImageCallBack() {
                    @Override
                    public void uploadSuccess(String mes, int imageId) {
                        checkDone[0]++;
                        listImageId[0] += String.valueOf(imageId) + ",";
                        //if upload success list image -> show message success
                        if(checkDone[0] == list.size()){
                            //format string
                            StringBuilder sb = new StringBuilder(listImageId[0]);
                            sb.deleteCharAt(listImageId[0].length()-1);
                            //return to view when upload success
                            view.uploadImageRegistrationSuccess(sb.toString());
                            //hidden dialog when upload done
                            view.hiddenProgressLoading();
                        }
                    }

                    @Override
                    public void uploadFailed(String mes) {
                        //count upload failed item
                        missItem[0]++;
                        view.hiddenProgressLoading();
                        //show only one
                        if (missItem[0]==1){
                            view.uploadImageFailed(context.getResources().getString(R.string.upload_image_missed_file));
                        }
                    }
                }
            );
        }
    }

    private void callApiUpdateRegistration(final Context context, APIService apiService,
                                           ReqUpdateRegistrationImage obj){
        apiService.doUpdateRegistrationImage(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjUpdateRegistrationImage())
                .enqueue(new Callback<ResUpdateRegistrationImage>() {
                    @Override
                    public void onResponse(Call<ResUpdateRegistrationImage> call,
                                           Response<ResUpdateRegistrationImage> response) {
                        validateReturnCodeUpdateRegistration(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResUpdateRegistrationImage> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCodeUpdateRegistration(Context context, Response<ResUpdateRegistrationImage> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.updateRegistrationSuccess((ArrayList<UpdateRegistrationImage>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }
}
