package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.PickImageAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationImage;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateRegistrationImage;
import isc.fpt.fsalecam.mobisalefcam.presenter.ImageRegistrationPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ImageRegistrationPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.ItemOffsetDecoration;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ImageRegistrationView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.CusInfoDetailsActivity.registrationDetail;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.getImageUri;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.getPathFromUri;

public class ImageRegistrationActivity extends BaseActivity
        implements View.OnClickListener, ImageRegistrationView{

    @BindView(R.id.ic_icon_nav_back_image_list) RelativeLayout loBack;
    @BindView(R.id.image_choose_image) ImageView imgChooseImage;
    @BindView(R.id.list_image) RecyclerView recyclerView;
    @BindView(R.id.layout_pick_image) LinearLayout loPickImage;
    @BindView(R.id.ic_icon_choose_image) RelativeLayout loPickMoreImage;
    @BindView(R.id.btn_upload_image) Button btnUpdate;

    private static final int REQUEST_CODE = 999;
    private static final int MY_PERMISSIONS_REQUEST = 100;
    private ArrayList<String> listImage;
    private PickImageAdapter adapter;
    private APIService apiService;
    private ImageRegistrationPresenter presenter;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initParamToCallApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_image_registration;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_icon_nav_back_image_list:
                onBackPressed();
                break;
            case R.id.image_choose_image:
                askForPermission();
                break;
            case R.id.ic_icon_choose_image:
                //pick more image if size < 5
                askForPermission();
                break;
            case R.id.btn_upload_image:
                /*
                * when click update registration info
                * first, upload image(waiting upload done -> do update info)
                * second, do update info
                * */
                if(listImage.size() == 0){
                    DialogUtils.showMessageDialogVerButton(this,
                            getResources().getString(R.string.mes_no_image_picked)).show();
                    return;
                }
                presenter.doUploadImageRegistration(this, apiService,
                        listImage, String.valueOf(registrationDetail.getRegId()), Utils.saleOfInfo);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS_REQUEST){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                selectAction();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            //select maximum 5 file
            if(listImage.size() >= 5){
                return;
            }
            //take picture from gallery
            if(data.getClipData() != null) {
                //select multiple item
                int count = data.getClipData().getItemCount();
                //check item pick
                if(count >= 6){ return; }
                for(int i = 0; i < count; i++){
                    Uri imageUri = data.getClipData().getItemAt(i).getUri();
                    //add list uri selected
                    listImage.add(getPathFromUri(this, imageUri));
                }
            } else if(data.getData() != null) {
                //selected one item
                Uri uri = data.getData();
                listImage.add(getPathFromUri(this, uri));
            }

            //take picture from camera
            if(data.getExtras()!=null) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                assert imageBitmap != null;
                String path = getImageUri(this, imageBitmap);
                listImage.add(path);
            }
            //notify adapter
            adapter.notifyData(listImage);
        }
        //display list image
        if(listImage.size()!=0){
            //hidden button pick image original
            loPickImage.setVisibility(View.GONE);
            //show button pick more image
            loPickMoreImage.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void initView() {
        listImage = new ArrayList<>();
        adapter = new PickImageAdapter(this, listImage, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) { }
        });
        recyclerView.setAdapter(adapter);
        //set padding for one item in list
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.item_offset);
        recyclerView.addItemDecoration(itemDecoration);
    }

    private void setEvent() {
        loBack.setOnClickListener(this);
        imgChooseImage.setOnClickListener(this);
        loPickMoreImage.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
    }

    private void initParamToCallApi() {
        apiService = ApiUtils.getApiService();
        presenter = new ImageRegistrationPresenterImpl(this);
    }

    //get permission from client
    private void askForPermission() {
        boolean hasPermission = ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

        if(hasPermission){
            selectAction();
        }else{
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST);
        }
    }

    //select action take a photo or pic image in storage
    private void selectAction(){
        Intent pick = new Intent(Intent.ACTION_GET_CONTENT);
        pick .putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        pick.setType("image/*");
        Intent photo = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent chooser = Intent.createChooser(pick, "Choose: ");
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS,new Intent[]{photo});
        startActivityForResult(chooser, REQUEST_CODE);
    }

    @Override
    public void updateRegistrationSuccess(ArrayList<UpdateRegistrationImage> list) {
        //upload success, back to parent
        finish();
    }

    @Override
    public void uploadImageRegistrationSuccess(String listId) {
        //this function called when upload image success
        //and after upload image success, all api update registration
        ReqUpdateRegistrationImage obj = new ReqUpdateRegistrationImage();
        obj.setRegId(registrationDetail.getRegId());
        obj.setRegCode(registrationDetail.getRegCode());
        obj.setImageInfo(listId);
        presenter.doUpdateRegistration(this, apiService, obj);
    }

    @Override
    public void uploadImageFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressLoading() {
        hiddenProgressLoading();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressLoading() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }
}
