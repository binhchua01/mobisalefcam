package isc.fpt.fsalecam.mobisalefcam.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Hau Le on 2018-08-17.
 */
public class PromotionCode extends RealmObject {
    @PrimaryKey
    private int id;
    private int promotionId;
    private String promotionName;
    private String description;
    private int monthOfPrepaid;
    private int realPrepaid;
    private int prepaidPromotion;
    private int cityID;
    private int packageServiceID;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public int getPackageServiceID() {
        return packageServiceID;
    }

    public void setPackageServiceID(int packageServiceID) {
        this.packageServiceID = packageServiceID;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public int getMonthOfPrepaid() {
        return monthOfPrepaid;
    }

    public void setMonthOfPrepaid(int monthOfPrepaid) {
        this.monthOfPrepaid = monthOfPrepaid;
    }

    public int getRealPrepaid() {
        return realPrepaid;
    }

    public void setRealPrepaid(int realPrepaid) {
        this.realPrepaid = realPrepaid;
    }

    public int getPrepaidPromotion() {
        return prepaidPromotion;
    }

    public void setPrepaidPromotion(int prepaidPromotion) {
        this.prepaidPromotion = prepaidPromotion;
    }
}
