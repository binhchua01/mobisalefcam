package isc.fpt.fsalecam.mobisalefcam.utils;

/**
 * Created by Hau Le on 2018-08-14.
 */
public interface ContinueRegisterService {
    void continueRegister();
}
