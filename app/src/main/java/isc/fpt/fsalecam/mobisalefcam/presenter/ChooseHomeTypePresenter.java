package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-09-27.
 */
public interface ChooseHomeTypePresenter {
    void getHomeTypeList(Context context, APIService apiService, Realm realm);
    void refreshHomeTypeList(Context context, APIService apiService, Realm realm);
    void searchHomeTypeList(Realm realm, String keyQuery);
}
