package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.BuildingAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChooseBuildingPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChooseBuildingPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.Building;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseBuildingView;

public class ChooseBuildingActivity extends BaseActivity
        implements ChooseBuildingView, View.OnClickListener{
    @BindView(R.id.recycler_view_building) RecyclerView recyclerView;
    @BindView(R.id.layout_choose_building) LinearLayout loParent;
    @BindView(R.id.img_nav_back_choose_building) RelativeLayout loBack;
    @BindView(R.id.img_building_refresh) RelativeLayout loRefresh;
    @BindView(R.id.edt_search_building) EditText edtBuildingName;

    private Realm realm;
    private Dialog dialog;
    private BuildingAdapter adapter;
    private ArrayList<Building> list;
    private ChooseBuildingPresenter presenter;
    private APIService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        //init DB
        realm = Realm.getDefaultInstance();
        initView();
        setEvent();
    }

    private void setEvent() {
        loBack.setOnClickListener(this);
        loRefresh.setOnClickListener(this);
        edtBuildingName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(getIntent().getStringExtra(Utils.BUILDING_ID) != null){
                    presenter.searchBuildingList(realm, String.valueOf(editable.toString()), getIntent().getStringExtra(Utils.BUILDING_ID));
                }
            }
        });
    }

    private void initView() {
        list = new ArrayList<>();
        adapter = new BuildingAdapter(this, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                Intent intent = new Intent();
                intent.putExtra(Utils.BUILDING, list.get(position).getId() + ":" + list.get(position).getName());
                setResult(Utils.REQUEST_CODE_BUILDING, intent);
                finish();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        callApi();
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new ChooseBuildingPresenterImpl(this);
        if(getIntent().getStringExtra(Utils.BUILDING_ID) != null){
            presenter.getBuildingList(this, apiService, getIntent().getStringExtra(Utils.BUILDING_ID), realm);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_choose_building;
    }

    @Override
    public void getBuildingListSuccess() {
        if(getIntent().getStringExtra(Utils.BUILDING_ID) != null){
            list = (ArrayList<Building>)realm.copyFromRealm(realm.where(Building.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(getIntent().getStringExtra(Utils.BUILDING_ID)))
                    .findAll());
            adapter.notifyData(list);
        }
    }

    @Override
    public void search(ArrayList<Building> list) {
        this.list = list;
        adapter.notifyData(list);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_nav_back_choose_building:
                onBackPressed();
                break;
            case R.id.img_building_refresh:
                if(getIntent().getStringExtra(Utils.BUILDING_ID) != null){
                    presenter.doRefreshBuildingList(this, apiService, getIntent().getStringExtra(Utils.BUILDING_ID), realm);
                }
                break;
        }
    }
}
