package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-08-02.
 */
public interface ChooseWardPresenter {
    void doGetWardList(Context context, APIService apiService,
                       String cityId, String districtId, Realm realm);
    void refreshWardList(Context context, APIService apiService,
                       String cityId, String districtId, Realm realm);
    void searchWardList(Realm realm, String keyQuery, String cityId, String districtId);
}
