package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-09-11.
 */
public class BookportAuto {
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ODCCableType")
    @Expose
    private String oDCCableType;
    @SerializedName("ContractTemp")
    @Expose
    private String contractTemp;
    @SerializedName("Port")
    @Expose
    private Integer port;
    @SerializedName("LengthSurvey_OutDoor")
    @Expose
    private Integer outDoor;
    @SerializedName("LengthSurvey_InDoor")
    @Expose
    private Integer inDoor;

    public Integer getOutDoor() {
        return outDoor;
    }

    public void setOutDoor(Integer outDoor) {
        this.outDoor = outDoor;
    }

    public Integer getInDoor() {
        return inDoor;
    }

    public void setInDoor(Integer inDoor) {
        this.inDoor = inDoor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getODCCableType() {
        return oDCCableType;
    }

    public void setODCCableType(String oDCCableType) {
        this.oDCCableType = oDCCableType;
    }

    public String getContractTemp() {
        return contractTemp;
    }

    public void setContractTemp(String contractTemp) {
        this.contractTemp = contractTemp;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
