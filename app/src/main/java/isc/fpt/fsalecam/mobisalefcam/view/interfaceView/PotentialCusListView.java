package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.PotentialCus;
import isc.fpt.fsalecam.mobisalefcam.database.PotentialCusTypeSearch;

/**
 * Created by Hau Le on 2018-09-14.
 */
public interface PotentialCusListView {
    void getPotentialCusListSuccess(ArrayList<PotentialCus> list);
    void getPotentialCusTypeList(ArrayList<PotentialCusTypeSearch> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
