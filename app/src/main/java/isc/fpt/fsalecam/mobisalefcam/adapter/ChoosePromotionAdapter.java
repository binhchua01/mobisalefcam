package isc.fpt.fsalecam.mobisalefcam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.database.PromotionCode;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;

/**
 * Created by Hau Le on 2018-08-17.
 */
public class ChoosePromotionAdapter extends RecyclerView.Adapter<ChoosePromotionAdapter.SimpleViewHolder>{
    private Context context;
    private ArrayList<PromotionCode> list;
    private OnItemClickListener listener;

    public ChoosePromotionAdapter(Context context, ArrayList<PromotionCode> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_choose_promotion_code, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickListener(view, mViewHolder.getAdapterPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, int position) {
        holder.tvPromotionName.setText(list.get(position).getPromotionName());
        holder.tvPromotionHidden.setText(list.get(position).getDescription());
        holder.tvShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.tvPromotionHidden.getVisibility() == View.VISIBLE){
                    holder.tvPromotionHidden.setVisibility(View.GONE);
                    holder.tvShowMore.setText(context.getResources().getString(R.string.show_more));
                }else{
                    holder.tvPromotionHidden.setVisibility(View.VISIBLE);
                    holder.tvShowMore.setText(context.getResources().getString(R.string.show_less));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_choose_promotion_code) TextView tvPromotionName;
        @BindView(R.id.tv_promotion_name_hidden) TextView tvPromotionHidden;
        @BindView(R.id.tv_show_more_promotion) TextView tvShowMore;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void notifyData(ArrayList<PromotionCode> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
