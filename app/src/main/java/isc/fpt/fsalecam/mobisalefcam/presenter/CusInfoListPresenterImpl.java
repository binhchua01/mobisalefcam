package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqRegistrationAll;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Registration;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetRegistrationAll;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.RegistrationSearchType;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.CusInfoListView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-30.
 */
public class CusInfoListPresenterImpl implements CusInfoListPresenter {
    private CusInfoListView view;

    public CusInfoListPresenterImpl(CusInfoListView view) {
        this.view = view;
    }

    @Override
    public void doGetRegistrationSearchType(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(RegistrationSearchType.class).findAll().size() == 0){
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetRegistrationSearchType(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetRegistrationSearchType(context, apiService, realm);
            }
        }else{
            view.getRegistrationSearchTypeListSuccess((ArrayList<RegistrationSearchType>)
                    realm.copyFromRealm(realm.where(RegistrationSearchType.class).findAll()));
        }
    }

    @Override
    public void doSearchRegistration(final Context context, final APIService apiService, final ReqRegistrationAll obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiSearchRegistration(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiSearchRegistration(context, apiService, obj);
        }
    }

    private void callApiSearchRegistration(final Context context, APIService apiService, ReqRegistrationAll obj){
        apiService.doGetRegistrationAll(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetRegistrationAll())
                .enqueue(new Callback<ResGetRegistrationAll>() {
                    @Override
                    public void onResponse(Call<ResGetRegistrationAll> call, Response<ResGetRegistrationAll> response) {
                        validateSearchRegistrationCode(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResGetRegistrationAll> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetRegistrationSearchType(final Context context, APIService apiService, final Realm realm){
        final LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        apiService.doGetRegistrationTypeList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.toString()),
                obj).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCode(context, response, realm);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> response, Realm realm){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                saveRegistrationSearchType((ArrayList<ItemOfListRes>) response.body().getData(), realm);
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void saveRegistrationSearchType(ArrayList<ItemOfListRes> list, Realm realm) {
        realm.beginTransaction();
        realm.where(RegistrationSearchType.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                RegistrationSearchType data = realm.createObject(RegistrationSearchType.class, list.get(i).getId());
                data.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            view.getRegistrationSearchTypeListSuccess((ArrayList<RegistrationSearchType>)
                    realm.copyFromRealm(realm.where(RegistrationSearchType.class).findAll()));
        }else{
            view.getRegistrationSearchTypeListSuccess((ArrayList<RegistrationSearchType>)
                    realm.copyFromRealm(realm.where(RegistrationSearchType.class).findAll()));
        }
    }

    private void validateSearchRegistrationCode(Context context, Response<ResGetRegistrationAll> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getSearchRegistrationSuccess((ArrayList<Registration>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }
}
