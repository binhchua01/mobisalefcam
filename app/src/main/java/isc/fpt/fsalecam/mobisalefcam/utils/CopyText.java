package isc.fpt.fsalecam.mobisalefcam.utils;

import android.content.ClipboardManager;
import android.content.Context;

/**
 * Created by Hau Le on 2018-07-06.
 */
public class CopyText {
    public static void copyText(Context context, String str){
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if(!str.isEmpty()){
            assert clipboardManager != null;
            clipboardManager.setText(str);
        }
    }
}
