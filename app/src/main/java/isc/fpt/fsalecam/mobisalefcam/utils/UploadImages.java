package isc.fpt.fsalecam.mobisalefcam.utils;

import android.content.Context;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;

import java.io.File;

import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.model.uploadImage.UploadImageRes;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hau Le on 2018-07-25.
 */
public class UploadImages {
    private static String SUCCESS = "Success";

    //this function use for upload image
    public static void uploadImages(final Context context, APIService apiService,
                                    String urlUploadImage, String authorization,
                                    String imagePath, final String src, String ImgType,
                                    String saleName, String linkId, String uploadImageToken,
                                    final UploadImageCallBack callBack){
        File file = new File(imagePath);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), file);
        MultipartBody.Part imageFile = MultipartBody.Part.createFormData("images", file.getName(), reqFile);
        RequestBody source = RequestBody.create(MediaType.parse("text/plain"), src);
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), ImgType);
        RequestBody sale = RequestBody.create(MediaType.parse("text/plan"), saleName);
        //param below is static
        RequestBody link = RequestBody.create(MediaType.parse("text/plain"), linkId);
        RequestBody authToken = RequestBody.create(MediaType.parse("text/plain"), uploadImageToken);

        apiService.doUploadImage(
                urlUploadImage,
                authorization,
                imageFile,
                link,
                source,
                type,
                sale,
                authToken
        ).enqueue(new Callback<UploadImageRes>() {
                @Override
                public void onResponse(Call<UploadImageRes> call, Response<UploadImageRes> response) {
                    if(response.body().getStatus() == null){
                        return;
                    }
                    try{
                        if(response.body().getStatus().equals(SUCCESS)){
                            callBack.uploadSuccess(response.body().getStatus(), response.body().getData().get(0).getId());
                        }else{
                            callBack.uploadFailed(response.body().getStatus());
                        }
                    }catch (IndexOutOfBoundsException ex){
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<UploadImageRes> call, Throwable t) {
                    callBack.uploadFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    //make string image
    public static String doDownloadImage(SaleOfInfo saleOfInfo, String imageId){
        //make string format to get image
        String baseUrl = saleOfInfo.getDownloadImageUrl();
        String id = "?ID=" + imageId;
        String token = "&Token=" + saleOfInfo.getUploadImageToken();
        return baseUrl + id + token;
    }

    //this class add header to url load image
    public static GlideUrl getUrlWithHeaders(Context context, String url){
        return new GlideUrl(url, new LazyHeaders.Builder()
                .addHeader("Authorization", Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""))
                .build());
    }
}
