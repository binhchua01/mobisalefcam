package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.BookPortManual;

/**
 * Created by Hau Le on 2018-08-14.
 */
public interface EnterLengthCableView {
    void bookportManualSuccess(BookPortManual obj);
    void callApiFailed(String mes);
    void createPotentialCusOrBookport(String mes);
    void indoorInvalid(String message);
    void outdoorInvalid(String message);
    void expiredToken(String str);
    void showProgressDialog();
    void hiddenProgressDialog();
}
