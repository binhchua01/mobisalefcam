package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGenerateAddress;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqPotentialCus;

/**
 * Created by Hau Le on 2018-09-14.
 */
public interface NewPotentialCusPresenter {
    void doCreatePotentialCus(Context context, APIService apiService, ReqPotentialCus obj);
    void doGenerateAddress(Context context, APIService apiService, ReqGenerateAddress obj);
}
