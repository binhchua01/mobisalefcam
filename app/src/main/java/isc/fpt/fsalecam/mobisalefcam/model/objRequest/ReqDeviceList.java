package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-22.
 */
public class ReqDeviceList {
    private String locationId, monthOfPrepaid, type;
    private int localType;

    public int getLocalType() {
        return localType;
    }

    public void setLocalType(int localType) {
        this.localType = localType;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getMonthOfPrepaid() {
        return monthOfPrepaid;
    }

    public void setMonthOfPrepaid(String monthOfPrepaid) {
        this.monthOfPrepaid = monthOfPrepaid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LinkedHashMap<String, String> generateObjGetDeviceList(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("LocationId", getLocationId());
        obj.put("MonthOfPrepaid", getMonthOfPrepaid());
        obj.put("Type", getType());
        obj.put("LocalType", String.valueOf(getLocalType()));
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("LocationId", getLocationId());
            obj.put("MonthOfPrepaid", getMonthOfPrepaid());
            obj.put("Type", getType());
            obj.put("LocalType", String.valueOf(getLocalType()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
