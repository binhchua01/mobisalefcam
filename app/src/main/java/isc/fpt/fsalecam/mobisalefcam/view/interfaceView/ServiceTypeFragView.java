package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.database.ConnectionFee;
import isc.fpt.fsalecam.mobisalefcam.database.CusInfoPaymentMethod;

/**
 * Created by Hau Le on 2018-08-16.
 */
public interface ServiceTypeFragView {
    void getServiceTypeListSuccess(ArrayList<ItemOfListRes> list);
    void getPaymentMethodSuccess(ArrayList<CusInfoPaymentMethod> list);
    void getConnectionFeeList(ArrayList<ConnectionFee> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
