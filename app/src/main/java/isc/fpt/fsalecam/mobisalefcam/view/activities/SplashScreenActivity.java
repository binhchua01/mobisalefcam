package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import java.io.File;
import java.util.Objects;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.BuildConfig;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCheckImei;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCheckVersion;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CheckImei;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CurrentVersion;
import isc.fpt.fsalecam.mobisalefcam.presenter.SplashScreenPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.SplashScreenPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.ContinueRegisterService;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.SplashScreenView;

public class SplashScreenActivity extends BaseActivity implements SplashScreenView, View.OnClickListener{
    @BindView(R.id.btn_check_app_version) Button btnUpdateVersion;
    @BindView(R.id.progress_bar) ProgressBar progressBar;

    private APIService apiService;
    private SplashScreenPresenter presenter;
    private static final int MY_PERMISSIONS_REQUEST = 100;
    public static CheckImei objCheckImei;
    private CurrentVersion currentVersion;
    private String imei = "";
    private Dialog dialog;
    private Dialog dialogShowFailed;
    public static  int imeiIsActive = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        askForPermission();
        setEvent();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_splash_screen;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                presenter.readImeiDevice(this);
            } else {
                //permission not granted
                callApiCheckImei("");
            }
        }
    }

    @Override
    public void getDeviceIMEI(String imei) {
        this. imei = imei;
        //call api check version
        ReqCheckVersion obj = new ReqCheckVersion();
        obj.setCurrentVersion(BuildConfig.VERSION_NAME);
        obj.setDeviceImei(imei);
        //1- Android, 2 - iOS
        obj.setPlatform("1");
        presenter.doCheckCurrentVersion(this, apiService, obj);
    }

    @Override
    public void checkCurrentVersionSuccess(CurrentVersion obj) {
        Log.d("TAG", "checkCurrentVersionSuccess: " + obj.getLink());
        currentVersion = obj;
        //enable button update version
        if(obj.getIsNew() == 1){
            btnUpdateVersion.setVisibility(View.VISIBLE);
            //hidden progress bar
            progressBar.setVisibility(View.GONE);
            return;
        }
        //call api check imei if granted permission
        callApiCheckImei(imei);
    }

    @Override
    public void checkCurrentVersionFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void checkImeiIsActiveSuccess(CheckImei checkImei) {
        imeiIsActive  = checkImei.getIsActive();
        objCheckImei = checkImei;
        nextToMain();
    }

    @Override
    public void checkImeiIsActiveFailed(String str) {
        nextToMain();
    }

    @Override
    public void downloadNewVersionSuccess(final String location) {
        installNewApp(location);
    }

    @Override
    public void downloadNewVersionFailed(String mes) {
        dialogShowFailed = DialogUtils.showDialogUpdateRegistrationSuccess(this,
            mes,
            new ContinueRegisterService() {
                @Override
                public void continueRegister() {
                    Handler handler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if(dialogShowFailed!=null && dialogShowFailed.isShowing()){
                                dialogShowFailed.dismiss();
                            }
                            finish();
                        }
                    };
                    handler.postDelayed(runnable, 2000);
                }
            }
        );
        dialogShowFailed.show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_check_app_version:
//                String fileUrl = "https://firebasestorage.googleapis.com/v0/b/mobisale-1531378413852.appspot.com/o/app-release.apk?alt=media&token=c9161a43-0c5c-4d4c-bcbe-2ca2c0b18cff";
                //call api
                presenter.doDownloadNewVersion(this, apiService, currentVersion.getLink());
//                presenter.doDownloadNewVersion(this, apiService, fileUrl);
                //disable this button
                btnUpdateVersion.setEnabled(false);
                break;
            default:
                break;
        }
    }

    private void nextToMain(){
        //Start activity here
        Intent intent;
        if (checkAuthorization()){
            intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        }else{
            intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
        }
        startActivity(intent);
        finish();
    }

    private void callApiCheckImei(String imei){
        ReqCheckImei obj = new ReqCheckImei();
        obj.setImei(imei);
        obj.setModelNumber(Utils.getDeviceName());
        obj.setAndroidVersion(Build.VERSION.RELEASE);
        //call api
        presenter.doCheckImei(this, apiService, obj);
    }

    boolean checkAuthorization(){
        boolean ret = false;
        if(!SharedPref.get(this, SharedPref.Key.FTEL_MOBISALECAM_HEADER,"").equals(""))
            ret = true;
        return  ret;
    }

    private void installNewApp(String location) {
        File file = new File(location, "app.apk");
        String mimeType = "application/vnd.android.package-archive";
        Intent promptInstall = new Intent(Intent.ACTION_VIEW);
        promptInstall.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        promptInstall.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            Uri apkURI = FileProvider.getUriForFile(
                    this,
                    getApplicationContext()
                            .getPackageName() + ".provider", file);
            promptInstall.setDataAndType(apkURI, mimeType);
        }else{
            promptInstall.setDataAndType(Uri.fromFile(file), mimeType);
        }
        startActivity(promptInstall);
    }

    private void initView() {
        apiService = ApiUtils.getApiService();
        presenter = new SplashScreenPresenterImpl(this);
    }

    private void askForPermission(){
        boolean hasPermission = ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if(hasPermission){
            //permission accept
            presenter.readImeiDevice(this);
        }else{
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST);
        }
    }

    private void setEvent() {
        btnUpdateVersion.setOnClickListener(this);
    }
}
