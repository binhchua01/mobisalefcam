package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-17.
 */
public class ReqGetPromotionList {
    private String username;
    private String locationId;
    private String servicePackageId;
    private String contract;

    public ReqGetPromotionList(String username, String locationId, String servicePackageId, String contract) {
        this.username = username;
        this.locationId = locationId;
        this.servicePackageId = servicePackageId;
        this.contract = contract;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getServicePackageId() {
        return servicePackageId;
    }

    public void setServicePackageId(String servicePackageId) {
        this.servicePackageId = servicePackageId;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public LinkedHashMap<String, String> generateObjGetPromotionList(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("LocationId", locationId);
        obj.put("LocalTypeID", servicePackageId);
        obj.put("Contract", contract);
        obj.put("UserName", username);
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("LocationId", getLocationId());
            obj.put("LocalTypeID", getServicePackageId());
            obj.put("Contract", getContract());
            obj.put("UserName", getUsername());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
