package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-27.
 */
public class ReqGetRegistrationDetail implements Serializable{
    private String  regId, regCode;

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public LinkedHashMap<String, String> generateObjGetRegistrationDetails(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("RegId", getRegId());
        obj.put("RegCode", getRegCode());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("RegId", getRegId());
            obj.put("RegCode", getRegCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
