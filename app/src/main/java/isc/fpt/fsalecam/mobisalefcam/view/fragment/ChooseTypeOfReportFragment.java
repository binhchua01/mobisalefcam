package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Objects;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;

/**
 * Created by Hau Le on 2018-09-17.
 */
public class ChooseTypeOfReportFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.layout_choose_report_of_type) LinearLayout layoutWrapper;
    @BindView(R.id.tv_subscriber_report) TextView tvSubscriberReport;
    @BindView(R.id.img_nav_back_choose_report_type) RelativeLayout loBack;

    private View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(getFragmentLayout(), container, false);
        }
        hiddenKeySoft(layoutWrapper, getActivity());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEvent();
    }

    private void setEvent() {
        tvSubscriberReport.setOnClickListener(this);
        loBack.setOnClickListener(this);
    }



    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_choose_type_of_report;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_subscriber_report:
                changedFragment(new ReportFilterFragment());
                break;
            case R.id.img_nav_back_choose_report_type:
                Objects.requireNonNull(getActivity()).onBackPressed();
                break;
        }
    }

    public void changedFragment(Fragment fragment){
        //create fragment manager
        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.content_report, fragment);
        fragmentTransaction.addToBackStack("tag");
        fragmentTransaction.commit();
    }
}
