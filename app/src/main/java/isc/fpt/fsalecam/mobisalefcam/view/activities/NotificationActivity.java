package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
/**
 * Created by Hau Le on 2018-07-11.
 */
public class NotificationActivity extends BaseActivity implements View.OnClickListener{
    @BindView(R.id.ic_icon_nav_back_notification) RelativeLayout imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setEvent();
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_notification;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_icon_nav_back_notification:
                onBackPressed();
                break;
        }
    }
}
