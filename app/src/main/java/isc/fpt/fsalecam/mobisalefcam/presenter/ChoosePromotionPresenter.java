package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-08-17.
 */
public interface ChoosePromotionPresenter {
    void doGetPromotionList(Context context, APIService apiService, Realm realm,
                            String localTypeId, String locationId, String contract);
    void doRefresh(Context context, APIService apiService, Realm realm,
                            String localTypeId, String locationId, String contract);
    void searchPromotion(Realm realm, String keyQuery);
}
