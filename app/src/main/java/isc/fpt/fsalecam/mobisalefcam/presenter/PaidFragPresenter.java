package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqRegistrationAll;

/**
 * Created by Hau Le on 2018-09-04.
 */
public interface PaidFragPresenter {
    void doGetCusPaidList(Context context, APIService apiService, ReqRegistrationAll obj);
}
