package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetRegistrationDetail;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationTotal;

/**
 * Created by Hau Le on 2018-09-06.
 */
public interface UpdateTotalOfMoneyPresenter {
    void doGetServiceTypeList(Context context, APIService apiService);
    void doGetConnectionFeeList(Context context, APIService apiService, String locationId, Realm realm);
    void doGetRegistrationById(Context context, APIService apiService, ReqGetRegistrationDetail obj);
    void doGetDepositList(Context context, APIService apiService, String locationId, Realm realm);
    void doGetVatList(Context context, APIService apiService, Realm realm);
    void doGetCalNewTotal(Context context, APIService apiService, ReqUpdateRegistrationTotal obj);
    void doUpdateRegistrationTotal(Context context, APIService apiService, ReqUpdateRegistrationTotal obj);
}
