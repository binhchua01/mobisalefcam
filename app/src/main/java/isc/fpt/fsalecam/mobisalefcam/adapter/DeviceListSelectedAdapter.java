package isc.fpt.fsalecam.mobisalefcam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;

/**
 * Created by Hau Le on 2018-08-23.
 */
public class DeviceListSelectedAdapter extends RecyclerView.Adapter<DeviceListSelectedAdapter.SimpleViewHolder> {
    private Context context;
    private ArrayList<ReqDevice> list;

    public DeviceListSelectedAdapter(Context context, ArrayList<ReqDevice> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_choose_device_list, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SimpleViewHolder holder, int position) {
        holder.tvDeviceName.setText(list.get(position).getName());
        holder.tvPrice.setText(String.valueOf(list.get(position).getPrice()));
        holder.tvQuantity.setText(String.valueOf(list.get(position).getNumber()));

        //check for choose callback
        //if changeNumber is 1 -> accept change callback else disable
        if(Integer.parseInt(list.get(position).getChangeNumber()) == 1){
            holder.loChangeQuantity.setEnabled(true);
            holder.loMin.setEnabled(true);
            holder.loMax.setEnabled(true);
            setEvent(holder, position);
        }else{
            holder.loMin.setEnabled(false);
            holder.loMax.setEnabled(false);
            holder.loChangeQuantity.setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_device_name) TextView tvDeviceName;
        @BindView(R.id.tv_price) TextView tvPrice;
        @BindView(R.id.lo_less_than) RelativeLayout loMin;
        @BindView(R.id.lo_more_than) RelativeLayout loMax;
        @BindView(R.id.tv_count_device) TextView tvQuantity;
        @BindView(R.id.layout_change_quantity) RelativeLayout loChangeQuantity;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private int calculatorQuantity(int num, int typeCal){
        int result = 1;
        //minus
        if(typeCal == 0){
            result = num - 1;
            if(num <= 1)
                return 1;
            else
                return result;
        }
        //add
        if(typeCal == 1){
            result = num + 1;
        }
        return result;
    }

    private void setEvent(final SimpleViewHolder holder, final int pos){
        holder.loMin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = calculatorQuantity(Integer.parseInt(holder.tvQuantity.getText().toString()), 0);
                holder.tvQuantity.setText(String.valueOf(quantity));
                //update quantity cus order
                list.get(pos).setNumber(holder.tvQuantity.getText().toString());
            }
        });

        holder.loMax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = calculatorQuantity(Integer.parseInt(holder.tvQuantity.getText().toString()), 1);
                holder.tvQuantity.setText(String.valueOf(quantity));
                list.get(pos).setNumber(holder.tvQuantity.getText().toString());
            }
        });
    }

    public void notifyData(ArrayList<ReqDevice> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
