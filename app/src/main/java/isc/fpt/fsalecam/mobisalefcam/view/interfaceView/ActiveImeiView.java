package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

/**
 * Created by Hau Le on 2018-07-06.
 */
public interface ActiveImeiView {
    void displayImei(String imei);
}
