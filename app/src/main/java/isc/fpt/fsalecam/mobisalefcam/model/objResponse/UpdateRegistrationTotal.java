package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateRegistrationTotal {
    @SerializedName("ResultID")
    @Expose
    private int resultId;
    @SerializedName("ObjID")
    @Expose
    private int objId;
    @SerializedName("Contract")
    @Expose
    private String contract;
    @SerializedName("Result")
    @Expose
    private String result;

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public int getObjId() {
        return objId;
    }

    public void setObjId(int objId) {
        this.objId = objId;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
