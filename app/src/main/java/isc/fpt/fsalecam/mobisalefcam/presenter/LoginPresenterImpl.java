package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import java.util.Calendar;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResLogin;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqLogin;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.LoginView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.hashPassword;

/**
 * Created by Hau Le on 2018-07-06.
 */

public class LoginPresenterImpl implements LoginPresenter {
    private LoginView view;

    public LoginPresenterImpl(LoginView loginView) {
        this.view = loginView;
    }

    @Override
    public void progressLogin(Context context, String user, String pass,
                              String imei, String deviceToken, APIService apiService) {
        if(user.isEmpty()){
            view.userError();
        }

        if(pass.isEmpty()){
            view.passError();
        }

        if(!user.isEmpty() && !pass.isEmpty()){
            //show dialog progress
            view.showDialogProgress();
            //call iap login in here
            generateTokenAndLogin(context, apiService, user, pass, imei, deviceToken);
        }
    }

    @Override
    public void readImeiDevice(Context context) {
        view.returnIMEI(Utils.getIMEIDevice(context));
    }

    private void generateTokenAndLogin(final Context context, final APIService apiService,
                                       final String user, final String pass,
                                       final String imei,final String deviceToken){
        apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //generate token success
                SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                ReqLogin userInfo = new ReqLogin(user, hashPassword(pass), imei, deviceToken);
                //login
                apiService.doLogin(Utils.checkSum(userInfo.userInfoToString()), Utils.CONTENT_TYPE,
                    Utils.TOKEN_TYPE + response.body(), userInfo.userObj())
                    .enqueue(new Callback<ResLogin>() {
                        @Override
                        public void onResponse(Call<ResLogin> call, Response<ResLogin> response) {
                            validateReturnCode(context, response);
                        }

                        @Override
                        public void onFailure(Call<ResLogin> call, Throwable t) {
                            view.hiddenDialogProgress();
                            view.callApiFailed(context.getResources().getString(R.string.check_internet));
                        }
                    });
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                view.callApiFailed(t.getMessage());
                view.hiddenDialogProgress();
            }
        });
    }

    private void validateReturnCode(Context context, Response<ResLogin> obj){
        if(obj.body()!=null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                //login success
                view.loginSuccess();
                //save Authorization header
                Utils.saveParamHeaders(context,
                        obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
                //save timer to refresh token
                SharedPref.put(context, SharedPref.Key.TIMER_TO_REFRESH,
                        Utils.setTimerToRefreshToken(Calendar.getInstance()));
            }else{
                view.callApiFailed(obj.body().getMessage());
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenDialogProgress();
    }
}