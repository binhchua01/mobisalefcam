package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqContractDetails;

/**
 * Created by Hau Le on 2018-08-30.
 */
public interface ContractDetailPresenter {
    void doGetContractDetails(Context context, APIService apiService, ReqContractDetails obj);
    void doGetUrlContractPdf(Context context, APIService apiService, int objId);
    void doDownloadFilePdf(Context context, APIService apiService, String url, int objId);
}
