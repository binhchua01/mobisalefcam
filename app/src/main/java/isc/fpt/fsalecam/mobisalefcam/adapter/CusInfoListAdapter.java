package isc.fpt.fsalecam.mobisalefcam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Registration;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;

/**
 * Created by Hau Le on 2018-08-29.
 */
public class CusInfoListAdapter extends RecyclerView.Adapter<CusInfoListAdapter.SimpleViewHolder> {
    private Context context;
    private ArrayList<Registration> list;
    private OnItemClickListener listener;

    public CusInfoListAdapter(Context context, ArrayList<Registration> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cus_info_list, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        mViewHolder.tvViewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickListener(view, mViewHolder.getAdapterPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        holder.tvStatus.setText(list.get(position).getRegStatus());
        holder.tvCusName.setText(list.get(position).getFullName());
        holder.tvPhone.setText(list.get(position).getPhone1());
        holder.tvCusAddress.setText(list.get(position).getAddress());
        /*
        * status of registration
        * 1: waiting confirm
        * 2: unpaid
        * 3: had paid
        * */
        if(list.get(position).getStatus() == 1){
            holder.tvLabelContractId.setText(context.getResources().getString(R.string.ui_text_code));
            holder.tvRegCode.setText(list.get(position).getRegCode());
        }else{
            holder.tvRegCode.setText(list.get(position).getContract());
        }
        //check status of cus and display UI
        if(list.get(position).getStatus() == 2){
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.wrong_pink));
            holder.imgIconStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_unpaid));
        }else if(list.get(position).getStatus() == 1){
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.s_grey_a9));
            holder.imgIconStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_waiting_confirm));
        }else{
            holder.tvStatus.setTextColor(context.getResources().getColor(R.color.correct_green));
            holder.imgIconStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_paid));
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_status_of_cus) TextView tvStatus;
        @BindView(R.id.tv_cus_name) TextView tvCusName;
        @BindView(R.id.tv_reg_code) TextView tvRegCode;
        @BindView(R.id.tv_phone_of_cus) TextView tvPhone;
        @BindView(R.id.tv_cus_address) TextView tvCusAddress;
        @BindView(R.id.tv_cus_wait_details) TextView tvViewDetail;
        @BindView(R.id.ic_icon_status) ImageView imgIconStatus;
        @BindView(R.id.tv_label_contract_id) TextView tvLabelContractId;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void notifyData(ArrayList<Registration> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
