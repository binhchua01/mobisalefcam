package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGenerateAddress;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqPotentialCus;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGenerateAddress;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResPotentailCus;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.NewPotentialCusView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-14.
 */
public class NewPotentialCusPresenterImpl implements NewPotentialCusPresenter {
    private NewPotentialCusView view;

    public NewPotentialCusPresenterImpl(NewPotentialCusView view) {
        this.view = view;
    }

    @Override
    public void doCreatePotentialCus(final Context context, final APIService apiService, final ReqPotentialCus obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiCreatePotentialCus(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiCreatePotentialCus(context, apiService, obj);
        }
    }

    @Override
    public void doGenerateAddress(final Context context, final APIService apiService, final ReqGenerateAddress obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGenerateAddress(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGenerateAddress(context, apiService, obj);
        }
    }

    private void callApiGenerateAddress(final Context context, APIService apiService, ReqGenerateAddress obj){
        apiService.doGenerationAddress(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjAddressReq()).enqueue(new Callback<ResGenerateAddress>() {
            @Override
            public void onResponse(Call<ResGenerateAddress> call, Response<ResGenerateAddress> response) {
                validateReturnCodeGenerateAddress(context, response);
            }

            @Override
            public void onFailure(Call<ResGenerateAddress> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiCreatePotentialCus(final Context context, APIService apiService, ReqPotentialCus obj){
        apiService.doCreatePotentialCus(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjCreatePotentialCus()).enqueue(new Callback<ResPotentailCus>() {
            @Override
            public void onResponse(Call<ResPotentailCus> call, Response<ResPotentailCus> response) {
                validateReturnCodeCreatePotentialCus(context, response);
            }

            @Override
            public void onFailure(Call<ResPotentailCus> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCodeCreatePotentialCus(Context context, Response<ResPotentailCus> response){
        if(response.body()!= null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.createPotentialCusSuccess(response.body().getData().get(0));
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodeGenerateAddress(Context context, Response<ResGenerateAddress> response) {
        if (response.body() != null) {
            int code = response.body().getCode();
            if (code == Utils.SUCCESS) {
                view.generateStringAddressSuccess(response.body().getData().get(0).getResult());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            } else {
                if (code == Utils.EXP_TOKEN) {
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                } else {
                    view.callApiFailed(response.body().getMessage());
                }
            }
        } else {
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }
}
