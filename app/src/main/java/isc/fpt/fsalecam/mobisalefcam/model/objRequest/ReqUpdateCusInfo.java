package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;

/**
 * Created by Hau Le on 2018-08-17.
 */
public class ReqUpdateCusInfo {
    private String regId, regCode, objId, contract, fullName, representive, birthday,
            address, noteAddress, passport, cusTypeDetail, email, taxCode, phone1, contact1,
            phone2, contact2, fax, nationality, nationalityName, vat, locationId, billToCity,
            districtId, billToDistrict, wardId, billToWard, streetId, billToStreet, billToNumber,
            typeHouse, typeHouseName, buildingId, buildingName, imageInfo, saleId, groupPoints,
            inDoor, outDoor, inDoorType, outDoorType, odcCableType, contractTemp, bookportIdentityId,
            latLngDevice, latLngMarker, localType, localTypeName, promotionId, promotionName,
            promotionDescription, monthOfPrepaid, total, internetTotal, deviceTotal, depositFee,
            connectionFee, paymentMethodPerMonth, paymentMethodPerMonthName, ip;

    private List<ReqDevice> listDevice;

    private List<ItemOfListRes> listServiceType;

    public String getLatLngMarker() {
        return latLngMarker;
    }

    public void setLatLngMarker(String latLngMarker) {
        this.latLngMarker = latLngMarker;
    }

    public String getPromotionDescription() {
        return promotionDescription;
    }

    public void setPromotionDescription(String promotionDescription) {
        this.promotionDescription = promotionDescription;
    }

    public String getPaymentMethodPerMonthName() {
        return paymentMethodPerMonthName;
    }

    public void setPaymentMethodPerMonthName(String paymentMethodPerMonthName) {
        this.paymentMethodPerMonthName = paymentMethodPerMonthName;
    }

    public List<ItemOfListRes> getListServiceType() {
        return listServiceType;
    }

    public void setListServiceType(List<ItemOfListRes> listServiceType) {
        this.listServiceType = listServiceType;
    }

    public String getTypeHouseName() {
        return typeHouseName;
    }

    public void setTypeHouseName(String typeHouseName) {
        this.typeHouseName = typeHouseName;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getLocalTypeName() {
        return localTypeName;
    }

    public void setLocalTypeName(String localTypeName) {
        this.localTypeName = localTypeName;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRepresentive() {
        return representive;
    }

    public void setRepresentive(String representive) {
        this.representive = representive;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNoteAddress() {
        return noteAddress;
    }

    public void setNoteAddress(String noteAddress) {
        this.noteAddress = noteAddress;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getCusTypeDetail() {
        return cusTypeDetail;
    }

    public void setCusTypeDetail(String cusTypeDetail) {
        this.cusTypeDetail = cusTypeDetail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getContact1() {
        return contact1;
    }

    public void setContact1(String contact1) {
        this.contact1 = contact1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getBillToCity() {
        return billToCity;
    }

    public void setBillToCity(String billToCity) {
        this.billToCity = billToCity;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getBillToDistrict() {
        return billToDistrict;
    }

    public void setBillToDistrict(String billToDistrict) {
        this.billToDistrict = billToDistrict;
    }

    public String getWardId() {
        return wardId;
    }

    public void setWardId(String wardId) {
        this.wardId = wardId;
    }

    public String getBillToWard() {
        return billToWard;
    }

    public void setBillToWard(String billToWard) {
        this.billToWard = billToWard;
    }

    public String getStreetId() {
        return streetId;
    }

    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    public String getBillToStreet() {
        return billToStreet;
    }

    public void setBillToStreet(String billToStreet) {
        this.billToStreet = billToStreet;
    }

    public String getBillToNumber() {
        return billToNumber;
    }

    public void setBillToNumber(String billToNumber) {
        this.billToNumber = billToNumber;
    }

    public String getTypeHouse() {
        return typeHouse;
    }

    public void setTypeHouse(String typeHouse) {
        this.typeHouse = typeHouse;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getImageInfo() {
        return imageInfo;
    }

    public void setImageInfo(String imageInfo) {
        this.imageInfo = imageInfo;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getGroupPoints() {
        return groupPoints;
    }

    public void setGroupPoints(String groupPoints) {
        this.groupPoints = groupPoints;
    }

    public String getInDoor() {
        return inDoor;
    }

    public void setInDoor(String inDoor) {
        this.inDoor = inDoor;
    }

    public String getOutDoor() {
        return outDoor;
    }

    public void setOutDoor(String outDoor) {
        this.outDoor = outDoor;
    }

    public String getInDoorType() {
        return inDoorType;
    }

    public void setInDoorType(String inDoorType) {
        this.inDoorType = inDoorType;
    }

    public String getOutDoorType() {
        return outDoorType;
    }

    public void setOutDoorType(String outDoorType) {
        this.outDoorType = outDoorType;
    }

    public String getOdcCableType() {
        return odcCableType;
    }

    public void setOdcCableType(String odcCableType) {
        this.odcCableType = odcCableType;
    }

    public String getContractTemp() {
        return contractTemp;
    }

    public void setContractTemp(String contractTemp) {
        this.contractTemp = contractTemp;
    }

    public String getBookportIdentityId() {
        return bookportIdentityId;
    }

    public void setBookportIdentityId(String bookportIdentityId) {
        this.bookportIdentityId = bookportIdentityId;
    }

    public String getLatLngDevice() {
        return latLngDevice;
    }

    public void setLatLngDevice(String latLngDevice) {
        this.latLngDevice = latLngDevice;
    }

    public String getLocalType() {
        return localType;
    }

    public void setLocalType(String localType) {
        this.localType = localType;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getMonthOfPrepaid() {
        return monthOfPrepaid;
    }

    public void setMonthOfPrepaid(String monthOfPrepaid) {
        this.monthOfPrepaid = monthOfPrepaid;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getInternetTotal() {
        return internetTotal;
    }

    public void setInternetTotal(String internetTotal) {
        this.internetTotal = internetTotal;
    }

    public String getDeviceTotal() {
        return deviceTotal;
    }

    public void setDeviceTotal(String deviceTotal) {
        this.deviceTotal = deviceTotal;
    }

    public String getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(String depositFee) {
        this.depositFee = depositFee;
    }

    public String getConnectionFee() {
        return connectionFee;
    }

    public void setConnectionFee(String connectionFee) {
        this.connectionFee = connectionFee;
    }

    public String getPaymentMethodPerMonth() {
        return paymentMethodPerMonth;
    }

    public void setPaymentMethodPerMonth(String paymentMethodPerMonth) {
        this.paymentMethodPerMonth = paymentMethodPerMonth;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<ReqDevice> getListDevice() {
        return listDevice;
    }

    public void setListDevice(List<ReqDevice> listDevice) {
        this.listDevice = listDevice;
    }

    public LinkedHashMap<String, Object> generateObjUpdateCusInfo() {
        //generate object to send
        LinkedHashMap<String, Object> obj = new LinkedHashMap<>();
        obj.put("RegId", getRegId());
        obj.put("RegCode", getRegCode());
        obj.put("ObjId", getObjId());
        obj.put("Contract", getContract());
        obj.put("FullName", getFullName());
        obj.put("Representive", getRepresentive());
        obj.put("Birthday", getBirthday());
        obj.put("Address", getAddress());
        obj.put("NoteAddress", getNoteAddress());
        obj.put("Passport", getPassport());
        obj.put("CusTypeDetail", getCusTypeDetail());
        obj.put("Email", getEmail());
        obj.put("TaxCode", getTaxCode());
        obj.put("Phone1", getPhone1());
        obj.put("Contact1", getContact1());
        obj.put("Phone2", getPhone2());
        obj.put("Contact2", getContact2());
        obj.put("Fax", getFax());
        obj.put("Nationality", getNationality());
        obj.put("VAT", getVat());
        obj.put("LocationId", getLocationId());
        obj.put("BillTo_City", getBillToCity());
        obj.put("DistrictId", getDistrictId());
        obj.put("BillTo_District", getBillToDistrict());
        obj.put("WardId", getWardId());
        obj.put("BillTo_Ward", getBillToWard());
        obj.put("StreetId", getStreetId());
        obj.put("BillTo_Street", getBillToStreet());
        obj.put("BillTo_Number", getBillToNumber());
        obj.put("TypeHouse", getTypeHouse());
        obj.put("BuildingId", getBuildingId());
        obj.put("ImageInfo", getImageInfo());
        obj.put("SaleId", getSaleId());
        obj.put("GroupPoints", getGroupPoints());
        obj.put("Indoor", getInDoor());
        obj.put("OutDoor", getOutDoor());
        obj.put("InDType", getInDoorType());
        obj.put("OutDType", getOutDoorType());
        obj.put("ODCCableType", getOdcCableType());
        obj.put("ContractTemp", getContractTemp());
        obj.put("BookportIdentityId", getBookportIdentityId());
        obj.put("LocalType", getLocalType());
        obj.put("PromotionId", getPromotionId());
        obj.put("MonthOfPrepaid", getMonthOfPrepaid());
        obj.put("Total", getTotal());
        obj.put("InternetTotal", getInternetTotal());
        obj.put("DeviceTotal", getDeviceTotal());
        obj.put("DepositFee", getDepositFee());
        obj.put("ConnectionFee", getConnectionFee());
        obj.put("PaymentMethodPerMonth", getPaymentMethodPerMonth());
        obj.put("IP", getIp());
        obj.put("ListDevice", linkedHashArrayDevice());
        obj.put("ListServiceType", linkedHashArrayServiceType());
        obj.put("Latlng", getLatLngMarker());
        obj.put("LatlngDevice", getLatLngDevice());
        return obj;
    }

    public JSONObject objToString() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("RegId", getRegId());
            obj.put("RegCode", getRegCode());
            obj.put("ObjId", getObjId());
            obj.put("Contract", getContract());
            obj.put("FullName", getFullName());
            obj.put("Representive", getRepresentive());
            obj.put("Birthday", getBirthday());
            obj.put("Address", getAddress());
            obj.put("NoteAddress", getNoteAddress());
            obj.put("Passport", getPassport());
            obj.put("CusTypeDetail", getCusTypeDetail());
            obj.put("Email", getEmail());
            obj.put("TaxCode", getTaxCode());
            obj.put("Phone1", getPhone1());
            obj.put("Contact1", getContact1());
            obj.put("Phone2", getPhone2());
            obj.put("Contact2", getContact2());
            obj.put("Fax", getFax());
            obj.put("Nationality", getNationality());
            obj.put("VAT", getVat());
            obj.put("LocationId", getLocationId());
            obj.put("BillTo_City", getBillToCity());
            obj.put("DistrictId", getDistrictId());
            obj.put("BillTo_District", getBillToDistrict());
            obj.put("WardId", getWardId());
            obj.put("BillTo_Ward", getBillToWard());
            obj.put("StreetId", getStreetId());
            obj.put("BillTo_Street", getBillToStreet());
            obj.put("BillTo_Number", getBillToNumber());
            obj.put("TypeHouse", getTypeHouse());
            obj.put("BuildingId", getBuildingId());
            obj.put("ImageInfo", getImageInfo());
            obj.put("SaleId", getSaleId());
            obj.put("GroupPoints", getGroupPoints());
            obj.put("Indoor", getInDoor());
            obj.put("OutDoor", getOutDoor());
            obj.put("InDType", getInDoorType());
            obj.put("OutDType", getOutDoorType());
            obj.put("ODCCableType", getOdcCableType());
            obj.put("ContractTemp", getContractTemp());
            obj.put("BookportIdentityId", getBookportIdentityId());
            obj.put("LocalType", getLocalType());
            obj.put("PromotionId", getPromotionId());
            obj.put("MonthOfPrepaid", getMonthOfPrepaid());
            obj.put("Total", getTotal());
            obj.put("InternetTotal", getInternetTotal());
            obj.put("DeviceTotal", getDeviceTotal());
            obj.put("DepositFee", getDepositFee());
            obj.put("ConnectionFee", getConnectionFee());
            obj.put("PaymentMethodPerMonth", getPaymentMethodPerMonth());
            obj.put("IP", getIp());
            obj.put("ListDevice", arrayDeviceListToString());
            obj.put("ListServiceType", arrayServiceTypeToString());
            obj.put("Latlng", getLatLngMarker());
            obj.put("LatlngDevice", getLatLngDevice());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    private JSONArray arrayDeviceListToString() {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        for (int i = 0; i < getListDevice().size(); i++) {
            jsonObject = new JSONObject();
            try {
                jsonObject.put("Value", getListDevice().get(i).getValue());
                jsonObject.put("Name", getListDevice().get(i).getName());
                jsonObject.put("Number", getListDevice().get(i).getNumber());
                jsonObject.put("Price", String.valueOf(getListDevice().get(i).getPrice()));
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private JSONArray arrayServiceTypeToString() {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        for (int i = 0; i < getListServiceType().size(); i++) {
            jsonObject = new JSONObject();
            try {
                jsonObject.put("Id", String.valueOf(getListServiceType().get(i).getId()));
                jsonObject.put("Name", getListServiceType().get(i).getName());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private ArrayList<Object> linkedHashArrayDevice(){
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < getListDevice().size(); i++) {
            LinkedHashMap<String, String> obj = new LinkedHashMap<>();
            obj.put("Value", getListDevice().get(i).getValue());
            obj.put("Name", getListDevice().get(i).getName());
            obj.put("Number", getListDevice().get(i).getNumber());
            obj.put("Price", String.valueOf(getListDevice().get(i).getPrice()));
            list.add(obj);
        }
        return list;
    }

    private ArrayList<Object> linkedHashArrayServiceType(){
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < getListServiceType().size(); i++) {
            LinkedHashMap<String, String> obj = new LinkedHashMap<>();
            obj.put("Id", String.valueOf(getListServiceType().get(i).getId()));
            obj.put("Name", getListServiceType().get(i).getName());
            list.add(obj);
        }
        return list;
    }
}
