package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-08-10.
 */
public class ListOfPoint {
    @SerializedName("deviceName")
    @Expose
    private String deviceName;
    @SerializedName("lengthCable")
    @Expose
    private Integer lengthCable;
    @SerializedName("lengthSuggest")
    @Expose
    private double lengthSuggest;
    @SerializedName("latLng")
    @Expose
    private String latLng;
    @SerializedName("portTotal")
    @Expose
    private Integer portTotal;
    @SerializedName("portFree")
    @Expose
    private Integer portFree;
    @SerializedName("address")
    @Expose
    private String address;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Integer getLengthCable() {
        return lengthCable;
    }

    public void setLengthCable(Integer lengthCable) {
        this.lengthCable = lengthCable;
    }

    public double getLengthSuggest() {
        return lengthSuggest;
    }

    public void setLengthSuggest(double lengthSuggest) {
        this.lengthSuggest = lengthSuggest;
    }

    public String getLatLng() {
        return latLng;
    }

    public void setLatLng(String latLng) {
        this.latLng = latLng;
    }

    public Integer getPortTotal() {
        return portTotal;
    }

    public void setPortTotal(Integer portTotal) {
        this.portTotal = portTotal;
    }

    public Integer getPortFree() {
        return portFree;
    }

    public void setPortFree(Integer portFree) {
        this.portFree = portFree;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
