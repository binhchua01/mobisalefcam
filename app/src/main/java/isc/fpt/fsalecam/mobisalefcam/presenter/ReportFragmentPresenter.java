package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqReportContract;

/**
 * Created by Hau Le on 2018-09-17.
 */
public interface ReportFragmentPresenter {
    void doGetReportContractList(Context context, APIService apiService, ReqReportContract obj);
}
