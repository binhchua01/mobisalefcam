package isc.fpt.fsalecam.mobisalefcam.utils;

import android.graphics.Bitmap;

/**
 * Created by Hau Le on 2018-07-19.
 */
public interface ReturnBitmap {
    void bitmapExport(Bitmap bitmap);
}
