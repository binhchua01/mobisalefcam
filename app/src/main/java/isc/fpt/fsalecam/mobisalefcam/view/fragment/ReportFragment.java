package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity;
import isc.fpt.fsalecam.mobisalefcam.adapter.ReportContractAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ReportContract;
import isc.fpt.fsalecam.mobisalefcam.presenter.ReportFragmentPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ReportFragmentPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ReportFragmentView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity.canBack;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity.objReport;


/**
 * Created by Hau Le on 2018-07-10.
 */
public class ReportFragment extends BaseFragment implements ReportFragmentView, View.OnClickListener{
    @BindView(R.id.layout_report) LinearLayout layoutWrapper;
    @BindView(R.id.tv_date) TextView tvDate;
    @BindView(R.id.tv_status) TextView tvStatus;
    @BindView(R.id.list_report) RecyclerView recyclerView;
    @BindView(R.id.layout_filter) RelativeLayout loFilter;

    private Dialog dialog;
    private ArrayList<ReportContract> list;
    private ReportContractAdapter adapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //enable function can back of fragment report
        canBack = 1;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        setEvent();
        hiddenKeySoft(layoutWrapper, getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        initParamToCallApi();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_report;
    }

    @Override
    public void getReportListSuccess(ArrayList<ReportContract> list) {
        this.list = list;
        adapter.notifyData(list);
    }

    @Override
    public void callApiFailed(String str) {
        DialogUtils.showMessageDialogVerButton(getActivity(), str).show();
    }

    @Override
    public void expiredToken(String str) {
        DialogUtils.showDialogRequiredLoginAgain(getActivity(), str).show();
    }

    @Override
    public void showProgressLoading() {
        hiddenProgressLoading();
        dialog = DialogUtils.showLoadingDialog(getActivity(), getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressLoading() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_filter:
                ((MainActivity) Objects.requireNonNull(getActivity())).changedFragment(new ReportFilterFragment());
                break;
        }
    }

    private void setEvent() {
        loFilter.setOnClickListener(this);
    }

    private void initParamToCallApi(){
        APIService apiService = ApiUtils.getApiService();
        ReportFragmentPresenter presenter = new ReportFragmentPresenterImpl(this);
        //call api get report list
        presenter.doGetReportContractList(getActivity(), apiService, objReport);
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        tvDate.setText("Date: " + objReport.getFromDate() + " - " + objReport.getToDate());
        switch (objReport.getStatus()){
            case 0:
                tvStatus.setText(getResources().getString(R.string.all));
                break;
            case 1:
                tvStatus.setText(getResources().getString(R.string.online));
                break;
            case 2:
                tvStatus.setText(getResources().getString(R.string.not_yet_online));
                break;
            default:
                tvStatus.setText(getResources().getString(R.string.all));
                break;
        }
        list = new ArrayList<>();
        adapter = new ReportContractAdapter(getActivity(), list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //TODO: set event onclick in item
            }
        });
        recyclerView.setAdapter(adapter);
    }
}
