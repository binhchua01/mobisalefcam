package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Objects;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqReportContract;
import isc.fpt.fsalecam.mobisalefcam.utils.CallbackDateTime;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity.canBack;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity.objReport;

/**
 * Created by Hau Le on 2018-09-17.
 */
public class ReportFilterFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.layout_status) RelativeLayout loChooseStatus;
    @BindView(R.id.tv_status) TextView tvStatus;
    @BindView(R.id.layout_from_date) RelativeLayout loFromDate;
    @BindView(R.id.tv_from_date) TextView tvFromDate;
    @BindView(R.id.layout_to_date) RelativeLayout loToDate;
    @BindView(R.id.to_date) TextView tvToDate;
    @BindView(R.id.btn_confirm) Button btnConfirm;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //disable can back
        canBack = 0;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //set obj is null
        objReport = new ReqReportContract();
        setEvent();
    }

    @Override
    public void onResume() {
        super.onResume();
        tvFromDate.setText(Utils.getCurrentDateTime());
        objReport.setFromDate(Utils.getCurrentDateTime());
        tvToDate.setText(Utils.getCurrentDateTime());
        objReport.setToDate(Utils.getCurrentDateTime());
    }

    private void setEvent() {
        loChooseStatus.setOnClickListener(this);
        loFromDate.setOnClickListener(this);
        loToDate.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_filter_report;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_status:
                DialogUtils.showDialogChooseContractStatus(getActivity(), new DialogCallbackDataToActivity() {
                    @Override
                    public void dataCallback(String data) {
                        String[] status = data.split(":");
                        tvStatus.setText(status[1]);
                        //put data to objReport
                        objReport.setStatus(Integer.parseInt(status[0]));
                    }
                }).show();
                break;
            case R.id.layout_from_date:
                DialogUtils.showDialogChooseDate(getContext(), new CallbackDateTime() {
                    @Override
                    public void dateTime(String date) {
                        tvFromDate.setText(date);
                        objReport.setFromDate(date);
                    }
                }).show();
                break;
            case R.id.layout_to_date:
                DialogUtils.showDialogChooseDate(getContext(), new CallbackDateTime() {
                    @Override
                    public void dateTime(String date) {
                        tvToDate.setText(date);
                        objReport.setToDate(date);
                    }
                }).show();
                break;
            case R.id.btn_confirm:
                if(checkValue()){
                    ((MainActivity) Objects.requireNonNull(getActivity())).changedFragment(new ReportFragment());
                }
                break;
        }
    }

    private boolean checkValue(){
        if(tvFromDate.getText().toString().isEmpty()){
            loFromDate.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
        }else{
            loFromDate.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        }
        if(tvToDate.getText().toString().isEmpty()){
            loToDate.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
        }else{
            loToDate.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        }
        return (!tvToDate.getText().toString().isEmpty() && !tvFromDate.getText().toString().isEmpty());
    }
}
