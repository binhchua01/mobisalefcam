package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.ViewImageAdapter;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.UploadImages;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.CusInfoDetailsActivity.registrationDetail;

public class ViewImageRegistrationActivity extends BaseActivity
        implements View.OnClickListener{
    @BindView(R.id.ic_icon_nav_back_image_list) RelativeLayout loBack;
    @BindView(R.id.image_view) ImageView imgView;
    @BindView(R.id.list_image) RecyclerView recyclerView;
    @BindView(R.id.progress_bar_loading_image) ProgressBar progressBar;

    private ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setEvent();
        initView();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_view_image_registration;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_icon_nav_back_image_list:
                onBackPressed();
                break;
        }
    }

    private void setEvent() {
        loBack.setOnClickListener(this);
    }

    private void initView(){
        list = new ArrayList<>();
        /*
        * TODO: get string image id from CusInfoDetailActivity
        * split string and add to list
        * */
        final String[] listId = registrationDetail.getImageInfo().split(",");
        for (String aListId : listId) {
            list.add(UploadImages.doDownloadImage(Utils.saleOfInfo, aListId));
//            Log.d("TAG", "initView: " + UploadImages.doDownloadImage(Utils.saleOfInfo, aListId));
        }
        ViewImageAdapter adapter = new ViewImageAdapter(this, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //TODO: st view image when click thumbnail
                Glide.with(getApplicationContext())
                        .load(UploadImages.getUrlWithHeaders(getApplicationContext(), list.get(position)))
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                        .into(imgView);
            }
        });
        //implement grid layout horizontal
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1,
                GridLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(this);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);

        //view image first
        Glide.with(getApplicationContext())
                .load(UploadImages.getUrlWithHeaders(getApplicationContext(), list.get(0)))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model,
                                                   Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imgView);
    }

}
