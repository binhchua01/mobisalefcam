package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqPotentialCusList;

/**
 * Created by Hau Le on 2018-09-14.
 */
public interface PotentialCusListPresenter {
    void doGetPotentialCusList(Context context, APIService apiService, ReqPotentialCusList obj);
    void doGetPotentialCusTypeList(Context context, APIService apiService, Realm realm);
}
