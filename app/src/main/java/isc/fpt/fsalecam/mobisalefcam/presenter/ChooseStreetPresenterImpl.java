package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import java.util.ArrayList;

import io.realm.Case;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetStreetList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.Street;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseStreetView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-02.
 */
public class ChooseStreetPresenterImpl implements ChooseStreetPresenter {
    private ChooseStreetView chooseStreetView;

    public ChooseStreetPresenterImpl(ChooseStreetView chooseStreetView) {
        this.chooseStreetView = chooseStreetView;
    }

    @Override
    public void doGetStreetList(final Context context, final APIService apiService,
                                final String cityId, final String districtId,
                                final String wardId, final int type,
                                final Realm realm) {
        if(realm.where(Street.class).findAll().size() == 0 ||
                realm.where(Street.class)
                .equalTo(Utils.CITY_ID, Integer.parseInt(cityId))
                .equalTo(Utils.DISTRICT_ID, Integer.parseInt(districtId))
                .equalTo(Utils.WARD_ID, Integer.parseInt(wardId))
                .findAll().size() == 0){
            //show dialog loading..
            chooseStreetView.showProgressLoading();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetStreetList(context, apiService, cityId, districtId, wardId, type, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        chooseStreetView.hiddenProgressLoading();
                        chooseStreetView.getStreetListFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiGetStreetList(context, apiService, cityId, districtId, wardId, type, realm);
            }
        }else{
            chooseStreetView.getStreetListSuccess();
        }
    }

    @Override
    public void refreshStreetList(final Context context, final APIService apiService, final String cityId,
                                  final String districtId, final String wardId, final int type, final Realm realm) {
        //show dialog loading..
        chooseStreetView.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetStreetList(context, apiService, cityId, districtId, wardId, type, realm);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    chooseStreetView.hiddenProgressLoading();
                    chooseStreetView.getStreetListFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetStreetList(context, apiService, cityId, districtId, wardId, type, realm);
        }
    }

    @Override
    public void searchStreetList(Realm realm, String keyQuery, String cityId, String districtId, String wardId) {
        ArrayList<Street> list = (ArrayList<Street>)
                realm.copyFromRealm(realm.where(Street.class)
                        .equalTo(Utils.CITY_ID, Integer.parseInt(cityId))
                        .equalTo(Utils.DISTRICT_ID, Integer.parseInt(districtId))
                        .equalTo(Utils.WARD_ID, Integer.parseInt(wardId))
                        .like(Utils.STREET_NAME, "*" + keyQuery + "*", Case.SENSITIVE)
                        .findAll());
        chooseStreetView.searchStreet(list);
    }

    private void callApiGetStreetList(final Context context, APIService apiService,
                                      final String cityId, final String districtId,
                                      final String wardId, final int type, final Realm realm){
        //put data to object
        final ReqGetStreetList location = new ReqGetStreetList(cityId, districtId, wardId, String.valueOf(type));
        //call api get district list...
        apiService.doGetStreetList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(location.objToString()),
                location.generateObjGetStreetList())
                .enqueue(new Callback<ResParentStructure>() {
                    @Override
                    public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                        //success
                        validateReturnCode(context, response,
                                Integer.parseInt(cityId), Integer.parseInt(districtId),
                                Integer.parseInt(wardId), realm);
                    }

                    @Override
                    public void onFailure(Call<ResParentStructure> call, Throwable t) {
                        chooseStreetView.hiddenProgressLoading();
                        chooseStreetView.getStreetListFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> obj,
                                    int cityId, int districtId, int wardId, Realm realm){
        if(obj.body()!=null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                saveStreetListToLocal((ArrayList<ItemOfListRes>) obj.body().getData(),
                        cityId, districtId, wardId, realm);
                //save Authorization header
                Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    chooseStreetView.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    chooseStreetView.getStreetListFailed(obj.body().getMessage());
                }
            }
        }else{
            chooseStreetView.getStreetListFailed(context.getResources().getString(R.string.server_err));
        }
        chooseStreetView.hiddenProgressLoading();
    }

    private void saveStreetListToLocal(final ArrayList<ItemOfListRes> list,
                                       final int cityId, final int districtId,
                                       final int wardId, Realm realm) {
        realm.beginTransaction();
        realm.where(Street.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size() != 0){
            //add
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                Street street = realm.createObject(Street.class, list.get(i).getId());
                street.setName(list.get(i).getName());
                street.setCityID(cityId);
                street.setDistrictID(districtId);
                street.setWardID(wardId);
                realm.commitTransaction();
            }
            chooseStreetView.getStreetListSuccess();
        }else{
            chooseStreetView.getStreetListSuccess();
        }
    }
}
