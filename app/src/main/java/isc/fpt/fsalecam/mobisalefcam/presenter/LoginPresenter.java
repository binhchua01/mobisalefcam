package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-07-06.
 */
public interface LoginPresenter {
    void progressLogin(Context context, String user, String pass,
                       String imei, String deviceToken, APIService apiService);
    void readImeiDevice(Context context);
}

