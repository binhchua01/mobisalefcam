package isc.fpt.fsalecam.mobisalefcam.utils;

import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Hau Le on 2018-07-06.
 */
public class EncryptDecryptPassword {
    private static final String SERECT_KEY = "MoBiSale_Password@0123456789!*.*";
    /**
     *
     * @param plainText
     * @return encrypted text
     * @throws Exception
     */
    public static String encyrpt(String plainText) throws Exception {
        char[] chars = new char[16];
        Arrays.fill(chars, '0');
        SecretKeySpec secretKey = new SecretKeySpec(SERECT_KEY.getBytes("UTF-8"), "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(new String(chars).getBytes());
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE,secretKey,ivParameterSpec);
        byte[] encryptedText = cipher.doFinal(plainText.getBytes("UTF-8"));
        return android.util.Base64.encodeToString(encryptedText, android.util.Base64.DEFAULT);
    }

    /**
     *
     * @param plainText
     * @return encrypted text
     * @throws Exception
     */
    public static String decrypt(String encryptText) throws Exception {
        char[] chars = new char[16];
        Arrays.fill(chars, '0');
        byte[] encryptTextBytes = android.util.Base64.decode(encryptText.getBytes(), android.util.Base64.DEFAULT);

        SecretKeySpec secretKey=new SecretKeySpec(SERECT_KEY.getBytes(), "AES");
        IvParameterSpec ivParameterSpec=new IvParameterSpec(new String(chars).getBytes());
        Cipher cipher=Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE,secretKey,ivParameterSpec);
        byte[] decryptedText = cipher.doFinal(encryptTextBytes);

        return new String(decryptedText);
    }
}
