package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalNewTotal {
    @SerializedName("ResultID")
    @Expose
    private int resultId;
    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("Total")
    @Expose
    private double total;
    @SerializedName("InternetTotal")
    @Expose
    private double internetTotal;
    @SerializedName("DeviceTotal")
    @Expose
    private double deviceTotal;
    @SerializedName("DepositFee")
    @Expose
    private double depositFee;
    @SerializedName("ConnectionFee")
    @Expose
    private double connectionFee;

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getInternetTotal() {
        return internetTotal;
    }

    public void setInternetTotal(double internetTotal) {
        this.internetTotal = internetTotal;
    }

    public double getDeviceTotal() {
        return deviceTotal;
    }

    public void setDeviceTotal(double deviceTotal) {
        this.deviceTotal = deviceTotal;
    }

    public double getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(double depositFee) {
        this.depositFee = depositFee;
    }

    public double getConnectionFee() {
        return connectionFee;
    }

    public void setConnectionFee(double connectionFee) {
        this.connectionFee = connectionFee;
    }
}

