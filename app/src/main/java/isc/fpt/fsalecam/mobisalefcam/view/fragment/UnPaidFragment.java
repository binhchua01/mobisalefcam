package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.view.activities.ContractDetailsActivity;
import isc.fpt.fsalecam.mobisalefcam.adapter.CusInfoListAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Registration;
import isc.fpt.fsalecam.mobisalefcam.presenter.UnPaidFragPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.UnPaidFragPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.UnPaidFragView;
import static isc.fpt.fsalecam.mobisalefcam.view.fragment.CusInfoListFragment.obj;

/**
 * Created by Hau Le on 2018-07-10.
 */
public class UnPaidFragment extends BaseFragment implements UnPaidFragView {
    @BindView(R.id.recycler_view_cus_unpaid) RecyclerView recyclerView;
    @BindView(R.id.progress_bar_un_paid) ProgressBar progressBar;
    @BindView(R.id.layout_upaid) LinearLayout layoutWrapper;

    public CusInfoListAdapter adapter;
    private ArrayList<Registration> list;
    private APIService apiService;
    private UnPaidFragPresenter presenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hiddenKeySoft(layoutWrapper, getActivity());
        initView();
        apiService = ApiUtils.getApiService();
        presenter = new UnPaidFragPresenterImpl(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        callApi();
    }

    private void callApi() {
        //set data type get lis cus unpaid
        obj.setDataType("2");
        obj.setSearchContent("");
        presenter.doGetCusUnpaidList(getContext(), apiService, obj);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_un_paid;
    }

    @Override
    public void getCusUnPaidSuccess(ArrayList<Registration> list) {
        adapter.notifyData(list);
        this.list = list;
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(getContext(), mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(getContext(), mes).show();
    }

    @Override
    public void showProgressLoading() {
        hiddenProgressLoading();
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hiddenProgressLoading() {
        if(progressBar!=null && recyclerView!=null){
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void initView(){
        list = new ArrayList<>();
        adapter = new CusInfoListAdapter(getContext(), list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                Intent intent = new Intent(Objects.requireNonNull(getContext()).getApplicationContext(),
                        ContractDetailsActivity.class);
                intent.putExtra(Utils.CONTRACT_INFO_BY_REG, list.get(position));
                //handle can back
                intent.putExtra(Utils.CAN_BACK, true);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    public void notifyData(ArrayList<Registration> list){
        adapter.notifyData(list);
    }
}
