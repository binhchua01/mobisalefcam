package isc.fpt.fsalecam.mobisalefcam.utils;

/**
 * Created by Hau Le on 2018-09-10.
 */
public interface OptionMenuCallback {
    void itemSelected(int pos);
}
