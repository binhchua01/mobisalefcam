package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetAbility;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetSubTeam;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateAppointment;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SubTeam;
import isc.fpt.fsalecam.mobisalefcam.presenter.DeployAppointmentPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.DeployAppointmentPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.CallbackDateTime;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.ResizeDialogPercent;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.DeployAppointmentView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.ContractDetailsActivity.contractDetails;

public class DeployAppointmentActivity extends BaseActivity
        implements View.OnClickListener, DeployAppointmentView{
    @BindView(R.id.ic_icon_nav_back_set_up) RelativeLayout imgBack;
    @BindView(R.id.btn_tick_appointment) Button btnAppointment;
    @BindView(R.id.tv_partner) TextView tvPartner;
    @BindView(R.id.tv_deployment_team) TextView tvDeploymentTeam;
    @BindView(R.id.layout_choose_deployment_date) RelativeLayout loChooseDate;
    @BindView(R.id.tv_deployment_date) TextView tvDeploymentDate;
    @BindView(R.id.layout_choose_time) RelativeLayout loChooseTime;
    @BindView(R.id.tv_show_space_time) TextView tvDeploymentTime;
    @BindView(R.id.btn_view_deploy_appointment) Button btnGetAbility;
    @BindView(R.id.layout_ability) LinearLayout loAbility;
    //table see ability
    //title
    @BindView(R.id.title_col_2) TextView tvDate1;
    @BindView(R.id.title_col_3) TextView tvDate2;
    @BindView(R.id.title_col_4) TextView tvDate3;
    @BindView(R.id.title_col_5) TextView tvDate4;
    //timezone 1
    @BindView(R.id.row_1_col_2) TextView tvTimeZone11;
    @BindView(R.id.row_1_col_3) TextView tvTimeZone12;
    @BindView(R.id.row_1_col_4) TextView tvTimeZone13;
    @BindView(R.id.row_1_col_5) TextView tvTimeZone14;
    //timezone 2
    @BindView(R.id.row_2_col_2) TextView tvTimeZone21;
    @BindView(R.id.row_2_col_3) TextView tvTimeZone22;
    @BindView(R.id.row_2_col_4) TextView tvTimeZone23;
    @BindView(R.id.row_2_col_5) TextView tvTimeZone24;

    private Dialog dialog;
    private APIService apiService;
    private DeployAppointmentPresenter presenter;
    private SubTeam subTeam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        callApi();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_deploy_appointment;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_icon_nav_back_set_up:
                onBackPressed();
                break;
            case R.id.btn_tick_appointment:
                if(checkValueIsOk() && subTeam != null){
                    ReqUpdateAppointment objAppointment = new ReqUpdateAppointment();
                    objAppointment.setDeptId(subTeam.getResultCode());
                    objAppointment.setSubId(subTeam.getResultSubID());
                    objAppointment.setTimeZone(tvDeploymentTime.getText().toString());
                    objAppointment.setAppointmentDate(tvDeploymentDate.getText().toString());
                    objAppointment.setUsername(SharedPref.get(this, SharedPref.Key.USERNAME, ""));
                    objAppointment.setRegCode(contractDetails.getRegCode());
                    objAppointment.setObjId(contractDetails.getObjId());
                    //call api
                    presenter.doUpdateAppointment(this, apiService, objAppointment);
                }
                break;
            case R.id.layout_choose_deployment_date:
                DialogUtils.showDialogChooseDate(this, new CallbackDateTime() {
                    @Override
                    public void dateTime(String date) {
                        tvDeploymentDate.setText(date);
                    }
                }).show();
                break;
            case R.id.layout_choose_time:
                presenter.doGetTimeZoneList(this, apiService);
                break;
            case R.id.btn_view_deploy_appointment:
                //if don't choose date, return or show message
                if(checkValueIsOk() && subTeam != null){
                    //TODO:call API get ability
                    ReqGetAbility obj = new ReqGetAbility();
                    obj.setPartnerId(subTeam.getResultCode());
                    obj.setSubId(subTeam.getResultSubID());
                    obj.setAppointmentDate(tvDeploymentDate.getText().toString());
                    //call api
                    presenter.doGetAbility(this, apiService, obj);
                }

                break;
        }
    }

    @Override
    public void getSubTeamListSuccess(SubTeam obj) {
        subTeam = obj;
        //set data to view
        tvPartner.setText(String.valueOf(obj.getResultDepID()));
        tvDeploymentTeam.setText(String.valueOf(obj.getResultSubID()));
    }

    @Override
    public void getAbilityListSuccess(ArrayList<String> listDate,
                                      ArrayList<Integer> listTimezone1,
                                      ArrayList<Integer> listTimezone2) {
        //set data to view
        setDataToView(listDate, listTimezone1, listTimezone2);
    }

    @Override
    public void getTimeZoneSuccess(ArrayList<String> list) {
        DialogUtils.showDialogChooseTimeDeployment(this, list,
                new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                tvDeploymentTime.setText(data);
            }
        }).show();

    }

    @Override
    public void updateDeployAppointment() {
        DialogUtils.showDialogTickAppointment(this,
                getResources().getString(R.string.mes_update_appointment), new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                if(Integer.parseInt(data) == 1){
                    onBackPressed();
                }
            }
        }).show();
    }

    @Override
    public void callApiFailed(String mes) {
        Dialog dialogCallApiFailed = DialogUtils.showDialogTickAppointment(this, mes, new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                if (Integer.parseInt(data) == 1) {
                    onBackPressed();
                }
            }
        });
        dialogCallApiFailed.show();
        ResizeDialogPercent.resize(this, dialogCallApiFailed);
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        btnAppointment.setOnClickListener(this);
        loChooseDate.setOnClickListener(this);
        loChooseTime.setOnClickListener(this);
        btnGetAbility.setOnClickListener(this);
        tvDeploymentDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).isEmpty()){
                    loChooseDate.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvDeploymentDate.setTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        tvDeploymentTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).isEmpty()){
                    loChooseTime.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvDeploymentTime.setTextColor(getResources().getColor(R.color.s_black));

                }
            }
        });
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new DeployAppointmentPresenterImpl(this);
        //call api get seb team id of contract
        ReqGetSubTeam obj = new ReqGetSubTeam();
        obj.setObjId(contractDetails.getObjId());
        obj.setRegCode(contractDetails.getRegCode());
        obj.setUsername(SharedPref.get(this, SharedPref.Key.USERNAME, ""));
        presenter.doGetSubTeamIdOfContract(this, apiService, obj);
    }

    private void setDataToView(ArrayList<String> listDate,
                               ArrayList<Integer> listTimezone1,
                               ArrayList<Integer> listTimezone2) {
        /*
         * display layout choose time zone, layout ability
         * if haven't get ability -> hidden layout choose time zone, layout see ability
         * default hidden all
         * */
        loAbility.setVisibility(View.VISIBLE);
        //set date to view
        tvDate1.setText(listDate.get(0));
        tvDate2.setText(listDate.get(1));
        tvDate3.setText(listDate.get(2));
        tvDate4.setText(listDate.get(3));
        //set time zone 1
        tvTimeZone11.setText(String.valueOf(listTimezone1.get(0)));
        tvTimeZone12.setText(String.valueOf(listTimezone1.get(1)));
        tvTimeZone13.setText(String.valueOf(listTimezone1.get(2)));
        tvTimeZone14.setText(String.valueOf(listTimezone1.get(3)));
        //set time zone 2
        tvTimeZone21.setText(String.valueOf(listTimezone2.get(0)));
        tvTimeZone22.setText(String.valueOf(listTimezone2.get(1)));
        tvTimeZone23.setText(String.valueOf(listTimezone2.get(2)));
        tvTimeZone24.setText(String.valueOf(listTimezone2.get(3)));
    }

    private boolean checkValueIsOk(){
        if(tvDeploymentDate.getText().toString().equals(getResources().getString(R.string.ui_text_choose_date))) {
            loChooseDate.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvDeploymentDate.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseDate.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvDeploymentDate.setTextColor(getResources().getColor(R.color.s_black));
        }
        if(tvDeploymentTime.getText().toString().equals(getResources().getString(R.string.ui_text_choose_time))){
            loChooseTime.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvDeploymentTime.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseTime.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvDeploymentTime.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvDeploymentDate.getText().toString().equals(getResources().getString(R.string.ui_text_choose_date))){
            DialogUtils.showMessageDialogVerButton(this, getResources().getString(R.string.mes_date_required)).show();
            return false;
        }else if(tvDeploymentTime.getText().toString().equals(getResources().getString(R.string.ui_text_choose_time))){
            DialogUtils.showMessageDialogVerButton(this, getResources().getString(R.string.mes_timezone_required)).show();
            return false;
        }
        return true;
    }
}
