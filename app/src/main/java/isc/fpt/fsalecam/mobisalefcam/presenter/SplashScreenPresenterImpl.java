package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCheckImei;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCheckVersion;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCheckImei;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCheckVersion;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.SplashScreenView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-18.
 */
public class SplashScreenPresenterImpl implements SplashScreenPresenter {
    private SplashScreenView view;

    public SplashScreenPresenterImpl(SplashScreenView view) {
        this.view = view;
    }

    @Override
    public void doCheckImei(final Context context, final APIService apiService, final ReqCheckImei obj) {
        apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //generate token success
                SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                callApiCheckImeiIsActive(context, apiService, obj);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                view.checkImeiIsActiveFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    @Override
    public void doCheckCurrentVersion(final Context context, final APIService apiService, final ReqCheckVersion obj) {
        apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //generate token success
                SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                callApiCheckCurrentVersion(context, apiService, obj);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                view.checkCurrentVersionFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    @Override
    public void readImeiDevice(Context context) {
        view.getDeviceIMEI(Utils.getIMEIDevice(context));
    }

    @Override
    public void doDownloadNewVersion(final Context context, final APIService apiService,
                                     final String fileUrl) {
        view.showProgressDialog();
        apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                //generate token success
                SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                callApiDownloadNewVersion(context, apiService, fileUrl);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                view.hiddenProgressDialog();
                view.downloadNewVersionFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiDownloadNewVersion(final Context context, APIService apiService, String fileUrl){
        apiService.downloadApk(
                fileUrl,
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        String pathFile = Utils.writeFileApkToDisk(response.body());
                        if(pathFile != null){
                            view.downloadNewVersionSuccess(pathFile);
                        }else{
                            view.downloadNewVersionFailed(context.getResources().getString(R.string.cant_store_new_app_version));
                        }
                        view.hiddenProgressDialog();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.downloadNewVersionFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiCheckCurrentVersion(final Context context, APIService apiService, ReqCheckVersion obj){
        apiService.doCheckVersion(
                Utils.checkSum(obj.objToString()),
                Utils.CONTENT_TYPE,
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                obj.generateObjCheckVersion())
                .enqueue(new Callback<ResCheckVersion>() {
                    @Override
                    public void onResponse(Call<ResCheckVersion> call, Response<ResCheckVersion> response) {
                        validateReturnCodeCheckCurrentVersion(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResCheckVersion> call, Throwable t) {
                        view.checkCurrentVersionFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiCheckImeiIsActive(final Context context, APIService apiService, ReqCheckImei obj){
        apiService.doCheckImei(
            Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
            Utils.CONTENT_TYPE,
            Utils.checkSum(obj.objToString()),
            obj.generateObjCheckImei())
            .enqueue(new Callback<ResCheckImei>() {
                @Override
                public void onResponse(Call<ResCheckImei> call, Response<ResCheckImei> response) {
                    validateReturnCodeCheckImeiIsActive(context, response);
                }

                @Override
                public void onFailure(Call<ResCheckImei> call, Throwable t) {
                    view.checkImeiIsActiveFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    private void validateReturnCodeCheckImeiIsActive(Context context, Response<ResCheckImei> response){
        if(response.body()!=null){
            if(response.body().getCode() == Utils.SUCCESS){
                //get string address from objReport return
                view.checkImeiIsActiveSuccess(response.body().getData().get(0));
            }else{
                view.checkImeiIsActiveFailed(response.body().getMessage());
            }
        }else{
            view.checkImeiIsActiveFailed(context.getResources().getString(R.string.mes_check_imei_err));
        }
    }

    private void validateReturnCodeCheckCurrentVersion(Context context, Response<ResCheckVersion> response){
        if(response.body()!=null){
            if(response.body().getCode() == Utils.SUCCESS){
                //get string address from objReport return
                view.checkCurrentVersionSuccess(response.body().getData().get(0));
            }else{
                view.checkCurrentVersionFailed(response.body().getMessage());
            }
        }else{
            view.checkCurrentVersionFailed(context.getResources().getString(R.string.server_err));
        }
    }

}
