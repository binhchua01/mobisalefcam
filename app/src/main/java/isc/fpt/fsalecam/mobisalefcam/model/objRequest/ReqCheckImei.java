package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-17.
 */
public class ReqCheckImei {
    private String imei;
    private String androidVersion;
    private String modelNumber;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public LinkedHashMap<String, String> generateObjCheckImei(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("DeviceIMEI", getImei());
        obj.put("AndroidVersion", getAndroidVersion());
        obj.put("ModelNumber", getModelNumber());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("DeviceIMEI", getImei());
            obj.put("AndroidVersion", getAndroidVersion());
            obj.put("ModelNumber", getModelNumber());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
