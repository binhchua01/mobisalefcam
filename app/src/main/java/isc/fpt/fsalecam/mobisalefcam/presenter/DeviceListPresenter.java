package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDeviceList;

/**
 * Created by Hau Le on 2018-08-22.
 */
public interface DeviceListPresenter {
    void deGetDeviceList(Context context, APIService apiService,
                         ReqDeviceList obj, Realm realm);
}
