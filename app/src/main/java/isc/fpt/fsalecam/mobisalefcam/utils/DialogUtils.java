package isc.fpt.fsalecam.mobisalefcam.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.adapter.TimeZoneAdapter;
import isc.fpt.fsalecam.mobisalefcam.view.activities.ActiveImeiActivity;
import isc.fpt.fsalecam.mobisalefcam.view.activities.LoginActivity;
import isc.fpt.fsalecam.mobisalefcam.adapter.ChooseCityAdapter;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.ChooseKeySearchAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.ConnectionFeeAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.CusTypeAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.DepositAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.NationalityAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.PaymentMethodAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.RegistrationSearchTypeAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.ServiceTypeAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.TypePaymentAdapter;
import isc.fpt.fsalecam.mobisalefcam.adapter.VatAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.customView.SimpleDrawingView;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqChangePass;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.database.ConnectionFee;
import isc.fpt.fsalecam.mobisalefcam.database.CusInfoPaymentMethod;
import isc.fpt.fsalecam.mobisalefcam.database.DepositFee;
import isc.fpt.fsalecam.mobisalefcam.database.Nationality;
import isc.fpt.fsalecam.mobisalefcam.presenter.MainPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.City;
import isc.fpt.fsalecam.mobisalefcam.database.CustomerType;
import isc.fpt.fsalecam.mobisalefcam.database.PaymentType;
import isc.fpt.fsalecam.mobisalefcam.database.PotentialCusTypeSearch;
import isc.fpt.fsalecam.mobisalefcam.database.RegistrationSearchType;
import isc.fpt.fsalecam.mobisalefcam.database.Vat;

/**
 * Created by Hau Le on 2018-07-05.
 */
public class DialogUtils {

    public static Dialog showLoadingDialog(Context context, String str) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.progress_dialog);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvMess = dialog.findViewById(R.id.tv_message);
        tvMess.setText(str);
        return dialog;
    }

    public static AlertDialog showMessageDialog(Context context, String str) {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(str)
            .setPositiveButton(R.string.agree, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                   dialog.dismiss();
                }
            });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public static AlertDialog showQuestionExitApp(final Context context, String str) {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.notification))
                .setMessage(str)
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity)context).finish();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public static Dialog showActiveImei(final Context context) {
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_active_imei);
        dialog.setCancelable(false);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel_dialog);
        TextView tvActive = dialog.findViewById(R.id.tv_active_dialog);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: progress active emei
                context.startActivity(new Intent(context, ActiveImeiActivity.class));
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showMessageDialogVerButton(final Context context, String str) {
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_notification_with_button);
        dialog.setCancelable(false);
        TextView tvMessage = dialog.findViewById(R.id.tv_message);
        tvMessage.setText(str);
        TextView tvOK = dialog.findViewById(R.id.tv_ok_dialog);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogBookportAutoFailed(final Context context,
                                                      final ContinueRegisterService continueRegisterService) {
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_required_create_potential_customer);
        dialog.setCancelable(false);
        TextView tvBookport = dialog.findViewById(R.id.tv_bookport);
        tvBookport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                continueRegisterService.continueRegister();
            }
        });
        return dialog;
    }

    public static Dialog showDialogBookportManualFailed(final Context context, String mes,
                                                        final CreatePotentialCustomer createPotentialCustomer){
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_create_potential_cus_info);
        dialog.setCancelable(false);
        TextView tvCreatePotentialCus = dialog.findViewById(R.id.tv_create_potential_cus);
        TextView tvContent = dialog.findViewById(R.id.tv_content_mes);
        TextView tvBookport = dialog.findViewById(R.id.tv_bookport_manual);
        tvContent.setText(mes);
        tvCreatePotentialCus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                createPotentialCustomer.potentialCustomer();
            }
        });
        tvBookport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((Activity)context).finish();

            }
        });
        return dialog;
    }

    public static Dialog showDialogAskForContinue(final Context context,
                                                    final CreatePotentialCustomer createPotentialCustomer,
                                                    final  ContinueRegisterService continueRegisterService) {
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_ask_for_continue);
        dialog.setCancelable(false);
        TextView tvSkip = dialog.findViewById(R.id.tv_skip);
        TextView tvContinue = dialog.findViewById(R.id.tv_continue);

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                createPotentialCustomer.potentialCustomer();

            }
        });

        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                continueRegisterService.continueRegister();
            }
        });
        return dialog;
    }

    public static Dialog dialogResetPassword(final Context context, final APIService apiService, final MainPresenter presenter){
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        //set content dialog
        dialog.setContentView(R.layout.dialog_reset_password);
        //show key board
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        //declare param
        final RelativeLayout loCurrentPass = dialog.findViewById(R.id.layout_current_pass);
        final EditText edtCurrentPass = dialog.findViewById(R.id.edt_current_pass);
        final RelativeLayout loNewPass = dialog.findViewById(R.id.layout_new_pass);
        final EditText edtNewPass = dialog.findViewById(R.id.edt_new_pass);
        final RelativeLayout loConfirmPass = dialog.findViewById(R.id.layout_confirm_pass);
        final EditText edtConfirmPass = dialog.findViewById(R.id.edt_confirm_pass);
        Button btnChangePass = dialog.findViewById(R.id.btn_change_pass);
        RelativeLayout imgClose = dialog.findViewById(R.id.close_dialog_reset_password);
        //add text change event
        edtCurrentPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loCurrentPass.setBackground(context.getResources().getDrawable(R.drawable.background_text_view_reset_password));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(String.valueOf(editable).equals("")){
                    loCurrentPass.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_error));
                }else{
                    loCurrentPass.setBackground(context.getResources().getDrawable(R.drawable.background_text_view_reset_password));
                }
            }
        });
        edtNewPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loNewPass.setBackground(context.getResources().getDrawable(R.drawable.background_text_view_reset_password));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(String.valueOf(editable).equals("")){
                    loNewPass.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_error));
                }else{
                    loNewPass.setBackground(context.getResources().getDrawable(R.drawable.background_text_view_reset_password));
                }
            }
        });
        edtConfirmPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                loConfirmPass.setBackground(context.getResources().getDrawable(R.drawable.background_text_view_reset_password));
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(String.valueOf(editable).equals("")){
                    loConfirmPass.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_error));
                }else{
                    loConfirmPass.setBackground(context.getResources().getDrawable(R.drawable.background_text_view_reset_password));
                }
            }
        });
        //don't dismiss dialog
        dialog.setCancelable(false);
        //set close dialog
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                dialog.dismiss();
            }
        });
        //add onclick event
        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edtCurrentPass.getText().toString().isEmpty() &&
                        !edtNewPass.getText().toString().isEmpty() &&
                        !edtConfirmPass.getText().toString().isEmpty()){
                    //check new pass the same confirm pass
                    if(Utils.hashPassword(edtNewPass.getText().toString())
                            .equals(Utils.hashPassword(edtConfirmPass.getText().toString()))){
                        //put data to obj
                        ReqChangePass obj = new ReqChangePass();
                        obj.setUsername(SharedPref.get(context, SharedPref.Key.USERNAME, ""));
                        obj.setNewPass(Utils.hashPassword(edtNewPass.getText().toString()));
                        obj.setOldPass(Utils.hashPassword(edtCurrentPass.getText().toString()));
                        //call Api change password
                        presenter.doChangePassword(context, apiService, obj);
                    }else{
                        loConfirmPass.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_error));
                    }
                }else{
                    //for each layout
                    if(edtCurrentPass.getText().toString().isEmpty()){
                        loCurrentPass.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_error));
                    }
                    if(edtNewPass.getText().toString().isEmpty()){
                        loNewPass.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_error));
                    }
                    if(edtConfirmPass.getText().toString().isEmpty()){
                        loConfirmPass.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_error));
                    }
                }
                ((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });
        return dialog;
    }

    //dialog use for signature of customer
    public static Dialog dialogSignature(Context context, final ReturnBitmap returnBitmap){
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_signature);
        dialog.setCancelable(true);
        //declare view in dialog
        Button btnClearCanvas = dialog.findViewById(R.id.btn_clear_canvas);
        Button btnExportBitmap = dialog.findViewById(R.id.btn_export_bitmap);
        final SimpleDrawingView canvas = dialog.findViewById(R.id.canvas_signature);

        //clear signature
        btnClearCanvas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                canvas.clearView();
            }
        });

        //confirm signature, and set to view
        btnExportBitmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnBitmap.bitmapExport(canvas.exportBitmapFromView(canvas));
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog dialogChooseCustomerType(Context context, final ArrayList<CustomerType> list,
                                                  final DialogCallbackDataToActivity customerType){
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_customer_type);
        dialog.setCancelable(false);
        //declare view in dialog
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_customer_type);
        RelativeLayout loClose = dialog.findViewById(R.id.img_close_dialog_choose_cus_type);
        CusTypeAdapter adapter = new CusTypeAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                customerType.dataCallback(list.get(position).getId() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });

        loClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);
        return dialog;
    }

    public static Dialog dialogChooseNationality(Context context, final ArrayList<Nationality> list,
                                                 final DialogCallbackDataToActivity chooseNationality){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.choose_nationality);
        dialog.setCancelable(false);
        //declare view in dialog
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_nationality);
        RelativeLayout loClose = dialog.findViewById(R.id.img_close_dialog_choose_nationality);
        NationalityAdapter adapter = new NationalityAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                chooseNationality.dataCallback(list.get(position).getValue() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });

        loClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);
        return dialog;
    }

    public static Dialog showDialogRequiredLoginAgain(final Context context, String str) {
        // Use the Builder class for convenient dialog construction
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_required_login_again);
        dialog.setCancelable(false);
        TextView tvMessage = dialog.findViewById(R.id.tv_message_login_again);
        tvMessage.setText(str);
        TextView tvOK = dialog.findViewById(R.id.tv_required_login_again);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                //route to login activity
                context.startActivity(new Intent(context, LoginActivity.class));
                ((Activity)context).finishAffinity();
            }
        });
        return dialog;
    }

    public static Dialog showDialogChooseCity(final Context context, Realm realm,
                                              final DialogCallbackDataToActivity callback){
        //get list city from DB local and cast to array list
        final ArrayList<City> arrayList = (ArrayList<City>) realm.copyFromRealm(realm.where(City.class).findAll());
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_city);
        dialog.setCancelable(false);
        ImageView imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_city);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_city);
        ChooseCityAdapter adapter = new ChooseCityAdapter(context, arrayList,
                new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //callbackDeviceList data when click on item in dialog
                callback.dataCallback(arrayList.get(position).getId() + ":" + arrayList.get(position).getName());
                dialog.dismiss();
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogChooseServiceType(Context context, final ArrayList<ItemOfListRes> list,
                                                     ArrayList<ItemOfListRes> listCheck, final SelectServiceTypeCallback callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_service_type);
        dialog.setCancelable(false);
        RelativeLayout imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_service_type);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_service_type);
        ServiceTypeAdapter adapter = new ServiceTypeAdapter(context, list, listCheck, new SelectServiceTypeCallback() {
            @Override
            public void serviceTypeList(ArrayList<String> list) {
                //callbackDeviceList to activity
                callback.serviceTypeList(list);
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static DatePickerDialog showDialogChooseDate(Context context, final CallbackDateTime callback){
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(context,
            new DatePickerDialog.OnDateSetListener(){
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    callback.dateTime((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                }
            }, mYear, mMonth, mDay);
    }

    public static Dialog showDialogChoosePaymentMethod(Context context, final ArrayList<CusInfoPaymentMethod> list,
                                                       final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_payment_method);
        dialog.setCancelable(false);
        RelativeLayout imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_payment_method);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_payment_method);
        PaymentMethodAdapter adapter = new PaymentMethodAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                callback.dataCallback(list.get(position).getId() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogChooseTypePayment(Context context, final ArrayList<PaymentType> list,
                                                       final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_payment_method);
        dialog.setCancelable(false);
        RelativeLayout imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_payment_method);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_payment_method);
        TypePaymentAdapter adapter = new TypePaymentAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                callback.dataCallback(list.get(position).getId() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogChooseConnectionFee(Context context, final ArrayList<ConnectionFee> list,
                                                       final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_connection_fee);
        dialog.setCancelable(false);
        RelativeLayout imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_connection_fee);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_connection_fee);
        ConnectionFeeAdapter adapter = new ConnectionFeeAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                callback.dataCallback(list.get(position).getId() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogUpdateRegistrationSuccess(final Context context, String strMes,
                                                             ContinueRegisterService continueRegisterService){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_update_registration_success);
        TextView tvMes = dialog.findViewById(R.id.tv_content_mes);
        tvMes.setText(strMes);
        dialog.setCancelable(false);
        //callback
        continueRegisterService.continueRegister();
        return dialog;
    }

    public static Dialog showDialogChooseKeySearch(Context context, String title ,final ArrayList<PotentialCusTypeSearch> list,
                                                   final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_connection_fee);
        dialog.setCancelable(false);
        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        tvTitle.setText(title);
        RelativeLayout imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_connection_fee);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_connection_fee);
        ChooseKeySearchAdapter adapter = new ChooseKeySearchAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                callback.dataCallback(list.get(position).getId() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogRegitsrationSearchType(Context context, String title ,
                                                          final ArrayList<RegistrationSearchType> list,
                                                   final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_connection_fee);
        dialog.setCancelable(false);
        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        tvTitle.setText(title);
        RelativeLayout imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_connection_fee);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_connection_fee);
        RegistrationSearchTypeAdapter adapter = new RegistrationSearchTypeAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                callback.dataCallback(list.get(position).getId() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogChooseVat(Context context, String title ,final ArrayList<Vat> list,
                                                   final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_connection_fee);
        dialog.setCancelable(false);
        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        tvTitle.setText(title);
        RelativeLayout imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_connection_fee);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_connection_fee);
        VatAdapter adapter = new VatAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                callback.dataCallback(list.get(position).getId() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogChooseDepositFee(Context context, String title ,final ArrayList<DepositFee> list,
                                             final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_connection_fee);
        dialog.setCancelable(false);
        TextView tvTitle = dialog.findViewById(R.id.tv_title);
        tvTitle.setText(title);
        RelativeLayout imgCloseDialog = dialog.findViewById(R.id.img_close_dialog_choose_connection_fee);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_choose_connection_fee);
        DepositAdapter adapter = new DepositAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                callback.dataCallback(list.get(position).getId() + ":" + list.get(position).getName());
                dialog.dismiss();
            }
        });
        //set adapter
        recyclerView.setAdapter(adapter);
        //close dialog
        imgCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogAskForUpdatePayment(Context context, String contractId, final ContinueRegisterService callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_confirm_update_payment);
        dialog.setCancelable(false);
        TextView tvContractId = dialog.findViewById(R.id.tv_contract_id);
        TextView tvCancel = dialog.findViewById(R.id.tv_cancel_dialog);
        TextView tvConfirm = dialog.findViewById(R.id.tv_confirm_dialog);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                callback.continueRegister();
            }
        });
        tvContractId.setText(contractId);
        return dialog;
    }

    public static Dialog showOptionMenu(Context context, final OptionMenuCallback itemSelected){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.option_menu);
        dialog.setCancelable(false);
        RelativeLayout loUpdateTotal = dialog.findViewById(R.id.layout_update_total_money);
        TextView tvUpdateTotal = dialog.findViewById(R.id.tv_update_total);
        RelativeLayout loDeployAppointment = dialog.findViewById(R.id.layout_deploy_appointment);
        TextView tvDeployAppointment = dialog.findViewById(R.id.tv_item_deploy_appointment);
        RelativeLayout tvCancel = dialog.findViewById(R.id.img_close_dialog_signature);
        RelativeLayout loDownloadContract = dialog.findViewById(R.id.layout_download_contract_pdf);
//        if(actionUpdateTotal == 1){
//            //enable function update total
//            loUpdateTotal.setBackground(context.getResources().getDrawable(R.drawable.background_item_choose_district));
//            tvUpdateTotal.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//            loUpdateTotal.setEnabled(true);
//        }else{
//            loUpdateTotal.setBackground(context.getResources().getDrawable(R.drawable.background_spinner));
//            tvUpdateTotal.setTextColor(context.getResources().getColor(R.color.s_grey_d0));
//            loUpdateTotal.setEnabled(false);
//        }
//
//        if(actionDeployAppointment == 1){
//            //enable function deploy appointment
//            loDeployAppointment.setBackground(context.getResources().getDrawable(R.drawable.background_item_choose_district));
//            tvDeployAppointment.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//            loDeployAppointment.setEnabled(true);
//        }else{
//            loDeployAppointment.setBackground(context.getResources().getDrawable(R.drawable.background_spinner));
//            tvDeployAppointment.setTextColor(context.getResources().getColor(R.color.s_grey_d0));
//            loDeployAppointment.setEnabled(false);
//        }
        //set event
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        loDeployAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemSelected.itemSelected(0);
                dialog.dismiss();
            }
        });
        loUpdateTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemSelected.itemSelected(1);
                dialog.dismiss();
            }
        });
        loDownloadContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemSelected.itemSelected(2);
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogCreatePotentialCus(Context context,
                                                      final ContinueRegisterService continueRegisterService,
                                                      final CreatePotentialCustomer createPotentialCustomer){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_create_potential_cus_info);
        dialog.setCancelable(false);
        TextView tvCreatePotential = dialog.findViewById(R.id.tv_create_potential_cus);
        TextView tvBookportManual = dialog.findViewById(R.id.tv_bookport_manual);
        tvCreatePotential.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPotentialCustomer.potentialCustomer();
                dialog.dismiss();
            }
        });
        tvBookportManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                continueRegisterService.continueRegister();
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogChooseTimeDeployment(Context context, final ArrayList<String> list,
                                                        final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_time_deployment);
        dialog.setCancelable(false);
        RelativeLayout loClose = dialog.findViewById(R.id.close_dialog_choose_time_deployment);
        loClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view_time_zone);
        TimeZoneAdapter adapter = new TimeZoneAdapter(context, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                callback.dataCallback(list.get(position));
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);
        return dialog;
    }

    public static Dialog showDialogChooseContractStatus(Context context, final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_choose_contract_status);
        dialog.setCancelable(false);
        RelativeLayout loClose = dialog.findViewById(R.id.close_dialog);
        loClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        final TextView tvAll = dialog.findViewById(R.id.tv_all);
        tvAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.dataCallback(0 + ":" + tvAll.getText().toString());
                dialog.dismiss();
            }
        });
        final TextView tvOnline = dialog.findViewById(R.id.tv_online);
        tvOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.dataCallback(1 + ":" + tvOnline.getText().toString());
                dialog.dismiss();
            }
        });
        final TextView tvNotYetOnline = dialog.findViewById(R.id.tv_not_yet_online);
        tvNotYetOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.dataCallback(2 + ":" + tvNotYetOnline.getText().toString());
                dialog.dismiss();
            }
        });
        return dialog;
    }

    public static Dialog showDialogTickAppointment(Context context, String mesContent,
                                                   final DialogCallbackDataToActivity callback){
        final Dialog dialog = new Dialog(context);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_tick_appoiment_failed);
        dialog.setCancelable(false);
        TextView tvMesContent = dialog.findViewById(R.id.tv_mes_content);
        TextView tvOk = dialog.findViewById(R.id.tv_ok);
        tvMesContent.setText(mesContent);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.dataCallback("1");
            }
        });
        return dialog;
    }

    public static AlertDialog confirmDialog(Context context, final DialogCallbackDataToActivity callback){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        // set title
        alertDialogBuilder.setTitle("Confirm");
        // set dialog message
        alertDialogBuilder
                .setMessage("Do you want cancel to register?")
                .setCancelable(true)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        callback.dataCallback("");
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        return alertDialogBuilder.create();
    }
}
