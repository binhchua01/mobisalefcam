package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-09-17.
 */
public class ReportContract {
    @SerializedName("RegID")
    @Expose
    private Integer regID;
    @SerializedName("RegCode")
    @Expose
    private String regCode;
    @SerializedName("ObjID")
    @Expose
    private Integer objID;
    @SerializedName("Contract")
    @Expose
    private String contract;
    @SerializedName("ObjCreateDate")
    @Expose
    private String objCreateDate;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("StatusName")
    @Expose
    private String statusName;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("LocalTypeName")
    @Expose
    private String localTypeName;

    public Integer getRegID() {
        return regID;
    }

    public void setRegID(Integer regID) {
        this.regID = regID;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public Integer getObjID() {
        return objID;
    }

    public void setObjID(Integer objID) {
        this.objID = objID;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getObjCreateDate() {
        return objCreateDate;
    }

    public void setObjCreateDate(String objCreateDate) {
        this.objCreateDate = objCreateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLocalTypeName() {
        return localTypeName;
    }

    public void setLocalTypeName(String localTypeName) {
        this.localTypeName = localTypeName;
    }
}
