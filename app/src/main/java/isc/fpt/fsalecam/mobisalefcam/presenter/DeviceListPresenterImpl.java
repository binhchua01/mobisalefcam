package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDeviceList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Device;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetDeviceList;
import isc.fpt.fsalecam.mobisalefcam.database.DeviceList;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.DeviceListView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-22.
 */
public class DeviceListPresenterImpl implements DeviceListPresenter {
    private DeviceListView view;

    public DeviceListPresenterImpl(DeviceListView view) {
        this.view = view;
    }

    @Override
    public void deGetDeviceList(final Context context, final APIService apiService,
                                final ReqDeviceList obj, final Realm realm) {
        //show dialog
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetDeviceList(context, apiService, obj, realm);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetDeviceList(context, apiService, obj, realm);
        }
    }

    private void callApiGetDeviceList(final Context context, APIService apiService, ReqDeviceList obj, final Realm realm){
        apiService.doGetDeviceList(Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetDeviceList()).enqueue(new Callback<ResGetDeviceList>() {
            @Override
            public void onResponse(Call<ResGetDeviceList> call, Response<ResGetDeviceList> response) {
                validateReturnCode(context, response, realm);
            }

            @Override
            public void onFailure(Call<ResGetDeviceList> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCode(Context context, Response<ResGetDeviceList> response, Realm realm){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                //save device list to local
                saveDeviceListLocal((ArrayList<Device>) response.body().getData(), realm);
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else {
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else {
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void saveDeviceListLocal(ArrayList<Device> list, Realm realm){
        realm.beginTransaction();
        //delete all old value
        realm.where(DeviceList.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        //add new value
        for (int i=0; i<list.size(); i++){
            realm.beginTransaction();
            DeviceList deviceList = realm.createObject(DeviceList.class);
            deviceList.setChangeNumber(String.valueOf(list.get(i).getChangeNumber()));
            deviceList.setName(list.get(i).getName());
            deviceList.setPrice(String.valueOf(list.get(i).getPrice()));
            deviceList.setValue(String.valueOf(list.get(i).getValue()));
            realm.commitTransaction();
        }
        view.getDeviceListSuccess((ArrayList<DeviceList>) realm.copyFromRealm(realm.where(DeviceList.class).findAll()));
    }
}
