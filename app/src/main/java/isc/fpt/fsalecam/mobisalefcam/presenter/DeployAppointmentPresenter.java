package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetAbility;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetSubTeam;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateAppointment;

/**
 * Created by Hau Le on 2018-09-13.
 */
public interface DeployAppointmentPresenter {
    void doGetSubTeamIdOfContract(Context context, APIService apiService, ReqGetSubTeam obj);
    void doGetAbility(Context context, APIService apiService, ReqGetAbility obj);
    void doUpdateAppointment(Context context, APIService apiService, ReqUpdateAppointment obj);
    void doGetTimeZoneList(Context context, APIService apiService);
}
