package isc.fpt.fsalecam.mobisalefcam.utils;

import java.util.ArrayList;

/**
 * Created by Hau Le on 2018-08-16.
 */
public interface SelectServiceTypeCallback {
    void serviceTypeList(ArrayList<String> list);
}
