package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-08-17.
 */
public class Device {
    @SerializedName("Value")
    @Expose
    private String value;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ChangeNumber")
    @Expose
    private Integer changeNumber;
    @SerializedName("Price")
    @Expose
    private Integer price;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getChangeNumber() {
        return changeNumber;
    }

    public void setChangeNumber(Integer changeNumber) {
        this.changeNumber = changeNumber;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
