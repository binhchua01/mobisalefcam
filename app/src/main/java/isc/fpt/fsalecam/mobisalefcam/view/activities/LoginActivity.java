package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import java.util.Objects;
import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.presenter.LoginPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.LoginPresenter;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.LoginView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.SplashScreenActivity.imeiIsActive;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.SplashScreenActivity.objCheckImei;

public class LoginActivity extends BaseActivity implements View.OnClickListener, LoginView {
    @BindDrawable(R.drawable.ic_icon_password_off) Drawable icIconPassOff;
    @BindDrawable(R.drawable.ic_icon_password_on) Drawable icIconPassOn;
    @BindDrawable(R.drawable.ic_icon_user_off) Drawable icIconUserOff;
    @BindDrawable(R.drawable.ic_icon_user_on) Drawable icIconUserOn;

    @BindColor(R.color.colorPrimary) int textBlue;
    @BindColor(R.color.s_black) int textBlack;

    @BindString(R.string.user_required) String userRequired;
    @BindString(R.string.pass_required) String passRequired;
    @BindString(R.string.press_back_again_to_exit) String messageExitApp;
    @BindString(R.string.mess_login) String messLogin;

    @BindView(R.id.edt_username) EditText edtUsername;
    @BindView(R.id.edt_password) EditText edtPassword;
    @BindView(R.id.ic_icon_username) ImageView icIconUser;
    @BindView(R.id.ic_icon_password) ImageView icIconPass;
    @BindView(R.id.layout_login_wrapper) RelativeLayout layoutWrapper;
    @BindView(R.id.btn_get_info_emei) Button btnInfoImei;
    @BindView(R.id.btn_login) Button btnLogin;
    @BindView(R.id.tv_app_version_name) TextView tvVersionName;

    private static final int MY_PERMISSIONS_REQUEST = 100;
    private LoginPresenter presenter;
    private APIService apiService;
    private boolean mBack;
    private Dialog progressDialog;
    private String deviceToken = "";
    private String imei = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, layoutWrapper);
        initView();
        setEvent();
        paramSystem();
    }

    private void paramSystem() {
        //ask permission get imei
        askForPermission();
        //get device token
        getDeviceToken();
    }

    private void initView() {
        presenter = new LoginPresenterImpl(this);
        //get instance api service
        apiService = ApiUtils.getApiService();
        //check edt state and changed view
        edtUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    icIconUser.setImageDrawable(icIconUserOn);
                    edtUsername.setTextColor(textBlue);
                }else{
                    icIconUser.setImageDrawable(icIconUserOff);
                    edtUsername.setTextColor(textBlack);
                }
            }
        });
        edtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    icIconPass.setImageDrawable(icIconPassOn);
                    edtPassword.setTextColor(textBlue);
                }else{
                    icIconPass.setImageDrawable(icIconPassOff);
                    edtPassword.setTextColor(textBlack);
                }
            }
        });
        //check if imei is active, set username to edit text
        if(objCheckImei != null && imeiIsActive == 1){
            edtUsername.setText(objCheckImei.getUserName());
        }else{
            DialogUtils.showActiveImei(this).show();
        }
        //set version name
        tvVersionName.setText(Utils.getVersionName(this));
    }

    private void setEvent() {
        btnInfoImei.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_login;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_get_info_emei:
                startActivity(new Intent(this, ActiveImeiActivity.class));
                break;
            case R.id.btn_login:
                //call function progress login
                presenter.progressLogin(this, edtUsername.getText().toString(),
                        edtPassword.getText().toString(), imei, deviceToken, apiService);
                break;
        }
    }

    @Override
    public void loginSuccess() {
        //TODO:progress data when login success
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void callApiFailed(String message) {
        //TODO: progress show message when login failed
        DialogUtils.showMessageDialogVerButton(this, message).show();
    }

    @Override
    public void userError() {
        edtUsername.setError(userRequired);
    }

    @Override
    public void passError() {
        edtPassword.setError(passRequired);
    }

    @Override
    public void showDialogProgress() {
        hiddenDialogProgress();
        progressDialog = DialogUtils.showLoadingDialog(this, messLogin);
        progressDialog.show();
    }

    @Override
    public void hiddenDialogProgress() {
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    @Override
    public void returnIMEI(String imei) {
        this.imei = imei;
        //save imei
        SharedPref.put(this, SharedPref.Key.IMEI, imei);
    }

    @Override
    public void onBackPressed() {
        //quit app
        if (mBack){
            finish();
            return;
        }
        mBack = true;
        showSnackbar(this, messageExitApp, layoutWrapper);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mBack = false;
            }
        },2000);
    }

    private void getDeviceToken(){
        // Get token
        FirebaseInstanceId.getInstance().getInstanceId()
            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (!task.isSuccessful()) {
                        Log.w("TAG", "getInstanceId failed", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    deviceToken = task.getResult().getToken();
                }
            });
    }

    private void askForPermission() {
        boolean hasPermission = ContextCompat.checkSelfPermission(Objects.requireNonNull(this), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
        if(hasPermission){
            //permission accept
            presenter.readImeiDevice(this);
        }else{
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                presenter.readImeiDevice(this);
            } else {
                DialogUtils.showMessageDialog(this, getResources().getString(R.string.mes_cant_read_imei)).show();
            }
        }
    }
}
