package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGenerateAddress;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqPotentialCus;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.PotentialCusResult;
import isc.fpt.fsalecam.mobisalefcam.presenter.NewPotentialCusPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.NewPotentialCusPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.ContinueRegisterService;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.ResizeDialogPercent;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.NewPotentialCusView;

import static isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity.setUpObj;

public class NewPotentialCusActivity extends BaseActivity
        implements View.OnClickListener, NewPotentialCusView{
    @BindView(R.id.ic_nav_back) RelativeLayout loBack;
    @BindView(R.id.layout_new_potential_cus) LinearLayout loParent;
    @BindView(R.id.layout_choose_city) RelativeLayout loChooseCity;
    @BindView(R.id.layout_choose_district) RelativeLayout loChooseDistrict;
    @BindView(R.id.layout_choose_ward) RelativeLayout loChooseWard;
    @BindView(R.id.layout_house_type) RelativeLayout loChooseHomeType;
    @BindView(R.id.layout_choose_street) RelativeLayout loChooseStreet;
    @BindView(R.id.layout_full_name) RelativeLayout loFullName;
    @BindView(R.id.layout_phone) RelativeLayout loPhone;
    @BindView(R.id.layout_home_number) RelativeLayout loHomeNumber;
    @BindView(R.id.tv_city) TextView tvCity;
    @BindView(R.id.tv_district) TextView tvDistrict;
    @BindView(R.id.tv_ward) TextView tvWard;
    @BindView(R.id.tv_street_name) TextView tvStreet;
    @BindView(R.id.tv_house_type) TextView tvHouseType;
    @BindView(R.id.edt_home_number) EditText edtHomeNumber;
    @BindView(R.id.edt_person_nal_full_name) EditText edtCusName;
    @BindView(R.id.edt_phone) EditText edtPhone;
    @BindView(R.id.btn_create_potential_cus) Button btnCreatePotentialCus;
    @BindView(R.id.edt_note) EditText edtNote;

    private Realm realm;
    private ReqPotentialCus potentialCus = new ReqPotentialCus();
    private APIService apiService;
    private NewPotentialCusPresenter presenter;
    private Dialog dialog;
    private boolean canBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        initDB();
        getDataIntent();
        fillDataWhenBookportFailed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        iniParamCallApi();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        setUpObj();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_new_potential_cus;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_nav_back:
                onBackPressed();
                break;
            case R.id.layout_choose_city:
                letChooseCity();
                break;
            case R.id.layout_choose_district:
                letChooseDistrict();
                break;
            case R.id.layout_choose_ward:
                letChooseWard();
                break;
            case R.id.layout_house_type:
                letChooseHomeType();
                break;
            case R.id.layout_choose_street:
                letChooseStreet();
                break;
            case R.id.btn_create_potential_cus:
                createPotentialCus();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null){
            if(requestCode == Utils.REQUEST_CODE_DISTRICT) {
                String[] district = data.getStringExtra(Utils.DISTRICT).split(":");
                tvDistrict.setText(district[1]);
                //put data to objReport
                potentialCus.setDistrictId(Integer.parseInt(district[0]));
                potentialCus.setBillToDistrict(district[1]);
            }else if(requestCode == Utils.REQUEST_CODE_WARD){
                String[] ward = data.getStringExtra(Utils.WARD).split(":");
                tvWard.setText(ward[1]);
                //put data objReport
                potentialCus.setWardId(Integer.parseInt(ward[0]));
                potentialCus.setBillToWard(ward[1]);
            }else if(requestCode == Utils.REQUEST_CODE_STREET){
                String[] street = data.getStringExtra(Utils.STREET).split(":");
                tvStreet.setText(street[1]);
                potentialCus.setStreetId(Integer.parseInt(street[0]));
                potentialCus.setBillToStreet(street[1]);
            }else if(requestCode == Utils.REQUEST_CODE_HOME_TYPE){
                String[] homeType = data.getStringExtra(Utils.HOME_TYPE).split(":");
                tvHouseType.setText(homeType[1]);
                potentialCus.setHomeType(Integer.parseInt(homeType[0]));
                //id = 4 ->building
//                if(Integer.parseInt(homeType[0]) == 4){
//                    loChooseBuilding.setVisibility(View.VISIBLE);
//                }else{
//                    loChooseBuilding.setVisibility(View.GONE);
//                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(canBack){return;}
        startActivity(new Intent(this, PotentialCusListActivity.class));
        finish();
    }

    @Override
    public void createPotentialCusSuccess(PotentialCusResult obj) {
        Dialog createPotentialCusSuccess = DialogUtils.showDialogUpdateRegistrationSuccess(this,
                obj.getResult(),
                new ContinueRegisterService() {
                    @Override
                    public void continueRegister() {
                        int SPLASH_DISPLAY_LENGTH = 2000;
                        Handler handler = new Handler();
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                onBackPressed();
                            }
                        };
                        handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);
                    }
                });
        createPotentialCusSuccess.show();
        ResizeDialogPercent.resize(this, createPotentialCusSuccess);
    }

    @Override
    public void generateStringAddressSuccess(String address) {
        potentialCus.setAddress(address);
        //call API create potential cus
        presenter.doCreatePotentialCus(this, apiService, potentialCus);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    private void iniParamCallApi() {
        apiService = ApiUtils.getApiService();
        presenter = new NewPotentialCusPresenterImpl(this);
    }

    private void initDB() {
        realm = Realm.getDefaultInstance();
    }

    //TODO: fill data when bookport failed and create potential customer
    private void fillDataWhenBookportFailed() {
        if(!Utils.updateCusInfo.getBillToCity().equals("")){
            tvCity.setText(Utils.updateCusInfo.getBillToCity());
            potentialCus.setBillToCity(Utils.updateCusInfo.getBillToCity());
            potentialCus.setLocationId(Integer.parseInt(Utils.updateCusInfo.getLocationId()));
        }
        if(!Utils.updateCusInfo.getBillToDistrict().equals("")){
            tvDistrict.setText(Utils.updateCusInfo.getBillToDistrict());
            potentialCus.setBillToDistrict(Utils.updateCusInfo.getBillToDistrict());
            potentialCus.setDistrictId(Integer.parseInt(Utils.updateCusInfo.getDistrictId()));
        }
        if(!Utils.updateCusInfo.getBillToWard().equals("")){
            tvWard.setText(Utils.updateCusInfo.getBillToWard());
            potentialCus.setBillToWard(Utils.updateCusInfo.getBillToWard());
            potentialCus.setWardId(Integer.parseInt(Utils.updateCusInfo.getWardId()));
        }

        if(!Utils.updateCusInfo.getBillToStreet().equals("")){
            tvStreet.setText(Utils.updateCusInfo.getBillToStreet());
            potentialCus.setBillToStreet(Utils.updateCusInfo.getBillToStreet());
            potentialCus.setStreetId(Integer.parseInt(Utils.updateCusInfo.getStreetId()));
        }

        if(!Utils.updateCusInfo.getTypeHouseName().equals("")){
            tvHouseType.setText(Utils.updateCusInfo.getTypeHouseName());
            potentialCus.setHomeType(Integer.parseInt(Utils.updateCusInfo.getTypeHouse()));
        }

        if(!Utils.updateCusInfo.getBillToNumber().equals("")){
            edtHomeNumber.setText(Utils.updateCusInfo.getBillToNumber());
            potentialCus.setBillToNumber(Utils.updateCusInfo.getBillToNumber());
        }
    }

    private void getDataIntent() {
        canBack = getIntent().getBooleanExtra(Utils.CAN_BACK, false);
    }

    //TODO: method create potential customer
    private void createPotentialCus(){
        //generate string address before call api create potential cus
        if(validateDataOnView()){
            //TODO: call api generate address
            ReqGenerateAddress obj = new ReqGenerateAddress();
            obj.setLocationId(potentialCus.getLocationId());
            obj.setDistrictId(potentialCus.getDistrictId());
            obj.setWardId(potentialCus.getWardId());
            obj.setStreetId(potentialCus.getStreetId());
            obj.setHomeType(potentialCus.getHomeType());
            obj.setHouseNumber(potentialCus.getBillToNumber());
            //cal api
            presenter.doGenerateAddress(this, apiService, obj);
        }
    }

    private void letChooseCity(){
        DialogUtils.showDialogChooseCity(this, realm,
            new DialogCallbackDataToActivity() {
                @Override
                public void dataCallback(String data) {
                    String[] elem = data.split(":");
                    tvCity.setText(elem[1]);
                    //
                    potentialCus.setLocationId(Integer.parseInt(elem[0]));
                    potentialCus.setBillToCity(elem[1]);
                }
            }).show();
    }

    private void letChooseDistrict(){
        //check if don't select city before -> show message
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            Intent intent = new Intent(this, ChooseDistrictActivity.class);
            intent.putExtra(Utils.CITY_ID, String.valueOf(potentialCus.getLocationId()));
            startActivityForResult(intent, Utils.REQUEST_CODE_DISTRICT);
        }
    }

    private void letChooseWard(){
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))
                && !tvDistrict.getText().toString().equals(getResources().getString(R.string.ui_text_choose_district))){
            Intent intent = new Intent(this, ChooseWardActivity.class);
            intent.putExtra(Utils.CITY_ID, String.valueOf(potentialCus.getLocationId()));
            intent.putExtra(Utils.DISTRICT_ID, String.valueOf(potentialCus.getDistrictId()));
            startActivityForResult(intent, Utils.REQUEST_CODE_WARD);
        }
    }

    private void letChooseStreet(){
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province)) &&
                !tvDistrict.getText().toString().equals(getResources().getString(R.string.ui_text_choose_district)) &&
                !tvWard.getText().toString().equals(getResources().getString(R.string.ui_text_choose_ward))){
            Intent intent = new Intent(this, ChooseStreetActivity.class);
            intent.putExtra(Utils.CITY_ID, String.valueOf(potentialCus.getLocationId()));
            intent.putExtra(Utils.DISTRICT_ID, String.valueOf(potentialCus.getDistrictId()));
            intent.putExtra(Utils.WARD_ID, String.valueOf(potentialCus.getWardId()));
            startActivityForResult(intent, Utils.REQUEST_CODE_STREET);
        }
    }

    private void letChooseHomeType(){
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            Intent intent = new Intent(this, ChooseHomeTypeActivity.class);
            startActivityForResult(intent, Utils.REQUEST_CODE_HOME_TYPE);
        }
    }

    private void setEvent() {
        loBack.setOnClickListener(this);
        loChooseCity.setOnClickListener(this);
        loChooseDistrict.setOnClickListener(this);
        loChooseWard.setOnClickListener(this);
        loChooseHomeType.setOnClickListener(this);
        loChooseStreet.setOnClickListener(this);
        btnCreatePotentialCus.setOnClickListener(this);
        //add test change
        tvCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvDistrict.setText(getResources().getString(R.string.ui_text_choose_district));
                tvWard.setText(getResources().getString(R.string.ui_text_choose_ward));
                tvHouseType.setText(getResources().getString(R.string.ui_text_choose_home_type));
                tvStreet.setText(getResources().getString(R.string.ui_text_choose_street));

                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_city_province))){
                    loChooseCity.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvCity.setTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        tvDistrict.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvWard.setText(getResources().getString(R.string.ui_text_choose_ward));
                tvHouseType.setText(getResources().getString(R.string.ui_text_choose_home_type));
                tvStreet.setText(getResources().getString(R.string.ui_text_choose_street));

                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_district))){
                    loChooseDistrict.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvDistrict.setTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        tvWard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvHouseType.setText(getResources().getString(R.string.ui_text_choose_home_type));
                tvStreet.setText(getResources().getString(R.string.ui_text_choose_street));

                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_ward))){
                    loChooseWard.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvWard.setTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        tvHouseType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_home_type))){
                    loChooseHomeType.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvHouseType.setTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        tvStreet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtHomeNumber.setText("");
                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_street))){
                    loChooseStreet.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvStreet.setTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        edtCusName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).isEmpty()){
                    potentialCus.setFullName(String.valueOf(editable));
                    loFullName.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    edtCusName.setHintTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).isEmpty()){
                    potentialCus.setPhoneNumber(String.valueOf(editable));
                    loPhone.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    edtPhone.setHintTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        edtHomeNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).isEmpty()){
                    potentialCus.setBillToNumber(String.valueOf(editable));
                    loHomeNumber.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    edtHomeNumber.setHintTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        edtNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).isEmpty()){
                    potentialCus.setDescription(String.valueOf(editable));
                }
            }
        });
    }

    //TODO: validate when fill data UI
    private boolean validateDataOnView(){
        if(edtCusName.getText().toString().isEmpty()){
            loFullName.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            edtCusName.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loFullName.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            edtCusName.setHintTextColor(getResources().getColor(R.color.s_black));
        }

        if(edtPhone.getText().toString().isEmpty()){
            loPhone.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            edtPhone.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loPhone.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            edtPhone.setHintTextColor(getResources().getColor(R.color.s_black));
        }

        if(edtHomeNumber.getText().toString().isEmpty()){
            loHomeNumber.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            edtHomeNumber.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loHomeNumber.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            edtHomeNumber.setHintTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            loChooseCity.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvCity.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseCity.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvCity.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvDistrict.getText().toString().equals(getResources().getString(R.string.ui_text_choose_district))){
            loChooseDistrict.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvDistrict.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseDistrict.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvDistrict.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvWard.getText().toString().equals(getResources().getString(R.string.ui_text_choose_ward))){
            loChooseWard.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvWard.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseWard.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvWard.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvHouseType.getText().toString().equals(getResources().getString(R.string.ui_text_choose_home_type))){
            loChooseHomeType.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvHouseType.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseHomeType.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvHouseType.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvStreet.getText().toString().equals(getResources().getString(R.string.ui_text_choose_street))){
            loChooseStreet.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvStreet.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseStreet.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvStreet.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(edtCusName.getText().toString().isEmpty()){
            showDialogRequired(getResources().getString(R.string.mes_full_name_potential_required)).show();
            return false;
        }else if(edtPhone.getText().toString().isEmpty()){
            showDialogRequired(getResources().getString(R.string.mes_phone_potential_required)).show();
            return false;
        }else if(tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            showDialogRequired(getResources().getString(R.string.mes_city_potential_required)).show();
            return false;
        }else if(tvDistrict.getText().toString().equals(getResources().getString(R.string.ui_text_choose_district))){
            showDialogRequired(getResources().getString(R.string.mes_district_potential_required)).show();
            return false;
        }else if(tvWard.getText().toString().equals(getResources().getString(R.string.ui_text_choose_ward))){
            showDialogRequired(getResources().getString(R.string.mes_ward_potential_required)).show();
            return false;
        }else if(tvHouseType.getText().toString().equals(getResources().getString(R.string.ui_text_choose_home_type))){
            showDialogRequired(getResources().getString(R.string.mes_home_type_potential_required)).show();
            return false;
        }else if(tvStreet.getText().toString().equals(getResources().getString(R.string.ui_text_choose_street))){
            showDialogRequired(getResources().getString(R.string.mes_street_potential_required)).show();
            return false;
        }else if(edtHomeNumber.getText().toString().isEmpty()){
            showDialogRequired(getResources().getString(R.string.mes_home_number_potential_required)).show();
            return false;
        }
        return true;
    }

    private Dialog showDialogRequired(String str){
        return DialogUtils.showMessageDialogVerButton(this, str);
    }
}
