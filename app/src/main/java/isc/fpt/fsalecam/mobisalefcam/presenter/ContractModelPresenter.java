package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetRegistrationDetail;

/**
 * Created by Hau Le on 2018-08-28.
 */
public interface ContractModelPresenter {
    void doGetHtmlModelContract(Context context, APIService apiService, ReqGetRegistrationDetail obj);
}
