package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;


import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateCusInfo;
import isc.fpt.fsalecam.mobisalefcam.database.DepositFee;
import isc.fpt.fsalecam.mobisalefcam.database.Vat;

/**
 * Created by Hau Le on 2018-08-23.
 */
public interface TotalServiceFeeView {
    void getDepositListSuccess(ArrayList<DepositFee> list);
    void getVatListSuccess(ArrayList<Vat> list);
//    void calculatorTotalDevice(ArrayList<TotalDevice> list);
    void calculatorTotalAmount(ArrayList<CalNewTotal> list);
    void updateCusInfoSuccess(UpdateCusInfo obj);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressLoading();
    void hiddenProgressLoading();
}
