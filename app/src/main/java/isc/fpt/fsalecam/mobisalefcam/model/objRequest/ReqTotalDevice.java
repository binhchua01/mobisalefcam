package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Hau Le on 2018-08-24.
 */
public class ReqTotalDevice {
    private String locationId;
    private List<ReqDevice> list;
    private String monthOfPrepaid;
    private String type;

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public List<ReqDevice> getList() {
        return list;
    }

    public void setList(List<ReqDevice> list) {
        this.list = list;
    }

    public String getMonthOfPrepaid() {
        return monthOfPrepaid;
    }

    public void setMonthOfPrepaid(String monthOfPrepaid) {
        this.monthOfPrepaid = monthOfPrepaid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LinkedHashMap<String, Object> generateObjTotalDevice(){
        //generate object to send
        LinkedHashMap<String, Object> obj = new LinkedHashMap<>();
        obj.put("LocationId", getLocationId());
        obj.put("MonthOfPrepaid", getMonthOfPrepaid());
        obj.put("ListDevice", linkedHashArray());
        obj.put("Type", getType());
        return obj;
    }

    public String objToString() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("LocationId", getLocationId());
            obj.put("MonthOfPrepaid", getMonthOfPrepaid());
            obj.put("ListDevice", arrayObjToString());
            obj.put("Type", getType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }

    private JSONArray arrayObjToString() {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        for (int i = 0; i < getList().size(); i++) {
            jsonObject = new JSONObject();
            try {
                jsonObject.put("Value", getList().get(i).getValue());
                jsonObject.put("Name", getList().get(i).getName());
                jsonObject.put("Number", getList().get(i).getNumber());
                jsonObject.put("Price", String.valueOf(getList().get(i).getPrice()));
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private ArrayList<Object> linkedHashArray(){
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < getList().size(); i++) {
            LinkedHashMap<String, String> obj = new LinkedHashMap<>();
            obj.put("Value", getList().get(i).getValue());
            obj.put("Name", getList().get(i).getName());
            obj.put("Number", getList().get(i).getNumber());
            obj.put("Price", String.valueOf(getList().get(i).getPrice()));
            list.add(obj);
        }
        return list;
    }
}
