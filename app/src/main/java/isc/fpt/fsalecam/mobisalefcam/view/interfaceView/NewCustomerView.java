package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationById;

/**
 * Created by Hau Le on 2018-07-23.
 */
public interface NewCustomerView {
    void refreshUI(int num);
    void getRegistrationByIdSuccess(RegistrationById obj);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressLoading();
    void hiddenProgressLoading();
}
