package isc.fpt.fsalecam.mobisalefcam.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Hau Le on 2018-07-16.
 */
public class SharedPref {

    public enum Key {
        FTEL_MOBISALECAM_HEADER,
        SALE_ID,
        USERNAME,
        FULL_NAME,
        PHONE,
        EMAIL,
        REGION,
        SALE_LIST_LOCATION,
        TOKEN_KONG,
        TIMER_TO_REFRESH,
        BookportIdentityID,
        IMEI,
        APP_SERVER_IMAGE_TOKEN
    }

    private static SharedPreferences sharedPreferences(Context context) {
        return context.getSharedPreferences("SharedPreferencesUtils", Context.MODE_PRIVATE);
    }

    public static void put(Context context, Key key, String value) {
        savePref(context,key.name(),value);
    }

    public static void put(Context context, String key, String value) {
        savePref(context,key,value);
    }

    public static String get(Context context, Key key, String defValue) {
        return sharedPreferences(context).getString(key.name(), defValue);
    }

    public static String get(Context context, String key, String defValue) {
        return sharedPreferences(context).getString(key, defValue);
    }

    private static void savePref(Context context, String key, String value){
        final SharedPreferences.Editor editor = sharedPreferences(context).edit();
        editor.putString(key, value);
        editor.apply();
    }
}
