package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-14.
 */
public class ReqBookportManual implements Serializable {
    private String username;
    private int locationId;
    private String regCode;
    private int wardId;
    private String deviceName;
    private int lengthSurveyOutDoor;
    private int lengthSurveyInDoor;
    private int typeOfNetworkInfrastructure;
    private String contractTemp;
    private int isRecovery;
    private String bookportIdentityId;
    private String latlngDevice;
    private String latLngMarker;

    public String getLatLngMarker() {
        return latLngMarker;
    }

    public void setLatLngMarker(String latLngMarker) {
        this.latLngMarker = latLngMarker;
    }

    public String getLatlngDevice() {
        return latlngDevice;
    }

    public void setLatlngDevice(String latlngDevice) {
        this.latlngDevice = latlngDevice;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getWardId() {
        return wardId;
    }

    public void setWardId(int wardId) {
        this.wardId = wardId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public int getLengthSurveyOutDoor() {
        return lengthSurveyOutDoor;
    }

    public void setLengthSurveyOutDoor(int lengthSurveyOutDoor) {
        this.lengthSurveyOutDoor = lengthSurveyOutDoor;
    }

    public int getLengthSurveyInDoor() {
        return lengthSurveyInDoor;
    }

    public void setLengthSurveyInDoor(int lengthSurveyInDoor) {
        this.lengthSurveyInDoor = lengthSurveyInDoor;
    }

    public int getTypeOfNetworkInfrastructure() {
        return typeOfNetworkInfrastructure;
    }

    public void setTypeOfNetworkInfrastructure(int typeOfNetworkInfrastructure) {
        this.typeOfNetworkInfrastructure = typeOfNetworkInfrastructure;
    }

    public String getContractTemp() {
        return contractTemp;
    }

    public void setContractTemp(String contractTemp) {
        this.contractTemp = contractTemp;
    }

    public int getIsRecovery() {
        return isRecovery;
    }

    public void setIsRecovery(int isRecovery) {
        this.isRecovery = isRecovery;
    }

    public String getBookportIdentityId() {
        return bookportIdentityId;
    }

    public void setBookportIdentityId(String bookportIdentityId) {
        this.bookportIdentityId = bookportIdentityId;
    }

    public LinkedHashMap<String, String> generateObjBookportManual(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("Username", String.valueOf(username));
        obj.put("LocationId", String.valueOf(locationId));
        obj.put("WardId", String.valueOf(wardId));
        obj.put("DeviceName", String.valueOf(deviceName));
        obj.put("LengthSurvey_OutDoor", String.valueOf(lengthSurveyOutDoor));
        obj.put("LengthSurvey_InDoor", String.valueOf(lengthSurveyInDoor));
        obj.put("typeOfNetworkInfrastructure", String.valueOf(typeOfNetworkInfrastructure));
        obj.put("ContractTemp", String.valueOf(contractTemp));
        obj.put("RegCode", getRegCode());
        obj.put("isRecovery", String.valueOf(isRecovery));
        obj.put("BookportIdentityID", String.valueOf(bookportIdentityId));
        obj.put("LatlngDevice", getLatlngDevice());
        obj.put("Latlng", getLatLngMarker());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("Username", String.valueOf(getUsername()));
            obj.put("LocationId", String.valueOf(getLocationId()));
            obj.put("WardId", String.valueOf(getWardId()));
            obj.put("DeviceName", String.valueOf(getDeviceName()));
            obj.put("LengthSurvey_OutDoor", String.valueOf(getLengthSurveyOutDoor()));
            obj.put("LengthSurvey_InDoor", String.valueOf(getLengthSurveyInDoor()));
            obj.put("typeOfNetworkInfrastructure", String.valueOf(getTypeOfNetworkInfrastructure()));
            obj.put("ContractTemp", String.valueOf(getContractTemp()));
            obj.put("RegCode", getRegCode());
            obj.put("isRecovery", String.valueOf(getIsRecovery()));
            obj.put("BookportIdentityID", String.valueOf(getBookportIdentityId()));
            obj.put("LatlngDevice", getLatlngDevice());
            obj.put("Latlng", getLatLngMarker());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
