package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-09-13.
 */
public class Ability {
    @SerializedName("Timezone")
    @Expose
    private String timezone;
    @SerializedName("DateEffect")
    @Expose
    private String dateEffect;
    @SerializedName("Ability")
    @Expose
    private Integer ability;

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getDateEffect() {
        return dateEffect;
    }

    public void setDateEffect(String dateEffect) {
        this.dateEffect = dateEffect;
    }

    public Integer getAbility() {
        return ability;
    }

    public void setAbility(Integer ability) {
        this.ability = ability;
    }
}
