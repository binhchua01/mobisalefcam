package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-14.
 */
public class ReqPotentialCus {
    private String fullName;
    private String phoneNumber;
    private String address;
    private String description;
    private int locationId;
    private String billToCity;
    private int districtId;
    private String billToDistrict;
    private int wardId;
    private String billToWard;
    private int streetId;
    private String billToStreet;
    private String billToNumber;
    private int homeType;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getBillToCity() {
        return billToCity;
    }

    public void setBillToCity(String billToCity) {
        this.billToCity = billToCity;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getBillToDistrict() {
        return billToDistrict;
    }

    public void setBillToDistrict(String billToDistrict) {
        this.billToDistrict = billToDistrict;
    }

    public int getWardId() {
        return wardId;
    }

    public void setWardId(int wardId) {
        this.wardId = wardId;
    }

    public String getBillToWard() {
        return billToWard;
    }

    public void setBillToWard(String billToWard) {
        this.billToWard = billToWard;
    }

    public int getStreetId() {
        return streetId;
    }

    public void setStreetId(int streetId) {
        this.streetId = streetId;
    }

    public String getBillToStreet() {
        return billToStreet;
    }

    public void setBillToStreet(String billToStreet) {
        this.billToStreet = billToStreet;
    }

    public String getBillToNumber() {
        return billToNumber;
    }

    public void setBillToNumber(String billToNumber) {
        this.billToNumber = billToNumber;
    }

    public int getHomeType() {
        return homeType;
    }

    public void setHomeType(int homeType) {
        this.homeType = homeType;
    }

    public LinkedHashMap<String, String> generateObjCreatePotentialCus(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("FullName", getFullName());
        obj.put("PhoneNumber", getPhoneNumber());
        obj.put("Address", getAddress());
        obj.put("Description", getDescription());
        obj.put("LocationId", String.valueOf(getLocationId()));
        obj.put("BillTo_City", getBillToCity());
        obj.put("DistrictId", String.valueOf(getDistrictId()));
        obj.put("BillTo_District", getBillToDistrict());
        obj.put("WardId", String.valueOf(getWardId()));
        obj.put("BillTo_Ward", getBillToWard());
        obj.put("StreetId", String.valueOf(getStreetId()));
        obj.put("BillTo_Street", getBillToStreet());
        obj.put("BillTo_Number", getBillToNumber());
        obj.put("TypeHouse", String.valueOf(getHomeType()));
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("FullName", getFullName());
            obj.put("PhoneNumber", getPhoneNumber());
            obj.put("Address", getAddress());
            obj.put("Description", getDescription());
            obj.put("LocationId", String.valueOf(getLocationId()));
            obj.put("BillTo_City", getBillToCity());
            obj.put("DistrictId", String.valueOf(getDistrictId()));
            obj.put("BillTo_District", getBillToDistrict());
            obj.put("WardId", String.valueOf(getWardId()));
            obj.put("BillTo_Ward", getBillToWard());
            obj.put("StreetId", String.valueOf(getStreetId()));
            obj.put("BillTo_Street", getBillToStreet());
            obj.put("BillTo_Number", getBillToNumber());
            obj.put("TypeHouse", String.valueOf(getHomeType()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
