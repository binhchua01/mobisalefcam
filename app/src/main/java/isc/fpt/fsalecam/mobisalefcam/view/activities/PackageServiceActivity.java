package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.PackageServiceAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.presenter.PackageServicePresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.PackageServicePresenter;
import isc.fpt.fsalecam.mobisalefcam.database.PackageService;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.PackageServiceView;

public class PackageServiceActivity extends BaseActivity
        implements View.OnClickListener, PackageServiceView{

    @BindView(R.id.img_nav_back_choose_package_service) RelativeLayout loBack;
    @BindView(R.id.layout_choose_package_service) LinearLayout loParent;
    @BindView(R.id.recycler_view_package_service) RecyclerView recyclerView;
    @BindView(R.id.img_package_service_refresh) RelativeLayout loRefresh;
    @BindView(R.id.edt_search_package_service) EditText edtSearch;

    private APIService apiService;
    private PackageServicePresenter presenter;
    private Realm realm;
    private Dialog dialog;
    private PackageServiceAdapter adapter;
    private ArrayList<PackageService> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        initDatabase();
        initDataView();
        setEvent();
        callApi();
    }

    private void initDataView() {
        list = new ArrayList<>();
        adapter = new PackageServiceAdapter(this, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //callbackDeviceList data
                Intent intent = new Intent();
                intent.putExtra(Utils.PACKAGE_SERVICE ,list.get(position).getId() + ":" + list.get(position).getName());
                setResult(Utils.REQUEST_CODE_PACKAGE_SERVICE, intent);
                finish();//finishing activity
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void initDatabase() {
        realm = Realm.getDefaultInstance();
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new PackageServicePresenterImpl(this);
        presenter.doGetPackageServiceList(this, apiService, realm, Utils.updateCusInfo.getObjId());
    }

    private void setEvent() {
        loBack.setOnClickListener(this);
        loRefresh.setOnClickListener(this);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.search(realm, String.valueOf(editable));
            }
        });
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_package_service;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_nav_back_choose_package_service:
                onBackPressed();
                break;
            case R.id.img_package_service_refresh:
                presenter.doRefreshPackageService(this, apiService, realm, "");
                break;
        }
    }

    @Override
    public void getPackageServiceListSuccess(ArrayList<PackageService> list) {
        adapter.notifyData(list);
        this.list = list;
    }

    @Override
    public void searchServicePackage(ArrayList<PackageService> list) {
        adapter.notifyData(list);
        this.list = list;
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showDialogProgress() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenDialogProgress() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
