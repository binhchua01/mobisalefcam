package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetRegistrationDetail;

/**
 * Created by Hau Le on 2018-07-23.
 */
public interface NewCustomerPresenter {
    //call when click button on child fragment
    void changedUIActivity(int num);
    void doGetRegistrationById(Context context, APIService apiService, ReqGetRegistrationDetail obj);
}
