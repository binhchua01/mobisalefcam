package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-24.
 */
public class ReqCheckVersion {
    private String deviceImei;
    private String currentVersion;
    private String platform;

    public String getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(String deviceImei) {
        this.deviceImei = deviceImei;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public LinkedHashMap<String, String> generateObjCheckVersion(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("DeviceIMEI", getDeviceImei());
        obj.put("CurrentVersion", getCurrentVersion());
        obj.put("Platform", getPlatform());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("DeviceIMEI", getDeviceImei());
            obj.put("CurrentVersion", getCurrentVersion());
            obj.put("Platform", getPlatform());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
