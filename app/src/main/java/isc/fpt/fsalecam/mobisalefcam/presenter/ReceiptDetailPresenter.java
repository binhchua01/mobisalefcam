package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdatePayment;

/**
 * Created by Hau Le on 2018-09-04.
 */
public interface ReceiptDetailPresenter {
    void doGetTypePaymentList(Context context, APIService apiService, Realm realm);
    void doUpdatePayment(Context context, APIService apiService, ReqUpdatePayment obj);
}
