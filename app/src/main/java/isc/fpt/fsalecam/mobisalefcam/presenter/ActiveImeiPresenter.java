package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

/**
 * Created by Hau Le on 2018-07-06.
 */
public interface ActiveImeiPresenter {
    void readImeiDevice(Context context);
}
