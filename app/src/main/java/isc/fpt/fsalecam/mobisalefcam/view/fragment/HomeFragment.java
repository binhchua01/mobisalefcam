package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity;
import isc.fpt.fsalecam.mobisalefcam.view.activities.PotentialCusListActivity;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;

/**
 * Created by Hau Le on 2018-07-10.
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener{
    @BindView(R.id.frag_layout_report) LinearLayout loReport;
    @BindView(R.id.layout_search) LinearLayout loContract;
    @BindView(R.id.frag_layout_create_customer_info) LinearLayout loCreateCI;
    @BindView(R.id.frag_layout_potential_customer) LinearLayout loPotentialCustomer;
    @BindView(R.id.text_view_name_user) TextView tvUsername;
    @BindView(R.id.tv_full_name) TextView tvFullName;
    @BindView(R.id.tv_region) TextView tvRegion;
    @BindView(R.id.tv_phone_number) TextView tvPhoneNumber;
    @BindView(R.id.tv_email) TextView tvEmail;

    private MainActivity activity;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        setEvent();
        initSaleInfo();
    }

    private void initView() {
        activity = (MainActivity) getActivity();
    }

    private void initSaleInfo(){
        tvUsername.setText(SharedPref.get(getContext(), SharedPref.Key.USERNAME, ""));
        tvFullName.setText(SharedPref.get(getContext(), SharedPref.Key.FULL_NAME, ""));
        tvRegion.setText(SharedPref.get(getContext(), SharedPref.Key.REGION, ""));
        tvEmail.setText(SharedPref.get(getContext(), SharedPref.Key.EMAIL, ""));
        tvPhoneNumber.setText(SharedPref.get(getContext(), SharedPref.Key.PHONE, ""));
    }

    public void initSaleInfo(SaleOfInfo saleOfInfo){
        tvUsername.setText(saleOfInfo.getUserName());
        tvFullName.setText(saleOfInfo.getFullName());
        tvRegion.setText(saleOfInfo.getLocation());
        tvEmail.setText(saleOfInfo.getEmail());
        tvPhoneNumber.setText(saleOfInfo.getPhone());
    }

    private void setEvent() {
        loReport.setOnClickListener(this);
        loContract.setOnClickListener(this);
        loCreateCI.setOnClickListener(this);
        loPotentialCustomer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.frag_layout_create_customer_info:
                activity.controlUIBottomMenu(2);
                activity.changedFragment(new CusInfoListFragment());
                break;
            case R.id.layout_search:
                DialogUtils.showMessageDialogVerButton(getActivity(),
                        getResources().getString(R.string.message_next_version)).show();
                break;
            case R.id.frag_layout_report:
                activity.changedFragment(new ReportFilterFragment());
                activity.controlUIBottomMenu(3);
                break;
            case R.id.frag_layout_potential_customer:
                startActivity(new Intent(getActivity(), PotentialCusListActivity.class));
                break;

        }
    }
}
