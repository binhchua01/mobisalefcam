package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hau Le on 2018-08-14.
 */
public class BookPortManual implements Serializable{
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ODCCableType")
    @Expose
    private String oDCCableType;
    @SerializedName("ContractTemp")
    @Expose
    private String contractTemp;
    @SerializedName("Port")
    @Expose
    private Integer port;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getODCCableType() {
        return oDCCableType;
    }

    public void setODCCableType(String oDCCableType) {
        this.oDCCableType = oDCCableType;
    }

    public String getContractTemp() {
        return contractTemp;
    }

    public void setContractTemp(String contractTemp) {
        this.contractTemp = contractTemp;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
