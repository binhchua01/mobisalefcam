package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Objects;
import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.view.activities.BaseActivity;
import isc.fpt.fsalecam.mobisalefcam.presenter.ActiveImeiPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ActiveImeiPresenter;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.utils.CopyText;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ActiveImeiView;

public class ActiveImeiActivity extends BaseActivity implements View.OnClickListener, ActiveImeiView{
    @BindView(R.id.img_active_nav_back) RelativeLayout imgBack;
    @BindView(R.id.tv_display_imei) TextView tvDisplayImei;
    @BindView(R.id.button_copy_imei) Button btnCopyImei;
    @BindView(R.id.layout_active_imei) RelativeLayout layoutWrapper;
    @BindView(R.id.tv_app_version) TextView tvAppVersion;
    @BindColor(R.color.s_white) int white;
    @BindDrawable(R.drawable.background_button_has_copied) Drawable btnHasCopied;

    private ActiveImeiPresenter presenter;
    private static final int MY_PERMISSIONS_REQUEST = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        setEvent();
    }

    private void initView() {
        tvAppVersion.setText(Utils.getVersionName(this));
        presenter = new ActiveImeiPresenterImpl(this);
        //ask permission read phone state
        askForPermission();
    }

    private void setEvent() {
        btnCopyImei.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_active_imei;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_active_nav_back:
                //back to parent activity
                onBackPressed();
                break;
            case R.id.button_copy_imei:
                //copy text and show message
                CopyText.copyText(this, tvDisplayImei.getText().toString());
                disableButton();
                break;
        }
    }

    //change button after copied imei
    private void disableButton(){
        btnCopyImei.setBackground(btnHasCopied);
        btnCopyImei.setText(getResources().getString(R.string.ui_text_copied));
        btnCopyImei.setTextColor(white);
        btnCopyImei.setClickable(false);
    }

    private void askForPermission() {
        boolean hasPermission = ContextCompat.checkSelfPermission(Objects.requireNonNull(this), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
        if(hasPermission){
            //permission accept
            presenter.readImeiDevice(this);
        }else{
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS_REQUEST){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                presenter.readImeiDevice(this);
            }else{
                DialogUtils.showMessageDialog(this, getResources().getString(R.string.mes_cant_read_imei)).show();
            }
        }
    }

    @Override
    public void displayImei(String imei) {
        //set imei to view if permission accept
        tvDisplayImei.setText(imei);
    }
}
