package isc.fpt.fsalecam.mobisalefcam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.database.District;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;

/**
 * Created by Hau Le on 2018-08-01.
 */
public class ChooseDistrictAdapter extends RecyclerView.Adapter<ChooseDistrictAdapter.SimpleViewHolder> {
    private Context context;
    private ArrayList<District> list;
    private OnItemClickListener listener;

    public ChooseDistrictAdapter(Context context, ArrayList<District> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_choose_district_one_row, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickListener(view, mViewHolder.getAdapterPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        holder.tvDistrictName.setText(list.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_choose_district) TextView tvDistrictName;

        SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void notifyData(ArrayList<District> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
