package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-08-20.
 */
public class BookportAutoData {
    @SerializedName("BookportIdentityID")
    @Expose
    private String bookportIdentityID;
    @SerializedName("BookPortAuto")
    @Expose
    private BookportAuto bookportAuto;

    public String getBookportIdentityID() {
        return bookportIdentityID;
    }

    public void setBookportIdentityID(String bookportIdentityID) {
        this.bookportIdentityID = bookportIdentityID;
    }

    public BookportAuto getBookportAuto() {
        return bookportAuto;
    }

    public void setBookportAuto(BookportAuto bookportAuto) {
        this.bookportAuto = bookportAuto;
    }
}
