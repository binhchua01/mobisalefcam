package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqBookportManual;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResBookportManual;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.EnterLengthCableView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-14.
 */
public class EnterLengthCablePresenterImpl implements EnterLengthCablePresenter {
    private EnterLengthCableView view;

    public EnterLengthCablePresenterImpl(EnterLengthCableView view) {
        this.view = view;
    }

    @Override
    public void doBookportManual(final Context context, final APIService apiService, final ReqBookportManual reqBookportManual) {
        //show dialog
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiBookportManual(context, apiService, reqBookportManual);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiBookportManual(context, apiService, reqBookportManual);
        }
    }

    private void callApiBookportManual(final Context context, APIService apiService, final ReqBookportManual reqBookportManual){
        apiService.doBookportManual(Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
            Utils.CONTENT_TYPE,
            SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
            Utils.checkSum(reqBookportManual.objToString()),
            reqBookportManual.generateObjBookportManual()).enqueue(new Callback<ResBookportManual>() {
                @Override
                public void onResponse(Call<ResBookportManual> call, Response<ResBookportManual> response) {
                    validateReturnCodeBookportManual(context, response);
                    view.hiddenProgressDialog();
                }

                @Override
                public void onFailure(Call<ResBookportManual> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    private void validateReturnCodeBookportManual(Context context, Response<ResBookportManual> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code >= Utils.SUCCESS){
                view.bookportManualSuccess(response.body().getData().get(0).getBookPortManual());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
                //save BookportIdentityID use for bookport after, when bookport success -> clear BookportIdentityID to default
                SharedPref.put(context, SharedPref.Key.BookportIdentityID, response.body().getData().get(0).getBookportIdentityID());
            }else{
                if(code == Utils.INDOOR_INVALID){
                    view.indoorInvalid(response.body().getMessage());
                }else if(code == Utils.OUTDOOR_INVALID){
                    view.outdoorInvalid(response.body().getMessage());
                }else if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.createPotentialCusOrBookport(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }
}
