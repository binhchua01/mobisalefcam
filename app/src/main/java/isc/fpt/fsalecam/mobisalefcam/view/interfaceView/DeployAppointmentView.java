package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SubTeam;

/**
 * Created by Hau Le on 2018-09-12.
 */
public interface DeployAppointmentView {
    void getSubTeamListSuccess(SubTeam obj);
    void getAbilityListSuccess(ArrayList<String> listDate,
                               ArrayList<Integer> listTimezone1,
                               ArrayList<Integer> listTimezone2);
    void getTimeZoneSuccess(ArrayList<String> list);
    void updateDeployAppointment();
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
