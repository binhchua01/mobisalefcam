package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-20.
 */
public class ReqBookportAuto {
    private String username, locationId, regCode, wardId, latPort, lngPort,
            typeOfNetworkInfrastructure, contractTemp, isRecovery, bookportIdentityId,
            latLngDevice, latLngMarker;

    public String getLatLngMarker() {
        return latLngMarker;
    }

    public void setLatLngMarker(String latLngMarker) {
        this.latLngMarker = latLngMarker;
    }

    public String getLatLngDevice() {
        return latLngDevice;
    }

    public void setLatLngDevice(String latLngDevice) {
        this.latLngDevice = latLngDevice;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getWardId() {
        return wardId;
    }

    public void setWardId(String wardId) {
        this.wardId = wardId;
    }

    public String getLatPort() {
        return latPort;
    }

    public void setLatPort(String latPort) {
        this.latPort = latPort;
    }

    public String getLngPort() {
        return lngPort;
    }

    public void setLngPort(String lngPort) {
        this.lngPort = lngPort;
    }

    public String getTypeOfNetworkInfrastructure() {
        return typeOfNetworkInfrastructure;
    }

    public void setTypeOfNetworkInfrastructure(String typeOfNetworkInfrastructure) {
        this.typeOfNetworkInfrastructure = typeOfNetworkInfrastructure;
    }

    public String getContractTemp() {
        return contractTemp;
    }

    public void setContractTemp(String contractTemp) {
        this.contractTemp = contractTemp;
    }

    public String getIsRecovery() {
        return isRecovery;
    }

    public void setIsRecovery(String isRecovery) {
        this.isRecovery = isRecovery;
    }

    public String getBookportIdentityId() {
        return bookportIdentityId;
    }

    public void setBookportIdentityId(String bookportIdentityId) {
        this.bookportIdentityId = bookportIdentityId;
    }

    public LinkedHashMap<String, String> generateObjBookportAuto(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("Username", getUsername());
        obj.put("LocationId", getLocationId());
        obj.put("RegCode", getRegCode());
        obj.put("WardId", getWardId());
        obj.put("latitude", getLatPort());
        obj.put("longitude", getLngPort());
        obj.put("typeOfNetworkInfrastructure", getTypeOfNetworkInfrastructure());
        obj.put("ContractTemp", getContractTemp());
        obj.put("isRecovery", getIsRecovery());
        obj.put("BookportIdentityID", getBookportIdentityId());
        obj.put("LatlngDevice", getLatLngDevice());
        obj.put("Latlng", getLatLngMarker());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("Username", getUsername());
            obj.put("LocationId", getLocationId());
            obj.put("RegCode", getRegCode());
            obj.put("WardId", getWardId());
            obj.put("latitude", getLatPort());
            obj.put("longitude", getLngPort());
            obj.put("typeOfNetworkInfrastructure", getTypeOfNetworkInfrastructure());
            obj.put("ContractTemp", getContractTemp());
            obj.put("isRecovery", getIsRecovery());
            obj.put("BookportIdentityID", getBookportIdentityId());
            obj.put("LatlngDevice", getLatLngDevice());
            obj.put("Latlng", getLatLngMarker());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
