package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-07.
 */
public class ReqUpdateRegistrationTotal {
    private int objId;
    private String contract;
    private int vat;
    private int locationId;
    private String username;
    private int localType;
    private int promotionId;
    private int monthOfPrepaid;
    private double total;
    private double internetTotal;
    private double deviceTotal;
    private double depositFee;
    private double connectionFee;
    private ArrayList<ReqDevice> list;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getObjId() {
        return objId;
    }

    public void setObjId(int objId) {
        this.objId = objId;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public int getVat() {
        return vat;
    }

    public void setVat(int vat) {
        this.vat = vat;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getLocalType() {
        return localType;
    }

    public void setLocalType(int localType) {
        this.localType = localType;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public int getMonthOfPrepaid() {
        return monthOfPrepaid;
    }

    public void setMonthOfPrepaid(int monthOfPrepaid) {
        this.monthOfPrepaid = monthOfPrepaid;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getInternetTotal() {
        return internetTotal;
    }

    public void setInternetTotal(double internetTotal) {
        this.internetTotal = internetTotal;
    }

    public double getDeviceTotal() {
        return deviceTotal;
    }

    public void setDeviceTotal(double deviceTotal) {
        this.deviceTotal = deviceTotal;
    }

    public double getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(double depositFee) {
        this.depositFee = depositFee;
    }

    public double getConnectionFee() {
        return connectionFee;
    }

    public void setConnectionFee(double connectionFee) {
        this.connectionFee = connectionFee;
    }

    public ArrayList<ReqDevice> getList() {
        return list;
    }

    public void setList(ArrayList<ReqDevice> list) {
        this.list = list;
    }

    public LinkedHashMap<String, Object> generateObjUpdateRegistrationTotal(){
        //generate object to send
        LinkedHashMap<String, Object> obj = new LinkedHashMap<>();
        obj.put("ObjId", String.valueOf(getObjId()));
        obj.put("Contract", getContract());
        obj.put("VAT", String.valueOf(getVat()));
        obj.put("LocationId", String.valueOf(getLocationId()));
        obj.put("UserName", getUsername());
        obj.put("LocalType", String.valueOf(getLocalType()));
        obj.put("PromotionId", String.valueOf(getPromotionId()));
        obj.put("MonthOfPrepaid", String.valueOf(getMonthOfPrepaid()));
        obj.put("Total", String.valueOf(getTotal()));
        obj.put("InternetTotal", String.valueOf(getInternetTotal()));
        obj.put("DeviceTotal", String.valueOf(getDeviceTotal()));
        obj.put("DepositFee", String.valueOf(getDepositFee()));
        obj.put("ConnectionFee", String.valueOf(getConnectionFee()));
        obj.put("ListDevice", linkedHashArray());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("ObjId", String.valueOf(getObjId()));
            obj.put("Contract", getContract());
            obj.put("VAT", String.valueOf(getVat()));
            obj.put("LocationId", String.valueOf(getLocationId()));
            obj.put("UserName", getUsername());
            obj.put("LocalType", String.valueOf(getLocalType()));
            obj.put("PromotionId", String.valueOf(getPromotionId()));
            obj.put("MonthOfPrepaid", String.valueOf(getMonthOfPrepaid()));
            obj.put("Total", String.valueOf(getTotal()));
            obj.put("InternetTotal", String.valueOf(getInternetTotal()));
            obj.put("DeviceTotal", String.valueOf(getDeviceTotal()));
            obj.put("DepositFee", String.valueOf(getDepositFee()));
            obj.put("ConnectionFee", String.valueOf(getConnectionFee()));
            obj.put("ListDevice", arrayObjToString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }

    private JSONArray arrayObjToString() {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        for (int i = 0; i < getList().size(); i++) {
            jsonObject = new JSONObject();
            try {
                jsonObject.put("Value", getList().get(i).getValue());
                jsonObject.put("Name", getList().get(i).getName());
                jsonObject.put("Number", getList().get(i).getNumber());
                jsonObject.put("Price", String.valueOf(getList().get(i).getPrice()));
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private ArrayList<Object> linkedHashArray(){
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < getList().size(); i++) {
            LinkedHashMap<String, String> obj = new LinkedHashMap<>();
            obj.put("Value", getList().get(i).getValue());
            obj.put("Name", getList().get(i).getName());
            obj.put("Number", getList().get(i).getNumber());
            obj.put("Price", String.valueOf(getList().get(i).getPrice()));
            list.add(obj);
        }
        return list;
    }
}
