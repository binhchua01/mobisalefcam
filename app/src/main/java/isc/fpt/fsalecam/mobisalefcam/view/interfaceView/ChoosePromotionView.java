package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.database.PromotionCode;

/**
 * Created by Hau Le on 2018-08-17.
 */
public interface ChoosePromotionView {
    void getPromotionListSuccess(ArrayList<PromotionCode> list);
    void searchPromotion(ArrayList<PromotionCode> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
