package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdatePayment;
import isc.fpt.fsalecam.mobisalefcam.presenter.ReceiptDetailPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ReceiptDetailPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.PaymentType;
import isc.fpt.fsalecam.mobisalefcam.utils.ContinueRegisterService;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.ResizeDialogPercent;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ReceiptDetailView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.ContractDetailsActivity.contractDetails;

public class ReceiptDetailActivity extends BaseActivity
        implements View.OnClickListener, ReceiptDetailView {
    @BindView(R.id.ic_icon_nav_back_bill_details) RelativeLayout imgBack;
    @BindView(R.id.tv_total_internet) TextView tvTotalInternet;
    @BindView(R.id.tv_connection_fee) TextView tvConnectionFee;
    @BindView(R.id.tv_total_device) TextView tvTotalDevice;
    @BindView(R.id.tv_vat) TextView tvVat;
    @BindView(R.id.tv_deposit_fee) TextView tvDepositFee;
    @BindView(R.id.tv_total_amount) TextView tvTotalAmount;
    @BindView(R.id.tv_type_payment) TextView tvTypePayment;
    @BindView(R.id.layout_payment) RelativeLayout loTypePayment;
    @BindView(R.id.btn_payment_bill) Button btnPayment;

    private APIService apiService;
    private ReceiptDetailPresenter presenter;
    private Dialog dialog;
    private String paymentMethod = "1";
    private Dialog dialogCreateContractSuccess;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        setEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initDataView();
        initParamToCallApi();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void initParamToCallApi() {
        apiService = ApiUtils.getApiService();
        presenter = new ReceiptDetailPresenterImpl(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_receipt_detail;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_icon_nav_back_bill_details:
                onBackPressed();
                break;
            case R.id.layout_payment:
                presenter.doGetTypePaymentList(this, apiService, realm);
                break;
            case R.id.btn_payment_bill:
                DialogUtils.showDialogAskForUpdatePayment(this, contractDetails.getContract(),
                        new ContinueRegisterService() {
                    @Override
                    public void continueRegister() {
                        ReqUpdatePayment obj = new ReqUpdatePayment();
                        obj.setUsername(SharedPref.get(getApplicationContext(), SharedPref.Key.USERNAME, ""));
                        obj.setObjId(String.valueOf(contractDetails.getObjId()));
                        obj.setPaymentMethodId(paymentMethod);
                        presenter.doUpdatePayment(getApplicationContext(), apiService, obj);
                    }
                }).show();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private void initDataView() {
        if(contractDetails==null){ return; }
        tvTotalInternet.setText(String.valueOf(contractDetails.getInternetTotal()));
        tvConnectionFee.setText(String.valueOf(contractDetails.getConnectionFee()));
        tvTotalDevice.setText(String.valueOf(contractDetails.getDeviceTotal()));
        tvVat.setText(contractDetails.getVAT() + "%");
        tvDepositFee.setText(String.valueOf(contractDetails.getDepositFee()));
        tvTotalAmount.setText(String.valueOf(contractDetails.getTotal()));
        tvTypePayment.setText(contractDetails.getPaymentMethodName());
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        loTypePayment.setOnClickListener(this);
        btnPayment.setOnClickListener(this);
    }

    @Override
    public void getTypePaymentList(ArrayList<PaymentType> list) {
        DialogUtils.showDialogChooseTypePayment(this, list, new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                paymentMethod = data.split(":")[0];
                tvTypePayment.setText(data.split(":")[1]);
            }
        }).show();
    }

    @Override
    public void updatePaymentSuccess() {
        dialogCreateContractSuccess = DialogUtils.showDialogUpdateRegistrationSuccess(this,
                getResources().getString(R.string.had_payment_for_this_contract),
                new ContinueRegisterService() {
            @Override
            public void continueRegister() {
                int SPLASH_DISPLAY_LENGTH = 2000;
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        //Start activity here
                        finish();
                    }
                };
                handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);
            }
        });
        dialogCreateContractSuccess.show();
        ResizeDialogPercent.resize(this, dialogCreateContractSuccess);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressLoading() {
        hiddenProgressLoading();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressLoading() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }
}
