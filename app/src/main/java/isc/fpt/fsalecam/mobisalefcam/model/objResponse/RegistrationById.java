package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;

/**
 * Created by Hau Le on 2018-08-29.
 */
public class RegistrationById implements Serializable{
    @SerializedName("RegId")
    @Expose
    private Integer regId;
    @SerializedName("RegCode")
    @Expose
    private String regCode;
    @SerializedName("ObjId")
    @Expose
    private Integer objId;
    @SerializedName("Contract")
    @Expose
    private String contract;
    @SerializedName("ObjCreateDate")
    @Expose
    private String objCreateDate;
    @SerializedName("RegStatus")
    @Expose
    private String regStatus;
    @SerializedName("RegType")
    @Expose
    private Integer regType;
    @SerializedName("RegTypeName")
    @Expose
    private String regTypeName;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Representive")
    @Expose
    private String representive;
    @SerializedName("Birthday")
    @Expose
    private String birthday;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("NoteAddress")
    @Expose
    private String noteAddress;
    @SerializedName("Passport")
    @Expose
    private String passport;
    @SerializedName("CusTypeDetail")
    @Expose
    private Integer cusTypeDetail;
    @SerializedName("CusTypeDetailName")
    @Expose
    private String cusTypeDetailName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("TaxCode")
    @Expose
    private String taxCode;
    @SerializedName("Phone1")
    @Expose
    private String phone1;
    @SerializedName("Contact1")
    @Expose
    private String contact1;
    @SerializedName("Phone2")
    @Expose
    private String phone2;
    @SerializedName("Contact2")
    @Expose
    private String contact2;
    @SerializedName("Fax")
    @Expose
    private String fax;
    @SerializedName("Nationality")
    @Expose
    private String nationality;
    @SerializedName("NationalityName")
    @Expose
    private String nationalityName;
    @SerializedName("VAT")
    @Expose
    private Integer vAT;
    @SerializedName("LocationId")
    @Expose
    private Integer locationId;
    @SerializedName("BillTo_City")
    @Expose
    private String billToCity;
    @SerializedName("DistrictId")
    @Expose
    private Integer districtId;
    @SerializedName("BillTo_District")
    @Expose
    private String billToDistrict;
    @SerializedName("WardId")
    @Expose
    private Integer wardId;
    @SerializedName("BillTo_Ward")
    @Expose
    private String billToWard;
    @SerializedName("StreetId")
    @Expose
    private Integer streetId;
    @SerializedName("BillTo_Street")
    @Expose
    private String billToStreet;
    @SerializedName("BillTo_Number")
    @Expose
    private String billToNumber;
    @SerializedName("TypeHouse")
    @Expose
    private Integer typeHouse;
    @SerializedName("BuildingId")
    @Expose
    private Integer buildingId;
    @SerializedName("ImageInfo")
    @Expose
    private String imageInfo;
    @SerializedName("IsUpdateImage")
    @Expose
    private Integer isUpdateImage;
    @SerializedName("ImageSignature")
    @Expose
    private String imageSignature;
    @SerializedName("ImageSurvey")
    @Expose
    private String imageSurvey;
    @SerializedName("SaleId")
    @Expose
    private Integer saleId;
    @SerializedName("GroupPoints")
    @Expose
    private String groupPoints;
    @SerializedName("Indoor")
    @Expose
    private Integer indoor;
    @SerializedName("OutDoor")
    @Expose
    private Integer outDoor;
    @SerializedName("InDType")
    @Expose
    private Integer inDType;
    @SerializedName("OutDType")
    @Expose
    private Integer outDType;
    @SerializedName("ODCCableType")
    @Expose
    private String oDCCableType;
    @SerializedName("Latlng")
    @Expose
    private String latlng;
    @SerializedName("ContractTemp")
    @Expose
    private String contractTemp;
    @SerializedName("BookportIdentityId")
    @Expose
    private String bookportIdentityId;
    @SerializedName("LatlngDevice")
    @Expose
    private String latlngDevice;
    @SerializedName("LocalType")
    @Expose
    private Integer localType;
    @SerializedName("LocalTypeName")
    @Expose
    private String localTypeName;
    @SerializedName("PromotionId")
    @Expose
    private Integer promotionId;
    @SerializedName("PromotionName")
    @Expose
    private String promotionName;
    @SerializedName("PromotionDescription")
    @Expose
    private String promotionDescription;
    @SerializedName("MonthOfPrepaid")
    @Expose
    private Integer monthOfPrepaid;
    @SerializedName("Total")
    @Expose
    private Double total;
    @SerializedName("InternetTotal")
    @Expose
    private Double internetTotal;
    @SerializedName("DeviceTotal")
    @Expose
    private Double deviceTotal;
    @SerializedName("DepositFee")
    @Expose
    private Double depositFee;
    @SerializedName("ConnectionFee")
    @Expose
    private Double connectionFee;
    @SerializedName("PaymentMethod")
    @Expose
    private Integer paymentMethod;
    @SerializedName("PaymentMethodName")
    @Expose
    private String paymentMethodName;
    @SerializedName("PaymentMethodPerMonth")
    @Expose
    private Integer paymentMethodPerMonth;
    @SerializedName("PaymentMethodPerMonthName")
    @Expose
    private String paymentMethodPerMonthName;
    @SerializedName("Approved")
    @Expose
    private Integer approved;
    @SerializedName("ApprovedName")
    @Expose
    private String approvedName;
    @SerializedName("UpdateReceiptStatus")
    @Expose
    private Integer updateReceiptStatus;
    @SerializedName("UpdateReceiptDate")
    @Expose
    private String updateReceiptDate;
    @SerializedName("ListDevice")
    @Expose
    private List<ReqDevice> listDevice = null;
    @SerializedName("ListServiceType")
    @Expose
    private List<ItemOfListRes> listServiceType = null;

    public String getPromotionDescription() {
        return promotionDescription;
    }

    public void setPromotionDescription(String promotionDescription) {
        this.promotionDescription = promotionDescription;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

    public Integer getvAT() {
        return vAT;
    }

    public void setvAT(Integer vAT) {
        this.vAT = vAT;
    }

    public String getoDCCableType() {
        return oDCCableType;
    }

    public void setoDCCableType(String oDCCableType) {
        this.oDCCableType = oDCCableType;
    }

    public Integer getRegId() {
        return regId;
    }

    public void setRegId(Integer regId) {
        this.regId = regId;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public Integer getObjId() {
        return objId;
    }

    public void setObjId(Integer objId) {
        this.objId = objId;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getObjCreateDate() {
        return objCreateDate;
    }

    public void setObjCreateDate(String objCreateDate) {
        this.objCreateDate = objCreateDate;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public Integer getRegType() {
        return regType;
    }

    public void setRegType(Integer regType) {
        this.regType = regType;
    }

    public String getRegTypeName() {
        return regTypeName;
    }

    public void setRegTypeName(String regTypeName) {
        this.regTypeName = regTypeName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRepresentive() {
        return representive;
    }

    public void setRepresentive(String representive) {
        this.representive = representive;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNoteAddress() {
        return noteAddress;
    }

    public void setNoteAddress(String noteAddress) {
        this.noteAddress = noteAddress;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public Integer getCusTypeDetail() {
        return cusTypeDetail;
    }

    public void setCusTypeDetail(Integer cusTypeDetail) {
        this.cusTypeDetail = cusTypeDetail;
    }

    public String getCusTypeDetailName() {
        return cusTypeDetailName;
    }

    public void setCusTypeDetailName(String cusTypeDetailName) {
        this.cusTypeDetailName = cusTypeDetailName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getContact1() {
        return contact1;
    }

    public void setContact1(String contact1) {
        this.contact1 = contact1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Integer getVAT() {
        return vAT;
    }

    public void setVAT(Integer vAT) {
        this.vAT = vAT;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getBillToCity() {
        return billToCity;
    }

    public void setBillToCity(String billToCity) {
        this.billToCity = billToCity;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getBillToDistrict() {
        return billToDistrict;
    }

    public void setBillToDistrict(String billToDistrict) {
        this.billToDistrict = billToDistrict;
    }

    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    public String getBillToWard() {
        return billToWard;
    }

    public void setBillToWard(String billToWard) {
        this.billToWard = billToWard;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public String getBillToStreet() {
        return billToStreet;
    }

    public void setBillToStreet(String billToStreet) {
        this.billToStreet = billToStreet;
    }

    public String getBillToNumber() {
        return billToNumber;
    }

    public void setBillToNumber(String billToNumber) {
        this.billToNumber = billToNumber;
    }

    public Integer getTypeHouse() {
        return typeHouse;
    }

    public void setTypeHouse(Integer typeHouse) {
        this.typeHouse = typeHouse;
    }

    public Integer getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Integer buildingId) {
        this.buildingId = buildingId;
    }

    public String getImageInfo() {
        return imageInfo;
    }

    public void setImageInfo(String imageInfo) {
        this.imageInfo = imageInfo;
    }

    public Integer getIsUpdateImage() {
        return isUpdateImage;
    }

    public void setIsUpdateImage(Integer isUpdateImage) {
        this.isUpdateImage = isUpdateImage;
    }

    public String getImageSignature() {
        return imageSignature;
    }

    public void setImageSignature(String imageSignature) {
        this.imageSignature = imageSignature;
    }

    public String getImageSurvey() {
        return imageSurvey;
    }

    public void setImageSurvey(String imageSurvey) {
        this.imageSurvey = imageSurvey;
    }

    public Integer getSaleId() {
        return saleId;
    }

    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    public String getGroupPoints() {
        return groupPoints;
    }

    public void setGroupPoints(String groupPoints) {
        this.groupPoints = groupPoints;
    }

    public Integer getIndoor() {
        return indoor;
    }

    public void setIndoor(Integer indoor) {
        this.indoor = indoor;
    }

    public Integer getOutDoor() {
        return outDoor;
    }

    public void setOutDoor(Integer outDoor) {
        this.outDoor = outDoor;
    }

    public Integer getInDType() {
        return inDType;
    }

    public void setInDType(Integer inDType) {
        this.inDType = inDType;
    }

    public Integer getOutDType() {
        return outDType;
    }

    public void setOutDType(Integer outDType) {
        this.outDType = outDType;
    }

    public String getODCCableType() {
        return oDCCableType;
    }

    public void setODCCableType(String oDCCableType) {
        this.oDCCableType = oDCCableType;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getContractTemp() {
        return contractTemp;
    }

    public void setContractTemp(String contractTemp) {
        this.contractTemp = contractTemp;
    }

    public String getBookportIdentityId() {
        return bookportIdentityId;
    }

    public void setBookportIdentityId(String bookportIdentityId) {
        this.bookportIdentityId = bookportIdentityId;
    }

    public String getLatlngDevice() {
        return latlngDevice;
    }

    public void setLatlngDevice(String latlngDevice) {
        this.latlngDevice = latlngDevice;
    }

    public Integer getLocalType() {
        return localType;
    }

    public void setLocalType(Integer localType) {
        this.localType = localType;
    }

    public String getLocalTypeName() {
        return localTypeName;
    }

    public void setLocalTypeName(String localTypeName) {
        this.localTypeName = localTypeName;
    }

    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public Integer getMonthOfPrepaid() {
        return monthOfPrepaid;
    }

    public void setMonthOfPrepaid(Integer monthOfPrepaid) {
        this.monthOfPrepaid = monthOfPrepaid;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getInternetTotal() {
        return internetTotal;
    }

    public void setInternetTotal(Double internetTotal) {
        this.internetTotal = internetTotal;
    }

    public Double getDeviceTotal() {
        return deviceTotal;
    }

    public void setDeviceTotal(Double deviceTotal) {
        this.deviceTotal = deviceTotal;
    }

    public Double getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(Double depositFee) {
        this.depositFee = depositFee;
    }

    public Double getConnectionFee() {
        return connectionFee;
    }

    public void setConnectionFee(Double connectionFee) {
        this.connectionFee = connectionFee;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public Integer getPaymentMethodPerMonth() {
        return paymentMethodPerMonth;
    }

    public void setPaymentMethodPerMonth(Integer paymentMethodPerMonth) {
        this.paymentMethodPerMonth = paymentMethodPerMonth;
    }

    public String getPaymentMethodPerMonthName() {
        return paymentMethodPerMonthName;
    }

    public void setPaymentMethodPerMonthName(String paymentMethodPerMonthName) {
        this.paymentMethodPerMonthName = paymentMethodPerMonthName;
    }

    public Integer getApproved() {
        return approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public String getApprovedName() {
        return approvedName;
    }

    public void setApprovedName(String approvedName) {
        this.approvedName = approvedName;
    }

    public Integer getUpdateReceiptStatus() {
        return updateReceiptStatus;
    }

    public void setUpdateReceiptStatus(Integer updateReceiptStatus) {
        this.updateReceiptStatus = updateReceiptStatus;
    }

    public String getUpdateReceiptDate() {
        return updateReceiptDate;
    }

    public void setUpdateReceiptDate(String updateReceiptDate) {
        this.updateReceiptDate = updateReceiptDate;
    }

    public List<ReqDevice> getListDevice() {
        return listDevice;
    }

    public void setListDevice(List<ReqDevice> listDevice) {
        this.listDevice = listDevice;
    }

    public List<ItemOfListRes> getListServiceType() {
        return listServiceType;
    }

    public void setListServiceType(List<ItemOfListRes> listServiceType) {
        this.listServiceType = listServiceType;
    }
}
