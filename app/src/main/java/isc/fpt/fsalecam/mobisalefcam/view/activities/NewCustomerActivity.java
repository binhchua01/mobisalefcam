package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationById;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.view.fragment.CusInfoFragment;
import isc.fpt.fsalecam.mobisalefcam.view.fragment.ServiceTypeFragment;
import isc.fpt.fsalecam.mobisalefcam.view.fragment.TotalServiceFeeFragment;
import isc.fpt.fsalecam.mobisalefcam.presenter.NewCustomerPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.NewCustomerPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.NewCustomerView;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.reqGetRegistrationDetail;

public class NewCustomerActivity extends BaseActivity
        implements View.OnClickListener, NewCustomerView{
    @BindView(R.id.ic_nav_back_customer_information) RelativeLayout imgBack;
    @BindView(R.id.tv_label_step_1) TextView labelStep1;
    @BindView(R.id.tv_title_step_1) TextView tvTitleStep1;
    @BindView(R.id.tv_label_step_2) TextView labelStep2;
    @BindView(R.id.tv_title_step_2) TextView tvTitleStep2;
    @BindView(R.id.tv_label_step_3) TextView labelStep3;
    @BindView(R.id.tv_title_step_3) TextView tvTitleStep3;
    @BindColor(R.color.s_grey_dark) int colorDefault;
    @BindColor(R.color.colorPrimary) int colorSelected;
    @BindDrawable(R.drawable.circle_selected) Drawable backgroundSelected;
    @BindDrawable(R.drawable.circle_default) Drawable backgroundDefault;
    @BindView(R.id.select_1) LinearLayout loStep1;
    @BindView(R.id.select_2) LinearLayout loStep2;
    @BindView(R.id.select_3) LinearLayout loStep3;

    public static NewCustomerPresenter presenter;
    private CusInfoFragment cusInfoFragment;
    private ServiceTypeFragment serviceTypeFragment;
    private TotalServiceFeeFragment totalServiceOfMoneyFragment;
    private Dialog dialog;
    //this objReport declare when update cus info, default objReport is null
    //if used this objReport, check null before use
    public static RegistrationById registrationById;
    public static String TAG_SERVICE_TYPE = "service_type";
    public static String TAG_CUS_INFO = "cus_info";
    public static String TAG_TOTAL_AMOUNT = "total_amount";
    public static int checkStep = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFragment();
        initDataViewToUpdate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
    }

    private void initDataViewToUpdate() {
        //init param to call api
        APIService apiService = ApiUtils.getApiService();
        presenter = new NewCustomerPresenterImpl(this);
        if(Utils.DO_UPDATE == 0){
            return;
        }
        //call api get registration by id
        presenter.doGetRegistrationById(this, apiService, reqGetRegistrationDetail);
    }

    private void initFragment() {
        cusInfoFragment = new CusInfoFragment();
        serviceTypeFragment = new ServiceTypeFragment();
        totalServiceOfMoneyFragment = new TotalServiceFeeFragment();
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        loStep1.setOnClickListener(this);
        loStep2.setOnClickListener(this);
        loStep3.setOnClickListener(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_new_customer;
    }

    @Override
    public void onClick(View view) {
        FragmentManager fm = getSupportFragmentManager();
        switch (view.getId()){
            case R.id.ic_nav_back_customer_information:
                onBackPressed();
                break;
            case R.id.select_1:
                if(checkStep == 1){
                    return;
                }
                //this step for back from step 3->2->1
                if(checkStep == 3 || checkStep == 2){
                    changedFragment(cusInfoFragment, TAG_CUS_INFO);
                    displayUiWhenClickStep(1);
                    checkStep = 1;
                    return;
                }
                //this step for move 1->2->3
                changedFragment(cusInfoFragment, TAG_CUS_INFO);
                displayUiWhenClickStep(1);
                break;
            case R.id.select_2:
                if(checkStep == 1 || checkStep == 2){
                    return;
                }
                if(checkStep == 3){
                    changedFragment(serviceTypeFragment, TAG_SERVICE_TYPE);
                    displayUiWhenClickStep(2);
                    checkStep = 2;
                    return;
                }
                CusInfoFragment fragCusInfo = (CusInfoFragment) fm.findFragmentByTag(TAG_CUS_INFO);
                if(fragCusInfo.checkValueCusInfo()){
                    changedFragment(serviceTypeFragment, TAG_SERVICE_TYPE);
                    displayUiWhenClickStep(2);
                }
                break;
            case R.id.select_3:
                if(checkStep == 1 || checkStep == 2 || checkStep == 3){
                    return;
                }
                checkStep = 3;
                ServiceTypeFragment fragService = (ServiceTypeFragment) fm.findFragmentByTag(TAG_SERVICE_TYPE);
                if(fragService.checkValueServiceType()){
                    changedFragment(totalServiceOfMoneyFragment, TAG_TOTAL_AMOUNT);
                    displayUiWhenClickStep(3);
                }
                break;
        }
    }

    @Override
    public void refreshUI(int num) {
        switch (num){
            case 1:
                labelStep1.setBackground(backgroundSelected);
                tvTitleStep1.setTextColor(colorSelected);
                break;
            case 2:
                labelStep2.setBackground(backgroundSelected);
                tvTitleStep2.setTextColor(colorSelected);
                break;
            case 3:
                labelStep3.setBackground(backgroundSelected);
                tvTitleStep3.setTextColor(colorSelected);
                break;
        }
    }

    @Override
    public void getRegistrationByIdSuccess(RegistrationById obj) {
        //this function just call when update cus info
        registrationById = new RegistrationById();
        registrationById = obj;
        this.putOldDataToObj();
        //call function from child frag
        CusInfoFragment frag = (CusInfoFragment) getSupportFragmentManager().findFragmentById(R.id.frag_content_new_customer);
        frag.setOldInfoUpdate();
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressLoading() {
        hiddenProgressLoading();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressLoading() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    //function use this activity
    private void displayUiWhenClickStep(int num){
        switch (num){
            case 1:
                //selected
                labelStep1.setBackground(backgroundSelected);
                tvTitleStep1.setTextColor(colorSelected);
                //unselected
                labelStep2.setBackground(backgroundDefault);
                tvTitleStep2.setTextColor(colorDefault);
                labelStep3.setBackground(backgroundDefault);
                tvTitleStep3.setTextColor(colorDefault);
                break;
            case 2:
                //selected
                labelStep1.setBackground(backgroundSelected);
                tvTitleStep1.setTextColor(colorSelected);
                labelStep2.setBackground(backgroundSelected);
                tvTitleStep2.setTextColor(colorSelected);
                //unselected
                labelStep3.setBackground(backgroundDefault);
                tvTitleStep3.setTextColor(colorDefault);
                break;
            case 3:
                //selected
                labelStep1.setBackground(backgroundSelected);
                tvTitleStep1.setTextColor(colorSelected);
                labelStep2.setBackground(backgroundSelected);
                tvTitleStep2.setTextColor(colorSelected);
                labelStep3.setBackground(backgroundSelected);
                tvTitleStep3.setTextColor(colorSelected);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        DialogUtils.confirmDialog(this, new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                if(data.equals("")){
                    if(getIntent().getBooleanExtra(Utils.CAN_BACK, false)){
                        NewCustomerActivity.super.onBackPressed();
                    }
                    //check if canBack = true, accept return parent activity else nav MainActivity
                    Intent intent = new Intent(NewCustomerActivity.this, MainActivity.class);
                    intent.putExtra(Utils.FLAG_FRAGMENT, Utils.FLAG_FRAG_VALUE);
                    startActivity(intent);
                    finish();
                }
            }
        }).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //reset status do update cus info
        Utils.DO_UPDATE = 0;
        //reset objReport
        registrationById = null;
    }

    private void changedFragment(Fragment fragment, String tag){
        //create fragment manager
        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.content_frag_new_register, fragment, tag);
        fragmentTransaction.commit();
    }

    private void putOldDataToObj(){
        //put data to objReport send to server
        Utils.updateCusInfo.setRegId(String.valueOf(registrationById.getRegId()));
        Utils.updateCusInfo.setRegCode(registrationById.getRegCode());
        Utils.updateCusInfo.setObjId(String.valueOf(registrationById.getObjId()));
        Utils.updateCusInfo.setContract(registrationById.getContract());
        Utils.updateCusInfo.setFullName(registrationById.getFullName());
        Utils.updateCusInfo.setRepresentive(registrationById.getRepresentive());
        Utils.updateCusInfo.setBirthday(registrationById.getBirthday());
        Utils.updateCusInfo.setAddress(registrationById.getAddress());
        Utils.updateCusInfo.setNoteAddress(registrationById.getNoteAddress());
        Utils.updateCusInfo.setPassport(registrationById.getPassport());
        Utils.updateCusInfo.setCusTypeDetail(String.valueOf(registrationById.getCusTypeDetail()));
        Utils.updateCusInfo.setEmail(registrationById.getEmail());
        Utils.updateCusInfo.setTaxCode(registrationById.getTaxCode());
        Utils.updateCusInfo.setPhone1(registrationById.getPhone1());
        Utils.updateCusInfo.setContact1(registrationById.getContact1());
        Utils.updateCusInfo.setPhone2(registrationById.getPhone2());
        Utils.updateCusInfo.setContact2(registrationById.getContact2());
        Utils.updateCusInfo.setFax(registrationById.getFax());
        Utils.updateCusInfo.setNationality(registrationById.getNationality());
        Utils.updateCusInfo.setVat(String.valueOf(registrationById.getVAT()));
        Utils.updateCusInfo.setLocationId(String.valueOf(registrationById.getLocationId()));
        Utils.updateCusInfo.setBillToCity(registrationById.getBillToCity());
        Utils.updateCusInfo.setDistrictId(String.valueOf(registrationById.getDistrictId()));
        Utils.updateCusInfo.setBillToDistrict(registrationById.getBillToDistrict());
        Utils.updateCusInfo.setWardId(String.valueOf(registrationById.getWardId()));
        Utils.updateCusInfo.setBillToWard(registrationById.getBillToWard());
        Utils.updateCusInfo.setStreetId(String.valueOf(registrationById.getStreetId()));
        Utils.updateCusInfo.setBillToStreet(registrationById.getBillToStreet());
        Utils.updateCusInfo.setBillToNumber(registrationById.getBillToNumber());
        Utils.updateCusInfo.setTypeHouse(String.valueOf(registrationById.getTypeHouse()));
        Utils.updateCusInfo.setBuildingId(String.valueOf(registrationById.getBuildingId()));
        Utils.updateCusInfo.setImageInfo("");
        Utils.updateCusInfo.setSaleId(String.valueOf(registrationById.getSaleId()));
        Utils.updateCusInfo.setGroupPoints(registrationById.getGroupPoints());
        Utils.updateCusInfo.setInDoor(String.valueOf(registrationById.getIndoor()));
        Utils.updateCusInfo.setOutDoor(String.valueOf(registrationById.getOutDoor()));
        Utils.updateCusInfo.setInDoorType(String.valueOf(registrationById.getInDType()));
        Utils.updateCusInfo.setOutDoorType(String.valueOf(registrationById.getOutDType()));
        Utils.updateCusInfo.setOdcCableType(registrationById.getODCCableType());
        Utils.updateCusInfo.setContractTemp(registrationById.getContractTemp());
        Utils.updateCusInfo.setBookportIdentityId(registrationById.getBookportIdentityId());
        Utils.updateCusInfo.setLatLngDevice(registrationById.getLatlngDevice());
        Utils.updateCusInfo.setLocalType(String.valueOf(registrationById.getLocalType()));
        Utils.updateCusInfo.setPromotionId(String.valueOf(registrationById.getPromotionId()));
        Utils.updateCusInfo.setMonthOfPrepaid(String.valueOf(registrationById.getMonthOfPrepaid()));
        Utils.updateCusInfo.setTotal(String.valueOf(registrationById.getTotal()));
        Utils.updateCusInfo.setInternetTotal(String.valueOf(registrationById.getInternetTotal()));
        Utils.updateCusInfo.setDeviceTotal(String.valueOf(registrationById.getDeviceTotal()));
        Utils.updateCusInfo.setDepositFee(String.valueOf(registrationById.getDepositFee()));
        Utils.updateCusInfo.setConnectionFee(String.valueOf(registrationById.getConnectionFee()));
        Utils.updateCusInfo.setPaymentMethodPerMonth(String.valueOf(registrationById.getPaymentMethodPerMonth()));
        Utils.updateCusInfo.setPaymentMethodPerMonthName(String.valueOf(registrationById.getPaymentMethodPerMonthName()));
        Utils.updateCusInfo.setIp("");
        Utils.updateCusInfo.setListServiceType(registrationById.getListServiceType());
        Utils.updateCusInfo.setListDevice(registrationById.getListDevice());
    }
}
