package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqBookportManual;

/**
 * Created by Hau Le on 2018-08-14.
 */
public interface EnterLengthCablePresenter {
    void doBookportManual(Context context, APIService apiService, ReqBookportManual reqBookportManual);
}
