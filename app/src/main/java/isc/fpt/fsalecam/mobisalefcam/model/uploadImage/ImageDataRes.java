package isc.fpt.fsalecam.mobisalefcam.model.uploadImage;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by Hau Le on 2018-07-25.
 */
public class ImageDataRes {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Path")
    @Expose
    private String path;
    @SerializedName("LinkId")
    @Expose
    private Integer linkId;
    @SerializedName("Source")
    @Expose
    private Integer source;
    @SerializedName("Type")
    @Expose
    private Integer type;
    @SerializedName("Date")
    @Expose
    private String date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getLinkId() {
        return linkId;
    }

    public void setLinkId(Integer linkId) {
        this.linkId = linkId;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
