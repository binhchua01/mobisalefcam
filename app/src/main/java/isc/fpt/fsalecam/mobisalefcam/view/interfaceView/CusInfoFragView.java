package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.database.CustomerType;
import isc.fpt.fsalecam.mobisalefcam.database.Nationality;

/**
 * Created by Hau Le on 2018-08-15.
 */
public interface CusInfoFragView {
    void getCusTypeListSuccess(ArrayList<CustomerType> list);
    void getNationalityListSuccess(ArrayList<Nationality> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
