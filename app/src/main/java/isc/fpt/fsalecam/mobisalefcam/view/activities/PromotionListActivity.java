package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.ChoosePromotionAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChoosePromotionPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChoosePromotionPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.PromotionCode;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChoosePromotionView;

public class PromotionListActivity extends BaseActivity
        implements View.OnClickListener, ChoosePromotionView {
    @BindView(R.id.img_nav_back_choose_promotion) RelativeLayout loBack;
    @BindView(R.id.img_promotion_refresh) RelativeLayout loRefresh;
    @BindView(R.id.layout_choose_promotion) LinearLayout loParent;
    @BindView(R.id.recycler_view_promotion) RecyclerView recyclerView;
    @BindView(R.id.edt_search_promotion) EditText edtSearch;

    private Realm realm;
    private APIService apiService;
    private ChoosePromotionPresenter presenter;
    private Dialog dialog;
    private ArrayList<PromotionCode> list;
    private ChoosePromotionAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        setEvent();
        initDatabase();
        initDataView();
        callApi();
    }

    private void initDataView() {
        list = new ArrayList<>();
        adapter = new ChoosePromotionAdapter(this, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //return data to parent view
                Intent intent = new Intent();
                intent.putExtra(Utils.PROMOTION,
                    list.get(position).getPromotionId() + ":" +
                            list.get(position).getPromotionName() + ":" +
                            list.get(position).getMonthOfPrepaid() + ":" +
                            list.get(position).getRealPrepaid() + ":" +
                            list.get(position).getDescription()
                );
                setResult(Utils.REQUEST_CODE_PROMOTION, intent);
                finish();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new ChoosePromotionPresenterImpl(this);
        presenter.doGetPromotionList(
                this,
                apiService,
                realm,
                Utils.updateCusInfo.getLocalType(),
                Utils.updateCusInfo.getLocationId(),
                Utils.updateCusInfo.getContract());
    }

    private void initDatabase() {
        realm = Realm.getDefaultInstance();
    }

    private void setEvent() {
        loBack.setOnClickListener(this);
        loRefresh.setOnClickListener(this);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.searchPromotion(realm, String.valueOf(editable));
            }
        });
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_promotion_list;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_nav_back_choose_promotion:
                onBackPressed();
                break;
            case R.id.img_promotion_refresh:
                //refresh promotion list
                presenter.doRefresh(
                        this,
                        apiService,
                        realm,
                        Utils.updateCusInfo.getLocalType(),
                        Utils.updateCusInfo.getLocationId(),
                        "");
                break;
        }
    }

    @Override
    public void getPromotionListSuccess(ArrayList<PromotionCode> list) {
        this.list = list;
        adapter.notifyData(list);
    }

    @Override
    public void searchPromotion(ArrayList<PromotionCode> list) {

    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
