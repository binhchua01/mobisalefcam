package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationById;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationDetail;

/**
 * Created by Hau Le on 2018-08-27.
 */
public interface CusInfoDetailsView {
    void getRegistrationDetailSuccess(RegistrationDetail obj);
    void getRegistrationById(RegistrationById obj);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showDialogProgress();
    void hiddenProgressDialog();
}
