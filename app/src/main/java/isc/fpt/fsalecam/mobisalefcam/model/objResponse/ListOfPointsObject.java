package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Hau Le on 2018-08-10.
 */
public class ListOfPointsObject {
    @SerializedName("BookportIdentityID")
    @Expose
    private String bookportIdentityID;
    @SerializedName("ListOfPoints")
    @Expose
    private List<ListOfPoint> listOfPoints = null;

    public String getBookportIdentityID() {
        return bookportIdentityID;
    }

    public void setBookportIdentityID(String bookportIdentityID) {
        this.bookportIdentityID = bookportIdentityID;
    }

    public List<ListOfPoint> getListOfPoints() {
        return listOfPoints;
    }

    public void setListOfPoints(List<ListOfPoint> listOfPoints) {
        this.listOfPoints = listOfPoints;
    }
}
