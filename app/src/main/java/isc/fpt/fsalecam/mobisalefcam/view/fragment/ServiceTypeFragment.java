package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.view.activities.EquipmentActivity;
import isc.fpt.fsalecam.mobisalefcam.view.activities.PackageServiceActivity;
import isc.fpt.fsalecam.mobisalefcam.view.activities.PromotionListActivity;
import isc.fpt.fsalecam.mobisalefcam.adapter.DeviceListSelectedAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.presenter.ServiceTypeFragPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ServiceTypeFragPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.ConnectionFee;
import isc.fpt.fsalecam.mobisalefcam.database.CusInfoPaymentMethod;
import isc.fpt.fsalecam.mobisalefcam.database.DeviceList;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.SelectServiceTypeCallback;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ServiceTypeFragView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.BaseActivity.showToast;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.TAG_TOTAL_AMOUNT;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.checkStep;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.presenter;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.NewCustomerActivity.registrationById;

/**
 * Created by Hau Le on 2018-07-23.
 */
public class  ServiceTypeFragment extends BaseFragment
        implements ServiceTypeFragView, View.OnClickListener{
    private Dialog dialog;
    private ServiceTypeFragPresenter serviceTypeFragPresenter;
    private APIService apiService;
    private ArrayList<DeviceList> listDeviceSelected;
    private ArrayList<ReqDevice> listDevice;
    private Dialog dialogChooseTypeService;
    private DeviceListSelectedAdapter adapter;
    //this is old list, use for set old data to view
    private ArrayList<ItemOfListRes> listServiceType = new ArrayList<>();

    @BindView(R.id.tv_service_type) TextView tvServiceType;
    @BindView(R.id.layout_choose_service_type) RelativeLayout loServiceType;
    @BindView(R.id.layout_of_device) LinearLayout loDevice;
    @BindView(R.id.layout_choose_package_service) RelativeLayout loPackageService;
    @BindView(R.id.tv_package_service) TextView tvPackageService;
    @BindView(R.id.tv_port_name) TextView tvPortName;
    @BindView(R.id.layout_promotion_list) RelativeLayout loPromotion;
    @BindView(R.id.tv_promotion_name) TextView tvPromotionName;
    @BindView(R.id.tv_show_more_promotion) TextView tvShowMore;
    @BindView(R.id.tv_promotion_name_hidden) TextView tvPromotionHidden;
    @BindView(R.id.label_tv_promotion) TextView tvLabelPromotion;
    @BindView(R.id.layout_promotion) LinearLayout loWrapperTvPromotion;
    @BindView(R.id.tv_show_payment_method) TextView tvPaymentMethod;
    @BindView(R.id.layout_payment_method) RelativeLayout loPaymentMethod;
    @BindView(R.id.tv_connection_fee) TextView tvConnectionFee;
    @BindView(R.id.layout_connection_fee) RelativeLayout loConnectionFee;
    @BindView(R.id.layout_choose_devices) RelativeLayout loChooseDevices;
    @BindView(R.id.recycler_view_device_list) RecyclerView recyclerViewDeviceList;
    @BindView(R.id.btn_submit_service_type) Button btnSubmit;

    private Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEvent();
        initDataView();
        setUpParamsCallApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        setOldInfoUpdate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_service_type;
    }

    @Override
    public void getServiceTypeListSuccess(ArrayList<ItemOfListRes> list) {
        dialogChooseTypeService = DialogUtils.showDialogChooseServiceType(getContext(), list, listServiceType,
            new SelectServiceTypeCallback() {
                @Override
                public void serviceTypeList(ArrayList<String> list) {
                    updateUiWhenSelectService(list);
                }
            });
    }

    @Override
    public void getPaymentMethodSuccess(ArrayList<CusInfoPaymentMethod> list) {
        DialogUtils.showDialogChoosePaymentMethod(getContext(), list,
                new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                String[] pm = data.split(":");
                Utils.updateCusInfo.setPaymentMethodPerMonth(pm[0]);
                Utils.updateCusInfo.setPaymentMethodPerMonthName(pm[1]);
                tvPaymentMethod.setText(pm[1]);
            }
        }).show();
    }

    @Override
    public void getConnectionFeeList(ArrayList<ConnectionFee> list) {
        DialogUtils.showDialogChooseConnectionFee(getContext(), list,
                new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                String[] connectionFee = data.split(":");
                Utils.updateCusInfo.setConnectionFee(connectionFee[0]);
                tvConnectionFee.setText(connectionFee[1]);
            }
        }).show();
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(getContext(), mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(getContext(), mes).show();
    }

    @Override
    public void showProgressDialog() {
        dialog = DialogUtils.showLoadingDialog(getContext(), getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.layout_choose_service_type:
                if(dialogChooseTypeService!=null && !dialogChooseTypeService.isShowing()){
                    dialogChooseTypeService.show();
                }
                break;
            case R.id.layout_choose_package_service:
                intent = new Intent(getContext(), PackageServiceActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_PACKAGE_SERVICE);
                break;
            case R.id.layout_promotion_list:
                if(!Utils.updateCusInfo.getLocalType().equals("")){
                    intent = new Intent(getContext(), PromotionListActivity.class);
                    startActivityForResult(intent, Utils.REQUEST_CODE_PROMOTION);
                }else{
                    loPackageService.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
                }
                break;
            case R.id.layout_payment_method:
                serviceTypeFragPresenter.doGetPaymentMethodList(getContext(), apiService, realm);
                break;
            case R.id.layout_connection_fee:
                serviceTypeFragPresenter.doGetConnectionFeeList(getContext(), apiService,
                        Utils.updateCusInfo.getLocationId(), realm);
                break;
            case R.id.layout_choose_devices:
                if(!Utils.updateCusInfo.getMonthOfPrepaid().equals("")){
                    intent = new Intent(getContext(), EquipmentActivity.class);
                    if(listDeviceSelected.size()!=0){
                        intent.putExtra(Utils.DEVICE_LIST_SELECTED, listDeviceSelected);
                    }
                    startActivityForResult(intent, Utils.REQUEST_CODE_DEVICE);
                }else{
                    showToast(getContext(), getResources().getString(R.string.select_promotion_code));
                }
                break;
            case R.id.btn_submit_service_type:
                if(checkValueServiceType()){
                    changedFragment(new TotalServiceFeeFragment(), TAG_TOTAL_AMOUNT);
                    //refresh ui
                    presenter.changedUIActivity(3);//call method from parent
                    checkStep = 3;
                }
                break;
            case R.id.tv_show_more_promotion:
                if(isVisibility(tvPromotionHidden)){
                    tvPromotionHidden.setVisibility(View.GONE);
                    tvShowMore.setText(getResources().getString(R.string.show_more));
                }else{
                    tvPromotionHidden.setVisibility(View.VISIBLE);
                    tvShowMore.setText(getResources().getString(R.string.show_less));
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null){
            if(requestCode == Utils.REQUEST_CODE_PACKAGE_SERVICE) {
                String[] ps = data.getStringExtra(Utils.PACKAGE_SERVICE).split(":");
                Utils.updateCusInfo.setLocalType(ps[0]);
                Utils.updateCusInfo.setLocalTypeName(ps[1]);
                tvPackageService.setText(ps[1]);
                //TODO: reset promotion when re-select service package
                Utils.updateCusInfo.setPromotionId("");
                Utils.updateCusInfo.setPromotionName("");
                Utils.updateCusInfo.setMonthOfPrepaid("");
                tvPromotionName.setText("");
                loWrapperTvPromotion.setVisibility(View.GONE);
                tvPromotionHidden.setVisibility(View.GONE);
                //reset list device
                listDevice.clear();
                listDeviceSelected.clear();
                Utils.updateCusInfo.setListDevice(listDevice);
                adapter.notifyData(listDevice);
            }else if(requestCode == Utils.REQUEST_CODE_PROMOTION){
                String[] pro = data.getStringExtra(Utils.PROMOTION).split(":");
                Utils.updateCusInfo.setPromotionId(pro[0]);
                Utils.updateCusInfo.setPromotionName(pro[1]);
                Utils.updateCusInfo.setMonthOfPrepaid(pro[2]);
                Utils.updateCusInfo.setPromotionDescription(pro[4]);
                tvPromotionName.setText(pro[1]);
                tvPromotionHidden.setText(pro[4]);
                //show promotion name
                loWrapperTvPromotion.setVisibility(View.VISIBLE);
                //reset list device
                listDevice.clear();
                listDeviceSelected.clear();
                Utils.updateCusInfo.setListDevice(listDevice);
                adapter.notifyData(listDevice);
            }else if(requestCode == Utils.REQUEST_CODE_DEVICE){
                listDeviceSelected = (ArrayList<DeviceList>) data.getSerializableExtra(Utils.DEVICE_LIST_SELECTED);
                listDevice.clear();
                for(int i = 0; i < listDeviceSelected.size(); i++){
                    listDevice.add(new ReqDevice(
                            listDeviceSelected.get(i).getValue(),
                            listDeviceSelected.get(i).getName(),
                            "1",
                            listDeviceSelected.get(i).getChangeNumber(),
                            Double.parseDouble(String.valueOf(listDeviceSelected.get(i).getPrice())),
                            0.0));
                }
                Utils.updateCusInfo.setListDevice(listDevice);
                adapter.notifyData(listDevice);
            }
        }
    }

    private void updateUiWhenSelectService(ArrayList<String> list){
        if(list.size() != 0){
            for(int i = 0; i < list.size(); i++){
                //compare all service if after add more.....
                if(Integer.parseInt(list.get(i)) == Utils.DEVICES){
                    tvServiceType.setText(getResources().getString(R.string.internet_and_devices));
                    //update list service
                    listServiceType.clear();
                    listServiceType.add(new ItemOfListRes(1, "Internet"));
                    listServiceType.add(new ItemOfListRes(2, "Devices"));
                }else{
                    tvServiceType.setText(getResources().getString(R.string.ui_text_internet));
                    listServiceType.clear();
                    listServiceType.add(new ItemOfListRes(1, "Internet"));
                }
                //.....
            }
        }else{
            //list size == 0, is haven't selected diff service
            //show only internet, hidden all diff service
            listDeviceSelected.clear();
            listDevice.clear();
            adapter.notifyData(listDevice);
            tvServiceType.setText(getResources().getString(R.string.ui_text_internet));
            listServiceType.clear();
            listServiceType.add(new ItemOfListRes(1, "Internet"));
        }
        Utils.updateCusInfo.setListServiceType(listServiceType);
    }

    private void changedFragment(Fragment fragment, String tag){
        //create fragment manager
        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.content_frag_new_register, fragment, tag);
        fragmentTransaction.commit();
    }

    public boolean checkValueServiceType(){
        if(tvPackageService.getText().toString().isEmpty()){
            loPackageService.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvPackageService.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loPackageService.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvPackageService.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvPromotionName.getText().toString().isEmpty()){
            loPromotion.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvLabelPromotion.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loPromotion.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvLabelPromotion.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvConnectionFee.getText().toString().equals(Objects.requireNonNull(getContext())
                .getResources().getString(R.string.ui_text_choose_connection_fee))){
            loConnectionFee.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvConnectionFee.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loConnectionFee.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvConnectionFee.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvPaymentMethod.getText().toString().equals(getContext().getResources().getString(R.string.ui_text_choose_type_payment))){
            loPaymentMethod.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvPaymentMethod.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loPaymentMethod.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvPaymentMethod.setTextColor(getResources().getColor(R.color.s_black));
        }

        //validate each line to show dialog message
        if(tvPackageService.getText().toString().isEmpty()){
            showDialogRequired(getResources().getString(R.string.mes_service_type_required)).show();
            return false;
        }else if(tvPromotionName.getText().toString().isEmpty()){
            showDialogRequired(getResources().getString(R.string.mes_promotion_required)).show();
            return false;
        }else if(tvConnectionFee.getText().toString().equals(Objects.requireNonNull(getContext())
                .getResources().getString(R.string.ui_text_choose_connection_fee))){
            showDialogRequired(getResources().getString(R.string.mes_connection_fee_required)).show();
            return false;
        }else if(tvPaymentMethod.getText().toString()
                .equals(getContext().getResources().getString(R.string.ui_text_choose_type_payment))){
            showDialogRequired(getResources().getString(R.string.mes_payment_type_required)).show();
            return false;
        }
        return true;
    }

    private void initDataView() {
        tvPortName.setText(Utils.updateCusInfo.getGroupPoints());
        //add list default
        if(Utils.updateCusInfo.getListServiceType().size() == 0){
            listServiceType.add(new ItemOfListRes(1, "Internet"));
            Utils.updateCusInfo.setListServiceType(listServiceType);
        }else{
            listServiceType = (ArrayList<ItemOfListRes>) Utils.updateCusInfo.getListServiceType();
        }
        //param of list device
        listDeviceSelected = new ArrayList<>();
        listDevice = new ArrayList<>();
        adapter = new DeviceListSelectedAdapter(getContext(), listDevice);
        recyclerViewDeviceList.setAdapter(adapter);
    }

    private void setEvent() {
        loServiceType.setOnClickListener(this);
        loPackageService.setOnClickListener(this);
        loPromotion.setOnClickListener(this);
        loPaymentMethod.setOnClickListener(this);
        loConnectionFee.setOnClickListener(this);
        loChooseDevices.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        tvShowMore.setOnClickListener(this);
        //refresh ui text change
        tvPackageService.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvPackageService.setTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    loPackageService.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvPromotionName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvLabelPromotion.setTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    loPromotion.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvConnectionFee.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvConnectionFee.setTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    loConnectionFee.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvPaymentMethod.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvPaymentMethod.setTextColor(getResources().getColor(R.color.s_black));
                if(!String.valueOf(editable).isEmpty()){
                    loPaymentMethod.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
    }

    private void setUpParamsCallApi() {
        apiService = ApiUtils.getApiService();
        serviceTypeFragPresenter = new ServiceTypeFragPresenterImpl(this);
        serviceTypeFragPresenter.doGetServiceTypeList(getContext(), apiService);
    }

    private void setOldInfoUpdate() {
        //this is update when not create cus info yet
        if(listServiceType.size() == 2){
            tvServiceType.setText(getResources().getString(R.string.internet_and_devices));
        }else{
            tvServiceType.setText(getResources().getString(R.string.ui_text_internet));
        }
        //internet
        if(!Utils.updateCusInfo.getLocalTypeName().equals("")){
            tvPackageService.setText(Utils.updateCusInfo.getLocalTypeName());
        }
        if(!Utils.updateCusInfo.getPromotionName().equals("")){
            loWrapperTvPromotion.setVisibility(View.VISIBLE);
            tvPromotionHidden.setText(Utils.updateCusInfo.getPromotionDescription());
            tvPromotionName.setText(Utils.updateCusInfo.getPromotionName());
        }
        if(!Utils.updateCusInfo.getConnectionFee().equals("")){
            tvConnectionFee.setText(Utils.updateCusInfo.getConnectionFee());
        }
        if(!Utils.updateCusInfo.getPaymentMethodPerMonth().equals("")){
            tvPaymentMethod.setText(Utils.updateCusInfo.getPaymentMethodPerMonthName());
        }
        if(Utils.updateCusInfo.getListDevice().size() !=0 ){
            //notify list device
            adapter.notifyData((ArrayList<ReqDevice>) Utils.updateCusInfo.getListDevice());
            //set list selected, it's use for reselected
            DeviceList obj;
            for(int i=0; i < Utils.updateCusInfo.getListDevice().size(); i++){
                obj = new DeviceList();
                obj.setValue(Utils.updateCusInfo.getListDevice().get(i).getValue());
                obj.setName(Utils.updateCusInfo.getListDevice().get(i).getName());
                obj.setChangeNumber(Utils.updateCusInfo.getListDevice().get(i).getChangeNumber());
                obj.setPrice(String.valueOf(Utils.updateCusInfo.getListDevice().get(i).getPrice()));
                obj.setIsSelected(1);
                listDeviceSelected.add(obj);
            }
        }

        //TODO: use for update when had create cus info
        if(registrationById == null){
            return;
        }
        if(registrationById.getListServiceType().size() == 2){
            tvServiceType.setText(getResources().getString(R.string.internet_and_devices));
        }else{
            tvServiceType.setText(getResources().getString(R.string.ui_text_internet));
        }
        //internet
        tvPackageService.setText(registrationById.getLocalTypeName());
        Utils.updateCusInfo.setLocalType(String.valueOf(registrationById.getLocalType()));
        tvPromotionName.setVisibility(View.VISIBLE);
        tvPromotionName.setText(registrationById.getPromotionName());
        Utils.updateCusInfo.setPromotionId(String.valueOf(registrationById.getPromotionId()));
        tvConnectionFee.setText(String.valueOf(registrationById.getConnectionFee()));
        tvPortName.setText(registrationById.getGroupPoints());
        tvPaymentMethod.setText(registrationById.getPaymentMethodPerMonthName());
        Utils.updateCusInfo.setPaymentMethodPerMonth(String.valueOf(registrationById.getPaymentMethodPerMonth()));
        //devices
        adapter.notifyData((ArrayList<ReqDevice>) registrationById.getListDevice());
        //set list selected, it's use for reselected
        DeviceList obj;
        for(int i=0; i < registrationById.getListDevice().size(); i++){
            obj = new DeviceList();
            obj.setValue(registrationById.getListDevice().get(i).getValue());
            obj.setName(registrationById.getListDevice().get(i).getName());
            obj.setChangeNumber(registrationById.getListDevice().get(i).getChangeNumber());
            obj.setPrice(String.valueOf(registrationById.getListDevice().get(i).getPrice()));
            obj.setIsSelected(1);
            listDeviceSelected.add(obj);
        }
    }

    private Dialog showDialogRequired(String str){
        return DialogUtils.showMessageDialogVerButton(getContext(), str);

    }
}
