package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-13.
 */
public class ReqUpdateAppointment {
    private int deptId;
    private int subId;
    private String timeZone;
    private String appointmentDate;
    private String username;
    private String regCode;
    private int objId;

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public int getObjId() {
        return objId;
    }

    public void setObjId(int objId) {
        this.objId = objId;
    }

    public LinkedHashMap<String, String> generateObjUpdateAppointment(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("DeptID",String.valueOf(getDeptId()));
        obj.put("SubID", String.valueOf(getSubId()));
        obj.put("TimeZone", getTimeZone());
        obj.put("AppointmentDate", getAppointmentDate());
        obj.put("UserName", getUsername());
        obj.put("RegCode", getRegCode());
        obj.put("ObjId", String.valueOf(getObjId()));
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("DeptID",String.valueOf(getDeptId()));
            obj.put("SubID", String.valueOf(getSubId()));
            obj.put("TimeZone", getTimeZone());
            obj.put("AppointmentDate", getAppointmentDate());
            obj.put("UserName", getUsername());
            obj.put("RegCode", getRegCode());
            obj.put("ObjId", String.valueOf(getObjId()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
