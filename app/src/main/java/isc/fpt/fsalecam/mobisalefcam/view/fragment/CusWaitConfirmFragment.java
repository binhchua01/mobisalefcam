package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import java.util.ArrayList;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.view.activities.CusInfoDetailsActivity;
import isc.fpt.fsalecam.mobisalefcam.view.activities.MainActivity;
import isc.fpt.fsalecam.mobisalefcam.adapter.CusInfoListAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Registration;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateCusInfo;
import isc.fpt.fsalecam.mobisalefcam.presenter.CusWaitConfirmPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.CusWaitConfirmPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.CusWaitConfirmView;
import static isc.fpt.fsalecam.mobisalefcam.view.fragment.CusInfoListFragment.obj;

/**
 * Created by Hau Le on 2018-07-10.
 */
public class CusWaitConfirmFragment extends BaseFragment
        implements CusWaitConfirmView {
    @BindView(R.id.recycler_view_cus_wait_confirm) RecyclerView recyclerView;
    @BindView(R.id.progress_bar_cus_wait) ProgressBar progressBar;
    @BindView(R.id.layout_cus_waiting_confirm) LinearLayout layoutWrapper;

    public CusInfoListAdapter adapter;
    private ArrayList<Registration> list;
    public static UpdateCusInfo cusInfo = new UpdateCusInfo();
    private MainActivity activity;
    private APIService apiService;
    private CusWaitConfirmPresenter presenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hiddenKeySoft(layoutWrapper, getActivity());
        initView();
        apiService = ApiUtils.getApiService();
        presenter = new CusWaitConfirmPresenterImpl(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        callApi();
    }

    private void initView() {
        list = new ArrayList<>();
        adapter = new CusInfoListAdapter(activity, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //send reg info to cus info detail activity
                cusInfo.setRegID(list.get(position).getRegID());
                cusInfo.setRegCode(list.get(position).getRegCode());
                //handle can back
                Intent intent = new Intent(activity, CusInfoDetailsActivity.class);
                intent.putExtra(Utils.CAN_BACK, true);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void callApi() {
        //Cus Info List waiting confirm
        obj.setDataType("1");
        obj.setSearchContent("");
        presenter.doGetRegistration(getContext(), apiService, obj);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_cus_wait_confirm;
    }

    @Override
    public void getRegistrationAllSuccess(ArrayList<Registration> list) {
        this.list = list;
        adapter.notifyData(list);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(getContext(), mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(getContext(), mes).show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hiddenProgressDialog() {
        if(progressBar!=null && recyclerView!=null){
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void notifyData(ArrayList<Registration> list){
        adapter.notifyData(list);
    }
}
