package isc.fpt.fsalecam.mobisalefcam.model.uploadImage;

/**
 * Created by Hau Le on 2018-07-25.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenerateTokenRes {
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Amount")
    @Expose
    private Integer amount;
    @SerializedName("Code")
    @Expose
    private Integer code;
    @SerializedName("Description")
    @Expose
    private Object description;
    @SerializedName("Data")
    @Expose
    private List<TokenDataRes> data = null;
    @SerializedName("FunctionInfo")
    @Expose
    private Object functionInfo;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public List<TokenDataRes> getData() {
        return data;
    }

    public void setData(List<TokenDataRes> data) {
        this.data = data;
    }

    public Object getFunctionInfo() {
        return functionInfo;
    }

    public void setFunctionInfo(Object functionInfo) {
        this.functionInfo = functionInfo;
    }
}
