package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;


import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.BookportAuto;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ListOfPoint;

/**
 * Created by Hau Le on 2018-08-10.
 */
public interface SurveyPopView {
    void generateAddressSuccess(String str);
    void getListOfPoints(ArrayList<ListOfPoint> list);
    //method called when getListOfPoints haven't Port at address to create potential cus
    void bookportAutoFailedCreatePotentialCus();//exception -150, -> create potential customer
    void bookportAutoSuccess(BookportAuto bookportAuto);
    void bookportAutoFailed(String mes);
    void callApiException(String str);
    void expiredToken(String str);
    void showProgressDialog();
    void hiddenProgressDialog();
}
