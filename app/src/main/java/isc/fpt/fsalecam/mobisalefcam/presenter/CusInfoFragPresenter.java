package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
/**
 * Created by Hau Le on 2018-08-15.
 */
public interface CusInfoFragPresenter {
    void doGetCusTypeList(Context context, APIService apiService, Realm realm);
    void doGetNationalityList(Context context, APIService apiService, Realm realm);
}
