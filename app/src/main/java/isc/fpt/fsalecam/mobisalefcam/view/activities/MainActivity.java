package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindColor;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqReportContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.view.fragment.CusInfoListFragment;
import isc.fpt.fsalecam.mobisalefcam.view.fragment.HomeFragment;
import isc.fpt.fsalecam.mobisalefcam.view.fragment.ReportFilterFragment;
import isc.fpt.fsalecam.mobisalefcam.presenter.MainPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.MainPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.MainView;

public class MainActivity extends BaseActivity implements View.OnClickListener, MainView{
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.img_open_nav_menu) RelativeLayout imgNavMenu;
    @BindView(R.id.img_report_back) RelativeLayout loBack;
    @BindView(R.id.nav_reset_password) TextView tvNavResetPass;
    @BindView(R.id.nav_potential_customer) TextView tvNavPotentialCustomer;
    @BindView(R.id.nav_notification) RelativeLayout loNavNotification;
    @BindView(R.id.nav_report) TextView tvNavReport;
    @BindView(R.id.nav_search) RelativeLayout loNavSearch;
    @BindView(R.id.nav_create_customer_information) TextView tvNavCreateCI;
    @BindView(R.id.tv_nav_search) TextView tvNavSearch;
    @BindView(R.id.tv_nav_notifications) TextView tvNavNotification;
    @BindView(R.id.nav_help) TextView tvNavHelp;
    @BindView(R.id.img_log_out) ImageView imgLogOut;
    @BindView(R.id.ic_icon_nav_notifications) RelativeLayout imgNotifications;
    @BindView(R.id.tv_app_version_left_menu) TextView tvVersionName;
    //wrapper bottom menu
    @BindView(R.id.layout_home) LinearLayout layoutHome;
    @BindView(R.id.layout_cus_info_list) LinearLayout layoutCustomerInfo;
    @BindView(R.id.layout_report) LinearLayout layoutSearch;
    @BindView(R.id.layout_impl) LinearLayout layoutImpl;
    @BindView(R.id.user_avatar) ImageView imgUserAvatar;
    @BindView(R.id.tv_label_activity) TextView tvLabelActivity;
    //icon bottom menu
    @BindView(R.id.img_home) ImageView imgHome;
    @BindView(R.id.img_info) ImageView imgInfo;
    @BindView(R.id.img_search) ImageView imgReport;
    @BindView(R.id.img_impl) ImageView imgImpl;
    @BindView(R.id.img_add_sale) ImageView imgAddSale;
    //line bellow icon bottom menu
//    @BindView(R.id.line_home) RelativeLayout lineHome;
//    @BindView(R.id.line_search) RelativeLayout lineReport;
//    @BindView(R.id.line_info) RelativeLayout lineInfo;
//    @BindView(R.id.line_impl) RelativeLayout lineImpl;
    @BindView(R.id.nav_left_menu_user_name_info) TextView tvNavUsername;
    @BindColor(R.color.nav_left_menu_selected) int colorSelected;
    @BindColor(R.color.nav_left_menu_default) int colorDefault;

    private Dialog dialog;
    private Dialog dialogChangePassword;
    private Realm realm;
    private HomeFragment homeFragment;
    private CusInfoListFragment cusInfoFragment;
    public static ReqReportContract objReport;
    private MainPresenter presenter;
    private APIService apiService;
    //this param use for Fragment Report
    public static int canBack = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        initFragment();
        initView();
        callApi();
        setDefaultFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        //set up object update cus info
        setUpObj();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if(canBack == 1){
            changedFragment(new ReportFilterFragment());
            controlUIBottomMenu(3);
            canBack = -1;
        }else if(canBack == 0){
            changedFragment(homeFragment);
            controlUIBottomMenu(1);
            canBack = -1;
        }else{
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                //quit app
                DialogUtils.showQuestionExitApp(this, getResources().getString(R.string.exit_app)).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            //open left menu
            case R.id.img_open_nav_menu:
                drawer.openDrawer(GravityCompat.START);
                break;
                //TODO: bottom menu event
            case R.id.layout_home:
                changedFragment(homeFragment);
                //refresh nav bottom menu
                controlUIBottomMenu(1);
                break;
            case R.id.layout_cus_info_list:
                changedFragment(cusInfoFragment);
                controlUIBottomMenu(2);
                break;
            case R.id.layout_report:
                changedFragment(new ReportFilterFragment());
                controlUIBottomMenu(3);
                break;
            case R.id.layout_impl:
                DialogUtils.showMessageDialogVerButton(this,
                        getResources().getString(R.string.message_dialog_implement)).show();
                break;
            case R.id.img_add_sale:
                startActivity(new Intent(this, AddressSetupActivity.class));
                break;
            case R.id.ic_icon_nav_notifications:
                DialogUtils.showMessageDialogVerButton(this,
                        getResources().getString(R.string.message_next_version)).show();
                break;
                //TODO: left menu event
            case R.id.nav_create_customer_information:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, AddressSetupActivity.class));
                break;
            case R.id.nav_search:
                drawer.closeDrawer(GravityCompat.START);
                DialogUtils.showMessageDialogVerButton(this,
                        getResources().getString(R.string.message_next_version)).show();
                break;
            case R.id.nav_report:
                drawer.closeDrawer(GravityCompat.START);
                changedFragment(new ReportFilterFragment());
                controlUIBottomMenu(3);
                break;
            case R.id.nav_notification:
                drawer.closeDrawer(GravityCompat.START);
                DialogUtils.showMessageDialogVerButton(this,
                        getResources().getString(R.string.message_next_version)).show();
                break;
            case R.id.nav_potential_customer:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, PotentialCusListActivity.class));
                break;
            case R.id.nav_help:
                drawer.closeDrawer(GravityCompat.START);
                DialogUtils.showMessageDialogVerButton(this,
                        getResources().getString(R.string.message_next_version)).show();
                break;
            case R.id.nav_reset_password:
                drawer.closeDrawer(GravityCompat.START);
                dialogChangePassword = DialogUtils.dialogResetPassword(this, apiService, presenter);
                dialogChangePassword.show();
                break;
            case R.id.img_log_out:
                drawer.closeDrawer(GravityCompat.START);
                logOut();
                break;
            case R.id.img_report_back:
                if(canBack == 1){
                    changedFragment(new ReportFilterFragment());
                    controlUIBottomMenu(3);
                    canBack = 0;
                }else{
                    changedFragment(homeFragment);
                    controlUIBottomMenu(1);
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void getInfoSuccess(SaleOfInfo saleInfo) {
        Utils.saleOfInfo = saleInfo;
        //TODO: get info success, progress anything...
        tvNavUsername.setText(saleInfo.getUserName());
        if(SharedPref.get(this, SharedPref.Key.EMAIL, "").equals("")){
            //call method set sale info from fragment
            FragmentManager fm = getSupportFragmentManager();
            HomeFragment frag = (HomeFragment) fm.findFragmentById(R.id.fragment_home);
            assert frag != null;
            frag.initSaleInfo(saleInfo);
        }
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void changePasswordSuccess(String mes) {
        /*
        * change password success
        * hidden dialog change password
        * hidden keyboard
        * show message dialog
        * */
        dialogChangePassword.dismiss();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showLoadingDialog() {
        hiddenLoadingDialog();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenLoadingDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    private void setDefaultFragment() {
        if(getIntent().getIntExtra(Utils.FLAG_FRAGMENT, 0) == Utils.FLAG_FRAG_VALUE){
            changedFragment(cusInfoFragment);
            controlUIBottomMenu(2);
        }
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new MainPresenterImpl(this);
        //call api get sale of info
        presenter.doGetInfoOfSale(this, apiService, realm,
                SharedPref.get(this, SharedPref.Key.USERNAME, ""));
    }

    private void initFragment() {
        homeFragment = new HomeFragment();
        cusInfoFragment = new CusInfoListFragment();
    }

    public void controlUIBottomMenu(int position){
        switch (position){
            case 1:
                //change icon nav bottom menu
                imgHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_home_on));
                imgInfo.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_list_customer_off));
                imgReport.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_search_off));
                imgImpl.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_deploy_off));

//                lineHome.setVisibility(View.VISIBLE);
//                lineInfo.setVisibility(View.INVISIBLE);
//                lineImpl.setVisibility(View.INVISIBLE);
//                lineReport.setVisibility(View.INVISIBLE);

                //change title activity
                imgUserAvatar.setVisibility(View.VISIBLE);
                tvLabelActivity.setVisibility(View.GONE);
                loBack.setVisibility(View.GONE);
                imgNavMenu.setVisibility(View.VISIBLE);
                imgNotifications.setVisibility(View.VISIBLE);
                canBack = -1;
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                break;
            case 2:
                imgHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_home_off));
                imgInfo.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_list_customer_on));
                imgReport.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_search_off));
                imgImpl.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_deploy_off));

//                lineHome.setVisibility(View.INVISIBLE);
//                lineInfo.setVisibility(View.VISIBLE);
//                lineImpl.setVisibility(View.INVISIBLE);
//                lineReport.setVisibility(View.INVISIBLE);

                imgUserAvatar.setVisibility(View.GONE);
                tvLabelActivity.setVisibility(View.VISIBLE);
                loBack.setVisibility(View.GONE);
                imgNavMenu.setVisibility(View.VISIBLE);
                imgNotifications.setVisibility(View.VISIBLE);
                //set title
                tvLabelActivity.setText(getResources().getString(R.string.cus_info_list));
                canBack = -1;
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                break;
            case 3:
                imgHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_home_off));
                imgInfo.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_list_customer_off));
                imgReport.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_reports));
                imgImpl.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_deploy_off));

//                lineHome.setVisibility(View.INVISIBLE);
//                lineInfo.setVisibility(View.INVISIBLE);
//                lineImpl.setVisibility(View.INVISIBLE);
//                lineReport.setVisibility(View.VISIBLE);

                imgUserAvatar.setVisibility(View.GONE);
                imgNavMenu.setVisibility(View.GONE);
                imgNotifications.setVisibility(View.GONE);
                loBack.setVisibility(View.VISIBLE);
                tvLabelActivity.setVisibility(View.VISIBLE);

                tvLabelActivity.setText(getResources().getString(R.string.ui_text_subscriber_report));
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
            case 4:
                imgHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_home_off));
                imgInfo.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_list_customer_off));
                imgReport.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_search_off));
                imgImpl.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_impl_on));

//                lineHome.setVisibility(View.INVISIBLE);
//                lineInfo.setVisibility(View.INVISIBLE);
//                lineImpl.setVisibility(View.VISIBLE);
//                lineReport.setVisibility(View.INVISIBLE);

                imgUserAvatar.setVisibility(View.GONE);
                tvLabelActivity.setVisibility(View.VISIBLE);

                tvLabelActivity.setText(getResources().getString(R.string.implement));
                break;
        }
    }

    public void changedFragment(Fragment fragment){
        //create fragment manager
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frag, fragment);
        fragmentTransaction.addToBackStack("tag");
        fragmentTransaction.commit();
    }

    private void setEvent() {
        imgNavMenu.setOnClickListener(this);
        layoutCustomerInfo.setOnClickListener(this);
        layoutHome.setOnClickListener(this);
        layoutImpl.setOnClickListener(this);
        layoutSearch.setOnClickListener(this);
        imgAddSale.setOnClickListener(this);
        imgNotifications.setOnClickListener(this);
        tvNavHelp.setOnClickListener(this);
        tvNavResetPass.setOnClickListener(this);
        tvNavPotentialCustomer.setOnClickListener(this);
        imgLogOut.setOnClickListener(this);
        loNavNotification.setOnClickListener(this);
        tvNavReport.setOnClickListener(this);
        loNavSearch.setOnClickListener(this);
        tvNavCreateCI.setOnClickListener(this);
        loBack.setOnClickListener(this);
    }

    private void logOut(){
        SharedPref.put(this, SharedPref.Key.FTEL_MOBISALECAM_HEADER, "");
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void initView() {
        tvVersionName.setText(Utils.getVersionName(this));
    }

    public static void setUpObj() {
        Utils.updateCusInfo.setRegId("");
        Utils.updateCusInfo.setRegCode("");
        Utils.updateCusInfo.setObjId("");
        Utils.updateCusInfo.setContract("");
        Utils.updateCusInfo.setFullName("");
        Utils.updateCusInfo.setRepresentive("");
        Utils.updateCusInfo.setBirthday("");
        Utils.updateCusInfo.setAddress("");
        Utils.updateCusInfo.setNoteAddress("");
        Utils.updateCusInfo.setPassport("");
        Utils.updateCusInfo.setCusTypeDetail("");
        Utils.updateCusInfo.setEmail("");
        Utils.updateCusInfo.setTaxCode("");
        Utils.updateCusInfo.setPhone1("");
        Utils.updateCusInfo.setContact1("");
        Utils.updateCusInfo.setPhone2("");
        Utils.updateCusInfo.setContact2("");
        Utils.updateCusInfo.setFax("");
        Utils.updateCusInfo.setNationality("");
        Utils.updateCusInfo.setNationalityName("");
        Utils.updateCusInfo.setVat("");
        Utils.updateCusInfo.setLocationId("");
        Utils.updateCusInfo.setBillToCity("");
        Utils.updateCusInfo.setDistrictId("");
        Utils.updateCusInfo.setBillToDistrict("");
        Utils.updateCusInfo.setWardId("");
        Utils.updateCusInfo.setBillToWard("");
        Utils.updateCusInfo.setStreetId("");
        Utils.updateCusInfo.setBillToStreet("");
        Utils.updateCusInfo.setBillToNumber("");
        Utils.updateCusInfo.setTypeHouse("");
        Utils.updateCusInfo.setTypeHouseName("");
        Utils.updateCusInfo.setBuildingId("");
        Utils.updateCusInfo.setBuildingName("");
        Utils.updateCusInfo.setImageInfo("");
        Utils.updateCusInfo.setSaleId("");
        Utils.updateCusInfo.setGroupPoints("");
        Utils.updateCusInfo.setInDoor("");
        Utils.updateCusInfo.setOutDoor("");
        Utils.updateCusInfo.setInDoorType("");
        Utils.updateCusInfo.setOutDoorType("");
        Utils.updateCusInfo.setOdcCableType("");
        Utils.updateCusInfo.setContractTemp("");
        Utils.updateCusInfo.setBookportIdentityId("");
        Utils.updateCusInfo.setLatLngDevice("");
        Utils.updateCusInfo.setLocalType("");
        Utils.updateCusInfo.setLocalTypeName("");
        Utils.updateCusInfo.setPromotionId("");
        Utils.updateCusInfo.setPromotionName("");
        Utils.updateCusInfo.setMonthOfPrepaid("");
        Utils.updateCusInfo.setTotal("");
        Utils.updateCusInfo.setInternetTotal("");
        Utils.updateCusInfo.setDeviceTotal("");
        Utils.updateCusInfo.setDepositFee("");
        Utils.updateCusInfo.setConnectionFee("");
        Utils.updateCusInfo.setPaymentMethodPerMonth("");
        Utils.updateCusInfo.setPaymentMethodPerMonthName("");
        Utils.updateCusInfo.setIp("");
        Utils.updateCusInfo.setListDevice(new ArrayList<ReqDevice>());
        Utils.updateCusInfo.setListServiceType(new ArrayList<ItemOfListRes>());
    }

//    private void highlightNavLeftMenu(int position){
//        switch (position){
//            case 0:
//                //item 0 is selected
//                tvNavCreateCI.setBackgroundColor(colorSelected);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorDefault);
//                tvNavReport.setBackgroundColor(colorDefault);
//                loNavNotification.setBackgroundColor(colorDefault);
//                tvNavPotentialCustomer.setBackgroundColor(colorDefault);
//                tvNavHelp.setBackgroundColor(colorDefault);
//                tvNavResetPass.setBackgroundColor(colorDefault);
//                break;
//            case 1:
//                tvNavCreateCI.setBackgroundColor(colorDefault);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorSelected);
//                tvNavReport.setBackgroundColor(colorDefault);
//                loNavNotification.setBackgroundColor(colorDefault);
//                tvNavPotentialCustomer.setBackgroundColor(colorDefault);
//                tvNavHelp.setBackgroundColor(colorDefault);
//                tvNavResetPass.setBackgroundColor(colorDefault);
//                break;
//            case 2:
//                tvNavCreateCI.setBackgroundColor(colorDefault);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorDefault);
//                tvNavReport.setBackgroundColor(colorSelected);
//                loNavNotification.setBackgroundColor(colorDefault);
//                tvNavPotentialCustomer.setBackgroundColor(colorDefault);
//                tvNavHelp.setBackgroundColor(colorDefault);
//                tvNavResetPass.setBackgroundColor(colorDefault);
//                break;
//            case 3:
//                tvNavCreateCI.setBackgroundColor(colorDefault);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorDefault);
//                tvNavReport.setBackgroundColor(colorDefault);
//                loNavNotification.setBackgroundColor(colorSelected);
//                tvNavPotentialCustomer.setBackgroundColor(colorDefault);
//                tvNavHelp.setBackgroundColor(colorDefault);
//                tvNavResetPass.setBackgroundColor(colorDefault);
//                break;
//            case 4:
//                tvNavCreateCI.setBackgroundColor(colorDefault);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorDefault);
//                tvNavReport.setBackgroundColor(colorDefault);
//                loNavNotification.setBackgroundColor(colorDefault);
//                tvNavPotentialCustomer.setBackgroundColor(colorSelected);
//                tvNavHelp.setBackgroundColor(colorDefault);
//                tvNavResetPass.setBackgroundColor(colorDefault);
//                break;
//            case 5:
//                tvNavCreateCI.setBackgroundColor(colorDefault);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorDefault);
//                tvNavReport.setBackgroundColor(colorDefault);
//                loNavNotification.setBackgroundColor(colorDefault);
//                tvNavPotentialCustomer.setBackgroundColor(colorDefault);
//                tvNavHelp.setBackgroundColor(colorSelected);
//                tvNavResetPass.setBackgroundColor(colorDefault);
//                break;
//            case 6:
//                tvNavCreateCI.setBackgroundColor(colorDefault);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorDefault);
//                tvNavReport.setBackgroundColor(colorDefault);
//                loNavNotification.setBackgroundColor(colorDefault);
//                tvNavPotentialCustomer.setBackgroundColor(colorDefault);
//                tvNavHelp.setBackgroundColor(colorDefault);
//                tvNavResetPass.setBackgroundColor(colorSelected);
//                break;
//            case 7:
//                tvNavCreateCI.setBackgroundColor(colorDefault);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorDefault);
//                tvNavReport.setBackgroundColor(colorDefault);
//                loNavNotification.setBackgroundColor(colorDefault);
//                tvNavPotentialCustomer.setBackgroundColor(colorDefault);
//                tvNavHelp.setBackgroundColor(colorDefault);
//                tvNavResetPass.setBackgroundColor(colorDefault);
//                break;
//            default:
//                //item 0 is selected
//                tvNavCreateCI.setBackgroundColor(colorSelected);
//                //unselected another item
//                loNavSearch.setBackgroundColor(colorDefault);
//                tvNavReport.setBackgroundColor(colorDefault);
//                loNavNotification.setBackgroundColor(colorDefault);
//                tvNavPotentialCustomer.setBackgroundColor(colorDefault);
//                tvNavHelp.setBackgroundColor(colorDefault);
//                tvNavResetPass.setBackgroundColor(colorDefault);
//                break;
//        }
//    }
}


