package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-09-04.
 */
public class UpdatePayment {
    @SerializedName("RegId")
    @Expose
    private Integer regId;
    @SerializedName("Total")
    @Expose
    private Integer total;
    @SerializedName("FullName")
    @Expose
    private Object fullName;
    @SerializedName("Phone1")
    @Expose
    private Object phone1;
    @SerializedName("Nationality")
    @Expose
    private Object nationality;

    public Integer getRegId() {
        return regId;
    }

    public void setRegId(Integer regId) {
        this.regId = regId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Object getFullName() {
        return fullName;
    }

    public void setFullName(Object fullName) {
        this.fullName = fullName;
    }

    public Object getPhone1() {
        return phone1;
    }

    public void setPhone1(Object phone1) {
        this.phone1 = phone1;
    }

    public Object getNationality() {
        return nationality;
    }

    public void setNationality(Object nationality) {
        this.nationality = nationality;
    }
}
