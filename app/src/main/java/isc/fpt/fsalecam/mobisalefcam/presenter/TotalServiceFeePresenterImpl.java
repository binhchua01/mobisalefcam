package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetDistrictList;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateCusInfo;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdateCusInfo;
import isc.fpt.fsalecam.mobisalefcam.database.DepositFee;
import isc.fpt.fsalecam.mobisalefcam.database.Vat;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.TotalServiceFeeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

public class TotalServiceFeePresenterImpl implements TotalServiceFeePresenter {
    private TotalServiceFeeView view;

    public TotalServiceFeePresenterImpl(TotalServiceFeeView view) {
        this.view = view;
    }

    @Override
    public void doGetDepositList(final Context context, final APIService apiService, final String locationId, final Realm realm) {
        if(realm.where(DepositFee.class).equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll().size() == 0){
            //show dialog
            view.showProgressLoading();
            long timerRefresh = Long.parseLong(SharedPref.get(context,
                    SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiDetDepositList(context, apiService, locationId, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiDetDepositList(context, apiService, locationId, realm);
            }
        }else{
            ArrayList<DepositFee> list = (ArrayList<DepositFee>) realm.copyFromRealm(realm.where(DepositFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId)).findAll());
            view.getDepositListSuccess(list);
        }
    }

    @Override
    public void doGetVatList(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(Vat.class).findAll().size() == 0){
            //show dialog
            view.showProgressLoading();
            long timerRefresh = Long.parseLong(SharedPref.get(context,
                    SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetVatList(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiGetVatList(context, apiService, realm);
            }
        }else{
            ArrayList<Vat> data = (ArrayList<Vat>) realm.copyFromRealm(realm.where(Vat.class).findAll());
            view.getVatListSuccess(data);
        }
    }

    @Override
    public void doUpdateCusInfo(final Context context, final APIService apiService, final ReqUpdateCusInfo obj) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiUpdateCusInfo(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiUpdateCusInfo(context, apiService, obj);
        }
    }

    @Override
    public void doCalculatorTotalAmount(final Context context, final APIService apiService, final ReqUpdateRegistrationTotal obj) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiCalculatorTotalAmount(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiCalculatorTotalAmount(context, apiService, obj);
        }
    }

    private void callApiCalculatorTotalAmount(final Context context, APIService apiService, ReqUpdateRegistrationTotal obj){
        apiService.doCalNewTotal(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjUpdateRegistrationTotal())
                .enqueue(new Callback<ResCalNewTotal>() {
                    @Override
                    public void onResponse(@NonNull Call<ResCalNewTotal> call, @NonNull Response<ResCalNewTotal> response) {
                        validateReturnCodeCalculatorTotal(context, response);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResCalNewTotal> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiUpdateCusInfo(final Context context, APIService apiService, ReqUpdateCusInfo obj){
        apiService.doUpdateCusInfo(Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString().toString()),
                obj.generateObjUpdateCusInfo())
                .enqueue(new Callback<ResUpdateCusInfo>() {
            @Override
            public void onResponse(Call<ResUpdateCusInfo> call, Response<ResUpdateCusInfo> response) {
               validateReturnCodeUpdateCusInfo(context, response);
            }

            @Override
            public void onFailure(Call<ResUpdateCusInfo> call, Throwable t) {
                view.hiddenProgressLoading();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiDetDepositList(final Context context, APIService apiService, final String locationId, final Realm realm){
        ReqGetDistrictList obj = new ReqGetDistrictList(locationId);
        apiService.doGetDepositList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetDistrictList())
                .enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCode(context, response, 1, realm, locationId);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressLoading();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiGetVatList(final Context context, APIService apiService, final Realm realm){
        final LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        apiService.doGetVatList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.toString()), obj)
                .enqueue(new Callback<ResParentStructure>() {
                    @Override
                    public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                        validateReturnCode(context, response, 2, realm, "");
                    }

                    @Override
                    public void onFailure(Call<ResParentStructure> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> response, int num, Realm realm, String cityID){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                if(num == 1){
                    saveDepositFee(realm, (ArrayList<ItemOfListRes>) response.body().getData(), cityID);
                } else if (num == 2) {
                    saveVat(realm, (ArrayList<ItemOfListRes>) response.body().getData());
                }
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else {
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCodeUpdateCusInfo(Context context, Response<ResUpdateCusInfo> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.updateCusInfoSuccess(response.body().getData().get(0));
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void validateReturnCodeCalculatorTotal(Context context, Response<ResCalNewTotal> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.calculatorTotalAmount((ArrayList<CalNewTotal>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void saveVat(Realm realm, ArrayList<ItemOfListRes> list) {
        realm.beginTransaction();
        realm.where(Vat.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                Vat vat = realm.createObject(Vat.class, list.get(i).getId());
                vat.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            ArrayList<Vat> data = (ArrayList<Vat>) realm.copyFromRealm(realm.where(Vat.class).findAll());
            view.getVatListSuccess(data);
        }else{
            ArrayList<Vat> data = (ArrayList<Vat>) realm.copyFromRealm(realm.where(Vat.class).findAll());
            view.getVatListSuccess(data);
        }
    }

    private void saveDepositFee(Realm realm, ArrayList<ItemOfListRes> list, String cityID) {
        realm.beginTransaction();
        realm.where(DepositFee.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                DepositFee depositFee = realm.createObject(DepositFee.class, list.get(i).getId());
                depositFee.setName(list.get(i).getName());
                depositFee.setCityID(Integer.parseInt(cityID));
                realm.commitTransaction();
            }
            ArrayList<DepositFee> data = (ArrayList<DepositFee>) realm.copyFromRealm(realm.where(DepositFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(cityID)).findAll());
            view.getDepositListSuccess(data);
        }else{
            ArrayList<DepositFee> data = (ArrayList<DepositFee>) realm.copyFromRealm(realm.where(DepositFee.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(cityID)).findAll());
            view.getDepositListSuccess(data);
        }
    }
}
