package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import butterknife.BindString;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.customView.ViewSwitch;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqBookportAuto;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqBookportManual;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.BookportAuto;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ListOfPoint;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationById;
import isc.fpt.fsalecam.mobisalefcam.presenter.SurveyPopPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.SurveyPopPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.ContinueRegisterService;
import isc.fpt.fsalecam.mobisalefcam.utils.CreatePotentialCustomer;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.SurveyPopView;

public class SurveyPopActivity extends BaseActivity
        implements OnMapReadyCallback, View.OnClickListener, SurveyPopView,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener {
    @BindView(R.id.btn_book_port) TextView btnBookPort;
    @BindView(R.id.btn_nav_back_book_port) RelativeLayout loBack;
    @BindView(R.id.box_info_map) LinearLayout layoutBox;
    @BindView(R.id.text_view_ftth) TextView tvFTTH;
    @BindView(R.id.text_view_adsl) TextView tvADSL;
    @BindView(R.id.tv_book_port_address) TextView tvAddressOfCus;
    @BindView(R.id.tv_show_connection_box) Button tvShowConnectionBox;
    @BindView(R.id.tv_connection_box) TextView tvConnectionBox;
    @BindString(R.string.message_book_port_failed) String messBookportFailed;
    @BindView(R.id.ic_icon_place_2) ImageView imgIconPort;
    @BindView(R.id.vsMySwitch) ViewSwitch chooseTypeBookport;
    @BindView(R.id.tv_type_bookport) TextView tvTypeBookport;

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 200;
    private SupportMapFragment mapFragment;
    private float mMapCameraZoom = 15f;
    private View mapView;
    private double lat = -34;
    private double lng = 151;
    private Dialog dialog;
    private APIService apiService;
    private SurveyPopPresenter presenter;
    private int typeOfNetworkInfrastructure = 2;
    private GoogleMap mGoogleMap;
    private HashMap<Marker, ListOfPoint> mHashMap;
    private boolean flag = true;
    private boolean checkUpdateBookport = false;//param use for update bookport
    private String deviceLocation;
    private String markerLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        initParamToCallApi();
        //check to bookport new or update bookport
        checkUpdateBookport();
    }

    private void checkUpdateBookport() {
        if (getIntent().getSerializableExtra(Utils.UPDATE_BOOKPORT) != null) {
            //this step, just use for update bookport
            //get data from intent to update bookport
            RegistrationById obj = (RegistrationById)
                    getIntent().getSerializableExtra(Utils.UPDATE_BOOKPORT);
            if(obj.getLatlng() != null && !obj.getLatlng().isEmpty()){
                String[] markerLocation = obj.getLatlng().split(",");
                lat =Double.parseDouble(markerLocation[0]);
                lng = Double.parseDouble(markerLocation[1]);
            }else{
                //get lat, lng at str address
                lat = Objects.requireNonNull(
                        Utils.getLocationFromAddress(this, obj.getAddress())).getLatitude();
                lng = Objects.requireNonNull(
                        Utils.getLocationFromAddress(this, obj.getAddress())).getLongitude();
            }
            tvAddressOfCus.setText(obj.getAddress());
            mapFragment.getMapAsync(this);
            //set value to getListOfPoint
            Utils.updateCusInfo.setLocationId(String.valueOf(obj.getLocationId()));
            Utils.updateCusInfo.setDistrictId(String.valueOf(obj.getDistrictId()));
            Utils.updateCusInfo.setWardId(String.valueOf(obj.getWardId()));
            Utils.updateCusInfo.setStreetId(String.valueOf(obj.getStreetId()));
            Utils.updateCusInfo.setTypeHouse(String.valueOf(obj.getTypeHouse()));
            if(!String.valueOf(obj.getBuildingId()).isEmpty()){
                Utils.updateCusInfo.setBuildingId(String.valueOf(obj.getBuildingId()));
            }
            Utils.updateCusInfo.setBillToNumber(String.valueOf(obj.getBillToNumber()));
            Utils.updateCusInfo.setContractTemp(obj.getContractTemp());
            Utils.BOOKPORT_IDENTITY_ID = obj.getBookportIdentityId();
            Utils.updateCusInfo.setRegCode(obj.getRegCode());
            //show button Connection Box
            tvShowConnectionBox.setVisibility(View.GONE);
            //set params flag = false to bookport manual, skip bookport auto
            flag = true;
            checkUpdateBookport = true;
        } else {
            //this step new bookport
            //call api generate address to bookport new
            presenter.doGenerateAddress(this, apiService);
            //set param check Update Bookport to false when New Bookport;
            checkUpdateBookport = false;
        }
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_survey_pop;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        // Add a marker in location Customer, and move the camera.
        LatLng locationOfCus = new LatLng(lat, lng);
        //set event onclick to marker
        mGoogleMap.setOnMarkerClickListener(this);
        //custom makers
        Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_location_of_cus))
                .position(locationOfCus)
        );
        marker.setDraggable(true);
        //hidden info window
        marker.setInfoWindowAnchor(-9999, -9999);
        mGoogleMap.setOnMarkerDragListener(this);
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationOfCus, mMapCameraZoom));

        boolean locationPermissionsGranted = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if (locationPermissionsGranted) {
            //display button my location
            mGoogleMap.setMyLocationEnabled(true);
            //get device location
            new Thread(new Runnable() {
                @Override
                public void run() {
                    getLocationDevice();
                }
            }).start();
        } else {
            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1"))
                    .getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 40, 540);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    mapFragment.getMapAsync(this);
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_nav_back_book_port:
                onBackPressed();
                break;
            case R.id.btn_book_port:
                checkToBookport();
                break;
            case R.id.text_view_ftth:
                changedStateSelectedServices(1);
                break;
            case R.id.text_view_adsl:
                changedStateSelectedServices(2);
                break;
            case R.id.tv_show_connection_box:
                clearPop();
                getListPort();
                break;
        }
    }

    @Override
    public void generateAddressSuccess(String str) {
        if(str == null){return;}
        //clear all marker, use in re-choose pop
        mGoogleMap.clear();
        mHashMap.clear();
        tvConnectionBox.setText(getResources().getString(R.string.ui_text_choose_connection_box));
        //set address to view
        tvAddressOfCus.setText(str);
        if (Utils.getLocationFromAddress(this, str) == null) {
            //can't get location from string address
            DialogUtils.showMessageDialogVerButton(this,
                    getResources().getString(R.string.cant_get_location)).show();
            return;
        }
        //get lat, lng at str address
        lat = Objects.requireNonNull(Utils.getLocationFromAddress(this, str)).getLatitude();
        lng = Objects.requireNonNull(Utils.getLocationFromAddress(this, str)).getLongitude();
        //refresh map after set up lat, lng
        mapFragment.getMapAsync(this);
        //put lat, lng of marker
        markerLocation = Utils.formatLatLngByPattern(lat, lng);
        Utils.updateCusInfo.setLatLngMarker(markerLocation);
    }

    @Override
    public void getListOfPoints(ArrayList<ListOfPoint> list) {
        //load list of points
        loadListOfPointsToView(list);
    }

    @Override
    public void bookportAutoFailedCreatePotentialCus() {
        DialogUtils.showDialogCreatePotentialCus(this,
                new ContinueRegisterService() {
                    @Override
                    public void continueRegister() {
                        //show button Connection Box
                        tvShowConnectionBox.setVisibility(View.VISIBLE);
                        //set params flag = false to bookport manual
                        flag = false;
                        //change text bookport type
                        tvTypeBookport.setText(getResources().getString(R.string.ui_text_bookport_manual_mode));
                        //just use for update bookport auto
                        Utils.updateCusInfo.setContractTemp("");
                    }
                },
                new CreatePotentialCustomer() {
                    @Override
                    public void potentialCustomer() {
                        changedActivityCreatePotentialCusInfo();
                    }
                }
        ).show();
    }

    @Override
    public void bookportAutoSuccess(BookportAuto bookportAuto) {
        if (checkUpdateBookport) {
            finish();
            return;
        }
        //set info after bookport success
        Utils.updateCusInfo.setGroupPoints(bookportAuto.getName());
        Utils.updateCusInfo.setOdcCableType(bookportAuto.getODCCableType());
        Utils.updateCusInfo.setContractTemp(bookportAuto.getContractTemp());
        Utils.updateCusInfo.setInDoor(String.valueOf(bookportAuto.getInDoor()));
        Utils.updateCusInfo.setOutDoor(String.valueOf(bookportAuto.getOutDoor()));
        //change activity
        Intent intent = new Intent(getApplicationContext(), NewCustomerActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void bookportAutoFailed(String mes) {
        //show dialog ask to create potential customer or bookport manual
        DialogUtils.showDialogBookportAutoFailed(this,
                new ContinueRegisterService() {
                    @Override
                    public void continueRegister() {
                        //show button Connection Box
                        tvShowConnectionBox.setVisibility(View.VISIBLE);
                        //set params flag = false to bookport manual
                        flag = false;
                    }
                }).show();
    }

    @Override
    public void callApiException(String str) {
        DialogUtils.showMessageDialogVerButton(this, str).show();
    }

    @Override
    public void expiredToken(String str) {
        DialogUtils.showDialogRequiredLoginAgain(this, str).show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        dialog = DialogUtils.showLoadingDialog(this,
                getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if(marker.getTitle() == null){
            return true;
        }
        final ListOfPoint item = mHashMap.get(marker);
        final View view = getLayoutInflater().inflate(R.layout.marker_info_pop, null);
        final TextView tvTitle = view.findViewById(R.id.tv_title);
        final TextView tvSnippet = view.findViewById(R.id.tv_snippet);
        LinearLayout loBg = view.findViewById(R.id.layout_background_window_info);
        //color text port name
        tvConnectionBox.setTextColor(getResources().getColor(R.color.s_black));
//        if(Integer.parseInt(marker.getTitle()) == 1){
//            //background of window info
//            loBg.setBackground(getResources().getDrawable(R.drawable.ic_icon_port_a_little));
//            //color of port free of number
//            tvTitle.setTextColor(getResources().getColor(R.color.s_orange));
//            //display icon
//            imgIconPort.setImageDrawable
//                    (getResources().getDrawable(R.drawable.ic_icon_connection_box_a_little));
//        }else
        if(Integer.parseInt(marker.getTitle()) <= 1){
            //background of window info
            loBg.setBackground(getResources().getDrawable(R.drawable.ic_icon_port_full));
            //color of port free of number
            tvTitle.setTextColor(getResources().getColor(R.color.wrong_pink));
            //display icon
            imgIconPort.setImageDrawable
                    (getResources().getDrawable(R.drawable.ic_icon_connection_box_full));
        } else{
            //background of window info
            loBg.setBackground(getResources().getDrawable(R.drawable.ic_icon_port_free));
            //color of port free of number
            tvTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
            //display icon
            imgIconPort.setImageDrawable
                    (getResources().getDrawable(R.drawable.ic_icon_connection_box_free));
        }

        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker marker) {
                tvTitle.setText(marker.getTitle());
                tvSnippet.setText(marker.getSnippet());
                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
        marker.showInfoWindow();
        //show port name on position user click
        if (item != null) {
            tvConnectionBox.setText(item.getDeviceName());
        }
        return true;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
    }

    @Override
    public void onMarkerDrag(Marker marker) {
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //clear all marker
        mGoogleMap.clear();
        mHashMap.clear();
        //new lat, lng in new location
        lat = marker.getPosition().latitude;
        lng = marker.getPosition().longitude;
        //add new marker MBS in new position
        Marker mk = mGoogleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_location_of_cus))
                .position(new LatLng(lat, lng))
        );
        mk.setDraggable(true);
        //reset port name on view
        tvConnectionBox.setText(getResources().getString(R.string.ui_text_choose_connection_box));
        //put lat, lng of marker
        markerLocation = Utils.formatLatLngByPattern(lat, lng);
        Utils.updateCusInfo.setLatLngMarker(markerLocation);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //init param use for call api and call API default when start activity
    private void initParamToCallApi() {
        apiService = ApiUtils.getApiService();
        presenter = new SurveyPopPresenterImpl(this);
    }

    //method check bookport manual or bookport auto
    private void checkToBookport() {
        if (flag) {
            //bookport auto
            if(!checkUpdateBookport){
                presenter.doBookportAuto(this, apiService, putDataToObjBookportAuto());
            }else{
                presenter.doBookportAuto(this, apiService, putDataToObjUpdateBookportAuto());
            }
        } else {
            if (!tvConnectionBox.getText().toString()
                    .equals(getResources().getString(R.string.ui_text_choose_connection_box))) {
                //changed activity to bookport manual
                Intent intent = new Intent(this, EnterLengthCableActivity.class);
                intent.putExtra(Utils.ACTION_BOOKPORT, Utils.MANUAL);
                if (!checkUpdateBookport) {
                    intent.putExtra(Utils.OBJ_BOOKPORT_MANUAL, putDataToObjBookportManual());
                    intent.putExtra(Utils.BOOKPORT_TYPE, Utils.NEW_BOOKPORT);
                } else {
                    intent.putExtra(Utils.OBJ_BOOKPORT_MANUAL, putDataToObjUpdateBookportManual());
                    intent.putExtra(Utils.BOOKPORT_TYPE, Utils.UPDATE_BOOKPORT);
                }
                startActivity(intent);
            } else {
                showToast(this, getResources().getString(R.string.ui_text_choose_connection_box));
            }
        }
    }

    private void initView() {
        mHashMap = new HashMap<>();
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_on_bookport);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();
    }

    //method set event for all view in activity
    private void setEvent() {
        loBack.setOnClickListener(this);
        btnBookPort.setOnClickListener(this);
        tvFTTH.setOnClickListener(this);
        tvADSL.setOnClickListener(this);
        tvShowConnectionBox.setOnClickListener(this);
        chooseTypeBookport.setOnStatusChangeListener(new ViewSwitch.SwitchStatusListener() {
            @Override
            public void onSwitchStatus(int status) {
                if (status == 1) {
                    if(deviceLocation == null){
                        return;
                    }
                    //clear all marker
                    mGoogleMap.clear();
                    mHashMap.clear();
                    String[] location = deviceLocation.split(",");
                    LatLng locationOfCus = new LatLng(Double.parseDouble(location[0]), Double.parseDouble(location[1]));
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationOfCus, mMapCameraZoom));
                } else {
                    //clear all marker
                    mGoogleMap.clear();
                    mHashMap.clear();
                    //get lat, lng from string address
                    //move camera to location
                    presenter.doGenerateAddress(SurveyPopActivity.this, apiService);
                }
            }
        });
    }

    private void getLocationDevice() {
        FusedLocationProviderClient fusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(this);
        try {
            Task mLocation = fusedLocationProviderClient.getLastLocation();
            mLocation.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull com.google.android.gms.tasks.Task task) {
                    if(task.isSuccessful()){
                        if(task.getResult() == null){
                            return;
                        }
                        deviceLocation = Utils.formatLatLngByPattern(((Location)
                                        task.getResult()).getLatitude(),
                                ((Location) task.getResult()).getLongitude());
                        //put location device to obj create registration
                        Utils.updateCusInfo.setLatLngDevice(deviceLocation);
                    }else{
                        showToast(getApplicationContext(), getResources().getString(R.string.cant_get_location_device));
                    }
                }
            });
        }catch (SecurityException ex){
            ex.printStackTrace();
        }
    }

    //method display refresh UI when changed service(FTTH/ADSL) and params: typeOfNetworkInfrastructure
    private void changedStateSelectedServices(int num) {
        switch (num) {
            case 1:
                //background
                tvFTTH.setBackground(getResources().getDrawable(R.drawable.background_text_view_selected));
                tvADSL.setBackground(getResources().getDrawable(R.drawable.background_text_view_un_selected));
                //color text
                tvFTTH.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvADSL.setTextColor(getResources().getColor(R.color.text_employee));
                typeOfNetworkInfrastructure = 2;
                clearPop();
                break;
            case 2:
                tvFTTH.setBackground(getResources().getDrawable(R.drawable.background_text_view_un_selected));
                tvADSL.setBackground(getResources().getDrawable(R.drawable.background_text_view_selected));
                tvFTTH.setTextColor(getResources().getColor(R.color.text_employee));
                tvADSL.setTextColor(getResources().getColor(R.color.colorPrimary));
                typeOfNetworkInfrastructure = 1;
                clearPop();
                break;
        }
    }

    private void clearPop() {
        //clear all marker
        mGoogleMap.clear();
        mHashMap.clear();
        //add new marker MBS in new position
        Marker mk = mGoogleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_location_of_cus))
                .position(new LatLng(lat, lng))
        );
        mk.setDraggable(true);
        //reset port name on view
        tvConnectionBox.setText(getResources().getString(R.string.ui_text_choose_connection_box));
    }

    //method add list marker to map by list location
    private void loadListOfPointsToView(final ArrayList<ListOfPoint> list) {
        if (list == null || list.size() == 0) {
            return;
        }
        for (int i = 0; i < list.size(); i++) {
            String[] location = list.get(i).getLatLng().split(",");
            LatLng point = new LatLng(Double.parseDouble(location[0]), Double.parseDouble(location[1]));
            Marker marker;
//            if (list.get(i).getPortFree() == 1) {
//                marker = mGoogleMap.addMarker(new MarkerOptions()
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_connection_box_a_little))
//                        .position(point)
//                        .title(String.valueOf(list.get(i).getPortFree()))
//                        .snippet(String.valueOf(list.get(i).getLengthSuggest()) + "(m)")
//                );
//            }else
            if(list.get(i).getPortFree() <= 1){
                marker = mGoogleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_connection_box_full))
                        .position(point)
                        .title(String.valueOf(list.get(i).getPortFree()))
                        .snippet(String.valueOf(list.get(i).getLengthSuggest()) + "(m)")
                );
            } else{
                marker = mGoogleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_connection_box_free))
                        .position(point)
                        .title(String.valueOf(list.get(i).getPortFree()))
                        .snippet(String.valueOf(list.get(i).getLengthSuggest()) + "(m)")
                );
            }
            marker.setDraggable(false);
            mHashMap.put(marker, list.get(i));
        }
    }

    //method call api get list port
    private void getListPort() {
        presenter.doGetListOfPoints(this, apiService,
                SharedPref.get(this, SharedPref.Key.USERNAME, ""),
                Utils.updateCusInfo.getLocationId(),
                Utils.updateCusInfo.getWardId(),
                String.valueOf(lat),
                String.valueOf(lng),
                String.valueOf(typeOfNetworkInfrastructure),
                Utils.BOOKPORT_IDENTITY_ID
        );
    }

    //put data to objReport to send next activity, use for bookport manual
    private ReqBookportManual putDataToObjBookportManual() {
        ReqBookportManual obj = new ReqBookportManual();
        obj.setUsername(SharedPref.get(this, SharedPref.Key.USERNAME, ""));
        obj.setLocationId(Integer.parseInt(Utils.updateCusInfo.getLocationId()));
        obj.setWardId(Integer.parseInt(Utils.updateCusInfo.getWardId()));
        obj.setDeviceName(tvConnectionBox.getText().toString());
        obj.setTypeOfNetworkInfrastructure(typeOfNetworkInfrastructure);
        obj.setBookportIdentityId("");
        obj.setRegCode("");
        obj.setContractTemp("");
        obj.setIsRecovery(0);
        obj.setLatlngDevice(deviceLocation);
        obj.setLatLngMarker(markerLocation);
        return obj;
    }

    private ReqBookportManual putDataToObjUpdateBookportManual() {
        ReqBookportManual obj = new ReqBookportManual();
        obj.setUsername(SharedPref.get(this, SharedPref.Key.USERNAME, ""));
        obj.setLocationId(Integer.parseInt(Utils.updateCusInfo.getLocationId()));
        obj.setWardId(Integer.parseInt(Utils.updateCusInfo.getWardId()));
        obj.setDeviceName(tvConnectionBox.getText().toString());
        obj.setTypeOfNetworkInfrastructure(typeOfNetworkInfrastructure);
        obj.setBookportIdentityId(Utils.BOOKPORT_IDENTITY_ID);
        obj.setRegCode(Utils.updateCusInfo.getRegCode());
        obj.setContractTemp(Utils.updateCusInfo.getContractTemp());
        obj.setIsRecovery(1);
        obj.setLatlngDevice(deviceLocation);
        obj.setLatLngMarker(markerLocation);
        return obj;
    }

    //put data to objReport, use for bookport auto
    private ReqBookportAuto putDataToObjBookportAuto() {
        ReqBookportAuto obj = new ReqBookportAuto();
        obj.setUsername(SharedPref.get(this, SharedPref.Key.USERNAME, ""));
        obj.setLocationId(Utils.updateCusInfo.getLocationId());
        obj.setRegCode("");
        obj.setWardId(Utils.updateCusInfo.getWardId());
        obj.setLatPort(String.valueOf(lat));
        obj.setLngPort(String.valueOf(lng));
        obj.setTypeOfNetworkInfrastructure(String.valueOf(typeOfNetworkInfrastructure));
        obj.setContractTemp("");
        obj.setIsRecovery("");
        obj.setBookportIdentityId("");
        obj.setLatLngDevice(deviceLocation);
        obj.setLatLngMarker(markerLocation);
        return obj;
    }

    private ReqBookportAuto putDataToObjUpdateBookportAuto(){
        ReqBookportAuto obj = new ReqBookportAuto();
        obj.setUsername(SharedPref.get(this, SharedPref.Key.USERNAME, ""));
        obj.setLocationId(Utils.updateCusInfo.getLocationId());
        obj.setRegCode(Utils.updateCusInfo.getRegCode());
        obj.setWardId(Utils.updateCusInfo.getWardId());
        obj.setLatPort(String.valueOf(lat));
        obj.setLngPort(String.valueOf(lng));
        obj.setTypeOfNetworkInfrastructure(String.valueOf(typeOfNetworkInfrastructure));
        obj.setContractTemp(Utils.updateCusInfo.getContractTemp());
        obj.setIsRecovery("1");
        obj.setBookportIdentityId(Utils.BOOKPORT_IDENTITY_ID);
        obj.setLatLngDevice(deviceLocation);
        obj.setLatLngMarker(markerLocation);
        return obj;
    }

    //activity called when don't have infrastructure or bookport auto failed
    private void changedActivityCreatePotentialCusInfo() {
        Intent intent = new Intent(getApplicationContext(), NewPotentialCusActivity.class);
        startActivity(intent);
        finishAffinity();
    }
}


