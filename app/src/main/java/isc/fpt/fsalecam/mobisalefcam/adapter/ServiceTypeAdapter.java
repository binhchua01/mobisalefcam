package isc.fpt.fsalecam.mobisalefcam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.utils.SelectServiceTypeCallback;

/**
 * Created by Hau Le on 2018-08-16.
 */
public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.SimpleViewHolder> {
    private Context context;
    private ArrayList<ItemOfListRes> list;
    private ArrayList<ItemOfListRes> listCheck;
    private SelectServiceTypeCallback callback;
    //add position of service to list -> when use, get position of service from list
    private ArrayList<String> listService;

    public ServiceTypeAdapter(Context context, ArrayList<ItemOfListRes> list, ArrayList<ItemOfListRes> listCheck,
                              SelectServiceTypeCallback callback) {
        this.context = context;
        this.list = list;
        this.listCheck = listCheck;
        this.callback = callback;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_choose_service_type, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        listService = new ArrayList<>();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressServiceWhenClick(mViewHolder);
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        holder.tvServiceType.setText(list.get(position).getName());
        if(list.get(position).getId() == 1){
            holder.loParent.setBackground(context.getResources().getDrawable(R.drawable.background_button_clear));
            holder.tvServiceType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.imageViewCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_check));
            holder.tvValueCheck.setText("1");
        }


        if(listCheck.size() == 2 && listCheck.get(position).getId() != 1){
            holder.loParent.setBackground(context.getResources().getDrawable(R.drawable.background_button_clear));
            holder.tvServiceType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.imageViewCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_check));
            holder.tvValueCheck.setText("1");
            //add 1 service to list
            listService.add(String.valueOf(holder.getAdapterPosition()));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_service_type) TextView tvServiceType;
        @BindView(R.id.layout_choose_service_type) RelativeLayout loParent;
        @BindView(R.id.img_check) ImageView imageViewCheck;
        @BindView(R.id.tv_value_check) TextView tvValueCheck;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void progressServiceWhenClick(SimpleViewHolder mViewHolder){
        //position "0" is internet service, it's default always selected
        if(mViewHolder.getAdapterPosition() != 0){
            if(Integer.parseInt(mViewHolder.tvValueCheck.getText().toString()) == 0){
                //set value flag
                mViewHolder.tvValueCheck.setText("1");
                //refresh UI
                refreshUI(mViewHolder, Integer.parseInt(mViewHolder.tvValueCheck.getText().toString()));
                //add 1 service to list
                listService.add(String.valueOf(mViewHolder.getAdapterPosition()));
                //callbackDeviceList data
                callback.serviceTypeList(listService);
            }else{
                mViewHolder.tvValueCheck.setText("0");
                refreshUI(mViewHolder, Integer.parseInt(mViewHolder.tvValueCheck.getText().toString()));
                //remove 1 service from list
                //find position of objReport - > remove objReport
                listService.remove(listService.indexOf(String.valueOf(mViewHolder.getAdapterPosition())));
                callback.serviceTypeList(listService);
            }
        }
    }

    private void refreshUI(SimpleViewHolder viewHolder, int value){
        if(value == 1){
            viewHolder.loParent.setBackground(context.getResources().getDrawable(R.drawable.background_button_clear));
            viewHolder.tvServiceType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            viewHolder.imageViewCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_check));
        }else {
            viewHolder.loParent.setBackground(context.getResources().getDrawable(R.drawable.background_spinner_solid));
            viewHolder.tvServiceType.setTextColor(context.getResources().getColor(R.color.blue_blur));
            viewHolder.imageViewCheck.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_icon_check_blur));
        }
    }
}
