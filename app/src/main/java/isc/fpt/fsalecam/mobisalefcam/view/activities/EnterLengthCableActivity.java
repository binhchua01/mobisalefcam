package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqBookportManual;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.BookPortManual;
import isc.fpt.fsalecam.mobisalefcam.presenter.EnterLengthCablePresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.EnterLengthCablePresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.ContinueRegisterService;
import isc.fpt.fsalecam.mobisalefcam.utils.CreatePotentialCustomer;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.EnterLengthCableView;

public class EnterLengthCableActivity extends BaseActivity
        implements View.OnClickListener, EnterLengthCableView {

    @BindView(R.id.tv_connection_box) TextView tvConnectionBox;
    @BindView(R.id.edt_enter_indoor_cable_of_width) EditText edtIndoorWidth;
    @BindView(R.id.edt_enter_outdoor_cable_of_width) EditText edtOutdoorWidth;
    @BindView(R.id.layout_back_fttp_bookport) RelativeLayout loBack;
    @BindView(R.id.btn_continue_bookport) Button btnContinue;
    @BindView(R.id.layout_fttp_bookport_parent) RelativeLayout loParent;
    @BindView(R.id.layout_indoor) RelativeLayout loIndoor;
    @BindView(R.id.layout_outdoor) RelativeLayout loOutdoor;
    @BindView(R.id.tv_reg_code) TextView tvRegCode;
    @BindView(R.id.layout_reg_code) RelativeLayout loRegCode;

    private ReqBookportManual objBookport;
    private Dialog dialog;
    private EnterLengthCablePresenter presenter;
    private APIService apiService;
//    param use for check bookport type
//    is true -> new bookport, false -> update bookport
    private boolean bookPortType = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        getDataIntent();
        initData();
        setEvent();
        callApi();
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new EnterLengthCablePresenterImpl(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_enter_length_cable;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_back_fttp_bookport:
                onBackPressed();
                break;
            case R.id.btn_continue_bookport:
                //check value is ok to call api
                if(checkValueIsOk()){
                    objBookport.setLengthSurveyOutDoor(Integer.parseInt(edtOutdoorWidth.getText().toString()));
                    objBookport.setLengthSurveyInDoor(Integer.parseInt(edtIndoorWidth.getText().toString()));
                    presenter.doBookportManual(this, apiService, objBookport);
                }
                break;
        }
    }

    private boolean checkValueIsOk() {
        if(edtIndoorWidth.getText().toString().isEmpty()){
            loIndoor.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            edtIndoorWidth.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loIndoor.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            edtIndoorWidth.setHintTextColor(getResources().getColor(R.color.s_black));
        }
        if(edtOutdoorWidth.getText().toString().isEmpty()){
            loOutdoor.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            edtOutdoorWidth.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loOutdoor.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            edtOutdoorWidth.setHintTextColor(getResources().getColor(R.color.s_black));
        }
        if(edtIndoorWidth.getText().toString().isEmpty()){
            DialogUtils.showMessageDialogVerButton(this,
                    getResources().getString(R.string.mes_indoor_required)).show();
            return false;
        }else if(edtOutdoorWidth.getText().toString().isEmpty()){
            DialogUtils.showMessageDialogVerButton(this,
                    getResources().getString(R.string.mes_outdoor_required)).show();
            return false;
        }
        return true;
    }

    private void setEvent(){
        loBack.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
        //refresh ui when changed text=
        edtIndoorWidth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!edtIndoorWidth.getText().toString().isEmpty()){
                    loIndoor.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    edtIndoorWidth.setHintTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        edtOutdoorWidth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!edtOutdoorWidth.getText().toString().isEmpty()){
                    loOutdoor.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    edtOutdoorWidth.setHintTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
    }

    private void initData() {
        tvConnectionBox.setText(objBookport.getDeviceName());
        if(!objBookport.getRegCode().isEmpty()){
            loRegCode.setVisibility(View.VISIBLE);
            tvRegCode.setText(objBookport.getRegCode());
        }else{
            loRegCode.setVisibility(View.GONE);
        }
    }

    private void getDataIntent() {
        if(getIntent().getSerializableExtra(Utils.OBJ_BOOKPORT_MANUAL)!=null){
            objBookport = (ReqBookportManual) getIntent().getSerializableExtra(Utils.OBJ_BOOKPORT_MANUAL);
        }

        if(getIntent().getStringExtra(Utils.BOOKPORT_TYPE)!=null){
            bookPortType = getIntent().getStringExtra(Utils.BOOKPORT_TYPE).equals(Utils.NEW_BOOKPORT);
        }
    }

    @Override
    public void bookportManualSuccess(final BookPortManual obj) {
        if(bookPortType){
            //bookport new
            //put data objReport
            Utils.updateCusInfo.setInDoor(edtIndoorWidth.getText().toString());
            Utils.updateCusInfo.setOutDoor(edtOutdoorWidth.getText().toString());
            Utils.updateCusInfo.setGroupPoints(obj.getName());
            Utils.updateCusInfo.setContractTemp(obj.getContractTemp());
            Utils.updateCusInfo.setOdcCableType(obj.getODCCableType());
            //function had call when bookport success
            DialogUtils.showDialogAskForContinue(this,
                    new CreatePotentialCustomer() {
                        @Override
                        public void potentialCustomer() {
                            changedActivityCreatePotentialCus();
                        }
                    },
                    new ContinueRegisterService() {
                        @Override
                        public void continueRegister() {
                            changedActivityNewCus();
                        }
                    }
            ).show();
        }else{
            //update bookport
            startActivity(new Intent(this, CusInfoDetailsActivity.class));
            finish();
        }
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void createPotentialCusOrBookport(String mes) {
        //call this function when call api bookport manual failed
        DialogUtils.showDialogBookportManualFailed(this, mes,
                new CreatePotentialCustomer() {
            @Override
            public void potentialCustomer() {
                changedActivityCreatePotentialCus();
            }
        }).show();
    }

    @Override
    public void indoorInvalid(String message) {
        loIndoor.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
        edtIndoorWidth.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        DialogUtils.showMessageDialogVerButton(this, message).show();

    }

    @Override
    public void outdoorInvalid(String message) {
        loOutdoor.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
        edtOutdoorWidth.setHintTextColor(getResources().getColor(R.color.wrong_pink));
        DialogUtils.showMessageDialogVerButton(this, message).show();
    }

    @Override
    public void expiredToken(String str) {
        DialogUtils.showDialogRequiredLoginAgain(this, str).show();
    }

    @Override
    public void showProgressDialog() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    private void changedActivityCreatePotentialCus(){
        Intent intent = new Intent(getApplicationContext(), NewPotentialCusActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    private void changedActivityNewCus(){
        Intent intent = new Intent(getApplicationContext(), NewCustomerActivity.class);
        startActivity(intent);
        finishAffinity();
    }
}
