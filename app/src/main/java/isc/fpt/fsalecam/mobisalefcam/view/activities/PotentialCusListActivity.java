package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.PotentialCusAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqPotentialCusList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.PotentialCus;
import isc.fpt.fsalecam.mobisalefcam.presenter.PotentialCusListPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.PotentialCusListPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.PotentialCusTypeSearch;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.PotentialCusListView;

/**
 * Created by Hau Le on 2018-07-11.
 */
public class PotentialCusListActivity extends BaseActivity
        implements View.OnClickListener, PotentialCusListView{
    @BindView(R.id.ic_nav_back_potential_customer) RelativeLayout imgBack;
    @BindView(R.id.layout_potential_customer) RelativeLayout layoutWrapper;
    @BindView(R.id.img_add_new_potential_cus) ImageView imgAddPotentialCus;
    @BindView(R.id.img_change) RelativeLayout loFilter;
    @BindView(R.id.list_potential_cus) RecyclerView recyclerView;
    @BindView(R.id.ic_button_search) RelativeLayout loSearch;
    @BindView(R.id.edt_search) EditText edtSearch;

    private Dialog dialog;
    private Dialog dialogMes;
    private APIService apiService;
    private PotentialCusListPresenter presenter;
    private ArrayList<PotentialCus> list;
    private PotentialCusAdapter adapter;
    private ReqPotentialCusList obj;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, layoutWrapper);
        realm = Realm.getDefaultInstance();
        setEvent();
        initView();
        callApi();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_potential_cus_list;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_nav_back_potential_customer:
                onBackPressed();
                break;
            case R.id.img_add_new_potential_cus:
                Intent intent = new Intent(this, NewPotentialCusActivity.class);
                intent.putExtra(Utils.CAN_BACK, true);
                startActivity(intent);
                break;
            case R.id.img_change:
                //call api get potential cus type list
                presenter.doGetPotentialCusTypeList(this, apiService, realm);
                break;
            case R.id.ic_button_search:
                //call api get potential cus list with search content
                presenter.doGetPotentialCusList(this, apiService, obj);
                break;
        }
    }

    @Override
    public void getPotentialCusListSuccess(ArrayList<PotentialCus> list) {
        this.list = list;
        adapter.notifyData(list);

    }

    @Override
    public void getPotentialCusTypeList(ArrayList<PotentialCusTypeSearch> list) {
        DialogUtils.showDialogChooseKeySearch(this,
                getResources().getString(R.string.choose_search_type),
                list, new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                int type = Integer.parseInt(data.split(":")[0]);
                obj.setSearchType(type);
                switch (type){
                    case 1:
                        edtSearch.setHint(getResources().getString(R.string.ui_text_enter_customer_name));
                        break;
                    case 2:
                        edtSearch.setHint(getResources().getString(R.string.ui_text_enter_customer_phone));
                        break;
                    case 3:
                        edtSearch.setHint(getResources().getString(R.string.ui_text_enter_customer_address));
                        break;
                }
            }
        }).show();
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new PotentialCusListPresenterImpl(this);
        //put value default to objReport
        obj = new ReqPotentialCusList();
        obj.setUsername(SharedPref.get(this, SharedPref.Key.USERNAME, ""));
        obj.setSearchType(1);
        obj.setSearchContent("");
        obj.setPageNumber(1);
        obj.setFromDate("01/01/2018");
        obj.setToDate("12/30/2018");
        obj.setSource(1);
        //call api get potential cus list
        presenter.doGetPotentialCusList(this, apiService, obj);
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        imgAddPotentialCus.setOnClickListener(this);
        loFilter.setOnClickListener(this);
        loSearch.setOnClickListener(this);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //add search content to objReport
                obj.setSearchContent(String.valueOf(editable));
            }
        });

    }

    private void initView() {
        list = new ArrayList<>();
        adapter = new PotentialCusAdapter(this, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //TODO: Create cus info fro this potential cus when item click
                //show dialog message...
                if(dialogMes!=null && dialogMes.isShowing()){
                    dialogMes.dismiss();
                }
                dialogMes = DialogUtils.showMessageDialogVerButton(PotentialCusListActivity.this,
                        getResources().getString(R.string.message_next_version));
                dialogMes.show();
            }
        });
        recyclerView.setAdapter(adapter);
    }
}
