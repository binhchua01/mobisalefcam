package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-13.
 */
public class ReqGetSubTeam {
    private int objId;
    private String username;
    private String regCode;

    public int getObjId() {
        return objId;
    }

    public void setObjId(int objId) {
        this.objId = objId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public LinkedHashMap<String, String> generateObjGetSubTeam(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("ObjID", String.valueOf(getObjId()));
        obj.put("UserName", getUsername());
        obj.put("RegCode", getRegCode());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("ObjID", String.valueOf(getObjId()));
            obj.put("UserName", getUsername());
            obj.put("RegCode", getRegCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
