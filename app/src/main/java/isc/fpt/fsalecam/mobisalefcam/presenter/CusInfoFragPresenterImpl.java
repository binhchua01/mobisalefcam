package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetDistrictList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.database.Nationality;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResNationality;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.CustomerType;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.CusInfoFragView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-15.
 */
public class CusInfoFragPresenterImpl implements CusInfoFragPresenter {
    private CusInfoFragView view;

    public CusInfoFragPresenterImpl(CusInfoFragView view) {
        this.view = view;
    }

    @Override
    public void doGetCusTypeList(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(CustomerType.class).findAll().size() == 0){
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetCusTypeList(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetCusTypeList(context, apiService, realm);
            }
        }else{
            List<CustomerType> list = realm.copyFromRealm(realm.where(CustomerType.class).findAll());
            view.getCusTypeListSuccess((ArrayList<CustomerType>) list);
        }
    }

    @Override
    public void doGetNationalityList(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(Nationality.class).findAll().size() == 0){
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetNationalityList(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetNationalityList(context, apiService, realm);
            }
        }else{
            view.getNationalityListSuccess((ArrayList<Nationality>)
                    realm.copyFromRealm(realm.where(Nationality.class).findAll()));
        }
    }

    private void callApiGetNationalityList(final Context context, APIService apiService, final Realm realm){
        final LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        apiService.doGetNationalityList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.toString()),
                obj).enqueue(new Callback<ResNationality>() {
            @Override
            public void onResponse(Call<ResNationality> call, Response<ResNationality> response) {
                validateReturnCodeNationality(context, response, realm);
            }

            @Override
            public void onFailure(Call<ResNationality> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiGetCusTypeList(final Context context, APIService apiService, final Realm realm){
        ReqGetDistrictList obj = new ReqGetDistrictList(Utils.updateCusInfo.getLocationId());
        apiService.doGetCusTypeList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetDistrictList()).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCodeCusType(context, response, realm);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenProgressDialog();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCodeCusType(Context context, Response<ResParentStructure> obj, Realm realm){
        if(obj.body()!=null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                saveCustomerList((ArrayList<ItemOfListRes>) obj.body().getData(), realm);
                //save Authorization header
                Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(obj.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void saveCustomerList(ArrayList<ItemOfListRes> list, Realm realm){
        realm.beginTransaction();
        realm.where(CustomerType.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                CustomerType customerType = realm.createObject(CustomerType.class, list.get(i).getId());
                customerType.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            view.getCusTypeListSuccess((ArrayList<CustomerType>)
                    realm.copyFromRealm(realm.where(CustomerType.class).findAll()));
        }else{
            view.getCusTypeListSuccess((ArrayList<CustomerType>)
                    realm.copyFromRealm(realm.where(CustomerType.class).findAll()));
        }
    }

    private void validateReturnCodeNationality(Context context, Response<ResNationality> obj, Realm realm){
        if(obj.body()!=null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                saveNationality(obj.body().getData(), realm);
                //save Authorization header
                Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(obj.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void saveNationality(List<isc.fpt.fsalecam.mobisalefcam.model.objResponse.Nationality> list, Realm realm) {
        realm.beginTransaction();
        realm.where(Nationality.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                Nationality nationality = realm.createObject(Nationality.class, list.get(i).getValue());
                nationality.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            view.getNationalityListSuccess((ArrayList<Nationality>)
                    realm.copyFromRealm(realm.where(Nationality.class).findAll()));
        }else{
            view.getNationalityListSuccess((ArrayList<Nationality>)
                    realm.copyFromRealm(realm.where(Nationality.class).findAll()));
        }
    }
}
