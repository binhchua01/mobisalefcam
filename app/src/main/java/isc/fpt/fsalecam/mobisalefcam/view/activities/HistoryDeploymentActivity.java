package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;

public class HistoryDeploymentActivity extends BaseActivity implements View.OnClickListener{
    @BindView(R.id.ic_icon_nav_back_deployment_history) ImageView imgBack;
    @BindView(R.id.btn_update_deployment_time) Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_history_deployment;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_icon_nav_back_deployment_history:
                onBackPressed();
                break;
            case R.id.btn_update_deployment_time:
                finish();
                break;
        }
    }
}
