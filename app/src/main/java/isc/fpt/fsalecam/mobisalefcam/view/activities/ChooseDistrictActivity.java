package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.ChooseDistrictAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChooseDistrictPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChooseDistrictPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.District;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseDistrictView;

/**
 * Created by Hau Le on 2018-08-01.
 */

public class ChooseDistrictActivity extends BaseActivity
        implements ChooseDistrictView, View.OnClickListener{
    private Dialog dialog;
    private Realm realm;
    private ArrayList<District> list;
    private ChooseDistrictAdapter adapter;
    private String cityId = "";
    private ChooseDistrictPresenter presenter;
    private  APIService apiService;

    @BindView(R.id.layout_choose_district) LinearLayout parentLayout;
    @BindView(R.id.img_nav_back_choose_district) RelativeLayout imgBack;
    @BindView(R.id.recycler_view_district) RecyclerView recyclerView;
    @BindView(R.id.img_district_refresh) RelativeLayout imgRefresh;
    @BindView(R.id.edt_search_district) EditText edtSearchDistrict;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, parentLayout);
        realm = Realm.getDefaultInstance();
        init();
        dataIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        callApi();
    }

    private void dataIntent() {
        //get data from intent
        cityId = getIntent().getStringExtra(Utils.CITY_ID);
    }

    private void init() {
        list = new ArrayList<>();
        adapter = new ChooseDistrictAdapter(this, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //return district data to create cus info activity
                Intent intent = new Intent();
                intent.putExtra(Utils.DISTRICT, list.get(position).getId() + ":" + list.get(position).getName());
                setResult(Utils.REQUEST_CODE_DISTRICT, intent);
                finish();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        imgRefresh.setOnClickListener(this);
        edtSearchDistrict.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.searchDistrictList(realm, String.valueOf(editable.toString()), cityId);
            }
        });
    }

    private void callApi(){
        apiService = ApiUtils.getApiService();
        presenter = new ChooseDistrictPresenterImpl(this);
        presenter.getDistrictList(this, apiService, cityId, realm);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_choose_district;
    }

    @Override
    public void getDistrictListSuccess() {
        list = (ArrayList<District>) realm.copyFromRealm(realm.where(District.class)
                .equalTo(Utils.CITY_ID, Integer.parseInt(cityId))
                .findAll());
        adapter.notifyData(list);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void authorizedInvalid(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void search(ArrayList<District> list) {
        this.list = list;
        adapter.notifyData(list);
    }

    @Override
    public void showLoadingProgress() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenLoadingProgress() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_nav_back_choose_district:
                onBackPressed();
                break;
            case R.id.img_district_refresh:
                //call api refresh page
                presenter.refreshDistrictList(this, apiService, cityId, realm);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
