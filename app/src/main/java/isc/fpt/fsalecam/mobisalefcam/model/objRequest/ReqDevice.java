package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hau Le on 2018-08-20.
 */

public class ReqDevice implements Serializable{
    @SerializedName("Value")
    @Expose
    private String value;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Number")
    @Expose
    private String number;
    @SerializedName("ChangeNumber")
    @Expose
    private String changeNumber;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("TotalPrice")
    @Expose
    private Double totalPrice;

    public ReqDevice(String value, String name, String number, String changeNumber, Double price, Double totalPrice) {
        this.value = value;
        this.name = name;
        this.number = number;
        this.changeNumber = changeNumber;
        this.price = price;
        this.totalPrice = totalPrice;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getChangeNumber() {
        return changeNumber;
    }

    public void setChangeNumber(String changeNumber) {
        this.changeNumber = changeNumber;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String objToString(){
        return "{\"Value\":\"" + getValue() + "\",\"Name\":\"" + getName() + "\",\"Number\":\"" + getNumber() + "\",\"Price\":\"" + getPrice() + "\"}";
    }
}

