package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CheckImei;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CurrentVersion;

/**
 * Created by Hau Le on 2018-09-18.
 */
public interface SplashScreenView {
    void checkImeiIsActiveSuccess(CheckImei obj);
    void checkCurrentVersionSuccess(CurrentVersion obj);
    void checkCurrentVersionFailed(String mes);
    void checkImeiIsActiveFailed(String str);
    void getDeviceIMEI(String imei);
    void downloadNewVersionSuccess(String path);
    void downloadNewVersionFailed(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
