package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqReportContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ReportContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResReportContract;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ReportFragmentView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-17.
 */
public class ReportFragmentPresenterImpl implements ReportFragmentPresenter {
    private ReportFragmentView view;

    public ReportFragmentPresenterImpl(ReportFragmentView view) {
        this.view = view;
    }

    @Override
    public void doGetReportContractList(final Context context, final APIService apiService, final ReqReportContract obj) {
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetReportContractList(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetReportContractList(context, apiService, obj);
        }
    }

    private void callApiGetReportContractList(final Context context, APIService apiService, ReqReportContract obj){
        apiService.doGetReportContract(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjReqReportContract())
                .enqueue(new Callback<ResReportContract>() {
                    @Override
                    public void onResponse(Call<ResReportContract> call, Response<ResReportContract> response) {
                        validateReturnCode(context, response);
                    }

                    @Override
                    public void onFailure(Call<ResReportContract> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResReportContract> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getReportListSuccess((ArrayList<ReportContract>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else {
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else {
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }
}
