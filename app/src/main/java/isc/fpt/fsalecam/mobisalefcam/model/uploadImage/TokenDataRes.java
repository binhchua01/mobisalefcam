package isc.fpt.fsalecam.mobisalefcam.model.uploadImage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenDataRes {

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("ExpiryDate")
    @Expose
    private String expiryDate;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
}
