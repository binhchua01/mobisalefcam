package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.DeviceListCusDetailAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationById;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationDetail;
import isc.fpt.fsalecam.mobisalefcam.presenter.CusInfoDetailsPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.CusInfoDetailsPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.CusInfoDetailsView;
import static isc.fpt.fsalecam.mobisalefcam.view.fragment.CusWaitConfirmFragment.cusInfo;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.reqGetRegistrationDetail;

public class CusInfoDetailsActivity extends BaseActivity
        implements View.OnClickListener, CusInfoDetailsView{
    @BindView(R.id.ic_icon_nav_back_details) RelativeLayout imgBack;
    @BindView(R.id.btn_see_contract) Button btnSeeContract;
    @BindView(R.id.tv_pop) TextView tvPop;
    @BindView(R.id.tv_status) TextView tvStatus;
    @BindView(R.id.tv_full_name) TextView tvFullName;
    @BindView(R.id.tv_reg_code) TextView tvRegCode;
    @BindView(R.id.tv_phone_number) TextView tvPhoneNumber;
    @BindView(R.id.tv_address) TextView tvAddress;
    @BindView(R.id.tv_total_internet) TextView tvTotalInternet;
    @BindView(R.id.tv_connection_fee) TextView tvConnectionFee;
    @BindView(R.id.tv_deposit_fee) TextView tvDepositFee;
    @BindView(R.id.tv_vat) TextView tvVat;
    @BindView(R.id.tv_type_payment) TextView tvTypePayment;
    @BindView(R.id.tv_total_amount) TextView tvTotalAmount;
    @BindView(R.id.tv_promotion_name) TextView tvPromotionName;
    @BindView(R.id.tv_promotion_name_hidden) TextView tvPromotionHidden;
    @BindView(R.id.tv_show_more_promotion) TextView tvShowMore;
    @BindView(R.id.recycler_view_list_devices) RecyclerView recyclerViewListDevice;
    @BindView(R.id.tv_update_bookport) TextView tvUpdateBookport;
    @BindView(R.id.tv_update_cus_info) TextView tvUpdateCusInfo;
    @BindView(R.id.tv_update_image_registration) TextView tvUpdateImage;
    @BindView(R.id.tv_detail_image_registration) TextView tvDetailImage;

    private APIService apiService;
    private CusInfoDetailsPresenter presenter;
    private Dialog dialog;
    private Dialog dialogChangeActivity;
    private DeviceListCusDetailAdapter adapter;
    //this param use at ContractActivity
    public static RegistrationDetail registrationDetail;
    //references in contract activity, if haven't upload image document -> don't accept create contract
    public static int isUploadImage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        putDataReg();
        initDeviceList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        callApi();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_cus_info_details;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.ic_icon_nav_back_details:
                onBackPressed();
                break;
            case R.id.btn_see_contract:
                if(isUploadImage == 0){
                    DialogUtils.showMessageDialogVerButton(this,
                            getResources().getString(R.string.ui_text_upload_image_contract)).show();
                    return;
                }
                intent = new Intent(this, ContractActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_update_bookport:
                presenter.doGetRegistrationById(this, apiService, reqGetRegistrationDetail);
                break;
            case R.id.tv_update_cus_info:
                intent = new Intent(this, NewCustomerActivity.class);
                //DO_UPDATE = 1 -> accept update, 0 isn't update. default DO_UPDATE = 0
                Utils.DO_UPDATE = 1;
                intent.putExtra(Utils.CAN_BACK, true);
                startActivity(intent);
                break;
            case R.id.tv_update_image_registration:
                startActivity(new Intent(this, ImageRegistrationActivity.class));
                break;
            case R.id.tv_detail_image_registration:
                startActivity(new Intent(this, ViewImageRegistrationActivity.class));
                break;
            case R.id.tv_show_more_promotion:
                if(isVisibility(tvPromotionHidden)){
                    tvPromotionHidden.setVisibility(View.GONE);
                    tvShowMore.setText(getResources().getString(R.string.show_more));
                }else{
                    tvPromotionHidden.setVisibility(View.VISIBLE);
                    tvShowMore.setText(getResources().getString(R.string.show_less));
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getIntent().getBooleanExtra(Utils.CAN_BACK, false)){
            return;
        }
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
    }

    @Override
    public void getRegistrationDetailSuccess(RegistrationDetail obj) {
        initDataView(obj);
        registrationDetail = obj;
    }

    @Override
    public void getRegistrationById(RegistrationById obj) {
        //show dialog progress
        dialogChangeActivity = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialogChangeActivity.show();
        //change activity
        Intent intent = new Intent(this, SurveyPopActivity.class);
        intent.putExtra(Utils.UPDATE_BOOKPORT, obj);
        startActivity(intent);
//        finish();
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showDialogProgress() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog.isShowing() && dialog!=null){
            dialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(dialogChangeActivity !=null && dialogChangeActivity.isShowing()){
            dialogChangeActivity.dismiss();
        }
    }

    @SuppressLint("SetTextI18n")
    private void initDataView(RegistrationDetail obj) {
        if(obj != null){
            //bookport info
            tvPop.setText(obj.getGroupPoints());
            //customer info
            tvStatus.setText(obj.getRegStatus());
            tvFullName.setText(obj.getFullName());
            tvRegCode.setText(obj.getRegCode());
            tvPhoneNumber.setText(obj.getPhone1());
            tvAddress.setText(obj.getAddress());
            //service info
            tvTotalInternet.setText(String.valueOf(obj.getInternetTotal()));
            tvDepositFee.setText(String.valueOf(obj.getDepositFee()));
            tvConnectionFee.setText(String.valueOf(obj.getConnectionFee()));
            tvVat.setText(String.valueOf(obj.getVAT())  + "%");
            tvTypePayment.setText(obj.getPaymentMethodName());
            tvTotalAmount.setText(String.valueOf(obj.getTotal()));
            //promotion code
            tvPromotionName.setText(obj.getPromotionName());
            tvPromotionHidden.setText(obj.getPromotionDescription());
            //notify data list device
            adapter.notifyData((ArrayList<ReqDevice>) obj.getListDevice());
            //check status upload image info
            isUploadImage = obj.getIsUpdateImage();
            if(obj.getIsUpdateImage() == 1){
                tvUpdateImage.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
            //check status bookport
            if(obj.getIsBookPort() == 0){
                //bookport again
                presenter.doGetRegistrationById(this, apiService, reqGetRegistrationDetail);
            }
        }
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new CusInfoDetailsPresenterImpl(this);
        presenter.doGetRegistrationDetails(this, apiService, reqGetRegistrationDetail);
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        btnSeeContract.setOnClickListener(this);
        tvUpdateBookport.setOnClickListener(this);
        tvUpdateCusInfo.setOnClickListener(this);
        tvUpdateImage.setOnClickListener(this);
        tvDetailImage.setOnClickListener(this);
        tvShowMore.setOnClickListener(this);
    }

    private void initDeviceList() {
        ArrayList<ReqDevice> list = new ArrayList<>();
        adapter = new DeviceListCusDetailAdapter(this, list);
        recyclerViewListDevice.setAdapter(adapter);
    }

    private void putDataReg() {
        if(cusInfo != null){
            reqGetRegistrationDetail.setRegId(String.valueOf(cusInfo.getRegID()));
            reqGetRegistrationDetail.setRegCode(String.valueOf(cusInfo.getRegCode()));
        }
    }
}
