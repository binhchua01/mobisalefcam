package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqRegistrationAll;

/**
 * Created by Hau Le on 2018-08-30.
 */
public interface CusInfoListPresenter {
    void doGetRegistrationSearchType(Context context, APIService apiService, Realm realm);
    void doSearchRegistration(Context context, APIService apiService, ReqRegistrationAll obj);
}
