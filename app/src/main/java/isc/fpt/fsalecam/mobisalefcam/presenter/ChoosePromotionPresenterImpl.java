package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import io.realm.Case;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetPromotionList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Promotion;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetPromotionList;
import isc.fpt.fsalecam.mobisalefcam.database.PromotionCode;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChoosePromotionView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-17.
 */
public class ChoosePromotionPresenterImpl implements ChoosePromotionPresenter {
    private ChoosePromotionView view;

    public ChoosePromotionPresenterImpl(ChoosePromotionView view) {
        this.view = view;
    }

    @Override
    public void doGetPromotionList(final Context context, final APIService apiService, final Realm realm,
                                   final String servicePackageId, final String locationId, final String contract) {
        if(realm.where(PromotionCode.class)
                .equalTo(Utils.CITY_ID, Integer.parseInt(locationId))
                .equalTo(Utils.SERVICE_PACKAGE_ID, Integer.parseInt(servicePackageId))
                .findAll().size() == 0){
            //show dialog loading..
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetPromotionList(context, apiService, realm, servicePackageId, locationId, contract);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiGetPromotionList(context, apiService, realm, servicePackageId, locationId, contract);
            }
        }else{
            ArrayList<PromotionCode> list = (ArrayList<PromotionCode>) realm.copyFromRealm(realm.where(PromotionCode.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId))
                    .equalTo(Utils.SERVICE_PACKAGE_ID, Integer.parseInt(servicePackageId))
                    .findAll());
            view.getPromotionListSuccess(list);
        }
    }

    @Override
    public void doRefresh(final Context context, final APIService apiService,
                          final Realm realm, final String localTypeId, final String locationId, final String contract) {
        //show dialog loading..
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetPromotionList(context, apiService, realm, localTypeId, locationId, contract);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetPromotionList(context, apiService, realm, localTypeId, locationId, contract);
        }
    }

    @Override
    public void searchPromotion(Realm realm, String keyQuery) {
        view.getPromotionListSuccess(
                (ArrayList<PromotionCode>) realm.copyFromRealm(realm.where(PromotionCode.class)
                .like(Utils.PROMOTION_NAME, "*" + keyQuery + "*", Case.SENSITIVE)
                .findAll())
        );
    }

    private void callApiGetPromotionList(final Context context, APIService apiService, final Realm realm,
                                         final String localTypeId, final String locationId, String contract){
        //put data to object
        final ReqGetPromotionList obj = new ReqGetPromotionList(
                SharedPref.get(context, SharedPref.Key.USERNAME, ""),
                locationId,
                localTypeId,
                contract
        );
        //call api get district list...
        apiService.doGetPromotionList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjGetPromotionList())
                .enqueue(new Callback<ResGetPromotionList>() {
                    @Override
                    public void onResponse(Call<ResGetPromotionList> call, Response<ResGetPromotionList> response) {
                        //success
                        validateReturnCode(context, realm, response, localTypeId, locationId);
                    }

                    @Override
                    public void onFailure(Call<ResGetPromotionList> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Realm realm,
                                    Response<ResGetPromotionList> response,
                                    String servicePackageId, String locationId){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                savePromotionListLocal(realm, (ArrayList<Promotion>)
                        response.body().getData(), servicePackageId, locationId);
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void savePromotionListLocal(final Realm realm, final ArrayList<Promotion> list,
                                        String servicePackageId, String locationId){
        if(list.size() != 0){
            //delete all table before add
            realm.beginTransaction();
            realm.where(PromotionCode.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId))
                    .equalTo(Utils.SERVICE_PACKAGE_ID, Integer.parseInt(servicePackageId))
                    .findAll().deleteAllFromRealm();
            realm.commitTransaction();
            //add
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                PromotionCode pro = realm.createObject(PromotionCode.class, realm.where(PromotionCode.class).findAll().size() + 1);
                pro.setPromotionId(list.get(i).getPromotionID());
                pro.setPromotionName(list.get(i).getPromotionName());
                pro.setDescription(list.get(i).getDescription());
                pro.setMonthOfPrepaid(list.get(i).getMonthOfPrepaid());
                pro.setRealPrepaid(list.get(i).getRealPrepaid());
                pro.setPrepaidPromotion(list.get(i).getPrepaidPromotion());
                pro.setPackageServiceID(Integer.parseInt(servicePackageId));
                pro.setCityID(Integer.parseInt(locationId));
                realm.commitTransaction();
            }
            ArrayList<PromotionCode> data = (ArrayList<PromotionCode>) realm.copyFromRealm(realm.where(PromotionCode.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId))
                    .equalTo(Utils.SERVICE_PACKAGE_ID, Integer.parseInt(servicePackageId))
                    .findAll());
            view.getPromotionListSuccess(data);
        }else{
            ArrayList<PromotionCode> data = (ArrayList<PromotionCode>) realm.copyFromRealm(realm.where(PromotionCode.class)
                    .equalTo(Utils.CITY_ID, Integer.parseInt(locationId))
                    .equalTo(Utils.SERVICE_PACKAGE_ID, Integer.parseInt(servicePackageId))
                    .findAll());
            view.getPromotionListSuccess(data);
        }
    }
}
