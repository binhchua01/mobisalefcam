package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-17.
 */
public class ReqReportContract implements Serializable{
    private String fromDate;
    private String toDate;
    private int status = 0;

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public LinkedHashMap<String, String> generateObjReqReportContract(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("FromDate", getFromDate());
        obj.put("ToDate", getToDate());
        obj.put("Status", String.valueOf(getStatus()));
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("FromDate", getFromDate());
            obj.put("ToDate", getToDate());
            obj.put("Status", String.valueOf(getStatus()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
