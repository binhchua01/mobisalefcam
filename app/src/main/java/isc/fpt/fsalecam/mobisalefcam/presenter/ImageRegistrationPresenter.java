package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationImage;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;

/**
 * Created by Hau Le on 2018-09-11.
 */
public interface ImageRegistrationPresenter {
    void doUpdateRegistration(Context context, APIService apiService, ReqUpdateRegistrationImage obj);
    void doUploadImageRegistration(Context context, APIService apiService,
                                   ArrayList<String> list, String regId, SaleOfInfo saleInfo);
}
