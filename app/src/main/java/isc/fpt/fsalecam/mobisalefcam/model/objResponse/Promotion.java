package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-08-16.
 */
public class Promotion {
    @SerializedName("PromotionID")
    @Expose
    private Integer promotionID;
    @SerializedName("PromotionName")
    @Expose
    private String promotionName;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("MonthOfPrepaid")
    @Expose
    private Integer monthOfPrepaid;
    @SerializedName("RealPrepaid")
    @Expose
    private Integer realPrepaid;
    @SerializedName("PrepaidPromotion")
    @Expose
    private Integer prepaidPromotion;

    public Integer getPromotionID() {
        return promotionID;
    }

    public void setPromotionID(Integer promotionID) {
        this.promotionID = promotionID;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMonthOfPrepaid() {
        return monthOfPrepaid;
    }

    public void setMonthOfPrepaid(Integer monthOfPrepaid) {
        this.monthOfPrepaid = monthOfPrepaid;
    }

    public Integer getRealPrepaid() {
        return realPrepaid;
    }

    public void setRealPrepaid(Integer realPrepaid) {
        this.realPrepaid = realPrepaid;
    }

    public Integer getPrepaidPromotion() {
        return prepaidPromotion;
    }

    public void setPrepaidPromotion(Integer prepaidPromotion) {
        this.prepaidPromotion = prepaidPromotion;
    }
}
