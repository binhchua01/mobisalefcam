package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import io.realm.Case;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetDistrictList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.Building;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseBuildingView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-18.
 */
public class ChooseBuildingPresenterImpl implements ChooseBuildingPresenter {
    private ChooseBuildingView view;

    public ChooseBuildingPresenterImpl(ChooseBuildingView view) {
        this.view = view;
    }

    @Override
    public void getBuildingList(final Context context, final APIService apiService, final String locationId, final Realm realm) {
        if(realm.where(Building.class).findAll().size() == 0 ||
                realm.where(Building.class)
                        .equalTo(Utils.CITY_ID, Integer.parseInt(locationId))
                        .findAll().size() == 0){
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetBuildingList(context, apiService, locationId, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiGetBuildingList(context, apiService, locationId, realm);
            }
        }else{
            view.getBuildingListSuccess();
        }
    }

    @Override
    public void doRefreshBuildingList(final Context context, final APIService apiService, final String locationId, final Realm realm) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetBuildingList(context, apiService, locationId, realm);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetBuildingList(context, apiService, locationId, realm);
        }
    }

    @Override
    public void searchBuildingList(Realm realm, String keyQuery, String cityId) {
        ArrayList<Building> list = (ArrayList<Building>) realm.copyFromRealm(realm.where(Building.class)
                .equalTo(Utils.CITY_ID, Integer.parseInt(cityId))
                .like(Utils.BUILDING_NAME,  "*" + keyQuery + "*", Case.SENSITIVE)
                .findAll());
        view.search(list);
    }

    private void callApiGetBuildingList(final Context context, APIService apiService,
                                        final String locationId, final Realm realm){
        //put data to object
        final ReqGetDistrictList location = new ReqGetDistrictList(locationId);
        //call api
        apiService.doGetBuildingList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(location.objToString()),
                location.generateObjGetDistrictList())
                .enqueue(new Callback<ResParentStructure>() {
                    @Override
                    public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                        validateReturnCode(context, response, realm, locationId);
                    }

                    @Override
                    public void onFailure(Call<ResParentStructure> call, Throwable t) {
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                        view.hiddenProgressDialog();
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> obj, Realm realm, String cityID){
        if(obj.body()!= null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                //save Building list to local
                saveBuildingListToLocal((ArrayList<ItemOfListRes>) obj.body().getData(), realm, cityID);
                //save Authorization header
                Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(obj.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void saveBuildingListToLocal(final ArrayList<ItemOfListRes> list, final Realm realm, final String cityID) {
        realm.beginTransaction();
        realm.where(Building.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size() != 0){
            //add
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                Building building = realm.createObject(Building.class, list.get(i).getId());
                building.setName(list.get(i).getName());
                building.setCityID(Integer.parseInt(cityID));
                realm.commitTransaction();
            }
            view.getBuildingListSuccess();
        }else{
            view.getBuildingListSuccess();
        }
    }
}
