package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ReportContract;

/**
 * Created by Hau Le on 2018-09-17.
 */
public interface ReportFragmentView {
    void getReportListSuccess(ArrayList<ReportContract> list);
    void callApiFailed(String str);
    void expiredToken(String str);
    void showProgressLoading();
    void hiddenProgressLoading();
}
