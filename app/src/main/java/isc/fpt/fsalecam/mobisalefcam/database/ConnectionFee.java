package isc.fpt.fsalecam.mobisalefcam.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Hau Le on 2018-10-02.
 */
public class ConnectionFee extends RealmObject {
    @PrimaryKey
    private int id;
    private int cityID;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
