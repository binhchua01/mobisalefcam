package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-08-02.
 */
public interface ChooseStreetPresenter {
    void doGetStreetList(Context context, APIService apiService, String cityId, String districtId,
                         String wardId, int type, Realm realm);
    void refreshStreetList(Context context, APIService apiService, String cityId, String districtId,
                           String wardId, int type, Realm realm);
    void searchStreetList(Realm realm, String keyQuery, String cityId, String districtId, String wardId);
}
