package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.ArrayList;

import io.realm.Case;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqPackageService;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.PackageService;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.PackageServiceView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-16.
 */
public class PackageServicePresenterImpl implements PackageServicePresenter {
    private PackageServiceView view;

    public PackageServicePresenterImpl(PackageServiceView view) {
        this.view = view;
    }

    @Override
    public void doGetPackageServiceList(final Context context, final APIService apiService,
                                        final Realm realm, final String contractId) {
        if(realm.where(PackageService.class).findAll().size() == 0){
            view.showDialogProgress();
            long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if (System.currentTimeMillis() >= timerRefresh) {
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetPackageServiceList(context, apiService, realm, contractId);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenDialogProgress();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            } else {
                callApiGetPackageServiceList(context, apiService, realm, contractId);
            }
        }else{
            view.getPackageServiceListSuccess((ArrayList<PackageService>)
                    realm.copyFromRealm(realm.where(PackageService.class).findAll()));
        }
    }

    @Override
    public void doRefreshPackageService(final Context context, final APIService apiService, final Realm realm, final String contractId) {
        view.showDialogProgress();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if (System.currentTimeMillis() >= timerRefresh) {
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetPackageServiceList(context, apiService, realm, contractId);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenDialogProgress();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        } else {
            callApiGetPackageServiceList(context, apiService, realm, contractId);
        }
    }

    @Override
    public void search(Realm realm, String keyQuery) {
        view.searchServicePackage((ArrayList<PackageService>) realm.copyFromRealm(realm.where(PackageService.class)
                .like(Utils.SERVICE_PACKAGE_NAME, "*" + keyQuery + "*", Case.SENSITIVE)
                .findAll()));
    }

    private void callApiGetPackageServiceList(final Context context, APIService apiService,
                                              final Realm realm, String contractId){
        final ReqPackageService obj = new ReqPackageService(
                SharedPref.get(context, SharedPref.Key.USERNAME, ""),
                contractId
        );

        apiService.doGetServicePackageList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generatePackageServiceObj()
        ).enqueue(new Callback<ResParentStructure>() {
            @Override
            public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                validateReturnCode(context, response, realm);
            }

            @Override
            public void onFailure(Call<ResParentStructure> call, Throwable t) {
                view.hiddenDialogProgress();
                view.callApiFailed(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> response, Realm realm){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                savePackageServiceLocal(realm, (ArrayList<ItemOfListRes>) response.body().getData());
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenDialogProgress();
    }

    private void savePackageServiceLocal(final Realm realm, final ArrayList<ItemOfListRes> list){
        if(list.size()!=0){
            for(int i=0; i<list.size(); i++){
                final int finalI = i;
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm bgRealm) {
                        PackageService pack = bgRealm.createObject(PackageService.class, list.get(finalI).getId());
                        pack.setName(list.get(finalI).getName());
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        view.getPackageServiceListSuccess((ArrayList<PackageService>)
                                realm.copyFromRealm(realm.where(PackageService.class).findAll()));
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(@NonNull Throwable error) {
                        // Transaction failed and was automatically canceled.
                        view.getPackageServiceListSuccess((ArrayList<PackageService>)
                                realm.copyFromRealm(realm.where(PackageService.class).findAll()));
                    }
                });
            }
        }
    }
}
