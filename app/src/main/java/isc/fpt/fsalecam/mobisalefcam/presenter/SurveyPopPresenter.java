package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqBookportAuto;

/**
 * Created by Hau Le on 2018-08-10.
 */
public interface SurveyPopPresenter {
    void doGenerateAddress(Context context, APIService apiService);
    void doGetListOfPoints(Context context, APIService apiService,
                           String username, String locationId, String wardId,
                           String lat, String lng, String typeOfNetwork, String bookPortIdentityId);
    void doBookportAuto(Context context, APIService apiService, ReqBookportAuto obj);
}
