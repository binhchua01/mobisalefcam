package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-07-06.
 */
public class ReqLogin {
    private String Username, Password, DeviceIMEI, DeviceToken;

    public ReqLogin(String username, String password, String deviceIMEI, String DeviceToken) {
        this.Username = username;
        this.Password = password;
        this.DeviceIMEI = deviceIMEI;
        this.DeviceToken = DeviceToken;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getDeviceIMEI() {
        return DeviceIMEI;
    }

    public void setDeviceIMEI(String deviceIMEI) {
        DeviceIMEI = deviceIMEI;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    public String userInfoToString(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Username", Username);
            jsonObject.put("Password", Password);
            jsonObject.put("DeviceIMEI", DeviceIMEI);
            jsonObject.put("DeviceToken", DeviceToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(jsonObject);
    }

    //object send to server
    public LinkedHashMap<String, String> userObj(){
        LinkedHashMap<String, String> userInfo = new LinkedHashMap<>();
        userInfo.put("Username", getUsername());
        userInfo.put("Password", getPassword());
        userInfo.put("DeviceIMEI", getDeviceIMEI());
        userInfo.put("DeviceToken", getDeviceToken());
        return userInfo;
    }


}
