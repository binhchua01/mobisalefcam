package isc.fpt.fsalecam.mobisalefcam.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ReportContract;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;

/**
 * Created by Hau Le on 2018-09-17.
 */
public class ReportContractAdapter extends RecyclerView.Adapter<ReportContractAdapter.SimpleViewHolder> {
    private Context context;
    private ArrayList<ReportContract> list;
    private OnItemClickListener listener;

    public ReportContractAdapter(Context context, ArrayList<ReportContract> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_one_report_contract, parent, false);
        final SimpleViewHolder mViewHolder = new SimpleViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClickListener(view, mViewHolder.getAdapterPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        switch (list.get(position).getStatus()){
            case 0:
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.s_grey));
                break;
            case 1:
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.correct_green));
                break;
            case 2:
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.wrong_pink));
                break;
            default:
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.s_grey));
                break;

        }
        holder.tvStatus.setText(list.get(position).getStatusName());
        holder.tvCusName.setText(list.get(position).getFullName());
        holder.tvContractNo.setText(list.get(position).getContract());
        holder.tvCreateContractDate.setText(list.get(position).getObjCreateDate().split(" ")[0]);
        holder.tvServicePackage.setText(list.get(position).getLocalTypeName());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_status_of_cus) TextView tvStatus;
        @BindView(R.id.tv_cus_name) TextView tvCusName;
        @BindView(R.id.tv_contract_no) TextView tvContractNo;
        @BindView(R.id.tv_create_contract_date) TextView tvCreateContractDate;
        @BindView(R.id.tv_service_package) TextView tvServicePackage;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void notifyData(ArrayList<ReportContract> list){
        this.list = list;
        notifyDataSetChanged();
    }
}
