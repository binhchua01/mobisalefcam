package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqPotentialCusList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.PotentialCus;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResPotentialCusList;
import isc.fpt.fsalecam.mobisalefcam.database.PotentialCusTypeSearch;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.PotentialCusListView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-09-14.
 */
public class PotentialCusListPresenterImpl implements PotentialCusListPresenter {
    private PotentialCusListView view;

    public PotentialCusListPresenterImpl(PotentialCusListView view) {
        this.view = view;
    }

    @Override
    public void doGetPotentialCusList(final Context context, final APIService apiService, final ReqPotentialCusList obj) {
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetPotentialCusList(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetPotentialCusList(context, apiService, obj);
        }
    }

    @Override
    public void doGetPotentialCusTypeList(final Context context, final APIService apiService, final Realm realm) {
        if(realm.where(PotentialCusTypeSearch.class).findAll().size() == 0){
            view.showProgressDialog();
            long timerRefresh = Long.parseLong(SharedPref.get(context,
                    SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetPotentialCusTypeList(context, apiService, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiGetPotentialCusTypeList(context, apiService, realm);
            }
        }else{
            view.getPotentialCusTypeList((ArrayList<PotentialCusTypeSearch>)
                    realm.copyFromRealm(realm.where(PotentialCusTypeSearch.class).findAll()));
        }
    }

    private void callApiGetPotentialCusTypeList(final Context context, APIService apiService, final Realm realm){
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        apiService.doGetPotentialCusTypeList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.toString()),
                obj)
                .enqueue(new Callback<ResParentStructure>() {
                    @Override
                    public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                        validateReturnCodePotentialCusTypeList(context, response, realm);
                    }

                    @Override
                    public void onFailure(Call<ResParentStructure> call, Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void callApiGetPotentialCusList(final Context context, APIService apiService, ReqPotentialCusList obj){
        apiService.doGetPotentialCusList(
            Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
            Utils.CONTENT_TYPE,
            SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
            Utils.checkSum(obj.objToString()),
            obj.generateObjGetPotentialCusList())
            .enqueue(new Callback<ResPotentialCusList>() {
                @Override
                public void onResponse(Call<ResPotentialCusList> call, Response<ResPotentialCusList> response) {
                    validateReturnCodePotentialCusList(context, response);
                }

                @Override
                public void onFailure(Call<ResPotentialCusList> call, Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    private void validateReturnCodePotentialCusList(Context context, Response<ResPotentialCusList> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.getPotentialCusListSuccess((ArrayList<PotentialCus>) response.body().getData());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else {
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else {
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodePotentialCusTypeList(Context context, Response<ResParentStructure> response, Realm realm){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                savePotentialCusTypeSearch((ArrayList<ItemOfListRes>) response.body().getData(), realm);
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else {
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else {
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void savePotentialCusTypeSearch(ArrayList<ItemOfListRes> list, Realm realm) {
        realm.beginTransaction();
        realm.where(PotentialCusTypeSearch.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                PotentialCusTypeSearch data = realm.createObject(PotentialCusTypeSearch.class, list.get(i).getId());
                data.setName(list.get(i).getName());
                realm.commitTransaction();
            }
            view.getPotentialCusTypeList((ArrayList<PotentialCusTypeSearch>)
                    realm.copyFromRealm(realm.where(PotentialCusTypeSearch.class).findAll()));
        }else{
            view.getPotentialCusTypeList((ArrayList<PotentialCusTypeSearch>)
                    realm.copyFromRealm(realm.where(PotentialCusTypeSearch.class).findAll()));
        }
    }
}
