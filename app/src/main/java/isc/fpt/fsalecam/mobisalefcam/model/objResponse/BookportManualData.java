package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-08-14.
 */
public class BookportManualData {
    @SerializedName("BookportIdentityID")
    @Expose
    private String bookportIdentityID;
    @SerializedName("BookPortManual")
    @Expose
    private BookPortManual bookPortManual;

    public String getBookportIdentityID() {
        return bookportIdentityID;
    }

    public void setBookportIdentityID(String bookportIdentityID) {
        this.bookportIdentityID = bookportIdentityID;
    }

    public BookPortManual getBookPortManual() {
        return bookPortManual;
    }

    public void setBookPortManual(BookPortManual bookPortManual) {
        this.bookPortManual = bookPortManual;
    }
}
