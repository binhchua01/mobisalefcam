package isc.fpt.fsalecam.mobisalefcam.utils;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.database.DeviceList;

/**
 * Created by Hau Le on 2018-08-22.
 */
public interface CallbackDeviceListSelected {
    void callbackDeviceList(ArrayList<DeviceList> obj);
}
