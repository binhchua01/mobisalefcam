package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-07-31.
 */

public class ReqGetWardList {
    private String locationID;
    private String districtID;

    public ReqGetWardList(String locationID, String districtID) {
        this.locationID = locationID;
        this.districtID = districtID;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getDistrictID() {
        return districtID;
    }

    public void setDistrictID(String districtID) {
        this.districtID = districtID;
    }

    public LinkedHashMap<String, String> generateObjGetWardList(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("LocationID", locationID);
        obj.put("DistrictID", districtID);
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("LocationID", getLocationID());
            obj.put("DistrictID", getDistrictID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
