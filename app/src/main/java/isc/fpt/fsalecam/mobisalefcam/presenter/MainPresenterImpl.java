package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import io.realm.Realm;
import io.realm.RealmResults;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqChangePass;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetSaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.LocationOfSale;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResChangePass;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetSaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.SaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.database.City;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.MainView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-02.
 */
public class MainPresenterImpl implements MainPresenter {
    private MainView view;

    public MainPresenterImpl(MainView mainView) {
        this.view = mainView;
    }

    @Override
    public void doGetInfoOfSale(final Context context, final APIService apiService, final Realm realm, final String username) {
        //show dialog
        view.showLoadingDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetSaleOfInfo(context, apiService, realm, username);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenLoadingDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetSaleOfInfo(context, apiService, realm, username);
        }
    }

    @Override
    public void doChangePassword(final Context context, final APIService apiService, final ReqChangePass obj) {
        //show dialog
        view.showLoadingDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context, SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiChangePassword(context, apiService, obj);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenLoadingDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiChangePassword(context, apiService, obj);
        }
    }

    private void callApiChangePassword(final Context context, APIService apiService, ReqChangePass obj){
        //do call api
        apiService.doChangePassword(
            Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
            Utils.CONTENT_TYPE,
            SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
            Utils.checkSum(obj.objToString()),
            obj.generateObjChangePass())
            .enqueue(new Callback<ResChangePass>() {
                @Override
                public void onResponse(Call<ResChangePass> call, Response<ResChangePass> response) {
                    validateReturnCodeChangePassword(context, response);
                }

                @Override
                public void onFailure(Call<ResChangePass> call, Throwable t) {
                    view.hiddenLoadingDialog();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
    }

    private void callApiGetSaleOfInfo(final Context context, APIService apiService, final Realm realm, String username){
        final ReqGetSaleOfInfo saleInfo = new ReqGetSaleOfInfo(username);
        //do call api
        apiService.doGetSaleOfInfo(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(saleInfo.objToString()),
                saleInfo.generateObjGetSaleInfo())
                .enqueue(new Callback<ResGetSaleOfInfo>() {
                    @Override
                    public void onResponse(Call<ResGetSaleOfInfo> call, Response<ResGetSaleOfInfo> response) {
                        checkCodeReturn(context, response, realm);
                    }

                    @Override
                    public void onFailure(Call<ResGetSaleOfInfo> call, Throwable t) {
                        view.hiddenLoadingDialog();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCodeChangePassword(Context context, Response<ResChangePass> response){
        if(response.body()!=null){
            int code = response.body().getCode();
            if(code == Utils.SUCCESS){
                view.changePasswordSuccess(response.body().getMessage());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(response.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenLoadingDialog();
    }

    private void checkCodeReturn(Context context, Response<ResGetSaleOfInfo> obj, Realm realm){
        if(obj.body()!=null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                //return data to vew
                view.getInfoSuccess(obj.body().getData().get(0));
                //save list city to local
                saveListCityLocal(obj.body().getData().get(0), realm);
                //save to sharedPref
                saveAvailableParams(context, obj);
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(obj.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenLoadingDialog();
    }

    private void saveAvailableParams(Context context, Response<ResGetSaleOfInfo> obj) {
        //save SaleID
        SharedPref.put(context, SharedPref.Key.SALE_ID,
                String.valueOf(obj.body().getData().get(0).getSaleId()));
        SharedPref.put(context, SharedPref.Key.FULL_NAME,
                String.valueOf(obj.body().getData().get(0).getFullName()));
        SharedPref.put(context, SharedPref.Key.PHONE,
                String.valueOf(obj.body().getData().get(0).getPhone()));
        SharedPref.put(context, SharedPref.Key.EMAIL,
                String.valueOf(obj.body().getData().get(0).getEmail()));
        SharedPref.put(context, SharedPref.Key.REGION,
                String.valueOf(obj.body().getData().get(0).getLocation()));
        SharedPref.put(context, SharedPref.Key.APP_SERVER_IMAGE_TOKEN,
                String.valueOf(obj.body().getData().get(0).getAppServerImageToken()));
        //save Authorization header
        if(obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER) == null){
            return;
        }
        Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
    }

    private void saveListCityLocal(SaleOfInfo saleOfInfo, Realm realm){
        //save sale of info to use after...
        final ArrayList<LocationOfSale> listCity = (ArrayList<LocationOfSale>) saleOfInfo.getListLocation();
        RealmResults<City> listLocal = realm.where(City.class).findAll();
        if(listCity.size() > listLocal.size()){
            for (int i=0; i<listCity.size(); i++){
                final int finalI = i;
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm bgRealm) {
                        City city = bgRealm.createObject(City.class, listCity.get(finalI).getLocationId());
                        city.setName(listCity.get(finalI).getDescription());
                        city.setIsLocation(listCity.get(finalI).getIsCurrentLocation());
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(@NonNull Throwable error) {
                        // Transaction failed and was automatically canceled.
                    }
                });
            }
        }
    }
}
