package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;

/**
 * Created by Hau Le on 2018-07-10.
 */
public class ImplementFragment extends BaseFragment{
    @BindString(R.string.message_dialog_implement) String txtMessageDialog;

    private Dialog dialog;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //show dialog message...
        if(dialog == null || dialog.isShowing()){
            return;
        }
        dialog =  DialogUtils.showMessageDialogVerButton(getActivity(), txtMessageDialog);
        dialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_implement;
    }
}
