package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Hau Le on 2018-08-02.
 */
public class SaleOfInfo implements Serializable {
    @SerializedName("SaleId")
    @Expose
    private Integer saleId;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("IsManager")
    @Expose
    private Boolean isManager;
    @SerializedName("ManagerName")
    @Expose
    private String managerName;
    @SerializedName("DomainMBS")
    @Expose
    private String domainMBS;
    @SerializedName("DomailImage")
    @Expose
    private String domailImage;
    @SerializedName("UploadImageUrl")
    @Expose
    private String uploadImageUrl;
    @SerializedName("DownloadImageUrl")
    @Expose
    private String downloadImageUrl;
    @SerializedName("UploadImageToken")
    @Expose
    private String uploadImageToken;
    @SerializedName("UploadLinkId")
    @Expose
    private String uploadLinkId;
    @SerializedName("UploadSource")
    @Expose
    private String uploadSource;
    @SerializedName("UploadTypeContract")
    @Expose
    private String uploadTypeContract;
    @SerializedName("UploadTypeTTKH")
    @Expose
    private String uploadTypeTTKH;
    @SerializedName("AppKongToken")
    @Expose
    private String appKongToken;
    @SerializedName("AppServerImageToken")
    @Expose
    private String appServerImageToken;
    @SerializedName("ListLocation")
    @Expose
    private List<LocationOfSale> listLocation = null;

    public Boolean getManager() {
        return isManager;
    }

    public void setManager(Boolean manager) {
        isManager = manager;
    }

    public String getUploadSource() {
        return uploadSource;
    }

    public void setUploadSource(String uploadSource) {
        this.uploadSource = uploadSource;
    }

    public Integer getSaleId() {
        return saleId;
    }

    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Boolean getIsManager() {
        return isManager;
    }

    public void setIsManager(Boolean isManager) {
        this.isManager = isManager;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getDomainMBS() {
        return domainMBS;
    }

    public void setDomainMBS(String domainMBS) {
        this.domainMBS = domainMBS;
    }

    public String getDomailImage() {
        return domailImage;
    }

    public void setDomailImage(String domailImage) {
        this.domailImage = domailImage;
    }

    public String getUploadImageUrl() {
        return uploadImageUrl;
    }

    public void setUploadImageUrl(String uploadImageUrl) {
        this.uploadImageUrl = uploadImageUrl;
    }

    public String getDownloadImageUrl() {
        return downloadImageUrl;
    }

    public void setDownloadImageUrl(String downloadImageUrl) {
        this.downloadImageUrl = downloadImageUrl;
    }

    public String getUploadImageToken() {
        return uploadImageToken;
    }

    public void setUploadImageToken(String uploadImageToken) {
        this.uploadImageToken = uploadImageToken;
    }

    public String getUploadLinkId() {
        return uploadLinkId;
    }

    public void setUploadLinkId(String uploadLinkId) {
        this.uploadLinkId = uploadLinkId;
    }

    public String getUploadTypeContract() {
        return uploadTypeContract;
    }

    public void setUploadTypeContract(String uploadTypeContract) {
        this.uploadTypeContract = uploadTypeContract;
    }

    public String getUploadTypeTTKH() {
        return uploadTypeTTKH;
    }

    public void setUploadTypeTTKH(String uploadTypeTTKH) {
        this.uploadTypeTTKH = uploadTypeTTKH;
    }

    public String getAppKongToken() {
        return appKongToken;
    }

    public void setAppKongToken(String appKongToken) {
        this.appKongToken = appKongToken;
    }

    public String getAppServerImageToken() {
        return appServerImageToken;
    }

    public void setAppServerImageToken(String appServerImageToken) {
        this.appServerImageToken = appServerImageToken;
    }

    public List<LocationOfSale> getListLocation() {
        return listLocation;
    }

    public void setListLocation(List<LocationOfSale> listLocation) {
        this.listLocation = listLocation;
    }
}
