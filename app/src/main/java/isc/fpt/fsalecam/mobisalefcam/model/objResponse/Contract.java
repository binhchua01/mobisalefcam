package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hau Le on 2018-09-04.
 */
public class Contract implements Serializable{
    @SerializedName("ObjID")
    @Expose
    private Integer objID;
    @SerializedName("Contract")
    @Expose
    private String contract;

    public Integer getObjID() {
        return objID;
    }

    public void setObjID(Integer objID) {
        this.objID = objID;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }
}
