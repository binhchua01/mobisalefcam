package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.DeviceListAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDeviceList;
import isc.fpt.fsalecam.mobisalefcam.presenter.DeviceListPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.DeviceListPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.DeviceList;
import isc.fpt.fsalecam.mobisalefcam.utils.CallbackDeviceListSelected;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.DeviceListView;

public class EquipmentActivity extends BaseActivity
        implements DeviceListView, View.OnClickListener {
    private APIService apiService;
    private DeviceListPresenter presenter;
    private Dialog dialog;
    private Realm realm;
    private DeviceListAdapter adapter;
    private ArrayList<DeviceList> listDeviceOriginal;
    private ArrayList<DeviceList> listDeviceSelected;
    private ArrayList<DeviceList> listDeviceFinal;

    @BindView(R.id.layout_choose_device) RelativeLayout loParent;
    @BindView(R.id.img_nav_back_choose_device) RelativeLayout loBack;
    @BindView(R.id.img_device_refresh) RelativeLayout loRefresh;
    @BindView(R.id.recycler_view_device) RecyclerView recyclerView;
    @BindView(R.id.btn_confirm_device_list_selected) Button btnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        initRealm();
        intiView();
        setEvent();
        getDataIntent();
        callApi();
    }

    private void intiView() {
        listDeviceOriginal = new ArrayList<>();
        listDeviceSelected = new ArrayList<>();
        listDeviceFinal = new ArrayList<>();
        adapter = new DeviceListAdapter(this, listDeviceOriginal, new CallbackDeviceListSelected() {
            @Override
            public void callbackDeviceList(ArrayList<DeviceList> list) {
                listDeviceSelected = list;
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void setEvent() {
        loBack.setOnClickListener(this);
        loRefresh.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
    }

    private void initRealm() {
        realm = Realm.getDefaultInstance();
    }

    private void getDataIntent() {
        if (getIntent().getSerializableExtra(Utils.DEVICE_LIST_SELECTED) != null) {
            listDeviceSelected = (ArrayList<DeviceList>) getIntent().getSerializableExtra(Utils.DEVICE_LIST_SELECTED);
        }
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new DeviceListPresenterImpl(this);
        //put data to objReport
        ReqDeviceList obj = new ReqDeviceList();
        obj.setLocationId(Utils.updateCusInfo.getLocationId());
        obj.setMonthOfPrepaid(Utils.updateCusInfo.getMonthOfPrepaid());
        obj.setType(Utils.TYPE_SALE_NEW);
        obj.setLocalType(Integer.parseInt(Utils.updateCusInfo.getLocalType()));
        //call api
        presenter.deGetDeviceList(this, apiService, obj, realm);
    }


    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_equipment;
    }

    @Override
    public void getDeviceListSuccess(ArrayList<DeviceList> list) {
        this.listDeviceOriginal = list;
        adapter.notifyData(list);
        //set old item selected
        if (listDeviceSelected !=null) {
            for (int i = 0; i < list.size(); i++) {
                for (DeviceList deviceList : listDeviceSelected) {
                    if (deviceList.getName().equals(list.get(i).getName())){
                        //set selected item
                        list.get(i).setIsSelected(1);
                    }
                }
            }
        }
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_nav_back_choose_device:
                onBackPressed();
                break;
            case R.id.img_device_refresh:
                break;
            case R.id.btn_confirm_device_list_selected:
                //make device listDeviceOriginal selected final return
                for(int i = 0; i < listDeviceSelected.size(); i++){
                    if(listDeviceSelected.get(i).getIsSelected() == 1){
                        listDeviceFinal.add(listDeviceSelected.get(i));
                    }
                }
                Intent intent = new Intent();
                intent.putExtra(Utils.DEVICE_LIST_SELECTED, listDeviceFinal);
                setResult(Utils.REQUEST_CODE_DEVICE, intent);
                finish();
                break;
        }
    }
}
