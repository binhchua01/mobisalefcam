package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationById;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.database.ConnectionFee;
import isc.fpt.fsalecam.mobisalefcam.database.DepositFee;
import isc.fpt.fsalecam.mobisalefcam.database.Vat;

/**
 * Created by Hau Le on 2018-09-06.
 */
public interface UpdateTotalOfMoneyView {
    void getServiceTypeListSuccess(ArrayList<ItemOfListRes> list);
    void getConnectionFeeListSuccess(ArrayList<ConnectionFee> list);
    void getDepositListSuccess(ArrayList<DepositFee> list);
    void getVatListSuccess(ArrayList<Vat> list);
    void getRegistrationById(RegistrationById obj);
    void getCalNewTotal(ArrayList<CalNewTotal> list);
    void getUpdateRegistrationTotal(ArrayList<UpdateRegistrationTotal> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressLoading();
    void hiddenProgressLoading();
}
