package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;

/**
 * Created by Hau Le on 2018-09-18.
 */
public interface ChooseBuildingPresenter {
    void getBuildingList(Context context, APIService apiService,
                         String locationId, Realm realm);
    void doRefreshBuildingList(Context context, APIService apiService,
                               String locationId, Realm realm);
    void searchBuildingList(Realm realm, String keyQuery, String cityId);
}
