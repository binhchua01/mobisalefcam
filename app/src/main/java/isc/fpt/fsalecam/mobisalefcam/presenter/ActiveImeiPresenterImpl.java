package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.annotation.SuppressLint;
import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ActiveImeiView;

/**
 * Created by Hau Le on 2018-07-06.
 */
public class ActiveImeiPresenterImpl implements ActiveImeiPresenter {
    private ActiveImeiView activeImeiView;

    public ActiveImeiPresenterImpl(ActiveImeiView activeImeiView) {
        this.activeImeiView = activeImeiView;
    }

    @SuppressLint("HardwareIds")
    @Override
    public void readImeiDevice(Context context) {
        activeImeiView.displayImei(Utils.getIMEIDevice(context));
    }

}
