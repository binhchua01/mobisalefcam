package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.DeviceListCusDetailAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqContractDetails;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Contract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ContractDetails;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Registration;
import isc.fpt.fsalecam.mobisalefcam.presenter.ContractDetailsPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ContractDetailPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OptionMenuCallback;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ContractDetailView;

public class ContractDetailsActivity extends BaseActivity
        implements View.OnClickListener, ContractDetailView{
    @BindView(R.id.btn_details_bill) Button btnPayment;
    @BindView(R.id.ic_icon_nav_back_bill_details) RelativeLayout imgBack;
    @BindView(R.id.contract_status) TextView tvContractStatus;
    @BindView(R.id.tv_cus_name) TextView tvCusName;
    @BindView(R.id.tv_contract_id) TextView tvContractId;
    @BindView(R.id.tv_number_phone) TextView tvNumberPhone;
    @BindView(R.id.tv_address) TextView tvAddress;
    @BindView(R.id.tv_total_internet) TextView tvTotalInternet;
    @BindView(R.id.recycler_view_list_devices) RecyclerView recyclerViewListDevice;
    @BindView(R.id.tv_connection_fee) TextView tvConnectionFee;
    @BindView(R.id.tv_deposit_fee) TextView tvDepositFee;
    @BindView(R.id.tv_vat) TextView tvVat;
    @BindView(R.id.tv_type_payment) TextView tvTypePayment;
    @BindView(R.id.tv_total_amount) TextView tvTotalAmount;
    @BindView(R.id.tv_promotion_name) TextView tvPromotionName;
    @BindView(R.id.tv_promotion_name_hidden) TextView tvPromotionHidden;
    @BindView(R.id.tv_show_more_promotion) TextView tvShowMore;
    @BindView(R.id.img_status) ImageView imgStatus;
    @BindView(R.id.layout_option_menu) RelativeLayout loOptionMenu;

    private Dialog dialog;
    private ReqContractDetails obj = new ReqContractDetails();
    private DeviceListCusDetailAdapter adapter;
    private APIService apiService;
    private ContractDetailPresenter presenter;
    //this objReport use for ReceiptDetailActivity
    public static ContractDetails contractDetails;
    private static final int MY_PERMISSIONS_REQUEST = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setEvent();
        getDataIntent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initDeviceList();
        callApi();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_contract_details;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_details_bill:
                startActivity(new Intent(this, ReceiptDetailActivity.class));
                break;
            case R.id.ic_icon_nav_back_bill_details:
                onBackPressed();
                break;
            case R.id.layout_option_menu:
                optionMenu();
                break;
            case R.id.tv_show_more_promotion:
                if(isVisibility(tvPromotionHidden)){
                    tvPromotionHidden.setVisibility(View.GONE);
                    tvShowMore.setText(getResources().getString(R.string.show_more));
                }else{
                    tvPromotionHidden.setVisibility(View.VISIBLE);
                    tvShowMore.setText(getResources().getString(R.string.show_less));
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS_REQUEST){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                presenter.doGetUrlContractPdf(getApplicationContext(), apiService, contractDetails.getObjId());
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getIntent().getBooleanExtra(Utils.CAN_BACK, false)){
            return;
        }
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
    }

    @Override
    public void getContractDetailSuccess(ContractDetails obj) {
        contractDetails = obj;
        loadInfoContractDetailsToView(obj);
    }

    @Override
    public void getUrlDownLoadContractPdf(String url) {
        /*
        * after get url file pdf success
        * call api download file from this url
        * */
        presenter.doDownloadFilePdf(this, apiService, url, contractDetails.getObjId());
    }

    @Override
    public void doDownloadFilePdf(String message) {
        DialogUtils.showMessageDialogVerButton(this, message).show();
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    private void getDataIntent() {
        //this objReport return after register cus info
        if(getIntent().getSerializableExtra(Utils.CONTRACT_INFO_BY_REG)!=null){
            Registration registration = (Registration) getIntent().getSerializableExtra(Utils.CONTRACT_INFO_BY_REG);
            obj.setObjId(String.valueOf(registration.getObjID()));
            obj.setContract(registration.getContract());
        }
        //this objReport return after create contract
        if(getIntent().getSerializableExtra(Utils.CONTRACT_INFO)!=null){
            Contract contract = (Contract)getIntent().getSerializableExtra(Utils.CONTRACT_INFO);
            obj.setObjId(String.valueOf(contract.getObjID()));
            obj.setContract(contract.getContract());
        }
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new ContractDetailsPresenterImpl(this);
        if(obj!=null){
            presenter.doGetContractDetails(this, apiService, obj);
        }
    }

    private void setEvent() {
        btnPayment.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        loOptionMenu.setOnClickListener(this);
        tvShowMore.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    private void loadInfoContractDetailsToView(ContractDetails obj){
        //check status contract to display UI
        if(obj.getPaidStatus() == 1){
            tvContractStatus.setTextColor(getResources().getColor(R.color.correct_green));
            imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_paid));
            tvContractStatus.setText(obj.getRegStatus());
            btnPayment.setVisibility(View.GONE);
        }else{
            tvContractStatus.setTextColor(getResources().getColor(R.color.wrong_pink));
            imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_unpaid));
            tvContractStatus.setText(obj.getRegStatus());
            btnPayment.setVisibility(View.VISIBLE);
        }

        //set value to view
        tvCusName.setText(obj.getFullName());
        tvContractId.setText(obj.getContract());
        tvNumberPhone.setText(obj.getPhone1());
        tvAddress.setText(obj.getAddress());
        tvTotalInternet.setText(String.valueOf(obj.getInternetTotal()));
        tvConnectionFee.setText(String.valueOf(obj.getConnectionFee()));
        tvDepositFee.setText(String.valueOf(obj.getDepositFee()));
        tvVat.setText(String.valueOf(obj.getVAT()) + "%");
        tvTypePayment.setText(obj.getPaymentMethodName());
        tvTotalAmount.setText(String.valueOf(obj.getTotal()));
        //TODO: update here
        tvPromotionName.setText(obj.getPromotionName());
        tvPromotionHidden.setText(obj.getPromotionDescription());
        //notify data list device
        adapter.notifyData((ArrayList<ReqDevice>) obj.getListDevice());
    }

    private void initDeviceList() {
        ArrayList<ReqDevice> list = new ArrayList<>();
        adapter = new DeviceListCusDetailAdapter(this, list);
        recyclerViewListDevice.setAdapter(adapter);
    }

    private boolean askForPermission() {
        return ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(Objects.requireNonNull(this),
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void optionMenu() {
        final Activity activity = this;
        DialogUtils.showOptionMenu(this, new OptionMenuCallback() {
            @Override
            public void itemSelected(int pos) {
                switch (pos){
                    case 0:
                        startActivity(new Intent(getApplicationContext(), DeployAppointmentActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(getApplicationContext(), UpdateTotalOfMoneyActivity.class));
                        break;
                    case 2:
                        //TODO:download file contract(PDF)
                        if(!askForPermission()){
                            //required permissions read and write file
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                            Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST);
                        }else{
                            presenter.doGetUrlContractPdf(getApplicationContext(), apiService, contractDetails.getObjId());
                        }
                        break;
                }
            }
        }).show();
    }
}
