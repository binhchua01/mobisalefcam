package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-14.
 */
public class ReqPotentialCusList {
    private String username;
    private int searchType;
    private String searchContent;
    private int pageNumber;
    private String fromDate;
    private String toDate;
    private int source;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getSearchType() {
        return searchType;
    }

    public void setSearchType(int searchType) {
        this.searchType = searchType;
    }

    public String getSearchContent() {
        return searchContent;
    }

    public void setSearchContent(String searchContent) {
        this.searchContent = searchContent;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public LinkedHashMap<String, String> generateObjGetPotentialCusList(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("UserName", getUsername());
        obj.put("SearchType", String.valueOf(getSearchType()));
        obj.put("SearchContent", getSearchContent());
        obj.put("PageNumber", String.valueOf(getPageNumber()));
        obj.put("FromDate", getFromDate());
        obj.put("ToDate", getToDate());
        obj.put("Source", String.valueOf(getSource()));
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("UserName", getUsername());
            obj.put("SearchType", String.valueOf(getSearchType()));
            obj.put("SearchContent", getSearchContent());
            obj.put("PageNumber", String.valueOf(getPageNumber()));
            obj.put("FromDate", getFromDate());
            obj.put("ToDate", getToDate());
            obj.put("Source", String.valueOf(getSource()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
