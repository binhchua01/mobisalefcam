package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqBookportAuto;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGenerateAddress;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqListOfPoints;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ListOfPoint;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResBookportAuto;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGenerateAddress;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResListOfPoints;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.SurveyPopView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-10.
 */
public class SurveyPopPresenterImpl implements SurveyPopPresenter {
    private SurveyPopView view;

    public SurveyPopPresenterImpl(SurveyPopView view) {
        this.view = view;
    }

    @Override
    public void doGenerateAddress(final Context context, final APIService apiService) {
        //show dialog
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGenerateAddress(context, apiService);
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiException(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGenerateAddress(context, apiService);
        }
    }

    @Override
    public void doGetListOfPoints(final Context context, final APIService apiService,
                                  final String username, final String locationId, final String wardId, final String lat,
                                  final String lng, final String typeOfNetwork, final String bookPortIdentityId) {
        //show dialog
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetListOfPoints(context, apiService, username, locationId,
                            wardId, lat, lng, typeOfNetwork, bookPortIdentityId);
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiException(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetListOfPoints(context, apiService, username, locationId,
                    wardId, lat, lng, typeOfNetwork, bookPortIdentityId);
        }
    }

    @Override
    public void doBookportAuto(final Context context, final APIService apiService, final ReqBookportAuto obj) {
        //show dialog
        view.showProgressDialog();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiBookportAuto(context, apiService, obj);
                }

                @Override
                public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                    view.hiddenProgressDialog();
                    view.callApiException(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiBookportAuto(context, apiService, obj);
        }
    }

    private void callApiBookportAuto(final Context context, final APIService apiService, ReqBookportAuto obj){
        apiService.doBookportAuto(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(obj.objToString()),
                obj.generateObjBookportAuto()
        ).enqueue(new Callback<ResBookportAuto>() {
            @Override
            public void onResponse(@NonNull Call<ResBookportAuto> call, @NonNull Response<ResBookportAuto> response) {
                validateReturnCodeBookportAuto(context, response);
            }

            @Override
            public void onFailure(@NonNull Call<ResBookportAuto> call, @NonNull Throwable t) {
                view.hiddenProgressDialog();
                view.callApiException(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiGetListOfPoints(final Context context, APIService apiService,
                                        String username, String locationId, String wardId, String lat,
                                        String lng, String typeOfNetwork, String bookPortIdentityId){
        //generate objReport to req
        ReqListOfPoints reqListOfPoints = new ReqListOfPoints(
                username,
                locationId,
                wardId,
                lat,
                lng,
                typeOfNetwork,
                bookPortIdentityId
        );
        //do call api
        apiService.doGetListOfPoints(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(reqListOfPoints.objToString()),
                reqListOfPoints.generateObjGetListOfPoints())
                .enqueue(new Callback<ResListOfPoints>() {
            @Override
            public void onResponse(@NonNull Call<ResListOfPoints> call, @NonNull Response<ResListOfPoints> response) {
                validateReturnCodeGetListOfPoints(context, response);
            }

            @Override
            public void onFailure(@NonNull Call<ResListOfPoints> call, @NonNull Throwable t) {
                view.hiddenProgressDialog();
                view.callApiException(context.getResources().getString(R.string.check_internet));
            }
        });
    }

    private void callApiGenerateAddress(final Context context, APIService apiService){
        //check if building not choose -> return value = 0
        int buildingId;
        if(Utils.updateCusInfo.getBuildingId().equals("")){
            buildingId = 0;
        }else {
            buildingId = Integer.parseInt(Utils.updateCusInfo.getBuildingId());
        }

        //generate objReport to res api
        ReqGenerateAddress reqObj = new ReqGenerateAddress();
        reqObj.setLocationId(Integer.parseInt(Utils.updateCusInfo.getLocationId()));
        reqObj.setDistrictId(Integer.parseInt(Utils.updateCusInfo.getDistrictId()));
        reqObj.setWardId(Integer.parseInt(Utils.updateCusInfo.getWardId()));
        reqObj.setStreetId(Integer.parseInt(Utils.updateCusInfo.getStreetId()));
        reqObj.setHomeType(Integer.parseInt(Utils.updateCusInfo.getTypeHouse()));
        reqObj.setBuildingId(buildingId);
        reqObj.setHouseNumber(String.valueOf(Utils.updateCusInfo.getBillToNumber()));

        apiService.doGenerationAddress(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(reqObj.objToString()),
                reqObj.generateObjAddressReq())
                .enqueue(new Callback<ResGenerateAddress>() {
                    @Override
                    public void onResponse(@NonNull Call<ResGenerateAddress> call, @NonNull Response<ResGenerateAddress> response) {
                        validationReturnCodeGenerateAddress(context, response);
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResGenerateAddress> call, @NonNull Throwable t) {
                        view.hiddenProgressDialog();
                        view.callApiException(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validationReturnCodeGenerateAddress(Context context, Response<ResGenerateAddress> response){
        if(response.body()!=null){
            if(response.body().getCode() == Utils.SUCCESS){
                //get string address from objReport return
                view.generateAddressSuccess(response.body().getData().get(0).getResult());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(response.body().getCode() == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiException(response.body().getMessage());
                }
            }
        }else{
            view.callApiException(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }

    private void validateReturnCodeGetListOfPoints(Context context, Response<ResListOfPoints> response){
        if(response.body()!=null){
            if(response.body().getCode() >= Utils.SUCCESS){
                view.getListOfPoints((ArrayList<ListOfPoint>) response.body().getData().get(0).getListOfPoints());
                //save Authorization header
                Utils.saveParamHeaders(context, response.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
                //save BookportIdentityID use for bookport after, when bookport success -> clear BookportIdentityID to default
                SharedPref.put(context, SharedPref.Key.BookportIdentityID, response.body().getData().get(0).getBookportIdentityID());
            }else{
                if(response.body().getCode() == Utils.CREATE_POTENTIAL_CUS){
                    view.bookportAutoFailedCreatePotentialCus();
                }else if(response.body().getCode() == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiException(response.body().getMessage());
                }
            }
        }else {
            view.callApiException(context.getResources().getString(R.string.server_err));
        }

        view.hiddenProgressDialog();
    }

    private void validateReturnCodeBookportAuto(Context context, Response<ResBookportAuto> obj){
        if(obj.body()!=null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                view.bookportAutoSuccess(obj.body().getData().get(0).getBookportAuto());
                //save Authorization header
                Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
                //save BookportIdentityID use for bookport after, when bookport success -> clear BookportIdentityID to default
                SharedPref.put(context, SharedPref.Key.BookportIdentityID, obj.body().getData().get(0).getBookportIdentityID());
            }else{
                if(code == Utils.EXP_TOKEN){
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else if(code == Utils.CREATE_POTENTIAL_CUS){
                    view.bookportAutoFailedCreatePotentialCus();
                } else{
                    view.bookportAutoFailed(obj.body().getMessage());
                }
            }
        }else{
            view.callApiException(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressDialog();
    }
}
