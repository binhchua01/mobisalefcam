package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.database.HomeType;

/**
 * Created by Hau Le on 2018-09-27.
 */
public interface ChooseHomeTypeView {
    void getHomeTypeListSuccess();
    void search(ArrayList<HomeType> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
