package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;

public class AddressSetupActivity extends BaseActivity
        implements View.OnClickListener {

    @BindView(R.id.ic_nav_back_create_customer_info) RelativeLayout imgBack;
    @BindView(R.id.layout_create_customer_info) RelativeLayout layoutWrapper;
    @BindView(R.id.btn_create_customer_info) Button btnCreateCustomerInfo;
    @BindView(R.id.layout_choose_district) RelativeLayout loChooseDistrict;
    @BindView(R.id.layout_choose_city) RelativeLayout loChooseCity;
    @BindView(R.id.layout_choose_ward) RelativeLayout loChooseWard;
    @BindView(R.id.layout_house_type) RelativeLayout loHouseType;
    @BindView(R.id.layout_choose_street) RelativeLayout loChooseStreet;
    @BindView(R.id.layout_building) RelativeLayout loChooseBuilding;
    @BindView(R.id.layout_enter_home_number) RelativeLayout loHomeNumber;
    @BindView(R.id.tv_city) TextView tvCity;
    @BindView(R.id.tv_district) TextView tvDistrict;
    @BindView(R.id.tv_ward) TextView tvWard;
    @BindView(R.id.tv_house_type) TextView tvHouseType;
    @BindView(R.id.tv_street_name) TextView tvStreet;
    @BindView(R.id.tv_building) TextView tvBuilding;
    @BindView(R.id.edt_home_number) EditText edtHomeNumber;

    private Realm realm;
    private Dialog dialog;
    private String[] city;
    private String[] district;
    private String[] ward;
    private String[] street;
    private String[] building;
    private String[] homeType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, layoutWrapper);
        initDB();
        initView();
        //set old data when update address
        setOldDataWhenUpdateAddress();
        //reset view when changed select city
        resetOnClickChangedDataView();
        setEvent();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //hidden dialog
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_nav_back_create_customer_info:
                onBackPressed();
                break;
            case R.id.btn_create_customer_info:
                surveyPop();
                break;
            case R.id.layout_choose_city:
                letChooseCity();
                break;
            case R.id.layout_choose_district:
                letChooseDistrict();
                break;
            case R.id.layout_choose_ward:
                letChooseWard();
                break;
            case R.id.layout_house_type:
                letChooseHomeTypeList();
                break;
            case R.id.layout_building:
                letChooseBuilding();
                break;
            case R.id.layout_choose_street:
                letChooseStreet();
                break;
        }
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_address_setup;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null){
            if(requestCode == Utils.REQUEST_CODE_DISTRICT) {
                district = data.getStringExtra(Utils.DISTRICT).split(":");
                tvDistrict.setText(district[1]);
            }else if(requestCode == Utils.REQUEST_CODE_WARD){
                ward = data.getStringExtra(Utils.WARD).split(":");
                tvWard.setText(ward[1]);
            }else if(requestCode == Utils.REQUEST_CODE_STREET){
                street = data.getStringExtra(Utils.STREET).split(":");
                tvStreet.setText(street[1]);
            }else if(requestCode == Utils.REQUEST_CODE_BUILDING){
                building = data.getStringExtra(Utils.BUILDING).split(":");
                tvBuilding.setText(building[1]);
            }else if(requestCode == Utils.REQUEST_CODE_HOME_TYPE){
                homeType = data.getStringExtra(Utils.HOME_TYPE).split(":");
                tvHouseType.setText(homeType[1]);
                //id = 4 ->building
                if(Integer.parseInt(homeType[0]) == 4){
                    loChooseBuilding.setVisibility(View.VISIBLE);
                }else{
                    loChooseBuilding.setVisibility(View.GONE);
                }
            }
        }
    }

    private void initView() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
    }

    //TODO: declare Realm
    private void initDB() {
        realm = Realm.getDefaultInstance();
    }

    //TODO: set event onclick
    private void setEvent() {
        imgBack.setOnClickListener(this);
        btnCreateCustomerInfo.setOnClickListener(this);
        loChooseDistrict.setOnClickListener(this);
        loChooseCity.setOnClickListener(this);
        loChooseWard.setOnClickListener(this);
        loHouseType.setOnClickListener(this);
        loChooseStreet.setOnClickListener(this);
        loChooseBuilding.setOnClickListener(this);
    }

    //TODO: change to survey pop activity
    private void surveyPop(){
        if(validateDataOnView()){
            //put locationId id to objReport
            Utils.updateCusInfo.setLocationId(city[0]);
            Utils.updateCusInfo.setBillToCity(city[1]);
            //put district to objReport
            Utils.updateCusInfo.setDistrictId(district[0]);
            Utils.updateCusInfo.setBillToDistrict(district[1]);
            //put ward objReport
            Utils.updateCusInfo.setWardId(ward[0]);
            Utils.updateCusInfo.setBillToWard(ward[1]);
            //put street
            Utils.updateCusInfo.setStreetId(street[0]);
            Utils.updateCusInfo.setBillToStreet(street[1]);
            //put building
            if(tvHouseType.getText().toString().equals(getResources().getString(R.string.building))){
                Utils.updateCusInfo.setBuildingId(building[0]);
                Utils.updateCusInfo.setBuildingName(building[1]);
            }else{
                Utils.updateCusInfo.setBuildingId("");
                Utils.updateCusInfo.setBuildingName("");
            }
            //put home type
            Utils.updateCusInfo.setTypeHouse(homeType[0]);
            Utils.updateCusInfo.setTypeHouseName(homeType[1]);
            //put home number
            Utils.updateCusInfo.setBillToNumber(edtHomeNumber.getText().toString());
            //start activity
            Intent intent = new Intent(this, SurveyPopActivity.class);
            startActivity(intent);
            //show dialog
            if(dialog != null && !dialog.isShowing()){
                dialog.show();
            }
        }
    }

    //TODO: show dialog choose city
    private void letChooseCity(){
        DialogUtils.showDialogChooseCity(this, realm,
            new DialogCallbackDataToActivity() {
                @Override
                public void dataCallback(String data) {
                    city = data.split(":");
                    tvCity.setText(city[1]);
                }
            }).show();
    }

    //TODO: change to choose district activity
    private void letChooseDistrict(){
        //check if don't select city before -> show message
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            Intent intent = new Intent(this, ChooseDistrictActivity.class);
            intent.putExtra(Utils.CITY_ID, city[0]);
            startActivityForResult(intent, Utils.REQUEST_CODE_DISTRICT);
        }
    }

    //TODO: change to choose ward activity
    private void letChooseWard(){
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))
                && !tvDistrict.getText().toString().equals(getResources().getString(R.string.ui_text_choose_district))){
            Intent intent = new Intent(this, ChooseWardActivity.class);
            intent.putExtra(Utils.CITY_ID, city[0]);
            intent.putExtra(Utils.DISTRICT_ID, district[0]);
            startActivityForResult(intent, Utils.REQUEST_CODE_WARD);
        }
    }

    //TODO: change to choose building activity
    private void letChooseBuilding(){
        //get building list by locationId
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            Intent intent = new Intent(this, ChooseBuildingActivity.class);
            intent.putExtra(Utils.BUILDING_ID, city[0]);
            startActivityForResult(intent, Utils.REQUEST_CODE_BUILDING);
        }

    }

    //TODO: change to choose home type activity
    private void letChooseHomeTypeList(){
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            Intent intent = new Intent(this, ChooseHomeTypeActivity.class);
            startActivityForResult(intent, Utils.REQUEST_CODE_HOME_TYPE);
        }
    }

    //TODO: change to choose street activity
    private void letChooseStreet(){
        if(!tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province)) &&
                !tvDistrict.getText().toString().equals(getResources().getString(R.string.ui_text_choose_district)) &&
                !tvWard.getText().toString().equals(getResources().getString(R.string.ui_text_choose_ward))){
            Intent intent = new Intent(this, ChooseStreetActivity.class);
            intent.putExtra(Utils.CITY_ID, city[0]);
            intent.putExtra(Utils.DISTRICT_ID, district[0]);
            intent.putExtra(Utils.WARD_ID, ward[0]);
            startActivityForResult(intent, Utils.REQUEST_CODE_STREET);
        }
    }

    //TODO: add text change event
    private void resetOnClickChangedDataView(){
        tvCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvDistrict.setText(getResources().getString(R.string.ui_text_choose_district));
                tvWard.setText(getResources().getString(R.string.ui_text_choose_ward));
                tvHouseType.setText(getResources().getString(R.string.ui_text_choose_home_type));
                tvBuilding.setText(getResources().getString(R.string.choose_building));
                tvStreet.setText(getResources().getString(R.string.ui_text_choose_street));
                //refresh ui when change text
                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_city_province))){
                    loChooseCity.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvCity.setTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        tvDistrict.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvWard.setText(getResources().getString(R.string.ui_text_choose_ward));
                tvHouseType.setText(getResources().getString(R.string.ui_text_choose_home_type));
                tvBuilding.setText(getResources().getString(R.string.choose_building));
                tvStreet.setText(getResources().getString(R.string.ui_text_choose_street));
                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_district))){
                    loChooseDistrict.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                    tvDistrict.setTextColor(getResources().getColor(R.color.s_black));
                }
            }
        });
        tvWard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvHouseType.setText(getResources().getString(R.string.ui_text_choose_home_type));
                tvBuilding.setText(getResources().getString(R.string.choose_building));
                tvStreet.setText(getResources().getString(R.string.ui_text_choose_street));
                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_ward))){
                    tvWard.setTextColor(getResources().getColor(R.color.s_black));
                    loChooseWard.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvHouseType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvBuilding.setText(getResources().getString(R.string.choose_building));
                tvStreet.setText(getResources().getString(R.string.ui_text_choose_street));
                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_home_type))){
                    tvHouseType.setTextColor(getResources().getColor(R.color.s_black));
                    loHouseType.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvBuilding.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvStreet.setText(getResources().getString(R.string.ui_text_choose_street));
                if(!String.valueOf(editable).equals(getResources().getString(R.string.choose_building))){
                    tvBuilding.setTextColor(getResources().getColor(R.color.s_black));
                    loChooseBuilding.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        tvStreet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edtHomeNumber.setText("");
                if(!String.valueOf(editable).equals(getResources().getString(R.string.ui_text_choose_street))){
                    tvStreet.setTextColor(getResources().getColor(R.color.s_black));
                    loChooseStreet.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
        edtHomeNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!String.valueOf(editable).isEmpty()){
                    edtHomeNumber.setHintTextColor(getResources().getColor(R.color.s_black));
                    loHomeNumber.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                }
            }
        });
    }

    //TODO: validate is't empty and high light when data is empty
    private boolean validateDataOnView(){
        //high light background and text
        if(tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            loChooseCity.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvCity.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseCity.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvCity.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvDistrict.getText().toString().equals(getResources().getString(R.string.ui_text_choose_district))){
            loChooseDistrict.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvDistrict.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseDistrict.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvDistrict.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvWard.getText().toString().equals(getResources().getString(R.string.ui_text_choose_ward))){
            loChooseWard.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvWard.setTextColor(getResources().getColor(R.color.wrong_pink));
        }else{
            loChooseWard.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            tvWard.setTextColor(getResources().getColor(R.color.s_black));
        }

        if(tvHouseType.getText().toString().equals(getResources().getString(R.string.ui_text_choose_home_type))){
            tvHouseType.setTextColor(getResources().getColor(R.color.wrong_pink));
            loHouseType.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
        }else{
            tvHouseType.setTextColor(getResources().getColor(R.color.s_black));
            loHouseType.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        }

        if(tvStreet.getText().toString().equals(getResources().getString(R.string.ui_text_choose_street))){
            tvStreet.setTextColor(getResources().getColor(R.color.wrong_pink));
            loChooseStreet.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
        }else {
            tvStreet.setTextColor(getResources().getColor(R.color.s_black));
            loChooseStreet.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        }

        if(edtHomeNumber.getText().toString().isEmpty()){
            edtHomeNumber.setHintTextColor(getResources().getColor(R.color.wrong_pink));
            loHomeNumber.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
        }else{
            edtHomeNumber.setHintTextColor(getResources().getColor(R.color.s_black));
            loHomeNumber.setBackground(getResources().getDrawable(R.drawable.background_spinner));
        }

        if(tvHouseType.getText().toString().equals(getResources().getString(R.string.building))){
            if(tvBuilding.getText().toString().equals(getResources().getString(R.string.choose_building))){
                tvBuilding.setTextColor(getResources().getColor(R.color.wrong_pink));
                loChooseBuilding.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            }else{
                tvBuilding.setTextColor(getResources().getColor(R.color.s_black));
                loChooseBuilding.setBackground(getResources().getDrawable(R.drawable.background_spinner));
            }
        }

        //check to show dialog required
        if(tvCity.getText().toString().equals(getResources().getString(R.string.ui_text_choose_city_province))){
            showDialogRequired(getResources().getString(R.string.mes_city_required)).show();
            return false;
        }else if(tvDistrict.getText().toString().equals(getResources().getString(R.string.ui_text_choose_district))){
            showDialogRequired(getResources().getString(R.string.mes_district_required)).show();
            return false;
        }else if(tvWard.getText().toString().equals(getResources().getString(R.string.ui_text_choose_ward))){
            showDialogRequired(getResources().getString(R.string.mes_ward_required)).show();
            return false;
        }else if(tvHouseType.getText().toString().equals(getResources().getString(R.string.ui_text_choose_home_type))){
            showDialogRequired(getResources().getString(R.string.mes_home_type_required)).show();
            return false;
        }else if(tvHouseType.getText().toString().equals(getResources().getString(R.string.building)) &&
                tvBuilding.getText().toString().equals(getResources().getString(R.string.choose_building))){
            showDialogRequired(getResources().getString(R.string.mes_building_required)).show();
            return false;
        }else if(tvStreet.getText().toString().equals(getResources().getString(R.string.ui_text_choose_street))){
            showDialogRequired(getResources().getString(R.string.mes_street_required)).show();
            return false;
        }else if(edtHomeNumber.getText().toString().isEmpty()){
            showDialogRequired(getResources().getString(R.string.mes_home_number_required)).show();
            return false;
        }
        return true;
    }

    //TODO: show dialog required
    private Dialog showDialogRequired(String str){
        return  DialogUtils.showMessageDialogVerButton(this, str);
    }

    //TODO: set old data when update address, ...
    private void setOldDataWhenUpdateAddress() {
        if(!Utils.updateCusInfo.getLocationId().equals("")){
            tvCity.setText(Utils.updateCusInfo.getBillToCity());
            city = (Utils.updateCusInfo.getLocationId() + ":" + Utils.updateCusInfo.getBillToCity()).split(":");
        }
        if(!Utils.updateCusInfo.getDistrictId().equals("")){
            tvDistrict.setText(Utils.updateCusInfo.getBillToDistrict());
            district = (Utils.updateCusInfo.getDistrictId() + ":" + Utils.updateCusInfo.getBillToDistrict()).split(":");
        }
        if(!Utils.updateCusInfo.getWardId().equals("")){
            tvWard.setText(Utils.updateCusInfo.getBillToWard());
            ward = (Utils.updateCusInfo.getWardId() + ":" + Utils.updateCusInfo.getBillToWard()).split(":");
        }
        if(!Utils.updateCusInfo.getTypeHouseName().equals("")){
            tvHouseType.setText(Utils.updateCusInfo.getTypeHouseName());
            homeType = (Utils.updateCusInfo.getTypeHouse() + ":" + Utils.updateCusInfo.getTypeHouseName()).split(":");
        }
        if(!Utils.updateCusInfo.getBuildingId().equals("")){
            loChooseBuilding.setVisibility(View.VISIBLE);
            tvBuilding.setText(Utils.updateCusInfo.getBuildingName());
            building = (Utils.updateCusInfo.getBuildingId() + ":" + Utils.updateCusInfo.getBuildingName()).split(":");
        }
        if(!Utils.updateCusInfo.getStreetId().equals("")){
            tvStreet.setText(Utils.updateCusInfo.getBillToStreet());
            street = (Utils.updateCusInfo.getStreetId() + ":" + Utils.updateCusInfo.getBillToStreet()).split(":");
        }
        if(!Utils.updateCusInfo.getBillToNumber().equals("")){
            edtHomeNumber.setText(Utils.updateCusInfo.getBillToNumber());
        }
    }
}
