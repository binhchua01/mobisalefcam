package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import butterknife.BindView;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.presenter.ContractModelPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ContractModelPresenter;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ContractModelView;

import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.reqGetRegistrationDetail;

public class ContractModelActivity extends BaseActivity implements
        View.OnClickListener, ContractModelView{
    @BindView(R.id.ic_icon_nav_back_contract_details) RelativeLayout imgBack;
    @BindView(R.id.web_view_model_contract) WebView webViewModelContract;

    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        webViewModelContract.getSettings().setJavaScriptEnabled(true);
        webViewModelContract.setWebViewClient(new WebViewClient());
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
        callApi();
    }

    private void callApi() {
        APIService apiService = ApiUtils.getApiService();
        ContractModelPresenter presenter = new ContractModelPresenterImpl(this);
        presenter.doGetHtmlModelContract(this, apiService, reqGetRegistrationDetail);
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_contract_model;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ic_icon_nav_back_contract_details:
                onBackPressed();
                break;
        }
    }

    @Override
    public void getHtmlModelContractSuccess(String htmlContent) {
        //load webview
        webViewModelContract.loadData(htmlContent, "text/html", "UTF-8");
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showDialogProgress() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenDialogProgress() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }
}
