package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-08-29.
 */
public class ReqRegistrationAll {
    private String searchType, searchContent, dataType;

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getSearchContent() {
        return searchContent;
    }

    public void setSearchContent(String searchContent) {
        this.searchContent = searchContent;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public LinkedHashMap<String, String> generateObjGetRegistrationAll(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("SearchType", getSearchType());
        obj.put("SearchContent", getSearchContent());
        obj.put("DataType", getDataType());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("SearchType", getSearchType());
            obj.put("SearchContent", getSearchContent());
            obj.put("DataType", getDataType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
