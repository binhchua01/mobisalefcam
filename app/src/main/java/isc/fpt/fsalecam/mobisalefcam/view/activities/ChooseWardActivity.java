package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.ChooseWardAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;

import isc.fpt.fsalecam.mobisalefcam.presenter.ChooseWardPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.ChooseWardPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.Ward;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.OnItemClickListener;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseWardView;

/**
 * Created by Hau Le on 2018-08-02.
 */
public class ChooseWardActivity extends BaseActivity
        implements View.OnClickListener, ChooseWardView{
    @BindView(R.id.img_nav_back_choose_ward) RelativeLayout imgBack;
    @BindView(R.id.layout_choose_ward) LinearLayout loParent;
    @BindView(R.id.recycler_view_ward) RecyclerView recyclerView;
    @BindView(R.id.img_ward_refresh) RelativeLayout imgRefresh;
    @BindView(R.id.edt_search_ward) EditText edtSearchWard;

    private ArrayList<Ward> list;
    private ChooseWardAdapter adapter;
    private Dialog dialog;
    private Realm realm;
    private String districtId;
    private String cityId;
    private APIService apiService;
    private ChooseWardPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        realm = Realm.getDefaultInstance();
        setEvent();
        init();
        callApi();
    }

    private void init() {
        list = new ArrayList<>();
        adapter = new ChooseWardAdapter(this, list, new OnItemClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                //callbackDeviceList data to parent activity
                Intent intent = new Intent();
                intent.putExtra(Utils.WARD, list.get(position).getId() + ":" + list.get(position).getName());
                setResult(Utils.REQUEST_CODE_WARD, intent);
                finish();
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new ChooseWardPresenterImpl(this);
        //get data from parent activity
        cityId = getIntent().getStringExtra(Utils.CITY_ID);
        districtId = getIntent().getStringExtra(Utils.DISTRICT_ID);
        //call api
        presenter.doGetWardList(this, apiService, cityId, districtId, realm);
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        imgRefresh.setOnClickListener(this);
        edtSearchWard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                presenter.searchWardList(realm, String.valueOf(editable), cityId, districtId);
            }
        });
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_choose_ward;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_nav_back_choose_ward:
                onBackPressed();
                break;
            case R.id.img_ward_refresh:
                presenter.refreshWardList(this, apiService, cityId, districtId, realm);
                break;
        }
    }

    @Override
    public void getWardListSuccess() {
        list = (ArrayList<Ward>) realm.copyFromRealm(realm.where(Ward.class)
                .equalTo(Utils.DISTRICT_ID, Integer.parseInt(districtId))
                .findAll());
        //notify data
        adapter.notifyData(list);

    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressLoading() {
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressLoading() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    @Override
    public void searchWard(ArrayList<Ward> list) {
        this.list = list;
        adapter.notifyData(list);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
