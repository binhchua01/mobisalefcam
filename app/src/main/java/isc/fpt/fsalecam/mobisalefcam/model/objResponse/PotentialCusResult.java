package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-09-14.
 */
public class PotentialCusResult {
    @SerializedName("ResultID")
    @Expose
    private String resultId;
    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("PotentObjID")
    @Expose
    private String potentialObjId;
    @SerializedName("Managers")
    @Expose
    private String manager;

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPotentialObjId() {
        return potentialObjId;
    }

    public void setPotentialObjId(String potentialObjId) {
        this.potentialObjId = potentialObjId;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }
}
