package isc.fpt.fsalecam.mobisalefcam.api;

import java.util.LinkedHashMap;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResChangePass;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCheckImei;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCheckVersion;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetAbility;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetSubTeam;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetUrlDownloadPdf;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResPotentailCus;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResPotentialCusList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResReportContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdateAppointment;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdateRegistrationImage;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResBookportAuto;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResBookportManual;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResContractDetails;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResCreateContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGenerateAddress;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetDeviceList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetHtmlModelContract;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetPromotionList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetRegistrationAll;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetRegistrationById;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResGetSaleOfInfo;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResListOfPoints;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResLogin;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResNationality;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResRegistrationDetail;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResTotalDevice;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdateCusInfo;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResUpdatePayment;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.TimeZone;
import isc.fpt.fsalecam.mobisalefcam.model.uploadImage.UploadImageRes;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

/**
 * Created by Hau Le on 2018-07-05.
 */

public interface APIService {
    @Multipart
    @POST
    Call<UploadImageRes> doUploadImage(@Url String url,
                                       @Header("Authorization") String tokenAuth,
                                       @Part MultipartBody.Part fileImage,
                                       @Part("LinkId") RequestBody linkId,
                                       @Part("Source") RequestBody source,
                                       @Part("Type") RequestBody type,
                                       @Part("SaleName") RequestBody saleName,
                                       @Part("Token") RequestBody token);

    @GET
    Call<String> generateTokenKong(@Url String url, @Header("Authorization") String authorization);

    @GET
    Call<ResponseBody> downloadContractPdf(@Url String fileUrl, @Header("Authorization") String token);

    @GET
    Call<ResponseBody> downloadApk(@Url String fileUrl, @Header("Authorization") String token);

    //TODO: add more api here
    @POST("user/login")
    Call<ResLogin> doLogin(@Header("CheckSum") String checkSum,
                           @Header("Content-Type") String contentType,
                           @Header("Authorization") String token,
                           @Body LinkedHashMap<String, String> userObject);

    @POST("User/GetVersion")
    Call<ResCheckVersion> doCheckVersion(@Header("CheckSum") String checkSum,
                                         @Header("Content-Type") String contentType,
                                         @Header("Authorization") String token,
                                         @Body LinkedHashMap<String, String> userObject);

    @POST("User/CheckIMEI")
    Call<ResCheckImei> doCheckImei(@Header("Authorization") String authorization,
                                   @Header("Content-Type") String contentType,
                                   @Header("CheckSum") String checkSum,
                                   @Body LinkedHashMap<String, String> obj);

    @POST("User/ChangePassword")
    Call<ResChangePass> doChangePassword(@Header("Authorization") String authorization,
                                         @Header("Content-Type") String contentType,
                                         @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                         @Header("CheckSum") String checkSum,
                                         @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetDistrictList")
    Call<ResParentStructure> doGetDistrictList(@Header("Authorization") String authorization,
                                               @Header("Content-Type") String contentType,
                                               @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                               @Header("CheckSum") String checkSum,
                                               @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetWardList")
    Call<ResParentStructure> doGetWardList(@Header("Authorization") String authorization,
                                           @Header("Content-Type") String contentType,
                                           @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                           @Header("CheckSum") String checkSum,
                                           @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetStreetList")
    Call<ResParentStructure> doGetStreetList(@Header("Authorization") String authorization,
                                             @Header("Content-Type") String contentType,
                                             @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                             @Header("CheckSum") String checkSum,
                                             @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetBuildingList")
    Call<ResParentStructure> doGetBuildingList(@Header("Authorization") String authorization,
                                               @Header("Content-Type") String contentType,
                                               @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                               @Header("CheckSum") String checkSum,
                                               @Body LinkedHashMap<String, String> obj);

    @POST("User/GetInfo")
    Call<ResGetSaleOfInfo> doGetSaleOfInfo(@Header("Authorization") String authorization,
                                           @Header("Content-Type") String contentType,
                                           @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                           @Header("CheckSum") String checkSum,
                                           @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetHomeTypeList")
    Call<ResParentStructure> doGetHomeTypeList(@Header("Authorization") String authorization,
                                               @Header("Content-Type") String contentType,
                                               @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                               @Header("CheckSum") String checkSum,
                                               @Body LinkedHashMap<String, String> obj);

    @POST("Data/GenerationAddress")
    Call<ResGenerateAddress> doGenerationAddress(@Header("Authorization") String authorization,
                                                 @Header("Content-Type") String contentType,
                                                 @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                 @Header("CheckSum") String checkSum,
                                                 @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetCusTypeDetailList")
    Call<ResParentStructure> doGetCusTypeList(@Header("Authorization") String authorization,
                                             @Header("Content-Type") String contentType,
                                             @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                             @Header("CheckSum") String checkSum,
                                             @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetNationalityList")
    Call<ResNationality> doGetNationalityList(@Header("Authorization") String authorization,
                                              @Header("Content-Type") String contentType,
                                              @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                              @Header("CheckSum") String checkSum,
                                              @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetServiceTypeList")
    Call<ResParentStructure> doGetServiceTypeList(@Header("Authorization") String authorization,
                                              @Header("Content-Type") String contentType,
                                              @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                              @Header("CheckSum") String checkSum,
                                              @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetLocalTypeList")
    Call<ResParentStructure> doGetServicePackageList(@Header("Authorization") String authorization,
                                                  @Header("Content-Type") String contentType,
                                                  @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                  @Header("CheckSum") String checkSum,
                                                  @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetPromotionList")
    Call<ResGetPromotionList> doGetPromotionList(@Header("Authorization") String authorization,
                                                 @Header("Content-Type") String contentType,
                                                 @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                 @Header("CheckSum") String checkSum,
                                                 @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetPaymentMethodPerMonthList")
    Call<ResParentStructure> doGetPaymentMethodPerMonthList(@Header("Authorization") String authorization,
                                                            @Header("Content-Type") String contentType,
                                                            @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                            @Header("CheckSum") String checkSum,
                                                            @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetConnectionFeeList")
    Call<ResParentStructure> doGetConnectionFeeList(@Header("Authorization") String authorization,
                                                    @Header("Content-Type") String contentType,
                                                    @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                    @Header("CheckSum") String checkSum,
                                                    @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetDeviceList")
    Call<ResGetDeviceList> doGetDeviceList(@Header("Authorization") String authorization,
                                           @Header("Content-Type") String contentType,
                                           @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                           @Header("CheckSum") String checkSum,
                                           @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetDepositList")
    Call<ResParentStructure> doGetDepositList(@Header("Authorization") String authorization,
                                           @Header("Content-Type") String contentType,
                                           @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                           @Header("CheckSum") String checkSum,
                                           @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetVatList")
    Call<ResParentStructure> doGetVatList(@Header("Authorization") String authorization,
                                              @Header("Content-Type") String contentType,
                                              @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                              @Header("CheckSum") String checkSum,
                                              @Body LinkedHashMap<String, String> obj);

    @POST("Data/GetTotalDevice")
    Call<ResTotalDevice> doCalculatorTotalDevice(@Header("Authorization") String authorization,
                                                 @Header("Content-Type") String contentType,
                                                 @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                 @Header("CheckSum") String checkSum,
                                                 @Body LinkedHashMap<String, Object> obj);

    @POST("Data/GetPaymentMethodList")
    Call<ResParentStructure> doGetPaymentMethodList(@Header("Authorization") String authorization,
                                                 @Header("Content-Type") String contentType,
                                                 @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                 @Header("CheckSum") String checkSum,
                                                 @Body LinkedHashMap<String, String> obj);

    @POST("Bookport/BookPortAuto")
    Call<ResBookportAuto> doBookportAuto(@Header("Authorization") String authorization,
                                         @Header("Content-Type") String contentType,
                                         @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                         @Header("CheckSum") String checkSum,
                                         @Body LinkedHashMap<String, String> obj);

    @POST("Bookport/BookPortManual")
    Call<ResBookportManual> doBookportManual(@Header("Authorization") String authorization,
                                             @Header("Content-Type") String contentType,
                                             @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                             @Header("CheckSum") String checkSum,
                                             @Body LinkedHashMap<String, String> obj);


    @POST("Bookport/ListOfPoints")
    Call<ResListOfPoints> doGetListOfPoints(@Header("Authorization") String authorization,
                                            @Header("Content-Type") String contentType,
                                            @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                            @Header("CheckSum") String checkSum,
                                            @Body LinkedHashMap<String, String> obj);

    @Headers({"Content-type: application/json", "Accept: */*"})
    @POST("Registration/UpdateRegistration")
    Call<ResUpdateCusInfo> doUpdateCusInfo(@Header("Authorization") String authorization,
                                           @Header("Content-Type") String contentType,
                                           @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                           @Header("CheckSum") String checkSum,
                                           @Body LinkedHashMap<String, Object> obj);

    @POST("Registration/GetRegistrationDetail")
    Call<ResRegistrationDetail> doGetRegistrationDetail(@Header("Authorization") String authorization,
                                                        @Header("Content-Type") String contentType,
                                                        @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                        @Header("CheckSum") String checkSum,
                                                        @Body LinkedHashMap<String, String> obj);

    @POST("Registration/GetHtmlObject")
    Call<ResGetHtmlModelContract> doGetModelContract(@Header("Authorization") String authorization,
                                                     @Header("Content-Type") String contentType,
                                                     @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                     @Header("CheckSum") String checkSum,
                                                     @Body LinkedHashMap<String, String> obj);

    @POST("Registration/CreateObject")
    Call<ResCreateContract> doCreateContract(@Header("Authorization") String authorization,
                                             @Header("Content-Type") String contentType,
                                             @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                             @Header("CheckSum") String checkSum,
                                             @Body LinkedHashMap<String, String> obj);

    @POST("Registration/GetRegistrationByID")
    Call<ResGetRegistrationById> doGetRegistrationById(@Header("Authorization") String authorization,
                                                       @Header("Content-Type") String contentType,
                                                       @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                       @Header("CheckSum") String checkSum,
                                                       @Body LinkedHashMap<String, String> obj);

    @POST("Registration/GetRegistrationAll")
    Call<ResGetRegistrationAll> doGetRegistrationAll(@Header("Authorization") String authorization,
                                                     @Header("Content-Type") String contentType,
                                                     @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                     @Header("CheckSum") String checkSum,
                                                     @Body LinkedHashMap<String, String> obj);

    @POST("Registration/GetRegistrationSearchTypeList")
    Call<ResParentStructure> doGetRegistrationTypeList(@Header("Authorization") String authorization,
                                                     @Header("Content-Type") String contentType,
                                                     @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                     @Header("CheckSum") String checkSum,
                                                     @Body LinkedHashMap<String, String> obj);

    @POST("Registration/GetContractDetail")
    Call<ResContractDetails> deGetContractDetails(@Header("Authorization") String authorization,
                                                  @Header("Content-Type") String contentType,
                                                  @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                  @Header("CheckSum") String checkSum,
                                                  @Body LinkedHashMap<String, String> obj);

    @POST("Registration/UpdatePayment")
    Call<ResUpdatePayment> doUpdatePayment(@Header("Authorization") String authorization,
                                           @Header("Content-Type") String contentType,
                                           @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                           @Header("CheckSum") String checkSum,
                                           @Body LinkedHashMap<String, String> obj);

    @POST("Registration/GetRegistrationTotal")
    Call<ResCalNewTotal> doCalNewTotal(@Header("Authorization") String authorization,
                                       @Header("Content-Type") String contentType,
                                       @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                       @Header("CheckSum") String checkSum,
                                       @Body LinkedHashMap<String, Object> obj);

    @POST("Registration/UpdateRegistrationTotal")
    Call<ResUpdateRegistrationTotal> doUpdateRegistrationTotal(@Header("Authorization") String authorization,
                                                               @Header("Content-Type") String contentType,
                                                               @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                               @Header("CheckSum") String checkSum,
                                                               @Body LinkedHashMap<String, Object> obj);

    @POST("Registration/UpdateRegistrationImage")
    Call<ResUpdateRegistrationImage> doUpdateRegistrationImage(@Header("Authorization") String authorization,
                                                               @Header("Content-Type") String contentType,
                                                               @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                               @Header("CheckSum") String checkSum,
                                                               @Body LinkedHashMap<String, String> obj);

    @POST("Registration/GetContractPDF")
    Call<ResGetUrlDownloadPdf> doGetUrlDownloadContractPdf(@Header("Authorization") String authorization,
                                                           @Header("Content-Type") String contentType,
                                                           @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                           @Header("CheckSum") String checkSum,
                                                           @Body LinkedHashMap<String, String> obj);

    @POST("DeployAppointment/GetSubTeamIDOfContract")
    Call<ResGetSubTeam> doGetSubTeamIdOfContract(@Header("Authorization") String authorization,
                                                 @Header("Content-Type") String contentType,
                                                 @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                 @Header("CheckSum") String checkSum,
                                                 @Body LinkedHashMap<String, String> obj);

    @POST("DeployAppointment/GetAbility4Days")
    Call<ResGetAbility> doGetAbility(@Header("Authorization") String authorization,
                                     @Header("Content-Type") String contentType,
                                     @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                     @Header("CheckSum") String checkSum,
                                     @Body LinkedHashMap<String, String> obj);

    @POST("DeployAppointment/UpdateDeployAppointment")
    Call<ResUpdateAppointment> doUpdateAppointment(@Header("Authorization") String authorization,
                                                   @Header("Content-Type") String contentType,
                                                   @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                   @Header("CheckSum") String checkSum,
                                                   @Body LinkedHashMap<String, String> obj);

    @POST("PotentialCustomer/create")
    Call<ResPotentailCus> doCreatePotentialCus(@Header("Authorization") String authorization,
                                               @Header("Content-Type") String contentType,
                                               @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                               @Header("CheckSum") String checkSum,
                                               @Body LinkedHashMap<String, String> obj);

    @POST("PotentialCustomer/getlist")
    Call<ResPotentialCusList> doGetPotentialCusList(@Header("Authorization") String authorization,
                                                    @Header("Content-Type") String contentType,
                                                    @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                    @Header("CheckSum") String checkSum,
                                                    @Body LinkedHashMap<String, String> obj);

    @POST("PotentialCustomer/GetPotentialCustomerSearchTypeList")
    Call<ResParentStructure> doGetPotentialCusTypeList(@Header("Authorization") String authorization,
                                                    @Header("Content-Type") String contentType,
                                                    @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                    @Header("CheckSum") String checkSum,
                                                    @Body LinkedHashMap<String, String> obj);

    @POST("Report/ReportPTTB")
    Call<ResReportContract> doGetReportContract(@Header("Authorization") String authorization,
                                                @Header("Content-Type") String contentType,
                                                @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                                                @Header("CheckSum") String checkSum,
                                                @Body LinkedHashMap<String, String> obj);

    @POST("DeployAppointment/GetTimezone")
    Call<TimeZone> doGetTimeZone(@Header("Authorization") String authorization,
                               @Header("Content-Type") String contentType,
                               @Header("FTEL_MOBISALECAM_HEADER") String headersAuth,
                               @Header("CheckSum") String checkSum,
                               @Body LinkedHashMap<String, String> obj);
}
