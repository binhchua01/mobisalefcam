package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Contract;

/**
 * Created by Hau Le on 2018-08-28.
 */
public interface CreateContractView {
    void createContractSuccess(Contract contract);
    void uploadSignatureSuccess(int imageId);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
