package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqRegistrationAll;

/**
 * Created by Hau Le on 2018-08-30.
 */
public interface UnPaidFragPresenter {
    void doGetCusUnpaidList(Context context, APIService apiService, ReqRegistrationAll obj);
}
