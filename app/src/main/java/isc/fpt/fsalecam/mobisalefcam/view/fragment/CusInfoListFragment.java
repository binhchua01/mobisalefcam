package isc.fpt.fsalecam.mobisalefcam.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Objects;
import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqRegistrationAll;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.Registration;
import isc.fpt.fsalecam.mobisalefcam.presenter.CusInfoListPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.CusInfoListPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.RegistrationSearchType;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.CusInfoListView;

/**
 * Created by Hau Le on 2018-07-10.
 */
public class CusInfoListFragment extends BaseFragment
        implements View.OnClickListener, CusInfoListView{
    @BindView(R.id.text_view_from_ci) TextView tvFromCI;
    @BindView(R.id.text_view_un_paid) TextView tvUnPaid;
    @BindView(R.id.text_view_paid) TextView tvPaid;
    @BindView(R.id.layout_contract_wrapper) LinearLayout layoutWrapper;
    @BindDrawable(R.drawable.background_text_view_selected) Drawable tvBackgroundSelected;
    @BindDrawable(R.drawable.background_text_view_un_selected) Drawable tvBackgroundUnSelected;
    @BindColor(R.color.colorPrimary) int tvColorSelected;
    @BindColor(R.color.text_employee) int tvColorUnSelected;
    @BindView(R.id.img_change_key_search) RelativeLayout loChangeKeySearch;
    @BindView(R.id.edt_search) EditText edtSearch;
    @BindView(R.id.ic_button_search) RelativeLayout loSearch;
    @BindView(R.id.content_frag_cus_info_list) FrameLayout fragContainer;

    private TextView tvTitle;
    private Dialog dialog;
    private APIService apiService;
    private CusInfoListPresenter presenter;
    //Search Type: 1 - Contract ID, 2 - Cus Name, 3 - Cus Phone
    //default is Cus name
    public static int searchType = 2;
    public static ReqRegistrationAll obj = new ReqRegistrationAll();
    //this's can use for dataType;
    private int screenIsActive = 1;
    //fragment tag
    private final String CUS_INO = "cus_info";
    private final String UNPAID = "un_paid";
    private final String PAID = "paid";
    private Realm realm;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        tvTitle = Objects.requireNonNull(getActivity()).findViewById(R.id.tv_label_activity);
        //set fragment default
        assert getFragmentManager() != null;
        getFragmentManager().beginTransaction()
                .add(R.id.content_frag_cus_info_list, new CusWaitConfirmFragment(), CUS_INO).commit();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hiddenKeySoft(layoutWrapper, getActivity());
        realm = Realm.getDefaultInstance();
        initObjCallListCus();
        setEvent();
        initParamToCallApi();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    private void setEvent() {
        tvFromCI.setOnClickListener(this);
        tvPaid.setOnClickListener(this);
        tvUnPaid.setOnClickListener(this);
        loChangeKeySearch.setOnClickListener(this);
        loSearch.setOnClickListener(this);
        //add text changed event
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //TODO:set search content
                obj.setSearchContent(String.valueOf(editable));
            }
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_cus_info_list;
    }

    private void changedFragment(Fragment fragment, String tag){
        //create fragment manager
        assert getFragmentManager() != null;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frag_cus_info_list, fragment, tag);
        fragmentTransaction.commit();
    }

    private void controlDisplayUI(int po){
        switch (po){
            case 1:
                tvFromCI.setBackground(tvBackgroundSelected);
                tvFromCI.setTextColor(tvColorSelected);

                tvUnPaid.setBackground(tvBackgroundUnSelected);
                tvUnPaid.setTextColor(tvColorUnSelected);

                tvPaid.setBackground(tvBackgroundUnSelected);
                tvPaid.setTextColor(tvColorUnSelected);
                //set title
                tvTitle.setText(getResources().getString(R.string.cus_info_list));
                //set screen is active
                screenIsActive = 1;
                //put DataTYpe
                obj.setDataType(String.valueOf(screenIsActive));
                break;
            case 2:
                tvFromCI.setBackground(tvBackgroundUnSelected);
                tvFromCI.setTextColor(tvColorUnSelected);

                tvUnPaid.setBackground(tvBackgroundSelected);
                tvUnPaid.setTextColor(tvColorSelected);

                tvPaid.setBackground(tvBackgroundUnSelected);
                tvPaid.setTextColor(tvColorUnSelected);

                tvTitle.setText(getResources().getString(R.string.contracts_unpaid));
                screenIsActive = 2;
                obj.setDataType(String.valueOf(screenIsActive));
                break;
            case 3:
                tvFromCI.setBackground(tvBackgroundUnSelected);
                tvFromCI.setTextColor(tvColorUnSelected);

                tvUnPaid.setBackground(tvBackgroundUnSelected);
                tvUnPaid.setTextColor(tvColorUnSelected);

                tvPaid.setBackground(tvBackgroundSelected);
                tvPaid.setTextColor(tvColorSelected);

                tvTitle.setText(getResources().getString(R.string.contracts_had_paid));
                screenIsActive = 3;
                obj.setDataType(String.valueOf(screenIsActive));
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.text_view_from_ci:
                controlDisplayUI(1);
                changedFragment(new CusWaitConfirmFragment(), CUS_INO);
                break;
            case R.id.text_view_un_paid:
                controlDisplayUI(2);
                changedFragment(new UnPaidFragment(), UNPAID);
                break;
            case R.id.text_view_paid:
                controlDisplayUI(3);
                changedFragment(new PaidFragment(), PAID);
                break;
            case R.id.img_change_key_search:
                presenter.doGetRegistrationSearchType(getContext(), apiService, realm);
                break;
            case R.id.ic_button_search:
                presenter.doSearchRegistration(getActivity(), apiService, obj);
                break;
        }
    }

    @Override
    public void getRegistrationSearchTypeListSuccess(ArrayList<RegistrationSearchType> list) {
        DialogUtils.showDialogRegitsrationSearchType(getContext(),
                getResources().getString(R.string.choose_search_type),
                list, new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                int type = Integer.parseInt(data.split(":")[0]);
                searchType = type;
                obj.setSearchType(String.valueOf(type));
                //Search Type: 1 - Contract ID, 2 - Cus Name, 3 - Cus Phone
                //set hint for edit text when change search type
                switch (type){
                    case 1:
                        edtSearch.setHint(getResources().getString(R.string.ui_text_enter_contract_id));
                        break;
                    case 2:
                        edtSearch.setHint(getResources().getString(R.string.ui_text_cus_info_list_enter_customer_name));
                        break;
                    case 3:
                        edtSearch.setHint(getResources().getString(R.string.ui_text_cus_info_list_enter_customer_phone));
                        break;
                }
            }
        }).show();
    }

    @Override
    public void getSearchRegistrationSuccess(ArrayList<Registration> list) {
        FragmentManager frag = getFragmentManager();
        assert frag != null;
        switch (screenIsActive){
            case 1:
                CusWaitConfirmFragment cusInfo = (CusWaitConfirmFragment) frag.findFragmentByTag(CUS_INO);
                assert cusInfo != null;
                cusInfo.notifyData(list);
                break;
            case 2:
                UnPaidFragment unPaid = (UnPaidFragment) frag.findFragmentByTag(UNPAID);
                assert unPaid != null;
                unPaid.notifyData(list);
                break;
            case 3:
                PaidFragment paid = (PaidFragment) frag.findFragmentByTag(PAID);
                assert paid != null;
                paid.notifyData(list);
                break;
            default:
                break;
        }
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(getContext(), mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(getContext(), mes).show();
    }

    @Override
    public void showProgressDialog() {
        hiddenProgressDialog();
        dialog = DialogUtils.showLoadingDialog(getContext(),
                Objects.requireNonNull(getContext()).getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    private void initParamToCallApi(){
        apiService = ApiUtils.getApiService();
        presenter = new CusInfoListPresenterImpl(this);
    }

    private void initObjCallListCus() {
        obj.setSearchType(String.valueOf(searchType));
        obj.setSearchContent(edtSearch.getText().toString());
    }
}
