package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;

import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateCusInfo;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationTotal;

public interface TotalServiceFeePresenter {
    void doGetDepositList(Context context, APIService apiService, String locationId, Realm realm);
    void doGetVatList(Context context, APIService apiService, Realm realm);
    void doUpdateCusInfo(Context context, APIService apiService, ReqUpdateCusInfo obj);
    void doCalculatorTotalAmount(Context context, APIService apiService, ReqUpdateRegistrationTotal obj);
}
