package isc.fpt.fsalecam.mobisalefcam.model.objResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hau Le on 2018-09-13.
 */
public class SubTeam {
    @SerializedName("ResultDepID")
    @Expose
    private String resultDepID;
    @SerializedName("ResultCode")
    @Expose
    private Integer resultCode;
    @SerializedName("ResultSubID")
    @Expose
    private Integer resultSubID;
    @SerializedName("Ability")
    @Expose
    private Integer ability;
    @SerializedName("AbilityLeft")
    @Expose
    private Integer abilityLeft;
    @SerializedName("StatusBK")
    @Expose
    private Integer statusBK;

    public String getResultDepID() {
        return resultDepID;
    }

    public void setResultDepID(String resultDepID) {
        this.resultDepID = resultDepID;
    }

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public Integer getResultSubID() {
        return resultSubID;
    }

    public void setResultSubID(Integer resultSubID) {
        this.resultSubID = resultSubID;
    }

    public Integer getAbility() {
        return ability;
    }

    public void setAbility(Integer ability) {
        this.ability = ability;
    }

    public Integer getAbilityLeft() {
        return abilityLeft;
    }

    public void setAbilityLeft(Integer abilityLeft) {
        this.abilityLeft = abilityLeft;
    }

    public Integer getStatusBK() {
        return statusBK;
    }

    public void setStatusBK(Integer statusBK) {
        this.statusBK = statusBK;
    }
}
