package isc.fpt.fsalecam.mobisalefcam.view.interfaceView;

import java.util.ArrayList;
import isc.fpt.fsalecam.mobisalefcam.database.DeviceList;

/**
 * Created by Hau Le on 2018-08-22.
 */
public interface DeviceListView {
    void getDeviceListSuccess(ArrayList<DeviceList> list);
    void callApiFailed(String mes);
    void expiredToken(String mes);
    void showProgressDialog();
    void hiddenProgressDialog();
}
