package isc.fpt.fsalecam.mobisalefcam.model.objRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;

/**
 * Created by Hau Le on 2018-09-13.
 */
public class ReqGetAbility {
    private int partnerId;
    private int subId;
    private String appointmentDate;

    public int getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public LinkedHashMap<String, String> generateObjGetAbility(){
        //generate object to send
        LinkedHashMap<String, String> obj = new LinkedHashMap<>();
        obj.put("PartnerID", String.valueOf(getPartnerId()));
        obj.put("SubID", String.valueOf(getSubId()));
        obj.put("AppointmentDate", getAppointmentDate());
        return obj;
    }

    public String objToString(){
        JSONObject obj = new JSONObject();
        try {
            obj.put("PartnerID", String.valueOf(getPartnerId()));
            obj.put("SubID", String.valueOf(getSubId()));
            obj.put("AppointmentDate", getAppointmentDate());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(obj);
    }
}
