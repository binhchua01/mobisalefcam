package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import java.util.ArrayList;
import io.realm.Case;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetWardList;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ResParentStructure;
import isc.fpt.fsalecam.mobisalefcam.database.Ward;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.ChooseWardView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.TOKEN_AUTH_KONG;
import static isc.fpt.fsalecam.mobisalefcam.utils.Utils.URL_GENERATE_TOKEN_KONG;

/**
 * Created by Hau Le on 2018-08-02.
 */
public class ChooseWardPresenterImpl implements ChooseWardPresenter {
    private ChooseWardView view;

    public ChooseWardPresenterImpl(ChooseWardView chooseWardView) {
        this.view = chooseWardView;
    }

    @Override
    public void doGetWardList(final Context context, final APIService apiService,
                              final String cityId, final String districtId,
                              final Realm realm) {
        if(realm.where(Ward.class).findAll().size() == 0 ||
                realm.where(Ward.class)
                .equalTo(Utils.CITY_ID, Integer.parseInt(cityId))
                .equalTo(Utils.DISTRICT_ID, Integer.parseInt(districtId))
                .findAll().size() == 0){
            //show dialog
            view.showProgressLoading();
            long timerRefresh = Long.parseLong(SharedPref.get(context,
                    SharedPref.Key.TIMER_TO_REFRESH, ""));
            //check if timer to refresh > current time -> refresh token
            if(System.currentTimeMillis() >= timerRefresh){
                apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        //generate token success
                        SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                        callApiGetWardList(context, apiService, cityId, districtId, realm);
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
            }else{
                callApiGetWardList(context, apiService, cityId, districtId, realm);
            }
        }else{
            view.getWardListSuccess();
        }
    }

    @Override
    public void refreshWardList(final Context context, final APIService apiService,
                                final String cityId, final String districtId, final Realm realm) {
        //show dialog
        view.showProgressLoading();
        long timerRefresh = Long.parseLong(SharedPref.get(context,
                SharedPref.Key.TIMER_TO_REFRESH, ""));
        //check if timer to refresh > current time -> refresh token
        if(System.currentTimeMillis() >= timerRefresh){
            apiService.generateTokenKong(URL_GENERATE_TOKEN_KONG, TOKEN_AUTH_KONG).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //generate token success
                    SharedPref.put(context, SharedPref.Key.TOKEN_KONG, response.body());
                    callApiGetWardList(context, apiService, cityId, districtId, realm);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    view.hiddenProgressLoading();
                    view.callApiFailed(context.getResources().getString(R.string.check_internet));
                }
            });
        }else{
            callApiGetWardList(context, apiService, cityId, districtId, realm);
        }
    }

    @Override
    public void searchWardList(Realm realm, String keyQuery, String cityId, String districtId) {
        ArrayList<Ward> list = (ArrayList<Ward>) realm.copyFromRealm(realm.where(Ward.class)
                .equalTo(Utils.CITY_ID, Integer.parseInt(cityId))
                .equalTo(Utils.DISTRICT_ID, Integer.parseInt(districtId))
                .like(Utils.WARD_NAME, "*" + keyQuery + "*", Case.SENSITIVE)
                .findAll());
        view.searchWard(list);
    }

    private void callApiGetWardList(final Context context, final APIService apiService,
                                    final String cityId, final String districtId, final Realm realm){
        //put data to object
        final ReqGetWardList location = new ReqGetWardList(cityId, districtId);
        //call api get district list...
        apiService.doGetWardList(
                Utils.TOKEN_TYPE + SharedPref.get(context, SharedPref.Key.TOKEN_KONG, ""),
                Utils.CONTENT_TYPE,
                SharedPref.get(context, SharedPref.Key.FTEL_MOBISALECAM_HEADER, ""),
                Utils.checkSum(location.objToString()),
                location.generateObjGetWardList())
                .enqueue(new Callback<ResParentStructure>() {
                    @Override
                    public void onResponse(Call<ResParentStructure> call, Response<ResParentStructure> response) {
                        //success
                        validateReturnCode(context, response, realm,
                                Integer.parseInt(cityId), Integer.parseInt(districtId));
                    }

                    @Override
                    public void onFailure(Call<ResParentStructure> call, Throwable t) {
                        view.hiddenProgressLoading();
                        view.callApiFailed(context.getResources().getString(R.string.check_internet));
                    }
                });
    }

    private void validateReturnCode(Context context, Response<ResParentStructure> obj,
                                    Realm realm, int cityId, int districtId){
        if(obj.body()!=null){
            int code = obj.body().getCode();
            if(code == Utils.SUCCESS){
                //save ward list to local
                saveWardListToLocal((ArrayList<ItemOfListRes>) obj.body().getData(),
                        realm, cityId, districtId);
                //save Authorization header
                Utils.saveParamHeaders(context, obj.headers().get(Utils.FTEL_MOBISALECAM_HEADER));
            }else{
                if(code == Utils.EXP_TOKEN){
                    //login again
                    view.expiredToken(context.getResources().getString(R.string.mes_login_again));
                }else{
                    view.callApiFailed(obj.body().getMessage());
                }
            }
        }else{
            view.callApiFailed(context.getResources().getString(R.string.server_err));
        }
        view.hiddenProgressLoading();
    }

    private void saveWardListToLocal(final ArrayList<ItemOfListRes> list, Realm realm,
                                     final int cityId, final int districtId) {
        realm.beginTransaction();
        realm.where(Ward.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        if(list.size()!=0){
            for (int i=0; i<list.size(); i++){
                realm.beginTransaction();
                Ward ward = realm.createObject(Ward.class, list.get(i).getId());
                ward.setName(list.get(i).getName());
                ward.setCityID(cityId);
                ward.setDistrictID(districtId);
                realm.commitTransaction();
            }
            view.getWardListSuccess();
        }else{
            view.getWardListSuccess();
        }
    }
}
