package isc.fpt.fsalecam.mobisalefcam.utils;

import android.view.View;

/**
 * Created by Hau Le on 2018-07-17.
 */
public interface OnItemClickListener {
    void itemClickListener(View view, int position);
}
