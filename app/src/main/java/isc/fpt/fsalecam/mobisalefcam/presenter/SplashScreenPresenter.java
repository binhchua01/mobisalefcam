package isc.fpt.fsalecam.mobisalefcam.presenter;

import android.content.Context;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCheckImei;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqCheckVersion;

/**
 * Created by Hau Le on 2018-09-18.
 */
public interface SplashScreenPresenter {
    void doCheckImei(Context context, APIService apiService, ReqCheckImei obj);
    void doCheckCurrentVersion(Context context, APIService apiService, ReqCheckVersion obj);
    void readImeiDevice(Context context);
    void doDownloadNewVersion(Context context, APIService apiService, String fileUrl);
}
