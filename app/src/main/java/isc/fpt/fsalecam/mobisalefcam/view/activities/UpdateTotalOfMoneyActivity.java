package isc.fpt.fsalecam.mobisalefcam.view.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;
import butterknife.BindView;
import io.realm.Realm;
import isc.fpt.fsalecam.mobisalefcam.R;
import isc.fpt.fsalecam.mobisalefcam.adapter.DeviceListSelectedAdapter;
import isc.fpt.fsalecam.mobisalefcam.api.APIService;
import isc.fpt.fsalecam.mobisalefcam.api.ApiUtils;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqDevice;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqGetRegistrationDetail;
import isc.fpt.fsalecam.mobisalefcam.model.objRequest.ReqUpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.CalNewTotal;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.ItemOfListRes;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.RegistrationById;
import isc.fpt.fsalecam.mobisalefcam.model.objResponse.UpdateRegistrationTotal;
import isc.fpt.fsalecam.mobisalefcam.presenter.UpdateTotalOfMoneyPresenterImpl;
import isc.fpt.fsalecam.mobisalefcam.presenter.UpdateTotalOfMoneyPresenter;
import isc.fpt.fsalecam.mobisalefcam.database.ConnectionFee;
import isc.fpt.fsalecam.mobisalefcam.database.DepositFee;
import isc.fpt.fsalecam.mobisalefcam.database.DeviceList;
import isc.fpt.fsalecam.mobisalefcam.database.Vat;
import isc.fpt.fsalecam.mobisalefcam.utils.ContinueRegisterService;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogCallbackDataToActivity;
import isc.fpt.fsalecam.mobisalefcam.utils.DialogUtils;
import isc.fpt.fsalecam.mobisalefcam.utils.ResizeDialogPercent;
import isc.fpt.fsalecam.mobisalefcam.utils.SelectServiceTypeCallback;
import isc.fpt.fsalecam.mobisalefcam.utils.SharedPref;
import isc.fpt.fsalecam.mobisalefcam.utils.Utils;
import isc.fpt.fsalecam.mobisalefcam.view.interfaceView.UpdateTotalOfMoneyView;
import static isc.fpt.fsalecam.mobisalefcam.view.activities.ContractDetailsActivity.contractDetails;

public class UpdateTotalOfMoneyActivity extends BaseActivity
        implements View.OnClickListener, UpdateTotalOfMoneyView {
    @BindView(R.id.ic_icon_nav_back_update_total_of_money) RelativeLayout imgBack;
    @BindView(R.id.layout_update_total_money_parent) LinearLayout loParent;
    @BindView(R.id.layout_choose_service_type) RelativeLayout loChooseServiceType;
    @BindView(R.id.tv_service_type) TextView tvServiceType;
    @BindView(R.id.layout_device) LinearLayout loDevice;
    @BindView(R.id.layout_choose_package_service) RelativeLayout loPackageService;
    @BindView(R.id.tv_package_service) TextView tvPackageService;
    @BindView(R.id.tv_message_choose_promotion) TextView tvLabelPromotion;
    @BindView(R.id.layout_promotion_list) RelativeLayout loPromotion;
    @BindView(R.id.tv_promotion_name) TextView tvPromotionName;
    @BindView(R.id.tv_promotion_name_hidden) TextView tvPromotionHidden;
    @BindView(R.id.tv_show_more_promotion) TextView tvShowMore;
    @BindView(R.id.layout_connection_fee) RelativeLayout loConnectionFee;
    @BindView(R.id.tv_connection_fee) TextView tvConnectionFee;
    @BindView(R.id.tv_vat) TextView tvVat;
    @BindView(R.id.tv_deposit_fee) TextView tvDepositFee;
    @BindView(R.id.layout_vat) RelativeLayout loVat;
    @BindView(R.id.layout_deposit_fee) RelativeLayout loDepositFee;
    @BindView(R.id.layout_choose_devices) RelativeLayout loChooseDevice;
    @BindView(R.id.recycler_view_device_list_update_total) RecyclerView recyclerViewListDevice;
    @BindView(R.id.tv_new_total) TextView tvNewTotal;
    @BindView(R.id.tv_old_total) TextView tvOldTotal;
    @BindView(R.id.tv_diff_money) TextView tvDiffMoney;
    @BindView(R.id.btn_cal_new_total) Button btnCalNewTotal;
    @BindView(R.id.btn_update_registration_total) Button btnUpdateRegistration;
    @BindView(R.id.layout_header) PercentRelativeLayout headerDeviceList;

    private Dialog dialog;
    private Dialog dialogChooseTypeService;
    private UpdateTotalOfMoneyPresenter presenter;
    private APIService apiService;
    private ArrayList<ReqDevice> listReqDevice;
    private ArrayList<DeviceList> listDeviceSelected;
    private DeviceListSelectedAdapter adapter;
    private Realm realm;
    //this is old list, use for set old data to view
    private ArrayList<ItemOfListRes> listServiceType = new ArrayList<>();
    private ReqUpdateRegistrationTotal reqUpdate = new ReqUpdateRegistrationTotal();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hiddenKeySoft(this, loParent);
        realm = Realm.getDefaultInstance();
        initView();
        callApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEvent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected int getlayoutResoureId() {
        return R.layout.activity_update_total_of_money;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.ic_icon_nav_back_update_total_of_money:
                onBackPressed();
                break;
            case R.id.layout_choose_service_type:
                if(dialogChooseTypeService!=null && !dialogChooseTypeService.isShowing()){
                    dialogChooseTypeService.show();
                }
                break;
            case R.id.layout_choose_package_service:
                intent = new Intent(this, PackageServiceActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_PACKAGE_SERVICE);
                break;
            case R.id.layout_promotion_list:
                if(!String.valueOf(reqUpdate.getLocalType()).equals("")){
                    intent = new Intent(this, PromotionListActivity.class);
                    startActivityForResult(intent, Utils.REQUEST_CODE_PROMOTION);
                }else{
                    showToast(this, getResources().getString(R.string.select_service_package));
                }
                break;
            case R.id.layout_connection_fee:
                presenter.doGetConnectionFeeList(this, apiService, String.valueOf(reqUpdate.getLocationId()), realm);
                break;
            case R.id.layout_vat:
                presenter.doGetVatList(this, apiService, realm);
                break;
            case R.id.layout_deposit_fee:
                presenter.doGetDepositList(this, apiService, String.valueOf(reqUpdate.getLocationId()), realm);
                break;
            case R.id.layout_choose_devices:
                if(!Utils.updateCusInfo.getMonthOfPrepaid().equals("")){
                    intent = new Intent(this, EquipmentActivity.class);
                    if(listDeviceSelected.size()!=0){
                        intent.putExtra(Utils.DEVICE_LIST_SELECTED, listDeviceSelected);
                    }
                    startActivityForResult(intent, Utils.REQUEST_CODE_DEVICE);
                }else{
                    showToast(this, getResources().getString(R.string.select_promotion_code));
                }
                break;
            case R.id.btn_cal_new_total:
                if(checkValidateIsOk()){
                    //TODO: call API cal new total
                    presenter.doGetCalNewTotal(this, apiService, reqUpdate);
                }
                break;
            case R.id.btn_update_registration_total:
                if(checkValidateIsOk()){
                    //TODO: call API update registration total
                    presenter.doUpdateRegistrationTotal(this, apiService, reqUpdate);
                }
                break;
            case R.id.tv_show_more_promotion:
                if(isVisibility(tvPromotionHidden)){
                    tvPromotionHidden.setVisibility(View.GONE);
                    tvShowMore.setText(getResources().getString(R.string.show_more));
                }else{
                    tvPromotionHidden.setVisibility(View.VISIBLE);
                    tvShowMore.setText(getResources().getString(R.string.show_less));
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null){
            if(requestCode == Utils.REQUEST_CODE_PACKAGE_SERVICE) {
                String[] ps = data.getStringExtra(Utils.PACKAGE_SERVICE).split(":");
                reqUpdate.setLocalType(Integer.parseInt(ps[0]));
                Utils.updateCusInfo.setLocalType(ps[0]);
                tvPackageService.setText(ps[1]);
                //This's here, when change Service package, we'll reset promotion code, user will choose again
                tvPromotionName.setText("");
                tvPromotionHidden.setVisibility(View.GONE);
                //reset list device when reselect service type
                listReqDevice.clear();
                listDeviceSelected.clear();
                if(listReqDevice.size() == 0){
                    headerDeviceList.setVisibility(View.GONE);
                }else{
                    headerDeviceList.setVisibility(View.VISIBLE);
                }
                //put device list to objReport
                reqUpdate.setList(listReqDevice);
                adapter.notifyData(listReqDevice);
            }else if(requestCode == Utils.REQUEST_CODE_PROMOTION){
                String[] pro = data.getStringExtra(Utils.PROMOTION).split(":");
                reqUpdate.setPromotionId(Integer.parseInt(pro[0]));
                reqUpdate.setMonthOfPrepaid(Integer.parseInt(pro[2]));
                reqUpdate.setInternetTotal(Double.parseDouble(pro[3]));
                Utils.updateCusInfo.setPromotionId(pro[0]);
                tvPromotionName.setText(pro[1]);
                tvPromotionHidden.setText(pro[4]);
                tvPromotionHidden.setVisibility(View.GONE);
                //reset device list
                listReqDevice.clear();
                listDeviceSelected.clear();
                if(listReqDevice.size() == 0){
                    headerDeviceList.setVisibility(View.GONE);
                }else{
                    headerDeviceList.setVisibility(View.VISIBLE);
                }
                //put device list to objReport
                reqUpdate.setList(listReqDevice);
                adapter.notifyData(listReqDevice);
            }else if(requestCode == Utils.REQUEST_CODE_DEVICE){
                listDeviceSelected = (ArrayList<DeviceList>) data.getSerializableExtra(Utils.DEVICE_LIST_SELECTED);
                listReqDevice.clear();
                for(int i = 0; i < listDeviceSelected.size(); i++){
                    listReqDevice.add(new ReqDevice(
                            listDeviceSelected.get(i).getValue(),
                            listDeviceSelected.get(i).getName(),
                            "1",
                            listDeviceSelected.get(i).getChangeNumber(),
                            Double.parseDouble(String.valueOf(listDeviceSelected.get(i).getPrice())),
                            0.0
                    ));
                }
                if(listReqDevice.size() == 0){
                    headerDeviceList.setVisibility(View.GONE);
                }else{
                    headerDeviceList.setVisibility(View.VISIBLE);
                }
                adapter.notifyData(listReqDevice);
                //put device list to objReport
                reqUpdate.setList(listReqDevice);
            }
        }
    }

    @Override
    public void getServiceTypeListSuccess(ArrayList<ItemOfListRes> list) {
        //TODO: modify the same fragment step 2
        dialogChooseTypeService = DialogUtils.showDialogChooseServiceType(this, list, listServiceType,
            new SelectServiceTypeCallback() {
                @Override
                public void serviceTypeList(ArrayList<String> list) {
                    updateUiWhenSelectService(list);
                }
            });
    }

    @Override
    public void getConnectionFeeListSuccess(ArrayList<ConnectionFee> list) {
        DialogUtils.showDialogChooseConnectionFee(this,
                list,
                new DialogCallbackDataToActivity() {
                    @Override
                    public void dataCallback(String data) {
                        //data callback
                        String[] connectionFee = data.split(":");
                        //set new data to objReport
                        reqUpdate.setConnectionFee(Double.parseDouble(connectionFee[0]));
                        //set to view
                        tvConnectionFee.setText(connectionFee[1]);
                    }
                }).show();
    }

    @Override
    public void getDepositListSuccess(ArrayList<DepositFee> list) {
        DialogUtils.showDialogChooseDepositFee(this,
                getResources().getString(R.string.choose_deposit_fee),
                list, new DialogCallbackDataToActivity() {
            @Override
            public void dataCallback(String data) {
                tvDepositFee.setText(data.split(":")[1]);
                //put data to objReport
                reqUpdate.setDepositFee(Double.parseDouble(data.split(":")[1]));
            }
        }).show();
    }

    @Override
    public void getVatListSuccess(ArrayList<Vat> list) {
        DialogUtils.showDialogChooseVat(this,
                getResources().getString(R.string.ui_text_vat),
                list, new DialogCallbackDataToActivity() {
            @SuppressLint("SetTextI18n")
            @Override
            public void dataCallback(String data) {
                tvVat.setText(data.split(":")[1] + "%");
                //put data to objReport
                reqUpdate.setVat(Integer.parseInt(data.split(":")[1]));
            }
        }).show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void getRegistrationById(RegistrationById obj) {
        //this param use for get package service and promotion list
        Utils.updateCusInfo.setLocationId(String.valueOf(obj.getLocationId()));
        Utils.updateCusInfo.setContract(obj.getContract());
        Utils.updateCusInfo.setLocalType(String.valueOf(obj.getLocalType()));
        Utils.updateCusInfo.setObjId(String.valueOf(obj.getObjId() ));
        Utils.updateCusInfo.setMonthOfPrepaid(String.valueOf(obj.getMonthOfPrepaid()));
        //put data to objReport update registration total
        reqUpdate.setObjId(obj.getObjId());
        reqUpdate.setContract(obj.getContract());
        reqUpdate.setVat(obj.getVAT());
        reqUpdate.setLocationId(obj.getLocationId());
        reqUpdate.setUsername(SharedPref.get(this, SharedPref.Key.USERNAME, ""));
        reqUpdate.setLocalType(obj.getLocalType());
        reqUpdate.setPromotionId(obj.getPromotionId());
        reqUpdate.setMonthOfPrepaid(obj.getMonthOfPrepaid());
        reqUpdate.setTotal(obj.getTotal());
        reqUpdate.setInternetTotal(obj.getInternetTotal());
        reqUpdate.setDeviceTotal(obj.getDeviceTotal());
        reqUpdate.setDepositFee(obj.getDepositFee());
        reqUpdate.setConnectionFee(obj.getConnectionFee());
        reqUpdate.setList((ArrayList<ReqDevice>) obj.getListDevice());
        //display UI with service type registration
        if(obj.getListServiceType().size() == 1){
            tvServiceType.setText(getResources().getString(R.string.ui_text_internet));
            listServiceType.add(new ItemOfListRes(1, "Internet"));
        }else{
            tvServiceType.setText(getResources().getString(R.string.internet_and_devices));
            listServiceType.add(new ItemOfListRes(1, "Internet"));
            listServiceType.add(new ItemOfListRes(2, "Devices"));
        }
        //bellow, load data had register to view
        tvPackageService.setText(obj.getLocalTypeName());
        tvPromotionName.setText(obj.getPromotionName());
        tvPromotionHidden.setText(obj.getPromotionDescription());
        tvConnectionFee.setText(String.valueOf(obj.getConnectionFee()));
        tvVat.setText(String.valueOf(obj.getvAT()) + "%");
        tvDepositFee.setText(String.valueOf(obj.getDepositFee()));
        tvOldTotal.setText(String.valueOf(obj.getTotal()));
        //set list device
        listReqDevice = (ArrayList<ReqDevice>) obj.getListDevice();
        if(listReqDevice.size() == 0){
            headerDeviceList.setVisibility(View.GONE);
        }else{
            headerDeviceList.setVisibility(View.VISIBLE);
        }
        adapter.notifyData(listReqDevice);
        //set list selected, it's use for reselected
        DeviceList deviceList;
        for(int i=0; i < listReqDevice.size(); i++){
            deviceList = new DeviceList();
            deviceList.setValue(listReqDevice.get(i).getValue());
            deviceList.setName(listReqDevice.get(i).getName());
            deviceList.setChangeNumber(listReqDevice.get(i).getChangeNumber());
            deviceList.setPrice(String.valueOf(listReqDevice.get(i).getPrice()));
            deviceList.setIsSelected(1);
            listDeviceSelected.add(deviceList);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void getCalNewTotal(ArrayList<CalNewTotal> list) {
        CalNewTotal calNewTotal = list.get(0);
        //reset new service fee
        tvNewTotal.setText(String.valueOf(calNewTotal.getTotal()));
        //update new total
        reqUpdate.setTotal(calNewTotal.getTotal());
        reqUpdate.setDeviceTotal(calNewTotal.getDeviceTotal());
    }

    @Override
    public void getUpdateRegistrationTotal(ArrayList<UpdateRegistrationTotal> list) {
        //TODO:update registration success
        //finish this activity
        Dialog dialogUpdateRegistrationSuccess = DialogUtils.showDialogUpdateRegistrationSuccess(this,
                getResources().getString(R.string.mes_update_registration_success),
                new ContinueRegisterService() {
                    @Override
                    public void continueRegister() {
                        int SPLASH_DISPLAY_LENGTH = 2000;
                        Handler handler = new Handler();
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                finish();
                            }
                        };
                        handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH);
                    }
                });
        dialogUpdateRegistrationSuccess.show();
        ResizeDialogPercent.resize(this, dialogUpdateRegistrationSuccess);
    }

    @Override
    public void callApiFailed(String mes) {
        DialogUtils.showMessageDialogVerButton(this, mes).show();
    }

    @Override
    public void expiredToken(String mes) {
        DialogUtils.showDialogRequiredLoginAgain(this, mes).show();
    }

    @Override
    public void showProgressLoading() {
        hiddenProgressLoading();
        dialog = DialogUtils.showLoadingDialog(this, getResources().getString(R.string.loading));
        dialog.show();
    }

    @Override
    public void hiddenProgressLoading() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }

    private void initView() {
        listDeviceSelected = new ArrayList<>();
        listReqDevice = new ArrayList<>();
        adapter = new DeviceListSelectedAdapter(this, listReqDevice);
        recyclerViewListDevice.setAdapter(adapter);
    }

    private void updateUiWhenSelectService(ArrayList<String> list){
        if(list.size() != 0){
            for(int i = 0; i < list.size(); i++){
                //compare all service if after add more.....
                if(Integer.parseInt(list.get(i)) == Utils.DEVICES){
                    tvServiceType.setText(getResources().getString(R.string.internet_and_devices));
                    //update list service
                    listServiceType.clear();
                    listServiceType.add(new ItemOfListRes(1, "Internet"));
                    listServiceType.add(new ItemOfListRes(2, "Devices"));
                }else{
                    tvServiceType.setText(getResources().getString(R.string.ui_text_internet));
                    listServiceType.clear();
                    listServiceType.add(new ItemOfListRes(1, "Internet"));
                }
                //.....
            }
        }else{
            //list size == 0, is haven't selected diff service
            //show only internet, hidden all diff service
            listDeviceSelected.clear();
            listReqDevice.clear();
            adapter.notifyData(listReqDevice);
            tvServiceType.setText(getResources().getString(R.string.ui_text_internet));
            listServiceType.add(new ItemOfListRes(1, "Internet"));
        }
    }

    private void callApi() {
        apiService = ApiUtils.getApiService();
        presenter = new UpdateTotalOfMoneyPresenterImpl(this);
        presenter.doGetServiceTypeList(this, apiService);
        //check contract info
        if(contractDetails == null){return;}
        //put data to objReport call api
        ReqGetRegistrationDetail obj = new ReqGetRegistrationDetail();
        obj.setRegId(String.valueOf(contractDetails.getRegId()));
        obj.setRegCode(contractDetails.getRegCode());
        //call api get registration by id
        presenter.doGetRegistrationById(this, apiService, obj);
    }

    private void setEvent() {
        imgBack.setOnClickListener(this);
        loChooseServiceType.setOnClickListener(this);
        loPackageService.setOnClickListener(this);
        loPromotion.setOnClickListener(this);
        loConnectionFee.setOnClickListener(this);
        loVat.setOnClickListener(this);
        loDepositFee.setOnClickListener(this);
        loChooseDevice.setOnClickListener(this);
        btnCalNewTotal.setOnClickListener(this);
        btnUpdateRegistration.setOnClickListener(this);
        tvShowMore.setOnClickListener(this);
        tvPromotionName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                loPromotion.setBackground(getResources().getDrawable(R.drawable.background_spinner));
                tvLabelPromotion.setTextColor(getResources().getColor(R.color.s_black));
            }
        });
        //TODO:cal diff new and old total
        tvNewTotal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable editable) {
                float oldTotal = Float.parseFloat(tvOldTotal.getText().toString());
                float newTotal = Float.parseFloat(tvNewTotal.getText().toString());
                float totalRound = 0;
                DecimalFormat  twoDForm = new DecimalFormat("#,###,###.#");
                if(newTotal > oldTotal){
                    float diffTotal = newTotal - oldTotal;
                    tvDiffMoney.setText("+" + String.valueOf(twoDForm.format(diffTotal)));
                }else if(oldTotal > newTotal){
                    float diffTotal = oldTotal - newTotal;
                    tvDiffMoney.setText("-" + String.valueOf(twoDForm.format(diffTotal)));
                }else{
                    tvDiffMoney.setText(String.valueOf(totalRound));
                }
            }
        });
    }

    private boolean checkValidateIsOk(){
        if(tvPromotionName.getText().toString().isEmpty()){
            loPromotion.setBackground(getResources().getDrawable(R.drawable.background_spinner_error));
            tvLabelPromotion.setTextColor(getResources().getColor(R.color.wrong_pink));
            DialogUtils.showMessageDialogVerButton(this,
                    getResources().getString(R.string.mes_update_total_promotion_required)).show();
            return false;
        }
        return true;
    }
}
